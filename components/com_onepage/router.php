<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;

jimport('joomla.application.categories');

//require_once (JPATH_ADMINISTRATOR.'/components/com_onepage/helpers/db.php');
//require_once (JPATH_ADMINISTRATOR.'/components/com_onepage/helpers/config.php');
//require_once (JPATH_ADMINISTRATOR.'/components/com_onepage/helpers/methods.php');

/**
 * Build the route for the com_content component
 *
 * @param	array	An array of URL arguments
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 * @since	1.5
 */
function OnepageBuildRoute(&$query)
{
	$segments	= array();

// get a menu item based on Itemid or currently active
	$app		= JFactory::getApplication();
	$menu		= $app->getMenu();
	$params		= JComponentHelper::getParams('com_onepage');
	$item = $menu->getActive();

	if (empty($query['Itemid']))
	{
		$menuItem = $menu->getActive();
		$menuItemGiven = false;
	}
	else
	{
		$menuItem = $menu->getItem($query['Itemid']);
		$menuItemGiven = true;
	}
	
	$tConfig = jsAppConfig::getInstance();
	// If set to home, we don't show any text
	
	if( isset($query['view']) && $query['view'] == 'page' )
	{
		$segments[] = $query['view'];
		$segments[] = $query['id'];
		unset( $query['view'] );
		unset( $query['id'] );
		return $segments;
	}
	
	
	
	return $segments;
}



/**
 * Parse the segments of a URL.
 *
 * @param	array	The segments of the URL to parse.
 *
 * @return	array	The URL attributes to be used by the application.
 * @since	1.5
 */
function OnepageParseRoute($segments)
{
	$vars = array();

	$vars['view'] = $segments[0];
	$vars['id'] = $segments[1];

	return $vars;
}
