<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
/**
 */
class OnepageHelperEl extends jsAppHelper
{
	static public function id($item)
	{
		$params = $item->params;
		if(is_string($params))
		{
			$params = jsJSON::decode($params);
		}
		$wrap_id = '';
		switch($item->type)
		{
			case('row'):
				$wrap_id = "section-wrap-{$item->id}";
				
				break;
			default:
				$wrap_id = "op-{$item->type}-{$item->id}";
				break;
		}
		$id = (jsGetValueNo0($params,'wrap_id',false))?jsGetValueNo0($params,'wrap_id',false):$wrap_id;
		
		return $id;
	}
	
	
	static public function getParentNode($item,$level)
	{
		//$tEl = JTable::getInstance('page','onepageTable');
		//$tEl->load($item->id);
		
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
		
		$query->select( '*' );
		$query->from('`#__onepage_page`');
		$query->where("`level` = '{$level}'");
		$query->where("lft <= {$item->lft}");
		$query->where("rgt >= {$item->rgt}");
		$obj = jsDB::loadObj($query,'obj');
		
		return $obj;
	}
}
