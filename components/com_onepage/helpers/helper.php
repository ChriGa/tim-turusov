<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
/**
 */
class OnepageHelperFront extends jsAppHelper
{
	
	public static function initPage()
	{
		$tConfig = jsAppConfig::getInstance();
		$b3 = 'components/com_onepage/bootstrap3';
		
		if(!parent::_isBt3Template())
		{
			JHtml::script('media/jsbt3/js/basic.js');
			JHtml::stylesheet('media/jsbt3/css/basic.css');
		}
		
		$app = JFactory::getApplication();
		$current = $app->getTemplate();
		if(JFile::exists(JPATH_SITE.'/templates/'.$current.'/css/theme.css'))
		{
			JHtml::stylesheet('templates/'.$current.'/css/theme.css');
		}
	}
	
	/**
	 * @legacy
	 */
	public static function initPageOld()
	{
		$tConfig = jsAppConfig::getInstance();
		$b3 = 'components/com_onepage/bootstrap3';
	
		if(!parent::_isBt3Template())
		{
			JHtml::script('media/jsbt3/js/basic.js');
			JHtml::stylesheet('media/jsbt3/css/basic.css');
		}
		
		JHtml::script('media/jsbt3/js/parsley.js');
		JHtml::script('media/jsbt3/js/jquery.jclock.js');
		
		JHtml::stylesheet('com_onepage/deprecated/site.css',false,true);
		JHtml::stylesheet('com_onepage/deprecated/customize.css',false,true);
	}
	
	public static function importJS($page,$bottom =false)
	{
		$uri = JFactory::getURI();
		$path = 'media/com_onepage/js/custom/';
		$params = $page->params;
		$jsFiles = jsGetValueNo0($params, 'js_files','');
		$scripts = array(); 
		if(!empty($jsFiles))
		{
			$jsFiles = explode(',',$jsFiles);
			foreach($jsFiles as $file)
			{
				if($bottom)
				{
					$scripts[] = '<script src="'.$uri->root().$path.$file.'" type="text/javascript"></script>';
				}
				else
				{
					JHtml::script($path.$file);
				}
			}
		}
		
		$scripts = implode("\r\n", $scripts);
		if($bottom)
		{
			return $scripts;
		}
		return ;
	}

	public static function importCSS($page)
	{
		$path = 'media/com_onepage/css/custom/';
		
		$params = $page->params;
		$cssFiles = jsGetValueNo0($params, 'css_files','');
		
		if(!empty($cssFiles))
		{
			$cssFiles = explode(',',$cssFiles);
			foreach($cssFiles as $file)
			{
				JHtml::stylesheet($path."{$file}");
			}
		}
		
		// import Google Font
		$font_links =  jsGetValueNo0($params, 'font_links',array());
		foreach($font_links as $file)
		{
			JHtml::stylesheet($file['link']);
		}
	}
	
	
	//////////////////////////// Deprecated ////////////////////////////////
	public static function getDT()
	{
		$date = JFactory::getDate();
		$format =  "Y-m-d H:i:s";
		$date = JFactory::getDate(JHTML::Date($date, $format, false));
		return $date->toMySQL();
	}
	
	public static function getImgPath($html = false)
	{
		if($html)
		{
			//return  '../components/com_onepage/bootstrap/site_images';
			return 'images/onepage/';
		}
		else
		{
			//return  JPATH_COMPONENT_SITE.'/bootstrap/site_images';
			return JPATH_SITE.'/images/onepage/';
		}
	}
	
	/**
	 * @deprecated
	 */
	public static function genId($title)
	{
		$noHtmlFilter = JFilterInput::getInstance();
		$var = $noHtmlFilter->clean($title, 'CMD');
		return strtolower($var);
	}
	
	/**
	* @deprecated
	*/
	static public function isElement($type)
	{
		if(in_array($type,array('article','image','subscription','gmap','webvideo','contactform')))
		{
			return true;
		}
	
		return false;
	}
	
	/**
	* @deprecated
	*/
	static public function filterRootId($id)
	{
		$id = ($id <3)?3:$id;
		return $id;
	}
	
	static public function addAppIcon($page)
	{
		$document = JFactory::getDocument();
		if( JFile::exists(JPATH_SITE."/images/onepage/page{$page->id}_appicon144.png") )
		{
			$document->addHeadLink("images/onepage/page{$page->id}_appicon144.png",'apple-touch-icon-precomposed','rel',array('sizes'=>'144x144') );
			$document->addHeadLink("images/onepage/page{$page->id}_appicon114.png",'apple-touch-icon-precomposed','rel',array('sizes'=>'114x114') );
			$document->addHeadLink("images/onepage/page{$page->id}_appicon72.png",'apple-touch-icon-precomposed','rel',array('sizes'=>'72x72') );
			$document->addHeadLink("images/onepage/page{$page->id}_appicon57.png",'apple-touch-icon-precomposed','rel',array('sizes'=>'57x57') );
		}
		elseif( JFile::exists(JPATH_SITE."/images/onepage/appicon144.png") )
		{
			$document->addHeadLink('images/onepage/appicon144.png','apple-touch-icon-precomposed','rel',array('sizes'=>'144x144') );
			$document->addHeadLink('images/onepage/appicon114.png','apple-touch-icon-precomposed','rel',array('sizes'=>'114x114') );
			$document->addHeadLink('images/onepage/appicon72.png','apple-touch-icon-precomposed','rel',array('sizes'=>'72x72') );
			$document->addHeadLink('images/onepage/appicon57.png','apple-touch-icon-precomposed','rel',array('sizes'=>'57x57') );
		}
	}
	
}
