<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class OnepageHelperRender extends JObject
{
	public static function getPage($pageId)
	{
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		return $tEl;
	}
	
	// Css
	public static function renderHtml($pageId)
	{
		$config = jsAppConfig::getInstance();
		$page = self::getPage($pageId);
		$opTpl = onepageTemplate::getInstance();
		$opTpl->page = $page;
		$opTpl->config = $config;
		$opTpl->items = $page->getChildren(null,false);
	
		foreach($opTpl->items as $item)
		{
			echo $opTpl->output($item->type);
		}
	}
	
	// Js
	public static function renderHtml($pageId)
	{
		$config = jsAppConfig::getInstance();
		$page = self::getPage($pageId);
		$opTpl = onepageTemplate::getInstance();
		$opTpl->page = $page;
		$opTpl->config = $config;
		$opTpl->items = $page->getChildren(null,false);
	
		foreach($opTpl->items as $item)
		{
			echo $opTpl->output($item->type);
		}
	}
	
	// Html
	public static function renderHtml($pageId)
	{
		$config = jsAppConfig::getInstance();
		$page = self::getPage($pageId);
		$opTpl = onepageTemplate::getInstance();
		$opTpl->page = $page;
		$opTpl->config = $config;
		$opTpl->items = $page->getChildren(null,false);
		
		foreach($opTpl->items as $item)
		{
			echo $opTpl->output($item->type);
		}
	}
	
	public static function get()
	{
		
	}
}

class onepageTemplate extends JObject
{
	protected $_template;
	protected $_path;
	protected $_output;
	protected static $instance;
	function __construct()
	{
		parent::__construct();
		$_path = JPATH_SITE."/components/com_onepage/el/";
	}
	
	static function getInstance()
	{
		$instance = self::$instance;
		if($instance instanceof onepageTemplate)
		{
			
		}
		else
		{
			$instance = new onepageTemplate();
			self::$instance = $instance;
		}
		
		return $instance;
	}
	
	function output($tpl)
	{
		ob_start();
		include $this->_path."{$tpl}";
		// Done with the requested template; get the buffer and
		// clear it.
		$this->_output = ob_get_contents();
		ob_end_clean();
		
		return $this->_output;
	}
}
