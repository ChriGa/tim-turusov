<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;

/**
 */
class OnepageController extends JControllerLegacy
{
	function __construct($config = array())
	{

		parent::__construct($config);
	}

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$cachable = true;

		JHtml::_('behavior.caption');

		// Set the default view name and format from the Request.
		// Note we are using a_id to avoid collisions with the router and the return page.
		// Frontend is a bit messier than the backend.
		$id		= JRequest::getInt('a_id');
		$vName	= JRequest::getCmd('view', 'categories');
		JRequest::setVar('view', $vName);

		$user = JFactory::getUser();

		if ($user->get('id') ||
			($_SERVER['REQUEST_METHOD'] == 'POST' &&
				(($vName == 'category' && JRequest::getCmd('layout') != 'blog') || $vName == 'archive' ))) {
			$cachable = false;
		}

		$safeurlparams = array( 'id'=>'INT',  'lang'=>'CMD');


		parent::display($cachable, $safeurlparams);

		return $this;
	}
	
	public function genJSDefault()
	{
		header('Content-Type: text/javascript');
		$tConfig = jsAppConfig::getInstance();
		$types = JRequest::getVar('types',array(),'get','array');
		//$ids = JRequest::getVar('ids',array(),'get','array');
		//JArrayHelper::toInteger($ids);
		$page_id = JRequest::getInt('id');
		$page_id = (empty($page_id))?$tConfig->get('home'):$page_id;
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($page_id);
		$children = $tEl->getPublishedChildren(true);
		$codes = array();
		$skips = array();
		
		$loadSocial = false;$loadImage = false;
		foreach($children as $child)
		{
			if(!$child->published) 
			{
				$skips[] = $child->id;
				continue;
			} elseif(in_array($child->parent_id,$skips)) {
				$skips[] = $child->id;
				continue;
			}
			
			$id = $child->id;
			$tEl->load($id);
			$params = $tEl->params;
			
			if( strtolower($tEl->type) == 'sociallike' )
			{
				$loadSocial = true;
			}
			elseif( strtolower($tEl->type) == 'image' )
			{
				$loadImage = true;
			}
			
			$func = "genJS{$tEl->type}";
			
			if(method_exists($this,$func))
			{
				//$codes[] = $this->{$func}($tEl);
				$title = strtolower($tEl->title);
				$title = (!empty($tEl->title))?$tEl->title.$tEl->id:$tEl->type.'_'.$tEl->id;
				$elid = onepageHelperFront::genId($title);
				
				 $code= call_user_func(array($this,$func),$tEl,$elid);
				 $codes[] = $code;
			}
		}
		
		if( $loadImage )
		{
			$codes[] = " $('.mplightbox').magnificPopup({type:'image'});";
		}
		
		
		if(empty($codes))
		{
			$output = "var OnepageInit = function()	{ return true; };";
		}
		else
		{
			$codes = implode("\r\n",$codes);
			$output = "var OnepageInit = function()	{ ".$codes." ;return true;};";
		}
		
		$tConfig = jsAppConfig::getInstance();
		if($tConfig->get('ga_enable'))
		{
			$code = $tConfig->get('ga_code');
			$ws = $tConfig->get('ga_website');
			$output.= "\r\n";
			$output.= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){";
		  $output.= "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),";
		  $output.= "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)";
		  $output.= "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";
		
		  $output.= "ga('create', '{$code}', '{$ws}');";
		  $output.= "ga('send', 'pageview');";
		}
		$output.= "\r\n";
		
		if( $loadSocial )
		{
			$output.= '!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");';
			$output.= "\r\n";
			$output.= "(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1\";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));";
		}
		/**/
		jsExit($output);
	}
	
	protected function genJSCarousel($tEl,$elId)
	{
		
		$txt = '';
		
		$params = $tEl->params;
			
		$autoplay = jsGetValue($params,'autoplay','1');
		//if(!$autoplay) return $txt;
		$id = $elId;
		
	
		//$title = str_replace(' ', '_', $title);
		//$id = jsGetValue($params,'id',$title);
		$conf = '';
		$conf = (!$autoplay)?'interval:false':'interval:5000';
		
		$txt.=  (empty($conf))?"$('#{$id}').carousel();":"$('#{$id}').carousel({{$conf}});";
		
		
		$output = $txt;
		
		return $output;
	}
	
	protected function genJSGmap($tEl,$elId)
	{
	
		$txt = '';
		$params = $tEl->params;

		
		$autoplay = jsGetValue($params,'autoplay','1');
		//if(!$autoplay) return $txt;
	
		
		$id = $elId;
	
		$params = $tEl->params;
		$markers = jsGetValue($params,'markers',array());
		$first = $markers[0];
		
		//$txt.= "var ptb =  $('#{$id}').parent().css('padding-top').replace('px','') + $('#{$id}').parent().css('padding-bottom').replace('px','') ;";
		
		//$txt.="$('#{$id}').parent().parent().css('height', $('#{$id}').parents('.row-fluid').height()  );";
		$txt.="var \$gmag_{$id} = $('#{$id}').parent();";
		$tConfig = jsAppConfig::getInstance();
		if($tConfig->get('bootstrap_version',2) == 2)	{
			$txt.="if( \$gmag_{$id}.hasClass('opautoheight')) {";
			$txt.=" \$gmag_{$id}.height( \$gmag_{$id}.height() - \$gmag_{$id}.outerHeight(true) + \$gmag_{$id}.parents('.opheight').height());";
			$txt.="}";
		} else {
			$txt.="if( \$gmag_{$id}.hasClass('opautoheight')) {";
			//$txt.=" \$gmag_{$id}.height( \$gmag_{$id}.height() - \$gmag_{$id}.outerHeight(true) + \$gmag_{$id}.parents('.opheight').height());";
			$txt.="}";
		}
		
		
		$txt.="var gmap{$tEl->id} = new GMaps({";
	    $txt.="div: '#{$id}',";
	    $txt.="width: '100%',";
	    $txt.="height: '100%',";
	    if( !empty($first['lat']) && !empty($first['lng']) )
	    {
	    	$txt.="lat: {$first['lat']},";
	    	$txt.="lng: {$first['lng']}";
	    }
	   
	    $txt.="});";
	    $txt.="\r\n";
	    foreach($markers as $marker)
	    {
	    	if( empty($marker['lat']) || empty($marker['lng']) )
	    	{
	    		continue;
	    	}
	    	
	    	$t = explode("\n", $marker['infotext'] );
	    	$t = '<p>'.implode('</p><p>',$t).'</p>';
	    	
	    	$txt.="gmap{$tEl->id}.addMarker({
	    		lat: {$marker['lat']},
	    		lng: {$marker['lng']},
	    		title: '{$marker['title']}',
	    		infoWindow: {
		          content: '{$t}'
		        }
	    	});";
	    	$txt.="\r\n";
	    }
		$output = $txt;
	
		return $output;
	}
	
	protected function genJSArticle($tEl,$elId)
	{
		$txt = '';
		$params = $tEl->params;
		
		$fixed_height = jsGetValue($params,'fixed_height');
	
		if (!empty($fixed_height)) {
			return '';
		}
		$id = $elId;
		
		//$txt.="$('#{$id}').parents('.span12').css('height','auto');";
		//$txt.="$('#{$id}').parents('.span12').height('100%');";
		$tConfig = jsAppConfig::getInstance();
		if($tConfig->get('bootstrap_version',2) == 2)	{
			$txt.="if($('#{$id}').hasClass('opautoheight')) {";
			$txt.="$('#{$id}').height($('#{$id}').height() - $('#{$id}').outerHeight(true) + $('#{$id}').parents('.opheight').height());";
			$txt.="}";
		} else {
			/*$txt.=" console.log($('#{$id}').parents('.opheight').height());";
			$txt.="if($('#{$id}').hasClass('opautoheight')) {";
			$txt.="$('#{$id}').height($('#{$id}').height() - $('#{$id}').outerHeight(true) + $('#{$id}').parents('.opheight').height());";
			$txt.="}";*/
		}
		
		$output = $txt;
	
		return $output;
	}
	
	protected function genJSImage($tEl,$elId)
	{
		$txt = '';
		$params = $tEl->params;
	
		$fixed_height = jsGetValue($params,'fixed_height');
		$image_height_unit = jsGetValue($params,'image_height_unit','percent');
		if (!empty($fixed_height) && $image_height_unit == 'px') {
			return '';
		}
		$id = $elId;
	
		$tConfig = jsAppConfig::getInstance();
		if($tConfig->get('bootstrap_version',2) == 2)	{
			
		} else {
			$txt.="$('#{$id}').css('height','100%')";
		}
		//$txt.="$('#{$id}').parents('.span12').css('height','auto');";
		//$txt.="$('#{$id}').parents('.span12').height('100%');";
		//$txt.="var \$opimg_{$id} = $('#{$id}').parent();";
		//$txt.=" $('#{$id}').css('height','') ;";
		/*$txt.="if(\$opimg_{$id}.hasClass('opautoheight')) {";
		$txt.=" \$opimg_{$id}.height( \$opimg_{$id}.height() - \$opimg_{$id}.outerHeight(true) +\$opimg_{$id}.parents('.opheight').height());";
		$txt.="\r\n";
		
		$txt.="}";*/
		$output = $txt;
		return $output;
	}
	
	protected function genJSWebvideo1($tEl,$elId)
	{
		$fixed_height = jsGetValue($params,'fixed_height');
	
		if (!empty($fixed_height)) {
			return '';
		}
		$id = $elId;
	
		//$txt.="$('#{$id}').parents('.span12').css('height','auto');";
		//$txt.="$('#{$id}').parents('.span12').height('100%');";
		$txt.="var \$wv_{$id} = $('#{$id}').parent();";
		$txt.="if(\$wv_{$id}.hasClass('opautoheight')) {";
		$txt.=" \$wv_{$id}.height( \$wv_{$id}.height() - \$wv_{$id}.outerHeight(true) +\$wv_{$id}.parents('.opheight').height());";
		$txt.="\r\n";
		$txt.="}";
		$output = $txt;
		return $output;
	}
	
	protected function genJSContactform($tEl,$elId)
	{
		$fixed_height = jsGetValue($params,'fixed_height');
	
		if (!empty($fixed_height)) {
			return '';
		}
		$id = $elId;
	
		//$txt.="$('#{$id}').parents('.span12').css('height','auto');";
		//$txt.="$('#{$id}').parents('.span12').height('100%');";
		$txt.="var \$cf_{$id} = $('#{$id}');";
		$txt.="if(\$cf_{$id}.hasClass('opautoheight')) {";
		$txt.=" var dif = \$cf_{$id}.height() - \$cf_{$id}.outerHeight(true);";
		//$txt.=" \$cf_{$id}.height( dif +\$cf_{$id}.parents('.opheight').height());";
		$txt.=" \$cf_{$id}.find('textarea').height( dif +\$cf_{$id}.find('textarea').height());";
		$txt.="\r\n";
		//$txt.="console.log( dif )";
		//$txt.="\r\n";
		$txt.="}";
				$output = $txt;
				return $output;
	}
	
	protected function genJSTab($tEl,$elId)
	{
		$txt = '';
		$params = $tEl->params;
	
		$fixed_height = jsGetValue($params,'fixed_height');
	
		if (!empty($fixed_height) || $tEl->level == 4) {
			return '';
		}
		$id = $elId;
	
		$tConfig = jsAppConfig::getInstance();
		if($tConfig->get('bootstrap_version',2) == 2)	{
			$txt.="var \$tab_{$id} = $('#{$id}');";
			//$txt.="if(\$tab_{$id}.hasClass('opautoheight')) {";
			$txt.=" \$tab_{$id}.children('.tab-content').height( \$tab_{$id}.height() - \$tab_{$id}.children('.nav-tabs').outerHeight(true) );";
		} else {
			$txt.="var \$tab_{$id} = $('#{$id}');";
			//$txt.="if(\$tab_{$id}.hasClass('opautoheight')) {";
			$txt.=" \$tab_{$id}.children('.tab-content').height( \$tab_{$id}.height() - \$tab_{$id}.children('.nav-tabs').outerHeight(true) );";
			
		}
		//$txt.="$('#{$id}').parents('.span12').css('height','auto');";
		//$txt.="$('#{$id}').parents('.span12').height('100%');";
		
		//$txt.="\r\n";
		//$txt.="}";
		$output = $txt;
		return $output;
	}
	
	protected function genJSCollapse($tEl,$elId)
	{
		$txt = '';
		$params = $tEl->params;
	
		$fixed_height = jsGetValue($params,'fixed_height');
	
		if (!empty($fixed_height) || $tEl->level == 4) {
			return '';
		}
		$id = $elId;
	
		$tConfig = jsAppConfig::getInstance();
		if($tConfig->get('bootstrap_version',2) == 2)	{
			$txt.="var \$collapse_{$id} = $('#{$id}');";
			//$txt.="if(\$tab_{$id}.hasClass('opautoheight')) {";
			$txt.=" var ctheight=0;$.each( \$collapse_{$id}.children('.accordion-group'), function(index, value) { ctheight += $(value).children('.accordion-heading').outerHeight(true);});";
			//$txt.="console.log( \$collapse_{$id}.height() );";
			$txt.=" \$collapse_{$id}.children('.accordion-group').children('.accordion-body').children('.accordion-inner').height( function(index,height){ var p = $(this).innerHeight()-$(this).height();return (\$collapse_{$id}.height() - ctheight - p); } );";

		} else {
			$txt.="var \$collapse_{$id} = $('#{$id}');";
			$txt.=" var ctheight=0;$.each( \$collapse_{$id}.children('.panel'), function(index, value) { ";
			$txt.="ctheight += $(value).outerHeight(true)-$(value).outerHeight()+$(value).children('.panel-heading').outerHeight(true)+3;});";
			$txt.=" \$collapse_{$id}.children('.panel').children('.panel-collapse').children('.panel-body').height( function(index,height){ var p = parseInt( $(this).css('padding-bottom') )+parseInt( $(this).css('padding-top') );return (\$collapse_{$id}.height() - ctheight - p); } );";
				
		}
		//$txt.="$('#{$id}').parents('.span12').css('height','auto');";
		//$txt.="$('#{$id}').parents('.span12').height('100%');";
		//$txt.="\r\n";
		//$txt.="}";
		$output = $txt;
		return $output;
	}
	
	protected function genJSClock($tEl,$elId)
	{
		$fixed_height = jsGetValue($params,'fixed_height');
	
		if (!empty($fixed_height)) {
			return '';
		}
		
		static $run;
		
		if(empty($run)) {$run =false;}
		
		$id = $elId;
		$params = $tEl->params;
		$utc = (int)$params['utctimezone'];
		$txt.="$('#{$id}').children('div').jclock({";
		if( !empty($utc) )
		{
			$txt.="utc: true,";
			$txt.="utcOffset: {$utc}});";
		}
		$output = $txt;
		$run = true;
		return $output;
	}
	
	protected function genJSSocialLike1($tEl,$elId)
	{
		$fixed_height = jsGetValue($params,'fixed_height');
		
		if (!empty($fixed_height)) {
			return '';
		}
		
		static $run;
		
		if(empty($run)) {
			$run =false;
		}
		
		$id = $elId;
		$params = $tEl->params;
		$utc = (int)$params['utctimezone'];
		$txt.="$('#{$id}').sharrre({
			 share: {
facebook: true,
twitter: true,
digg: false,
delicious: false
},
enableTracking: false,
buttons: {
facebook: {layout: 'box_count'},
twitter: {count: 'vertical'}
},
hover: function(api, options){
$(api.element).find('.buttons').show();
},
hide: function(api, options){
$(api.element).find('.buttons').hide();
}
		});";
		$output = $txt;
		$run = true;
		return $output;
		
	}
}
