<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

// tables
JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');

// custom
//require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/config.php';
/*
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/legacy.php';

require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/db.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/methods.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/pagination.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/treetable.php';*/

// mobile detect
//require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/Mobile_Detect.php';
// Include dependancies

jsImportHelper('onepage');
jsImportHelper('onepage',1);
jsImportHelperOther('onepage','el');
jsImportHelperOther('onepage','plg',0);
//require_once JPATH_COMPONENT.'/helpers/route.php';
//require_once JPATH_COMPONENT.'/helpers/query.php';

$controller = JControllerLegacy::getInstance('Onepage');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
