<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;

class OnepageViewSingle extends JViewLegacy
{
	protected $form;
	protected $items;
	protected $item;
	protected $return_page;
	protected $state;
	protected $params;

	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->params	= $this->state->get('params');
		
		$tConfig = jsAppConfig::getInstance();
		if($tConfig->get('bootstrap_version',2) == 2)	{
			$this->addTemplatePath(JPATH_COMPONENT_SITE.'/views/home/tmpl/el');
			$this->addTemplatePath(JPATH_COMPONENT_SITE.'/views/home/tmpl/basic');
		} else {
			$this->addTemplatePath(JPATH_COMPONENT_SITE.'/views/home/tmpl/el3');
			$this->addTemplatePath(JPATH_COMPONENT_SITE.'/views/home/tmpl/basic3');
		}
		
		//
		$applied = $tConfig->get('contactform_applied_to',array());
		$contactform_enable = $tConfig->get('contactform_enable');
		if(!empty($applied))
		{
			$applied = explode(',',$applied);
			if(!in_array($id,$applied))
			{
				$contactform_enable = false;
			}
		}
		
		$applied = $tConfig->get('footer_applied_to',array());
		$footer_published = $tConfig->get('footer_published');
		if(!empty($applied))
		{
			$applied = explode(',',$applied);
			if(!in_array($id,$applied))
			{
				$footer_published = false;
			}
		}
		
		JRequest::setVar('tmpl','component');
		OnepageHelperFront::initPageOld();
		$this->_prepareDocument();
		//$this->setLayout('test');
		
		$id= JRequest::getInt('id');
		$id = OnepageHelperFront::filterRootId($id);
		$this->items = $this->getModel()->getPublishedChildren($id,false);
		
		$this->id = $id;
		$elId = '';
		$this->assignRef('elId', $elId);
		$this->assignRef('contactform_enable', $contactform_enable);
		$this->assignRef('footer_published', $footer_published);
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		$app		= JFactory::getApplication();
		$menus		= $app->getMenu();
		$pathway	= $app->getPathway();
		$title 		= null;

	
		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if ($menu)
		{
			
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', JText::_('COM_ONEPAGE_HOME'));
		}
		
		
		$showNavbar = jsGetValue($menu->query, 'show_navbar',1);
		$this->assignRef('show_navbar', $showNavbar);
		
		$title = $this->params->def('page_title', JText::_('COM_ONEPAGE_HOME'));
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		$pathway = $app->getPathWay();
		$pathway->addItem($title, '');

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
	
	protected function fetchVars($level = 5)
	{
		$row = $this->item;
		//$params  = jsJSON::decode($row->params);
		$params  = $row->params;
		$title = (!empty($row->title))?$row->title.$row->id:$row->type.'_'.$row->id;
		$id = onepageHelperFront::genId($title);
		
		$return = array();
		$return[] = $row;
		$return[] = $params;
		$return[] = $id;
		
		if($level != 4)
		{
			$children = $this->getModel()->getPublishedChildren($row->id,false);
			$return[] = $children;
		}
		
		return $return;
	}
}
