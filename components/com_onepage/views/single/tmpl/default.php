<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$firstBody = false;

$postString = array();
$ids = array();

$postString['option']='com_onepage';
$postString['task']='genJSDefault';
$postString['id'] = $this->id;
$document = JFactory::getDocument();
$document->addScript('index.php?'.http_build_query($postString));

$tConfig = jsAppConfig::getInstance();
// Google Fonts
if($tConfig->get('ga_fonts',false))
{
	JHtml::stylesheet('http://fonts.googleapis.com/css?'.$tConfig->get('ga_fonts'));
}

// Component JS
if($tConfig->get('com_carousel',false))
{
	JHtml::script('components/com_onepage/bootstrap/js/'.$tConfig->get('com_carousel',false).'.min.js');
}

// AppStore Meta
$app_ids = explode(',',$tConfig->get('app_id'));
$num = count($app_ids) ;
if( $num > 1 )
{
	$app_id = $app_ids[rand(0, $num)];
	$document->setMetaData('apple-itunes-app','app-id='.$app_id );
}
elseif( $num == 1 && !empty($app_ids[0]))
{
	$app_id =  $app_ids[0];
	$document->setMetaData('apple-itunes-app','app-id='.$app_id );
}
$document->setMetaData('viewport','width=device-width, initial-scale=1.0' );
// Foreach
$i = 0;$withHelloUnit = false;

if( JFile::exists(JPATH_SITE."/images/onepage/appicon144.png") )
{
	$document->addHeadLink('images/onepage/appicon144.png','apple-touch-icon-precomposed','rel',array('sizes'=>'144x144') );
	$document->addHeadLink('images/onepage/appicon114.png','apple-touch-icon-precomposed','rel',array('sizes'=>'114x114') );
	$document->addHeadLink('images/onepage/appicon72.png','apple-touch-icon-precomposed','rel',array('sizes'=>'72x72') );
	$document->addHeadLink('images/onepage/appicon57.png','apple-touch-icon-precomposed','rel',array('sizes'=>'57x57') );
}
?>

<div id="onepage" >
<?php if($this->show_navbar) echo $this->loadTemplate('navbar');?>
<?php foreach($this->items as $k => $item):?>
<?php 
if(!$item->published) continue;
if(in_array($item->type,array('modal','link'))) continue;

$this->item = $item;
echo $this->loadTemplate('row');
//$itemParams = jsJson::decode($item->params);
$itemParams = $item->params;
if( jsGetValue($itemParams,'showhr',1) == 1 )
{
	if( !in_array($item->path,array('navbar','footer')) )
	{
		echo '<hr>';
	}

}
elseif( jsGetValue($itemParams,'showhr',1) == 2 )
{
	if( !in_array($item->path,array('navbar','footer')) )
	{
		echo '<br />';
	}

}
?>
<?php endforeach;?>
</div>

<div id="onepage-modalgroup">
<?php if($tConfig->get('contactform_enable')) echo $this->loadTemplate('contact');?>

<?php 
// Modal
foreach($this->items as $k => $item)
{
	if(!$item->published) continue;
	if(!in_array($item->type,array('modal'))) continue;
	$this->item = $item;
	echo $this->loadTemplate($item->tpl);
}
?>
</div>
<script>
$(function () {
	$('.btpopover').popover({});
	$('body').removeClass('modal');
	OnepageInit(); 
	
	//$('body').scrollspy();
	$('.nav-tabs a:first').tab('show');
	
	//$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
});

var submitContact = function() {
	$('#contactusform').ajaxSubmit({
		dataType:  'json',
	    type: 'POST',
	    beforeSubmit: function()	{
			//$.blockUI({ message: "Processing..." });
			$('#contactusModal').block({ message: "Processing..." });
		},
	    success:    function(res) {
	    	window.location.reload();
	    }
	});
}
</script>