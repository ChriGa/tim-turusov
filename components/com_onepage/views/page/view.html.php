<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;

class OnepageViewPage extends JViewLegacy
{
	protected $form;
	protected $items;
	protected $item;
	protected $return_page;
	protected $state;
	protected $params;

	public function display($tpl = null)
	{
		$model = $this->getModel();
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		// Setup the variables
		$this->page = $tEl;
		$this->config = jsAppConfig::getInstance();
		$this->items = $tEl->getPublishedChildren();//$model->getPublishedChildren($tEl->id,false);
		
		
		// is component?
		$tmpl = jsGetValue( $this->page->params, 'single', $this->config->get('default_single',true) );
		if($tmpl)
		{
			JRequest::setVar('tmpl','component');
		}
		
		$layout = onepageHelperFront::checkDevice();
		if($layout != 'desktop')
		{
			$this->setLayout($layout);
		}
		
		// Preload Variable
		$this->loaddlsubscription = false;
		$this->loadjsmpurchase = false;
		
		// load Template
		$dispather = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype');
		$results = jsArrayNoEmpty( $dispather->trigger('getOPElementType',array()) );
		foreach($results as $result)
		{
			$this->addTemplatePath(JPATH_PLUGINS."/opeltype/{$result['value']}/tpl");
		}
		//$this->addTemplatePath(JPATH_COMPONENT_SITE.'/el');
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		$app		= JFactory::getApplication();
		$menus		= $app->getMenu();
		$pathway	= $app->getPathway();
		$title 		= null;

	
		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if ($menu)
		{
			
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', JText::_('COM_ONEPAGE_HOME'));
		}
		
		
		$showNavbar = jsGetValue($menu->query, 'show_navbar',1);
		$this->assignRef('show_navbar', $showNavbar);
		
		$title = $this->params->def('page_title', JText::_('COM_ONEPAGE_HOME'));
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		$pathway = $app->getPathWay();
		$pathway->addItem($title, '');

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
	
	protected function fetchVars($level = 5)
	{
		$row = $this->item;
		$params  = $row->params;
		$title = (!empty($row->title))?$row->title.$row->id:$row->type.'_'.$row->id;
		$id = onepageHelperFront::genId($title);
		
		$return = array();
		$return[] = $row;
		$return[] = $params;
		$return[] = $id;
		
		if($level != 4)
		{
			$children = $this->getModel()->getPublishedChildren($row->id,false);
			$return[] = $children;
		}
		
		return $return;
	}
	
	protected function outputHtml( $item )
	{
		$item->getPublishedChildren();
		$children = $item->get('children');
		$this->item = $item;
		
		$html = '';
		$dispather = jsGetDispatcher();
		
		$tpl = $item->get('tpl');// ($item->type == 'item')?$item->type.'_item':$item->type;

		$loadtemplate = true;
		$layout = onepageHelperFront::checkDevice();
		if( !empty($item->html) ) // tpl code
		{
			$tplItem = JTable::getInstance('Tpl','onepageTable');
			$tplItem->loadByCode($item->html);
			
			if($tplItem->id > 0)
			{
				$loadtemplate = false;
				if($layout=='desktop')
				{
					$html = $tplItem->html;
				}
				else
				{
					$html = jsGetValueNo0($tplItem,$layout,$tplItem->html);
				}
				
			}
		}
		
		if($loadtemplate)
		{
			$html = $this->loadTemplate($tpl);
		}
		
		
		if( empty($html) )
		{
			$html = $item->content;
		}
		
		
		
		if(count($children) > 0)
		{
			//$dispather->trigger('parseTpl',array(&$html,$item)) ;
			$children = array();
			$k = 0; $col = 0;
			foreach($item->get('children') as $k => $child)
			{
				if($item->level == 3)
				{
					$cparams = $child->params;
					$col += $child->md;
					$col +=  jsGetValueNo0($cparams,'offset',0);
					
					if($col > 12)
					{
						$children[] = '</div><div class="row">';
						$col=0;
					}
				}
				$this->k = $k;
				$children[] = $this->outputHtml( $child );
			}
		}
		else
		{
			$children = array();
		}
		
		$dispather->trigger('parseTpl',array(&$html,$item,$children)) ;
		
		return $html;
	}
	
	/**
	 * @deprecated
	 */
	protected function outputNav( $page )
	{
		$html = '';
		$params = $page->params;
		
		// here, we have to make a fake item
		$item = new JObject();
		$item->type = $page->params['nav_type'];
		$item->content = $page->content;
		$item->params =  $page->params;
		$item->id =  999999;
		$item->title =  $page->title;
		$this->item = $item;
		
		$dispather = jsGetDispatcher();
		$type = $page->params['nav_type'];
		if(!empty($type))
		{
			JPluginHelper::importPlugin('opeltype',$type);
			$this->addTemplatePath(JPATH_PLUGINS."/opeltype/{$type}/tpl");
			$html = $this->loadTemplate($type);
		}
		
		
		
		$dispather->trigger('parseTpl',array(&$html,$item)) ;
		
		return $html;
	}
}
