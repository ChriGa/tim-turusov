<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

OnepageHelperFront::initPage();
JHtml::script('media/jsbt3/js/waypoints.js');
$document = JFactory::getDocument();


$postString = array();
$postString['option']='com_onepage';
$postString['task']='genJSDefault';
$document = JFactory::getDocument();
//$document->addScript('index.php?'.http_build_query($postString));
JHtml::stylesheet('media/jsbt3/css/linea.css');
JHtml::stylesheet('com_onepage/op.css',false,true);
JHtml::stylesheet('media/jsbt3/css/animate.css');
JHtml::stylesheet('media/jsbt3/css/bootstrap-social.css');
//JHtml::stylesheet('media/jsbt3/css/animations.css');

// preload
$postString = array();
$postString['option']='com_onepage';
$postString['task']='page.preloadCssSrc';
$postString['id']=$this->page->id;
$document->addStyleSheet('index.php?'.http_build_query($postString));

$postString['task']='page.preloadJsSrc';
$document->addScript('index.php?'.http_build_query($postString));

// load JS & CSS
if(!jsGetValueNo0($this->page->params,'js_bottom',false))
{
	OnepageHelperFront::importJS($this->page);
}
OnepageHelperFront::importCSS($this->page);

$mixpanel_api = $this->config->getNo0('mixpanel_apikey',false);
if($mixpanel_api)
{
	JHtml::script('media/jsbt3/js/mixpanel.js');
}



$document->setMetaData('apple-mobile-web-app-capable','yes' );
$document->setMetaData('viewport','width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' );
$document->setMetaData('apple-mobile-web-app-status-bar-style','black' );

OnepageHelperFront::addAppIcon($this->page);

?>

<div id="onepage" ng-app="onepage">
<div id="page-wrapper" ng-controller="page">

<!-- Preloader  -->
<?php echo $this->loadTemplate('preloader');?>
<!-- Preloader  -->

<!-- Layout  -->
<?php foreach($this->items as $item):?>
<?php echo $this->outputHtml( $item );?>
<?php endforeach;?>
<!-- Layout  -->

<!-- Modals  -->
<?php echo ($this->loaddlsubscription)?$this->loadTemplate( 'dlproduct_subscription' ):'';?>
<?php echo ($this->loadjsmpurchase)?$this->loadTemplate( 'jsmproduct_purchase' ):'';?>

<!-- Modals  -->
</div>
</div>


<?php 
$postString = array();
$postString['option']='com_onepage';
$postString['task']='page.css';
$postString['id']=$this->page->id;
$document->addStyleSheet('index.php?'.http_build_query($postString));

$postString['task']='page.js';
$document->addScript('index.php?'.http_build_query($postString));
?>

<?php if($this->config->get('ga_enable',false)):?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '<?php echo $this->config->search('ga_code');?>', '<?php echo $this->config->search('ga_website');?>');
ga('send', 'pageview');
</script>
<?php endif; ?>

<?php
if(jsGetValueNo0($this->page->params,'js_bottom',false))
{
	echo OnepageHelperFront::importJS($this->page,true);
}
?>