<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;


list($tItem,$params,$id,$children) = $this->fetchVars();

// Tab Parameters
$fixed_width = jsGetValue($params,'fixed_width');
$fixed_height = jsGetValueNo0($params,'fixed_height');
if($tItem->level == 4)
{
	$fixed_height = jsGetValueNo0($params,'fixed_height',400);
}

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);
$i = 0;

$div_cls = jsGetValue($params,'csscls');
?>

<div class="col-md-12 opheight">
<div id="<?php echo $id;?>" class="optab opheight <?php echo $div_cls;?> ">
				
		<ul class="nav nav-tabs ">
<?php foreach($children as $k => $child): ?>
	<?php if($i == 0):?>
			<li class="active">
				<a href="#tabitem_<?php echo $child->id;?>" data-toggle="tab">
				<?php echo $child->title;?>
				</a>
			</li>

	<?php else:?>
			<li>
			<a href="#tabitem_<?php echo $child->id;?>" data-toggle="tab"><?php echo $child->title;?></a>
			</li>
	<?php endif;?>	
<?php $i++;endforeach;?>
		</ul>
 
	<div class="tab-content opheight"  >
	<?php 
		$i = 0;
		foreach($children as $k => $child):
			$child->height = $fixed_height;
			$this->item = $child;
	?>
			<div class="tab-pane <?php echo ($i == 0)?'active':'';?> opheight" id="tabitem_<?php echo $child->id;?>" style="<?php echo $style;?>">
				<div class="row opheight">
					<?php echo $this->loadTemplate($child->tpl);;?>
				</div>
			</div>
	<?php $i++;endforeach;?>
	</div>
</div>	
</div>	