<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$params  = jsJSON::decode($this->item->params);
$showTitleOnBody = jsGetValue($params,'showtitleonbody',0);
$nocontainer = jsGetValue($params,'nocontainer',0);
$title = (!empty($this->item->title))?$this->item->title:$this->item->type.'_'.$this->item->id;
$id = 'modal'.$this->item->id;;//onepageHelper::genId($title);
?>

<div id="<?php echo $id; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="<?php echo $id; ?>Label" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?php echo $this->item->title;?></h3>
  </div>
  <div class="modal-body">
  		
<?php echo $this->item->content;?>

	</div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('Close');?></button>
  </div>
</div>