<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// we assume this layout contains only 2 elements
$child1 = jsGetValue($children, 0,array());
$child2 = jsGetValue($children, 1,array());
$first_width = jsGetValueNo0($params, 'first_width',4);
$span = (empty($child2))?12:min(array($first_width,11));
$span = min(array($first_width,11));

$fixed_width = jsGetValue($params,'fixed_width');
$fixed_height = jsGetValueNo0($params,'fixed_height',jsGetValueNo0($tItem,'height',0));

$child1->height = $fixed_height;


$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);
$div_cls = jsGetValue($params,'csscls');
?>

<div class="col-xs-12 col-md-12 opheight" >
<div id="<?php echo $id;?>" class="ophorizontal opheight <?php echo $div_cls;?>" style="<?php echo $style;?>">
<div class="row opheight">
<?php if(!empty($child1) ):?>
<div class="col-xs-<?php echo $span;?> col-md-<?php echo $span;?> opheight">
	<div class="row opheight">
	<?php $this->item = $child1;echo $this->loadTemplate($child1->tpl);?>
	</div>
</div>
<?php endif;?>

<?php if(!empty($child2)):?>
<div class="col-xs-<?php echo 12 - $span;?> col-md-<?php echo 12 - $span;?> opheight" >
	<div class="row opheight">
	<?php $child2->height = $fixed_height;$this->item = $child2;echo $this->loadTemplate($child2->tpl);?>
	</div>
</div>
<?php endif;?>
</div>
</div>
</div>