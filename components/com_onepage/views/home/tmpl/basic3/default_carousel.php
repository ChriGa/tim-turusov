<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

//$imgPath = OnepageHelper::getImgPath();

// Carousel Parameters
$indicator = jsGetValue($params,'indicator','1');
$navcontrol = jsGetValue($params,'navcontrol','1');
$autoplay = jsGetValue($params,'autoplay','1');
$fullwidth = jsGetValue($params,'fullwidth','1');
$showTitleOnBody = jsGetValue($params,'showtitleonbody','1');

$fixed_height = jsGetValueNo0($params,'fixed_height',jsGetValueNo0($tItem,'height',0));

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);

$div_cls = jsGetValue($params,'csscls');
?>

<?php if( $tItem->level != 4 ):?>
	<div class="col-md-12 opheight">
<?php endif;?>


<div id="<?php echo $id;?>" class=" carousel slide <?php echo $div_cls;?>" >
	<?php if( $indicator && count($children) > 1 ):?>
	<ol class="carousel-indicators">
		<?php foreach($children as $k => $child):?>
			<li data-target="#<?php echo $id;?>" data-slide-to="<?php echo $k;?>" <?php echo ($k==0)?'class="active"':'';?> ></li>
		<?php endforeach;?>
	</ol>
	<?php endif;?>	
	
	<!-- Carousel items -->
	<div class="carousel-inner">
	<?php 
		foreach($children as $k => $child):
			$child->height = $fixed_height;
			$this->item = $child;
			//$childParams  = jsJSON::decode($child->params);
			$childParams  = $child->params;
	?>
		<div class="<?php echo ($k==0)?'active':'';?> item opheight" style="<?php echo $style;?>">
	
			<div class="row"><?php echo $this->loadTemplate($child->tpl);;?></div>
		</div>
	<?php endforeach;?>	
	</div>
	
	<!-- Carousel nav -->
	<?php if( $navcontrol && count($children) > 1):?>
	<a class="left carousel-control" href="#<?php echo $id;?>" data-slide="prev"><span class="icon-prev"></span></a>
	<a class="right carousel-control" href="#<?php echo $id;?>" data-slide="next"><span class="icon-next"></span></a>
	<?php endif;?>	
</div>

<?php if( $tItem->level != 4 ):?>
	</div>
<?php endif;?>