<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$params  = jsJSON::decode($this->item->params);
$showTitleOnBody = jsGetValue($params,'showtitleonbody',0);
$nocontainer = jsGetValue($params,'nocontainer',0);
$title = (!empty($this->item->title))?$this->item->title:$this->item->type.'_'.$this->item->id;
$id = onepageHelperFront::genId($title);
?>

<div id="contactusModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="contactusModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="contactusModalLabel"><?php echo JText::_('COM_ONEPAGE_LABEL_CONTACTUS');?></h3>
	</div>
	
	<div class="modal-body">

		<div>
			<form class="form-horizontal" id="contactusform" action="index.php?option=com_onepage" method="POST" novalidate>
				
					<fieldset>
					
						<div class="control-group">
							<label class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?></label>
							<div class="controls">
								<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?>" name="email" required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?></label>
							<div class="controls">
								<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?>" name="name" required>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_SUBJECT');?></label>
							<div class="controls">
								<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_SUBJECT');?>" name="subject" required>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_MESSAGE');?></label>
							<div class="controls">
								<textarea rows="5" name="message" ></textarea>
								
							</div>
						</div>
						
						<input type="hidden" name="task" value="contact.submit"> 
						<?php echo JHtml::_( 'form.token' );?>
						
					</fieldset>
				
			</form>
		</div>

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ONEPAGE_LABEL_CLOSE');?></button>
		<button class="btn btn-primary" type="button" onClick="$('#contactusform').submit();"><?php echo JText::_('COM_ONEPAGE_LABEL_SUBMIT');?></button>
	</div>
</div>
