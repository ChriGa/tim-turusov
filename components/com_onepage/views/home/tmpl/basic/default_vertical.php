<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// Thumbnail Parameters
$fixed_width = jsGetValue($params,'fixed_width',0);
$fixed_height = jsGetValueNo0($params,'fixed_height',jsGetValueNo0($tItem,'height',0));

//$first_height = jsGetValue($params,'first_height',5) * 10;
//$second_height = 100 - $first_height;

// Style Setting
$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style[] = 'margin:0 auto';
$style = implode(';',$style);



$div_cls = jsGetValue($params,'csscls');



?>

<div id="<?php echo $id;?>" class="opvertical <?php echo $div_cls;?> opheight span12" style=" <?php echo $style;?>" >
<?php 
	foreach($children as $k => $child):
		//$child->height = $fixed_height;
		$this->item = $child;
		$child->parent_type = 'vertical';
?>
		
			<div class="row-fluid">
				<?php echo $this->loadTemplate($child->tpl);;?>
			</div>
		
<?php  endforeach; ?>
</div>	
