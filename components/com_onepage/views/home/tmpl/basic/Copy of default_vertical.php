<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// Thumbnail Parameters
$fixed_width = jsGetValue($params,'fixed_width',0);
$fixed_height = jsGetValue($params,'fixed_height',0);
$first_height = jsGetValue($params,'first_height',5) * 10;
$second_height = 100 - $first_height;

// Style Setting
$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style[] = 'margin:0 auto';
$style = implode(';',$style);

// we assume this layout contains only 2 elements
$child1 = jsGetValue($children, 0,array());
$child2 = jsGetValue($children, 1,array());

$div_cls = jsGetValue($params,'csscls');

$child1->parent_type = 'vertical';

?>

<div id="<?php echo $id;?>">

<div class="div-vertical <?php echo $div_cls;?>" style="<?php echo (!empty($style))?$style:'';?>">
	<div class="row-fluid opheight" style="<?php echo "height:{$first_height}%;";?>">
	<div class="span12">
	<?php $this->item = $child1;echo $this->loadTemplate($child1->tpl);?>
	</div>
	</div>
<?php 
	if( !empty($child2) ):
	echo '<div class="opheight" style="height:'.$second_height.'%;">';
?>
<?php if( 1 > 2 ):?>
<h4 style="padding: 8px 10px 5px;margin: 20px 3px 0 0;border-bottom: 1px solid #F0F0F0;"><?php echo $child2->title;?></h4>
<?php endif;?>
	
	<?php 
		$child2->parent_type = 'vertical';
		$this->item = $child2;
		echo $this->loadTemplate($child2->tpl);
		echo '</div>';
	?>

<?php endif;?>
</div>

</div>