<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$params  = jsJSON::decode($this->item->params);

$indicator = jsGetValue($params,'indicator','1');
$navcontrol = jsGetValue($params,'navcontrol','1');
$autoplay = jsGetValue($params,'autoplay','1');
$fullwidth = jsGetValue($params,'fullwidth','1');
$showTitleOnBody = jsGetValue($params,'showtitleonbody','1');


$title = (!empty($this->item->title))?$this->item->title:$this->item->type.'_'.$this->item->id;
$id = onepageHelper::genId($title);

$tEl = JTable::getInstance('Menu','OnepageTable');
$tEl->load($this->item->id);
$children = $tEl->getChildren();

foreach($children as $k => $child)
{
	if(!$child->published) unset($children[$k]);
}
$children = array_values($children);


$imgPath = OnepageHelper::getImgPath();
?>

<div id="block_<?php echo $id;?>">
	
	<?php if($showTitleOnBody):?>
		<div class="container">
		<h1 class="mbl pbl"><?php echo $this->item->title;?></h1>
		</div>
	<?php endif;?>	
	
	
	<div class="unslider-banner" id="<?php echo $id;?>1" style="height: 415px;">
	    <ul style="height: 415px;">
	    	<?php 
				foreach($children as $k => $item):
				//if(!JFile::exists($imgPath.'/'.$item->type.'_'.$item->id.'.png')) continue;
				//jsExit($imgPath.'/'.$item->type.'_'.$item->id.'.png');
			?>
			
	        <li style="background-image: url(<?php echo OnepageHelper::getImgPath(true).'/'.$item->type.'_'.$item->id.'.png';?>);background-size: 100%;min-height: 350px">
	        	<div class="container">
					<div class="carousel-caption ">
					<div class="row-fluid">
						<?php echo $item->content;?>
					</div>
					</div>
				</div>
	        </li>
	        
	        <?php endforeach;?>	
	    </ul>
	</div>

		
</div>