<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// Image Parameters
$com = 'components'.DS.'com_onepage'.DS.'bootstrap';
//js
//JHtml::script($com.DS.'js'.DS.'jquery.fitvids.js');




// we do the iframe
$iframe = jsGetValueNo0($params,'videolink');
preg_match('/height="(\w+)"/', $iframe,$match);
$iframe = preg_replace('/width="(\w+)"/', 'width="100%"', $iframe);
$iframe = preg_replace('/height="(\w+)"/', 'height="100%"', $iframe);

$fixed_width = jsGetValueNo0($params,'fixed_width');
$fixed_height = jsGetValueNo0($params,'fixed_height',0);

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) {
	$style[] = 'height:'.$fixed_height.'px';
}	elseif($tItem->level == 4) {
	$style[] = 'height:'.$match[1].'px';
}
$style = implode(';',$style);

$div_style = jsGetValue($params,'csscls');
if(empty($fixed_height) && $tItem->level >4)
{
	$div_style .= ' opautoheight';
}
$div_style .= ' span12';
?>

<?php if( $tItem->level == 4 ):?>
<div class="row-fluid">
<?php endif;?>

<div class="webvideo <?php echo $div_style;?> " style="<?php echo $style;?>">
<div id="<?php echo $id;?>" class="height-100"><?php echo $iframe;?></div>
</div>

<?php if( $tItem->level == 4 ):?>
</div>
<?php endif;?>