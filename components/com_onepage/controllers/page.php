<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;


/**
 */
class OnepageControllerPage extends JControllerLegacy
{
	
	
	public function css()
	{
		$id = JRequest::getInt('id');
		$uri = JFactory::getURI();
		$tpl = JTable::getInstance('Tpl','onepageTable');
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		
		$list = $tEl->getPublishedChildren(true);
		
		$layout = onepageHelperFront::checkDevice();
		
		$codes = array();
		JPluginHelper::importPlugin('opeltype');
		$dispatch = jsGetDispatcher();
		
		foreach($list as $item)
		{
			if(!$item->published) {
				continue;
			}
			
			if( !empty($item->html) )
			{
				$tpl->loadByCode($item->html);
				//$codes[$item->html] = $tpl->css;
				$codes[$item->html] = $tpl->css;
				$code = jsGetValueNo0($tpl,'itemcss');
				if(!empty($code))
				{
					$dispatch->trigger('getItemCSS',array(&$code,$item));
					$codes["node_{$item->id}"] = $code;
				}
			}
			else
			{
				$dispatch->trigger('getCSS',array(&$codes,$item));
				if(JFile::exists(JPATH_SITE."/media/plg_opeltype_{$item->type}/style.css"))
				{
					$code = JFile::read(JPATH_SITE."/media/plg_opeltype_{$item->type}/style.css");
					$dispatch->trigger('getItemCSS',array(&$code,$item));
					$codes["node_{$item->id}"] = $code;
				}
				
			}
			
		}
		
		$codes = array_values($codes);
		
		$codes[] = "\r\n";
		$codes[] = $tEl->css;
		$code = implode(" ",$codes);
		header('Content-Type: text/css');
		echo $code;
		exit;
	}
	
	public function js()
	{
		$id = JRequest::getInt('id');
		$uri = JFactory::getURI();
		$tpl = JTable::getInstance('Tpl','onepageTable');
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		$list = $tEl->getPublishedChildren(true);
		
		$codes = array();
		
		JPluginHelper::importPlugin('opeltype');
		$dispatch = jsGetDispatcher();
		$codes[] = '$(window).load(function(){$(".preloader").delay(350).fadeOut("slow"); $("body").delay(350).css({overflow:"auto"});});';
		$codes[] = '$(document).ready(function() {'; // onready begin
		$codes[] = 'if( $("body").hasClass("modal") ) {$("body").removeClass("modal");}';
		foreach($list as $item)
		{
			$loadJs = true;
			if( !empty($item->html) )
			{
				$tpl->loadByCode($item->html);
				switch($tpl->enable_js)
				{
					case(1):
						if(!isset($codes[$item->html]))
						{
							$codes[$item->html] = $tpl->js;
						}
						
						$loadJs = false;
						break;
				
					case(2):
						$loadJs = false;
						break;
					default:
							
						break;
				}
			}
			
			if($loadJs)
			{
				$dispatch->trigger('getJS',array(&$codes,$item));
			}
		}
		
		$codes['smoothScroll'] =  "$('.smoothScroll').click(function(event){
				  	event.preventDefault();
				  	$('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
				});";
		//$codes[] = '$(".preloader").delay(350).fadeOut("slow"); $("body").delay(350).css({overflow:"auto"});';
		$codes[] = '});';  // onready end
		
		// init Wow
		$codes[] = 'new WOW({offset:150}).init();';
		//$codes[] = 'new WOW({offset:150}).init();';
		$codes = array_values($codes);
		
		$tConfig = jsAppConfig::getInstance('onepage');
		$mixpanel_api = $tConfig->getNo0('mixpanel_apikey',false);
		if($mixpanel_api)
		{
			$codes[] = "mixpanel.init(\"{$mixpanel_api}\");";
			$codes[] = "mixpanel.track( \"PageView.{$tEl->title}\");";
		}
		
		
		$codes[] = $tEl->js;
		$code = implode(" ",$codes);
		header('Content-Type: text/javascript');
		echo $code;
		exit;
	}
	
	public function formSubmit()
	{
		$return = JRequest::getString('returnUrl');
		$return = base64_decode($return);
		
		$result = array();
		$result['success'] = false;
		$result['title'] = JText::_('ERROR');
		$result['content'] = JText::_('ERROR');
		$result['reload'] = true;
		
		// Check for request forgeries.
		//JSession::checkToken() or jexit( jsJSON::encode($result) );
		if(!JSession::checkToken())
		{
			$result['content'] = JText::_('Session Timeout, Please Refresh Your Page!');
			jexit( jsJSON::encode($result) );
		}
	
		$id = JRequest::getInt('eid');
		$returnType = JRequest::getCmd('returnType');
		
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		
		if(!$tEl->published)
		{
			jsExit('');
		}
		
		JPluginHelper::importPlugin('opeltype',$tEl->type);
		$dispatcher = jsGetDispatcher();
		$results = $dispatcher->trigger( 'formaction',array(&$tEl) );
		$results = jsArrayNoEmpty($results);
		
		if( empty($results) )
		{
			jsExit(jsJSON::encode($result));
		}
		elseif(is_array($results[0]))
		{
			$ret = $results[0];
			if(!jsGetValue($ret, 'success',true))
			{
				$result['content'] = jsGetValueNo0($ret,'content',$result['content']);
				jsExit(jsJSON::encode($result));
			}
		}
		
		if($returnType == 'msg')
		{
			$result['success'] = true;
			$result['title'] = JText::_('SUCCESS');
			$result['content'] = JText::_('SUCCESS');
			$result['reload'] = false;
			jsExit(jsJSON::encode($result));
		}
		
		
		// Default Return Url
		$this->setRedirect($return);
	}
	
	
	public function preloadJsSrc()
	{
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('DISTINCT `type`');
		$query->from('`#__onepage_page`');
		$query->where("`lft` > {$tEl->lft}");
		$query->where("`rgt` < {$tEl->rgt}");
		$query->where("`published` = 1");
		$objs = jsDB::loadObjs($query,'obj');
		
		$code = OnepageHelperPlg::preloadScriptSrc('js',$objs);
		$code.= "\r\n";
		$code.= JFile::read(JPATH_SITE."/media/com_onepage/js/app.js");
		header('Content-Type: text/javascript');
		echo $code;
		exit;
	}
	
	public function preloadCssSrc()
	{
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('DISTINCT `type`');
		$query->from('`#__onepage_page`');
		$query->where("`lft` > {$tEl->lft}");
		$query->where("`rgt` < {$tEl->rgt}");
		$query->where("`published` = 1");
		$objs = jsDB::loadObjs($query,'obj');
		
		$code = OnepageHelperPlg::preloadScriptSrc('css',$objs);
		header('Content-Type: text/css');
		echo $code;
		exit;
	}
}