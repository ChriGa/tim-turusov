<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;


/**
 */
class OnepageControllerContact extends JControllerLegacy
{
	public function submit()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
	
		$tConfig = jsAppConfig::getInstance();
		$enabled = $tConfig->get('enable_contact');
		$contact = $tConfig->get('contact_email');
		
		if(!$enabled)
		{
			
		}
		
		
		// Initialise variables.
		$app	= JFactory::getApplication();
	
	
		// Get the data from POST
		$return = JRequest::getString('returnUrl');
		$return = base64_decode($return);
		
		$email = JRequest::getString('email');
		
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)
		{
			
		}
		else
		{
			$name = JRequest::getString('firstname').' '.JRequest::getString('lastname');
			$subject = JRequest::getString('subject');
			$msg = JRequest::getString('message');
			
			if(!empty($contact) && !empty($msg))
			{
				$data = array();
				$data['email'] = $email;
				$data['name'] = $name;
				$data['subject'] = $subject;
				$data['message'] = $msg;
				$this->_sendEmail($data,$contact);
			}
		}
		
	
		$this->setRedirect($return);
	
		return true;
	}
	
	private function _sendEmail($data,$contact)
	{
		$app		= JFactory::getApplication();
		$params 	= JComponentHelper::getParams('com_contact');
		
		
		$mailfrom	= $app->getCfg('mailfrom');
		$fromname	= $app->getCfg('fromname');
		$sitename	= $app->getCfg('sitename');
		
		$name		= $data['name'];
		$email		= $data['email'];
		$subject	= $data['subject'];
		$body		= $data['message'];
	
		// Prepare email body
		$prefix = JText::sprintf('COM_ONEPAGET_TEXT_ENQUIRY', JURI::base());
		$body	= $prefix."\n".$name.' <'.$email.'>'."\r\n\r\n".stripslashes($body);
		
		$mail = JFactory::getMailer();
		$mail->addRecipient($contact);
		$mail->addReplyTo(array($email, $name));
		$mail->setSender(array($mailfrom, $fromname));
		$mail->setSubject($sitename.': '.$subject);
		$mail->setBody($body);
		$sent = $mail->Send();
	
		return $sent;
	}
	
	public function subscribe()
	{
		JRequest::checkToken() or die( 'Please make sure your cookies are enabled' );
		require_once (JPATH_SITE.'/administrator/components/com_acymailing/helpers/helper.php');
		acymailing_checkRobots();
		$app = JFactory::getApplication();
		
		$config = acymailing_config();
		$subscriberClass = acymailing_get('class.subscriber');
		
		
		$status = $subscriberClass->saveForm();
		$subscriberClass->sendNotification();
		
		/*$result = array();
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		
		header('Content-Type: text/javascript');
		jsExit(jsJson::encode($result));*/
		$this->setRedirect(JURI::base());
	}
}