<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');


class OnepageModelHome extends JModelLegacy
{
		
	public function getEnglish()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_page');
		$query->where('`level` = "3"');
		$query->where('`code` = "en"');
		$item = jsDB::loadObj($query,'obj');
		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_page');
		$query->where('`parent_id` = "'.$item->id.'"');
		//$query->where('`level` = 4');
		$query->order(' `lft` ASC');
		$objs = jsDB::loadObjs($query,'obj');
	
		$lastKey = count($objs) - 1;
		foreach($objs as $k => $obj)
		{
			switch($obj->type)
			{
				case('article'):
				case('editor'):
					if($obj->path != 'footer')
					{
						$obj->tpl = 'body';
					}
					else
					{
						$obj->tpl = 'footer';
					}
					
					break;
		
				default:
					$obj->tpl = $obj->type;
					break;
			}
			
			$objs[$k] = $obj;
		}
		return $objs;
	}
	
	protected function populateState($ordering = 'ordering', $direction = 'ASC')
	{
		$app = JFactory::getApplication();
	
		$params = $app->getParams();
		$this->setState('params', $params);
		
	}
	
	public function getPublishedChildren($id,$rec = true)
	{
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$children = $tEl->getPublishedChildren($rec);
		
		$vals = array();
		foreach($children as $k => $child)
		{
			if(!$child->published) continue;
			
			switch($child->type)
			{
				case('article'):
				case('editor'):
				case('item'):
					if($child->path != 'footer')
					{
						$child->set('tpl','body');
					}
					else
					{
						$child->set('tpl','footer');
					}
						
					break;
			
				default:
					$child->set('tpl', $child->type) ;
				break;
			}
			
			$vals[] = $child;
		}
		
		return $vals;
	}

	public function getPages()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_page');
		$query->where('`level` = "3"');
		$query->where('`code` = "en"');
		$query->where('`published` = "1"');
		$query->order(' `lft` ASC');
		$objs = jsDB::loadObjs($query,'obj');
		
		$tConfig = jsAppConfig::getInstance();
		$home = $tConfig->get('home');
		$hObj = new stdClass();
		foreach($objs as $k=> $obj)
		{
			if($obj->id == $home)
			{
				$hObj = $obj;
				unset($objs[$k]);
				break;
			}
		}
		
		array_unshift($objs, $hObj);
		$objs = array_values($objs);
		return $objs;
	}
	
	public function getPage($id = 3 )
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_page');
		$query->where("`parent_id` = '{$id}'");
		$query->where("`level` = '4'");
		//$query->where('`code` = "en"');
		$query->where('`published` = "1"');
		$query->where('`path` NOT IN ( "navbar","footer","hellounit" ) ');
		$query->order(' `lft` ASC');
		
		$objs = jsDB::loadObjs($query,'obj');
	
		$lastKey = count($objs) - 1;
		foreach($objs as $k => $obj)
		{
			switch($obj->type)
			{
				case('article'):
				case('editor'):
					if($obj->path != 'footer')
					{
						$obj->tpl = 'body';
					}
					else
					{
						$obj->tpl = 'footer';
					}
						
					break;
		
				default:
					$obj->tpl = $obj->type;
				break;
			}
				
			$objs[$k] = $obj;
		}
		
		return $objs;
	}
}
