<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');


class OnepageModelPage extends JModelLegacy
{
	
	protected function populateState($ordering = 'ordering', $direction = 'ASC')
	{
		$app = JFactory::getApplication();
	
		$params = $app->getParams();
		$this->setState('params', $params);
		
	}
	
	public function getPublishedChildren($id,$rec = true)
	{
		$tEl = JTable::getInstance('Page','OnepageTable');
		//$tEl->load($id);
		$children = $tEl->getChildren($id,$rec);
		
		$vals = array();
		foreach($children as $k => $child)
		{
			if(!$child->published) continue;
			$tChild = JTable::getInstance('Page','OnepageTable');
			$tChild->bind($child);
			$vals[] = $tChild;
		}
		
		return $vals;
	}

}
