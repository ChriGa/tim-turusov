<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class OnepageTableTpl extends jsTable
{
	/**
	 * @param	JDatabase	A database connector object
	 */
	public $id = 0;
	public $title  = null;
	public $type  = null;
	public $create_date  = '0000-00-00 00:00:00';
	public $code  = '';
	public $html = '';
	public $js = '';
	public $css = '';
	public $mobile = '';
	public $tablet = '';
	public $enable_js = 0;
	public $from = 'custom';
	public $params = array();
	
	
	function __construct(&$db)
	{
		parent::__construct('#__onepage_tpl', 'id', $db);
	}
	
	
	function loadByCode($code)
	{
		$db = jsDB::instance();
		$code = $db->quote($code);
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__onepage_tpl'));
		$query->where("`code` = {$code}");
		$item = jsDB::loadObj($query,'obj');
	
		if(!empty($item))
		{
			$this->bind($item);
		}
		else
		{
			$this->id = 0;
		}
	
		return $this;
	}
	
	public function generateToken()
	{
		return md5(uniqid());
	}
	
	public function store($updateNulls = false)
	{
		if(empty($this->code))
		{
			$this->code = $this->generateToken();
		}
	
		return parent::store($updateNulls);
	}
}
