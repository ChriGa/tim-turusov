<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;


class OnepageTablePage extends jsTableNested
{
	/**
	 * @param	JDatabase	A database connector object
	 */
	public $id = 0;
	public $title  = null;
	public $code  = 'en';
	public $showtitle = 0;
	public $parent_id  = 1;
	public $level  = 3;
	public $published  = 1;
	public $path = 'virtual';
	public $type = 'page';
	public $lft = 0;
	public $rgt = 1;
	public $content = null;
	public $js = null;
	public $css = null;
	public $md = 4;
	public $html = '';
	public $ver = '1.4.0';
	public $params = array();
	
	protected $children = array();
	protected $tpl = null;
	protected $group = false;
	protected $item = false;
	function __construct(&$db)
	{
		parent::__construct('#__onepage_page', 'id', $db);
	}
	
	public function getPublishedChildren($recurring = false)
	{
		$children = $this->getChildren($this->id,$recurring);
		$a = array();
		$upids = array();
		foreach($children as $child)
		{
			if(!$child->published)
			{
				$upids[] = $child->id;
				continue;
			}
			elseif( in_array($child->parent_id,$upids) )
			{
				$upids[] = $child->id;
				continue;
			}
			
			$nChild = JTable::getInstance('Page','onepageTable');
			$nChild->bind($child);
			$nChild->set( 'tpl', $nChild->type );
			if(!$recurring)
			{
				if($nChild->type == 'item')
				{
					$path = JPATH_PLUGINS."/opeltype/{$this->type}/tpl/default_{$this->type}_item.php";
					if(JFile::exists($path))
					{
						$nChild->set( 'tpl', $this->type.'_'.$nChild->type );
					}
				}
				else
				{
					// compatible with old version.
					$path = JPATH_PLUGINS."/opeltype/{$nChild->type}/tpl/default_{$nChild->type}.php";
					
					if(!JFile::exists($path))
					{
						$nChild->set( 'tpl', 'item' );
						
					}
				}
				
			}
			
			$a[] = $nChild;
		}
		$this->set('children',$a);
		return $a;
	}
	
	
}
