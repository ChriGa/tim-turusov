<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class OnepageTableScript extends jsTableNested
{
	/**
	 * @param	JDatabase	A database connector object
	 */
	public $id = 0;
	public $parent_id  = 0;
	public $title  = null;
	public $type  = null;
	public $level  = 4;
	public $published  = 1;
	public $path = 'virtual';
	public $lft = 0;
	public $rgt = 1;
	public $params = array();
	
	
	function __construct(&$db)
	{
		parent::__construct('#__onepage_script', 'id', $db);
	}
	
	public function bind($array, $ignore = array())
	{
		$updated = parent::bind($array,$ignore);
		
		$this->params = jsGetValue($array,'params',array());
		if( is_string( $this->params) )
		{
			$this->params = jsJson::decode($this->params,true);
		}
		
		return $updated;
	}
	
	function store($updateNulls = false)
	{
		if( is_array( $this->params) || is_object( $this->params) )
		{
			$this->params = jsJson::encode($this->params);
		}
		return parent::store($updateNulls);
	}
	
	function createDefaultBlocks()
	{
		$parent_id = $this->id;
	
		$vals = array();
		$vals['parent_id'] = $parent_id;
		$vals['type'] = 'css';
		
		// One Page
		$tEl = JTable::getInstance('Script','OnepageTable');
		$tEl->setLocation($parent_id, 'last-child');
	
		//$vals['path'] = 'Navigation Bar';
	
		$tEl->bind($vals);
		$tEl->store();
	
		// One Page
		$vals['type'] = 'js';
		$tEl = JTable::getInstance('Script','OnepageTable');
		$tEl->setLocation($parent_id, 'last-child');
		
		//$vals['path'] = 'Navigation Bar';
		
		$tEl->bind($vals);
		$tEl->store();
	
		return true;
	}
}
