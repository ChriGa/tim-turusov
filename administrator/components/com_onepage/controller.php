<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;

/**
 * Component Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 */
class OnepageController extends JControllerLegacy
{
	/**
	 * @var		string	The default view.
	 * @since	1.6
	 */
	protected $default_view = 'pages';

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		// Load the submenu.
		$view		= JRequest::getCmd('view', 'pages');
		
		//OnepageHelper::addSubmenu($view);

		//$layout 	= JRequest::getCmd('layout', 'articles');
		$id			= JRequest::getInt('id');


		parent::display();

		return $this;
	}
	// deprecated after v1.4
	public function ignoreVersion()
	{
		$not = jsNotification::getInstance();
		$latest = $not->call('product',array('id'=>JS_APP_ID));
		
		$tConfig = jsAppConfig::getInstance();
		$tConfig->set( 'ignore_version', $tConfig->get('latest_version') );
		$result =array();
		$result['success'] = true;
		jsExit(jsJSON::encode($result));
	}
	
	public function saveConfig()
	{
		$db = jsDB::instance();
		// Error in upload
		$result = array();
		$result['success']= true;
		
		$id = JRequest::getInt('id');
		$name = JRequest::getCmd('name');
		$val = JRequest::getString('value','','default',JREQUEST_ALLOWRAW);
		
		$tConfig = jsAppConfig::getInstance('onepage');
		$updated = $tConfig->set($name,$val);
		// save title
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	/**
	 * @deprecated after 1.4
	 */
	public function saveContact()
	{
		$user = JFactory::getUser();
		//jsExit($user->groups);
		if(!in_array(8,$user->groups))
		{
			$result =array();
			$result['success'] = false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
	
			jsExit(jsJSON::encode($result));
		}
	
		$config = jsAppConfig::getInstance();
		$contact_email = JRequest::getString('contact_email','');
		$config->set('contact_email',$contact_email);
	
		$this->setRedirect('index.php?option=com_onepage');
	}
	/**
	 * @deprecated after 1.4
	 */
	public function saveMenuSetting()
	{
		$user = JFactory::getUser();
		$db = jsDB::instance();
		
		$result =array();
		$result['success'] = false;
		$result['title'] = JText::_('ERROR');
		$result['content'] = JText::_('ERROR');
		
		if(!in_array(8,$user->groups))
		{
			jsExit(jsJSON::encode($result));
		}
		
		
		$tConfig = jsAppConfig::getInstance();
		
		$item = new stdClass();
		$topmenu_published = JRequest::getInt('topmenu_published');
		$tConfig->set('topmenu_published',$topmenu_published);
		
		$topmenu_barcolor = JRequest::getCmd('topmenu_barcolor','black');
		$tConfig->set('topmenu_barcolor',$topmenu_barcolor);
		
		$topmenu_fixed = JRequest::getInt('topmenu_fixed',1);
		$tConfig->set('topmenu_fixed',$topmenu_fixed);
		
		$topmenu_showpng = JRequest::getInt('topmenu_showpng');
		$tConfig->set('topmenu_showpng',$topmenu_showpng);
		
		$topmenu_title = JRequest::getString('topmenu_title');
		$tConfig->set('topmenu_title',$topmenu_title);
		
		// Logo
		$image = JRequest::getVar('image_file', null, 'files', 'array');
		$filePath = OnepageHelper::getImgPath().'/logo.png';
		if(!empty($image['size']))
		{
			if( strtolower(JFile::getExt($image['name']) ) != 'png')
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_PNG_ONLY');
					
			}
		
			if ( !JFile::upload($image['tmp_name'], $filePath ))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
					
			}
			
			// resize it
			/*$destHeight = JRequest::getInt('topmenu_logoheight',32);
			$imginfo = getimagesize($filePath);
			$srcWidth = $imginfo[0];
			$srcHeight = $imginfo[1];
			
			$destWidth = $srcWidth * $destHeight / $srcHeight;
			
			$img = imagecreatefrompng($filePath);
			$dst_img = imagecreatetruecolor($destWidth, $destHeight);
				
			imagecopyresized($dst_img, $img, 0, 0, 0, 0, $destWidth, $destHeight, $srcWidth, $srcHeight);
			//imagepng($img);
			ob_start();
			imagepng($dst_img);
			$imageData = ob_get_contents();
			ob_end_clean();
				
			ImageDestroy($img);
			ImageDestroy($dst_img);
			JFile::write($filePath, $imageData);*/
		}
	
		$this->setRedirect('index.php?option=com_onepage&view=hf');
	}
	/**
	 * @deprecated after 1.4
	 */
	public function saveFooterSetting()
	{
		$user = JFactory::getUser();
		$db = jsDB::instance();
		
		$result =array();
		$result['success'] = false;
		$result['title'] = JText::_('ERROR');
		$result['content'] = JText::_('ERROR');
	
		if(!in_array(8,$user->groups))
		{
			jsExit(jsJSON::encode($result));
		}
		
		$tConfig = jsAppConfig::getInstance();
		
		$item = new stdClass();
		$footer_published = JRequest::getInt('footer_published');
		$tConfig->set('footer_published',$footer_published);
		
		$adv = JRequest::getInt('footer_adv');
		$tConfig->set('footer_adv',$adv);
		
		$footer_csscls = JRequest::getCmd('footer_csscls');
		$tConfig->set('footer_csscls',$footer_csscls);
		
		$content = JRequest::getString('footer_content','','post',JREQUEST_ALLOWRAW);
		$tConfig->set('footer_content',$content);
		
		$footer_applied = JRequest::getVar('footer_applied_to',array());
		
		JArrayHelper::toInteger($footer_applied);
		$footer_applied = implode(',',$footer_applied);
		$tConfig->set('footer_applied_to',$footer_applied);
		
		$link = 'index.php?option=com_onepage&view=hf&layout=footer';
		$this->setRedirect($link);
	}
	
	/**
	 * @deprecated after 1.4
	 */
	public function restoreSample()
	{
		$user = JFactory::getUser();
		//jsExit($user->groups);
		if(!in_array(8,$user->groups))
		{
			$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_ERROR_NOT_SUPERADMIN'),'error');
		}
		
		$db = JFactory::getDbo();
		$query = "TRUNCATE TABLE `#__onepage_menu`";
		$db->setQuery($query);
		
		$db->query();
		
		$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_SUCCESS_SAMPLE_RESTORED'));
	}
	
	/**
	 * @deprecated after 1.4
	 */
	public function replaceDP()
	{
		$user = JFactory::getUser();
		//jsExit($user->groups);
		if(!in_array(8,$user->groups))
		{
			$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_ERROR_NOT_SUPERADMIN'),'error');
		}
		jimport('joomla.filesystem.archive');
		$result = array();
		
		$pack = JRequest::getVar('pack', null, 'files', 'array');
		$tmpPath = JApplication::getCfg('tmp_path');
		$zip = JArchive::getAdapter('zip');
		$defaultPath = JPATH_SITE.'/media/com_onepage/DefaultPage.zip';
		
		if( empty($pack) || $pack['size'] < 0 )
		{
			$result['success'] = false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ONEPAGE_ACTION_ERROR_DPZIP_EMPTY');
			jsExit(jsJSON::encode($result));
		}
		
		if(JFile::exists($defaultPath))
		{
			// rename it first!
			$updated = JFile::copy($defaultPath, dirname($defaultPath).'/dpbackup.zip');
			if(!$updated)
			{
				$result['success'] = false;
				$result['title'] = JText::_('ERROR');
				$result['content'] = JText::_('ONEPAGE_ACTION_ERROR_DPZIP_RENAME_FAILED');
				jsExit(jsJSON::encode($result));
			}
			
			JFile::delete($defaultPath);
		}
		
		$updated = JFile::copy($pack['tmp_name'], $defaultPath);
		
		$result['success'] = true;
		$result['title'] = JText::_('SUCCESS');
		$result['content'] = JText::_('SUCCESS');
		jsExit(jsJSON::encode($result));
	}
	
	
	/**
	 * @deprecated after 1.4
	 */
	public function uploadTheme()
	{
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		//jsExit($user->groups);
		if(!in_array(8,$user->groups))
		{
			$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_ERROR_NOT_SUPERADMIN'),'error');
		}
	
		$image = JRequest::getVar('theme', null, 'files', 'array');
		if(!empty($image))
		{
			if( strtolower(JFile::getExt($image['name']) ) != 'css')
			{
				$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_ERROR_CSS_ONLY'),'error');	
			}
				
			if ( !JFile::upload($image['tmp_name'], JPATH_COMPONENT_SITE.'/bootstrap3/theme.css' ))
			{
				// Error in upload
				$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE'),'error');	
				
			}
		}
	
	
		$this->setRedirect('index.php?option=com_onepage&view=config');
	}
	
	/**
	* @deprecated after 1.4
	*/
	public function restoreTheme()
	{
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		//jsExit($user->groups);
		if(!in_array(8,$user->groups))
		{
			$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_ERROR_NOT_SUPERADMIN'),'error');
		}
	
		if ( !JFile::copy( JPATH_COMPONENT_SITE.'/bootstrap/flat-ui.css' , JPATH_COMPONENT_SITE.'/bootstrap/theme.css' ))
		{
			// Error in upload
			$this->setRedirect('index.php?option=com_onepage',JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE'),'error');
				
		}
	
		$this->setRedirect('index.php?option=com_onepage&view=config');
	}
	
	public function uploadIcon()
	{
		$result =array();
		$result['success'] = false;
		$result['title'] = JText::_('ERROR');
		$result['content'] = JText::_('ERROR');
		
		$db = JFactory::getDbo();
		
		$query = " SELECT * FROM  `#__template_styles`"
				." WHERE `client_id` = 0 AND `home` = 1"
				;
		$db->setQuery($query);
		$item = $db->loadObject();
		
		$image = JRequest::getVar('favicon', null, 'files');
		
		if(!empty($image))
		{
			if( strtolower(JFile::getExt($image['name']) ) != 'ico')
			{
				$result['content'] = JText::_('.ico format only');
				jsExit(jsJSON::encode($result));
			}
	
			$path = JPATH_SITE."/templates/{$item->template}/favicon.ico";

			if ( !JFile::upload($image['tmp_name'], $path ))
			{
				$result['content'] = JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				jsExit(jsJSON::encode($result));	
			}
		}
		
	// App Icon
		$image = JRequest::getVar('appicon144', null, 'files');
		if(!empty($image))
		{
			if( strtolower(JFile::getExt($image['name']) ) != 'png')
			{
				$result['content'] = JText::_('ONEPAGE_ERROR_ICO_ONLY');
				jsExit(jsJSON::encode($result));
			}
		
			$path = JPATH_SITE."/images/onepage/appicon144.png";
		
			if ( !JFile::upload($image['tmp_name'], $path ))
			{
				// Error in upload
				$result['content'] = JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				jsExit(jsJSON::encode($result));
			}
			
			$img = new jsImage($path);
			$img->resize('144', '144')->toFile($path);
			$img->resize('114', '114')->toFile( dirname($path).'/appicon114.png' );
			$img->resize('72', '72')->toFile( dirname($path).'/appicon72.png' );
			$img->resize('57', '57')->toFile( dirname($path).'/appicon57.png' );	
		}
		
		$tConfig = jsAppConfig::getInstance();
		$enable_default_style = JRequest::getCmd( 'enable_default_style' );
		$tConfig->set( 'enable_default_style',$enable_default_style );
		
		
	
		$result['success'] = true;
		$result['title'] = JText::_('SUCCESS');
		$result['content'] = JText::_('SUCCESS');
		jsExit(jsJSON::encode($result));
	}
	
	
	public function updateTpls()
	{
		$endpoint = 'https://j-soho.com/index.php?option=com_onepage';
		$not = jsNotification::getInstance('onepage',$endpoint);
		$tpls = $not->call('update.getTpls',array());
		
		$result = array();
		$result['success'] = true;
		$result['title'] = JText::_('SUCCESS');
		$result['content'] = JText::_('SUCCESS');
		
		// exclude, j-soho.com
		$uri = JFactory::getURI();
		$prefix = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
		if( in_array($prefix,array('http://j-soho.com')) )
		{
			$not->config->set('updatetpl',0);
			jsExit(jsJSON::encode($result));
		}
		
		$tEl = JTable::getInstance('Tpl','onepageTable');
		if($tpls !== false)
		{
			$default = array();
			$default['id'] = '';
			$default['title'] = '';
			$default['type'] = '';
			$default['html'] = '';
			$default['js'] = '';
			$default['css'] = '';
			$default['itemcss'] = '';
			$default['mobile'] = '';
			$default['tablet'] = '';
			$default['code'] = '';
			$default['create_date'] = '0000-00-00 00:00:00';
			$default['from'] = 'system';
			$default['params'] = array();
			
			foreach($tpls as $tpl)
			{
				$tEl->loadByCode($tpl->code);
				if($tEl->id < 1)
				{
					$tEl->bind($default);
				}
				$tEl->set('html',$tpl->html);
				$tEl->set('title',$tpl->title);
				$tEl->set('type',$tpl->type);
				$tEl->set('js',$tpl->js);
				$tEl->set('mobile',$tpl->mobile);
				$tEl->set('tablet',$tpl->tablet);
				$tEl->set('enable_js',$tpl->enable_js);
				$tEl->set('css',$tpl->css);
				$tEl->set('itemcss',$tpl->itemcss);
				$tEl->set('code',$tpl->code);
				$tEl->set('from', 'system');
				$tEl->store();
			}
			
			$not->config->set('updatetpl',0);
		}
		else
		{
			$result['success'] = false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
		}
		
		jsExit(jsJSON::encode($result));
	}
	
	
}
