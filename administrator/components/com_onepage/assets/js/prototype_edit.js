$(document).ready(function() {
	vex.defaultOptions.className = 'vex-theme-os';
	
	$('.jstips').tooltip({placement:'bottom'});
	$('.summernote').summernote({
		height: 250,
		toolbar: [
				    ['style', ['style']],
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
				    ['insert', ['link']],//, 'picture', 'video'
				    ['table', ['table']],
				    ['view', ['fullscreen','codeview']],
				    ['help', ['help']]
				]
		
	});
	
	/*$('.js-switch').each(function(idx,e) {
	  var switchery = new Switchery(e);
	});*/
	$('nav .dropdown-toggle').on('click',function() {
		return false;
	})
	
});


var app = angular.module('myApp', ['NgSwitchery']);

function wholepage($scope) {
	$scope.item = getInfo();
	$scope.id = getId();
	$scope.smove = false;
	$scope.sm = false;
	$scope.sc = false;
	$scope.st = false;
	$scope.embed = false;
	
	$scope.list = getList();
	
	$scope.sp = '';
	
	$scope.activeClass = function(o) {
		if(o.id == $scope.id) {
			return 'uactive';
		}
		
		return '';
	}
	
	$scope.isEnable = function(v) {
		if(v > 0) {
			return 'primary';
		} else {
			return 'default';
		}
	}
	
	$scope.changePage = function() {
		window.location.href="index.php?option=com_onepage&view=prototype&layout=edit&id="+$scope.pageid;
	}
	
	$scope.mapClick = function(id,clickEvent) {
		clickEvent.stopPropagation();
		window.location.href="index.php?option=com_onepage&view=prototype&layout=edit&id="+id;
	}
	
	
	$scope.showPanel = function(t) {
		//$scope.sp = ($scope.sp == t)?'':t;
		$scope.sp = t;
		$scope.sm = ($scope.sp=='map')?true:false;
		$scope.smove = ($scope.sp=='move')?true:false;
		$scope.sc = ($scope.sp=='create')?true:false;
		$scope.st = ($scope.sp=='type')?true:false;
		
		if(t == '') {
			$scope.embed = false;
		}
	}
	
	$scope.createEl = function(lv,t) {
		var data = {'lv':lv,type:t};
		if($scope.embed) {
			data.task = 'prototype.embed';
		}
		data.title = $scope.createtitle;
		$('#createForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			'data':data,
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					window.location.href="index.php?option=com_onepage&view=prototype&layout=edit&id="+res.id;
				}

				NProgress.done();
			}
		});
	}
	
	$scope.changeType = function(t) {
		$('#typeForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data:{type:t},
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					window.location.reload();
				}

				NProgress.done();
			}
		});
	}
	
	$scope.moveTo = function(t,clickEvent) {
		clickEvent.stopPropagation();
		$('#moveForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data:{'target':t},
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					window.location.reload();
				}

				NProgress.done();
			}
		});
	}
	
	$scope.del = function() {
		vex.dialog.confirm({
			message: 'Sure to delete?',
			callback: function(value) {
				if(value === true) {
					$('#aform').ajaxSubmit({
						dataType:  'json',
						type: 'POST',
						data:{id:$scope.item.id,task:'prototype.deleteItem'},
						beforeSubmit: function()	{
							NProgress.start();
						},
						success: function(res)	{
							if(res.success) {
								window.location.href = 'index.php?option=com_onepage&view=prototype&layout=edit&id='+res.id;
							}

							NProgress.done();
						}
					});
				}
			}
		});
		
	}

	$scope.showEmbed = function() {
		$scope.embed = true;
		$scope.showPanel('create');
	}
	
	$scope.canShowEmbed = function() {
		var m = $scope.item;
		var it = getItemType();
		if(m.level == 4 && jQuery.inArray(m.type,it) >= 0)	{
			return true;
		}
		
		return false;
	}
	
	$scope.duplicate = function() {
		$('#aform').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data:{id:$scope.item.id,task:'prototype.duplicate'},
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					window.location.href = 'index.php?option=com_onepage&view=prototype&layout=edit&id='+res.id;
				}

				NProgress.done();
			}
		});
	}
}

