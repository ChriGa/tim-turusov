$(document).ready(function() {
	$('.jstips').tooltip();
	$('#jscss').ajaxForm({
		dataType:  'json',
		type: 'POST',
		beforeSubmit: function()	{
			//$('#changeformModal').block({ message: "Processing..." });
			//$.blockUI({ message: "Processing..." });
			NProgress.start();
		},
		success: function(res)	{
			if(res.success) {
				var scope = $('#jscss').scope();
				scope.$apply(function() {
					scope.js = res.js;
					scope.css = res.css;
				});
				/*$scope.list.unshift(res.entry);
				$scope.add_title='';
				$scope.$apply();*/
			}
			$('#jscss .fileinput').fileinput('clear');
			NProgress.done();
		}
	});
	
	$('#jsconfig').ajaxForm({
		dataType:  'json',
		type: 'POST',
		beforeSubmit: function()	{
			NProgress.start();
		},
		success: function(res)	{
			if(res.success) {
				
			}
			NProgress.done();
		}
	});
	
	$('#jsform').ajaxForm({
		dataType:  'json',
		type: 'POST',
		beforeSubmit: function()	{
			//$('#changeformModal').block({ message: "Processing..." });
			//$.blockUI({ message: "Processing..." });
			NProgress.start();
		},
		success: function(res)	{
			if(res.success) {
				var scope = $('#jscss').scope();
				scope.$apply(function() {
					scope.js = res.js;
					scope.css = res.css;
				});
				/*$scope.list.unshift(res.entry);
				$scope.add_title='';
				$scope.$apply();*/
			}
			//$('#jscss .fileinput').fileinput('clear');
			NProgress.done();
		}
	});
	
	$('#cssform').ajaxForm({
		dataType:  'json',
		type: 'POST',
		beforeSubmit: function()	{
			//$('#changeformModal').block({ message: "Processing..." });
			//$.blockUI({ message: "Processing..." });
			NProgress.start();
		},
		success: function(res)	{
			if(res.success) {
				var scope = $('#jscss').scope();
				scope.$apply(function() {
					scope.js = res.js;
					scope.css = res.css;
				});
				/*$scope.list.unshift(res.entry);
				$scope.add_title='';
				$scope.$apply();*/
			}
			//$('#jscss .fileinput').fileinput('clear');
			NProgress.done();
		}
	});
	
	$('.pageparams').editable({
		 mode: 'inline',
         url: 'index.php?option=com_onepage',
         ajaxOptions: {
        	 type: 'post',
        	 dataType: 'json'
         },
         params: { task:'prototype.saveConfig',id:getId() }
    });
	
	$('.pageinfo').editable({
		 mode: 'inline',
		 url: 'index.php?option=com_onepage',
         ajaxOptions: {
	       	 type: 'post',
	       	 dataType: 'json'
         },
         params: { task:'prototype.saveInfo',id:getId() }
   });
	
	$('.summernote').summernote({
		height: 250,
		toolbar: [
				    ['style', ['style']],
				    ['style', ['bold', 'italic', 'underline', 'clear']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
				    ['insert', ['link']],//, 'picture', 'video'
				    ['table', ['table']],
				    ['view', ['fullscreen','codeview']],
				    ['help', ['help']]
				],
		codemirror: { // codemirror options
		    theme: 'monokai'
		}
		
	});
	
});

var app = angular.module('myApp', []);

function wholepage($scope) {
	$scope.pageid = getId();
	$scope.js = getJS();
	$scope.css = getCSS();
	$scope.item = getInfo();
	$scope.changePage = function() {
		window.location.href="index.php?option=com_onepage&view=page&layout=config&id="+$scope.pageid;
	}
	
	$scope.saveInfo = function(v) {
		$('#infoForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}

				NProgress.done();
			}
		});
	}
	
	$scope.submit = function(v) {
		var sHTML = $('.summernote').code();
		$('#fontForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'content':sHTML},
			type: 'POST',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				//$('#elform .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.addFontLink = function() {
		if($scope.item.params.font_links == null) {
			$scope.item.params.font_links = [];
		}
		$scope.item.params.font_links.push({link:''});
	}
	
	$scope.delLink = function(idx) {
		$scope.item.params.font_links.splice(idx,1);
	}
}