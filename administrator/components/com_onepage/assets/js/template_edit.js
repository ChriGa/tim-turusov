$(document).ready(function() {
	
	$('.jstips').tooltip({placement:'bottom'});
	
	var html = CodeMirror.fromTextArea(document.getElementById("html"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/html'
	});
	
	var info = getInfo();
	html.setValue(info.html);
	
	var csseditor = CodeMirror.fromTextArea(document.getElementById("csscode"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/css'
	});
	
	var itemcsseditor = CodeMirror.fromTextArea(document.getElementById("itemcsscode"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/css'
	});
	
	var jseditor = CodeMirror.fromTextArea(document.getElementById("jscode"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/javascript'
	});
	
	var mobile = CodeMirror.fromTextArea(document.getElementById("mobile"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/html'
	});
	
	var tablet = CodeMirror.fromTextArea(document.getElementById("tablet"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/html'
	});
	
	$('#html').data('CodeMirrorInstance', html);
	$('#csscode').data('CodeMirrorInstance', csseditor);
	$('#itemcsscode').data('CodeMirrorInstance', itemcsseditor);
	$('#jscode').data('CodeMirrorInstance', jseditor);
	$('#mobile').data('CodeMirrorInstance', mobile);
	$('#tablet').data('CodeMirrorInstance', tablet);
	
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var tab = e.target // activated tab
		var id = $(tab).attr('href');
		var editor = $(id).children('textarea').data('CodeMirrorInstance');
		editor.refresh();
	  //e.relatedTarget // previous tab
	})
});


var app = angular.module('myApp', []);

function wholepage($scope) {
	$scope.item = getInfo();
	$scope.tpl = getTpl();
	
	$scope.submit = function() {
		$('#html').data('CodeMirrorInstance').save();
		$('#csscode').data('CodeMirrorInstance').save();
		$('#itemcsscode').data('CodeMirrorInstance').save();
		$('#jscode').data('CodeMirrorInstance').save();
		$('#mobile').data('CodeMirrorInstance').save();
		$('#tablet').data('CodeMirrorInstance').save();
		
		$('#elform').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			//'data':data,
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.item.code = res.code;
					});
				}

				NProgress.done();
			}
		});
	}
	
	$scope.restore = function() {
		var tpl = $scope.tpl[0];
		var he = $('#html').data('CodeMirrorInstance');
		var ce = $('#csscode').data('CodeMirrorInstance');
		var ice = $('#itemcsscode').data('CodeMirrorInstance');
		var je = $('#jscode').data('CodeMirrorInstance');
		var mobile = $('#mobile').data('CodeMirrorInstance');
		var tablet = $('#tablet').data('CodeMirrorInstance');
		
		he.setValue('');he.replaceSelection(tpl.html);
		ce.setValue('');ce.replaceSelection(tpl.css);
		ice.setValue('');ice.replaceSelection(tpl.itemcss);
		je.setValue('');je.replaceSelection(tpl.js);
		
	}
	
	$scope.addCssSnip = function() {
		var editor = $('#csscode').data('CodeMirrorInstance'),
			txt='';
		if($scope.csssnip == 'xs') {
			txt="@media (max-width: 767px) {\r\r}";
			
		} else if($scope.csssnip == 'sm') {
			txt="@media (min-width: 768px) and (max-width: 991px) {\r\r}";
		} else if($scope.csssnip == 'md') {
			txt="@media (min-width: 992px) {\r\r}";
		}
		
		editor.replaceSelection(txt);
	}
}

