'use strict';

/**
 * Module to use Switchery as a directive for angular.
 * @TODO implement Switchery as a service, https://github.com/abpetkov/switchery/pull/11
 */
angular.module('NgSwitchery', [])
    .directive('uiSwitch', ['$window', '$timeout', '$parse', function($window, $timeout, $parse) {

        /**
         * Initializes the HTML element as a Switchery switch.
         *
         * $timeout is in place as a workaround to work within angular-ui tabs.
         *
         * @param scope
         * @param elem
         * @param attrs
         */
        function linkSwitchery(scope, elem, attrs, ngModel) {
        	//var ngModel = ctrls[0];
      
            var options = {};
            try {
                options = $parse(attrs.uiSwitch)(scope);
            }
            catch (e) {}

            $timeout(function() {
                var init = new $window.Switchery(elem[0], options);
                /*if (attrs.ngModel) {
                    scope.$watch(attrs.ngModel, function() {
                    	console.log();
                        init.setPosition((ngModel.$modelValue == 1));
                    });
                }*/
                init.setPosition((ngModel.$modelValue == 1));
                
            }, 0);
            
            elem[0].onchange = function() {
            	var v = elem[0].checked?1:0;
            	scope.$apply(function() {
            		ngModel.$setViewValue(elem[0].checked);
            		ngModel.$modelValue = v;
            	})
            	
            };
            
            /*ngModel.$render = function() {
            	elem[0].checked = (ngModel.$viewValue == 1)?true:false;//(ngModel.$viewValue || '');
            };*/
            
            //(ngModel.$viewValue)?true:false;//(ngModel.$viewValue || '');
            //console.log(elem[0].checked);
            
        }
        return {
            restrict: 'EA',
            require: '?ngModel',
            link: linkSwitchery
        }
    }]);