$(document).ready(function() {
	vex.defaultOptions.className = 'vex-theme-os';
	
	$('.jstips').tooltip({placement:'bottom'});
	
});

var app = angular.module('myApp', ['ui.tree']);

function wholepage($scope) {
	$scope.list = getList();
	$scope.add_title = '';
	$scope.pageid = getId();
	$scope.selected = 0;
	$scope.elparams = {};
	$scope.type = 'row';
	$scope.sorted = false;
	
	$scope.showType = function(type) {
		if(type != 'carousel') {
			return true;
		} else {
			return false;
		}
	}
	
	$scope.editScript = function(type) {
		var returnUrl = getReturn();
		var link = 'index.php?option=com_onepage&view=script&type='+type+'&id='+$scope.pageid+'&returnUrl='+returnUrl;
		window.open(link,'_blank');
	}
	
	$scope.enableSort = function() {
		$scope.sorted = ($scope.sorted==false)?true:false;
	}
	
	$scope.add = function(idx) {
		$('#addForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'task':'prototype.add',id:$scope.pageid},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.list.push(res.entry);
					});
				}

				NProgress.done();
			}
		});
	}
	
	$scope.removeI = function(idx) {
		console.log('test');
		var obj = $scope.list[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
		$('#adminForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'task':'prototype.removeItem',id:obj.id},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.list.splice(idx,1);
					});
				}

				NProgress.done();
			}
		});
				}
				/**/
			}
		});
	}
	
	
	
	$scope.edit = function(idx) {
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&view=prototype&layout=edit&id="+obj.id;
	}
	
	$scope.openConfig = function(idx) {
		var obj = $scope.list[idx];
		$scope.selected = idx;
		$scope.elparams = obj.params;
		$scope.elparams.id = obj.id;
		$scope.elparams.title = obj.title;
		$scope.elparams.type = obj.type;
		$scope.elparams.showtitle = obj.showtitle;
		$('#elConfigModal').modal('show');
	}
	
	$scope.saveConfig = function() {
		$('#elConfigForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					var obj = $scope.list[$scope.selected];
					obj.params = $scope.elparams;
					obj.title = res.entryTitle;
					$scope.list[$scope.selected] = obj;
					$scope.$apply();
				}
				$('#elConfigForm .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.selectType = function(idx) {
		var obj = $scope.list[idx];
		delete obj.$$hashKey;
		$('#rowGroupForm').loadJSON(obj);
		$('#rowGroupModal').data('index',idx);
		$('#rowGroupModal').modal('show');
	}
	
	$scope.publishEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'prototype.publish', id:obj.id },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});	
	}
	
	$scope.changeType = function() {
		//console.log($scope.list[idx]);
		$('#rowGroupForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					var idx = $('#rowGroupModal').data('index');
					$scope.list[idx].type = res.type;
					$scope.$apply();
					$('#rowGroupModal').modal('hide');
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});	
	}
	
	$scope.changeItemType = function() {
		//console.log($scope.list[idx]);
		$('#itemGroupForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					var idx = $('#itemGroupModal').data('index');
					var id = $('#itemGroupModal').data('id');
					var scope = angular.element('#'+id).scope();
					
					scope.rowUnits[idx].type = res.type;
					scope.$apply();
					$('#itemGroupModal').modal('hide');
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});	
	}
	
	$scope.treeOptions = {
	    accept: function(sourceNodeScope, destNodesScope, destIndex) {
	      return true;
	    },
	    dropped: function(event) {
	    	var a = [];
	    	jQuery.each($scope.list,function(idx,item) {
	    		a.push(item.id);
	    	});
	    	$('#adminForm').ajaxSubmit({
				dataType:  'json',
				type: 'POST',
				url: 'index.php',
				data: {ids: a, task: 'prototype.sort'},
				beforeSubmit: function()	{
					//$('#changeformModal').block({ message: "Processing..." });
					//$.blockUI({ message: "Processing..." });
					NProgress.start();
				},
				success: function(res)	{
					if(res.success) { }
					NProgress.done();
				}
			});
	    }
	};
}

function row($scope) {
	$scope.rowUnits = [];//getList();
	$scope.add_title = '';
	$scope.rowId = 0;
	$scope.selected = 0;
	$scope.elparams = {};
	
	$scope.add = function(idx) {
		var title = ($scope.rowUnits.length+1);
		title = '#'+title;
		$('#adminForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'task':'prototype.addChild',id:$scope.rowId,'title':title},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.rowUnits.push(res.entry);
					});
				}

				NProgress.done();
			}
		});
	}
	
	$scope.changeGrid = function(idx) {
		var obj = $scope.rowUnits[idx];
		$('#adminForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'task':'prototype.changeGrid',id: obj.id,'md':obj.md},
			type: 'POST',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}

				NProgress.done();
			}
		});
	}
	
	$scope.changeOrder = function(idx,change) {
		var obj = $scope.rowUnits[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'prototype.changeOrder', id: obj.id, change: change},
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.rowUnits = res.list;
					$scope.$apply();
				}
				
				NProgress.done();
			}
		});
	}
	
	

	$scope.edit = function(idx) {
		var obj = $scope.rowUnits[idx];
		window.location.href="index.php?option=com_onepage&view=prototype&layout=edit&id="+obj.id;
	}
	
	$scope.addChild = function(idx) {
		var obj = {'unit':4,'children':[],'type':'item'};
		$scope.rowUnits.push(obj);

	}
	
	$scope.remove = function(idx) {
		var obj = $scope.rowUnits[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
		$('#adminForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'task':'prototype.removeItem',id:obj.id},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.rowUnits.splice(idx,1);
					});
				}

				NProgress.done();
			}
		});
				}
				/**/
			}
		});
	}
	
	$scope.selectType = function(idx) {
		var obj = $scope.rowUnits[idx];
		
		delete obj.$$hashKey;
		$('#itemGroupForm').loadJSON(obj);
		$('#itemGroupModal').data('id','unit_'+obj.id);
		$('#itemGroupModal').data('index',idx);
		$('#itemGroupModal').modal('show');
	}
	
	$scope.publishEntry = function(idx) {
		var obj = $scope.rowUnits[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'prototype.publish', id:obj.id },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.rowUnits[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});	
	}
	
	$scope.showNav = function(item) {
		return !(item.type == 'item');
	}
}

function group($scope) {
	$scope.groupUnits = [];//getList();
	$scope.add_title = '';
	$scope.pageid = 0;
	$scope.selected = 0;
	$scope.elparams = {};
	
	$scope.addChild = function(idx) {
		var obj = {unit:12};
		$scope.groupUnits.push(obj);

	}
	
	$scope.remove = function(idx) {
		var obj = $scope.groupUnits[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
		$('#adminForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'task':'prototype.removeItem',id:obj.id},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.groupUnits.splice(idx,1);
					});
				}

				NProgress.done();
			}
		});
		
				}
				/**/
			}
		});
	}
	
	$scope.showNav = function(item) {
		return !(item.type == 'item');
	}
	
	$scope.publishEntry = function(idx) {
		var obj = $scope.groupUnits[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'prototype.publish', id:obj.id },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.groupUnits[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});	
	}
}

