$(document).ready(function() {
	//$.pnotify.defaults.history = false;
	//$.pnotify.defaults.delay = 3000;
	
	vex.defaultOptions.className = 'vex-theme-os';
	
	// Confirm Modal
	$('#confirmModal').on('show', function() {
		// do something…
	}).on('hidden', function() {
		// do something…
		$("#confirmYes").off('click');
	});
	
	
	
	// Page
	$("#move-page").on('change',function() {
		var scope = $(this).scope();
		scope.pageid = $(this).val();
		scope.$apply();
		
	});
	
	$("#copy-page").on('change',function() {
		var scope = $(this).scope();
		scope.copypageid = $(this).val();
		scope.$apply();
		
	});
	
	$('.pageremove').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var cid = new Array();
		cid.push(data);
		
		$('#listForm input:checkbox').attr('checked',false);
		$('#confirmModal').modal('show');
		$('#confirmYes').click(function()	{
			removePage(cid);
		});
	});
	
	$('.pagepublish').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var p = me.data('publish');
		p = (p == 1)?0:1;
		var cid = new Array();
		cid.push(data);
		
		$('#listForm input:checkbox').attr('checked',false);
		publish(cid,p);
	});
	
	
	// Menu Item
	$('.itemremove').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var cid = new Array();
		cid.push(data);
		
		$('#adminForm input:checkbox').attr('checked',false);
		$('#confirmModal').modal('show');
		$('#confirmYes').click(function()	{
			removeElement(cid);
		});
	});
	
	$('.itempublish').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var p = me.data('publish');
		p = (p == 1)?0:1;
		var cid = new Array();
		cid.push(data);
		
		$('#adminForm input:checkbox').attr('checked',false);
		
		publish(cid,p);
		//return false;
	});
	
	$('.itemchange').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
	
		
		
		$('#changeformid').val(data);
		
		$('#adminForm input:checkbox').attr('checked',false);
		$('#changeformModal').modal('show');

	});
	
	
	
	
	/*$( ".list-sortable" ).sortable({ 
		opacity: 0.6, cursor: 'move',
		placeholder: "list-sortable-placeholder",
		handle: ".ulhandle",
		update: function() {
			var form = $(this).parents('form');
			var order = $(this).sortable("toArray");
			//console.log(order);
			$.blockUI({ message: "Processing..." });
			form.ajaxSubmit({
				data: {'el_ids': order, task: 'menus.resort'},
				type: 'POST',
			    dataType:  'json',
			    success:    function(res) {
			    	$.unblockUI();
			    	console.log(res);
			    }
			});
		}
	});
		
    $( ".list-sortable" ).disableSelection();*/
    
    // collapse
    /*$('.collapse').on('hidden', function () {

    	var me = $(this);
    	if(me.hasClass('in')) {  return;}
    	var li = me.parent();
    	var arr = li.children('.opinline').find('.li-close');
    	var btn = $(arr[0]);
    	
    	//var target = btn.data('target');
    	btn.children().removeClass('icon-minus').addClass('icon-plus');

	});
    
    $('.collapse').on('shown', function () {
    	var me = $(this);
    	if(!me.hasClass('in')) { return;}
    	var li = me.parent();
    	var arr = li.children('.opinline').find('.li-close');
    	var btn = $(arr[0]);
    	//var target = btn.data('target');
    	btn.children().removeClass('icon-plus').addClass('icon-minus');
  	})*/
    
    // Tooltip
    $('.btntip').tooltip({});
    

    
   /* $('.nestable').nestable({ 
    	rootClass: 'nestable',
    	listNodeName: 'ul',
    	maxDepth: 10
    	//expandBtnHTML: '<div class="span1"><button data-action="expand">Expand</button></div>',
    	//collapseBtnHTML: '<div class="span1"><button data-action="collapse">Collapse</button></div>'
    });*/
});

/* --------------------------------------------------- Page -------- */
function addPage() {
	$('#formModal').block({ message: "Processing..." });
	$('#pageform-form').submit();
}

function copyPage() {
	if ( $('#listForm input:checkbox:checked').length==0){ 
		alert('Please first make a selection from the list');
	} else {
		$('#listForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data: { task: 'pages.copy' },
			beforeSubmit: function()	{
				$.blockUI({ message: "Processing..." });
			},
			success: function(res)	{
				$.unblockUI();
				if(res.success)	{
					window.location.reload();
				}	else	{
					
				}
				
				$.pnotify({
			        title: res.title,
			        text: res.content,
			        type: res.success?'success':'error'
			    });
			}
		});
	};
}

function exportPage() {
	if ( $('#listForm input:checkbox:checked').length==0 ){ 
		alert('Please first make a selection from the list');
	} else {
		
		var title = $('#export-title').val();
		var data = $('#listForm input:checkbox').fieldSerialize() ;
		window.open(
			'index.php?option=com_onepage&task=pages.exportPage&'+data+'&title='+title
			,'win1'
			,'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'
		);
		$('#exportModal').modal('hide');
	};
}

function importPage() {
	$('#import-form').ajaxSubmit({
		dataType:  'json',
		type: 'POST',
		//data: { task: 'pages.exportPage',title: $('#export-title').val() },
		beforeSubmit: function()	{
			$('#importModal').block({ message: "Processing..." });
		},
		success: function(res)	{
			$('#importModal').unblock();
			if(res.success)	{
				window.location.reload();
			}	else	{
				$.pnotify({
			        title: res.title,
			        text: res.content,
			        type: res.success?'success':'error'
			    });
			}
			
		}
	});
}

function homepage() {
	$('#home-form').ajaxSubmit({
		dataType:  'json',
		type: 'POST',
		data: { task: 'pages.homepage' },
		beforeSubmit: function()	{
			$('#homeModal').block({ message: "Processing..." });
		},
		success: function(res)	{
			$('#homeModal').unblock();
			$.pnotify({
		        title: res.title,
		        text: res.content,
		        type: res.success?'success':'error'
		    });
			
			$('#homeModal').modal('hide');
		}
	});
}

function removePage(cid) {
	$('#listForm').ajaxSubmit({
		dataType:  'json',
		type: 'POST',
		data: { task: 'pages.remove' , 'cid[]':cid},
		beforeSubmit: function()	{
			$('#confirmModal').block({ message: "Processing..." });
		},
		success: function(res)	{
			$('#confirmModal').unblock();
			if(res.success)	{
				$('#confirmModal').modal('hide');
				if(cid.length < 1 ) {
					cid = $('#listForm input:checkbox').fieldValue();
				}
				
				jQuery.each(cid,function(i,item) {
					$('#listForm').find('#li_'+item).remove();
				});
				
				if($('#listForm li.active').length < 1) {
					window.location.href="index.php?option=com_onepage&view=pages";
				}
				
			}	else	{
				
			}
			
			$.pnotify({
		        title: res.title,
		        text: res.content,
		        type: res.success?'success':'error'
		    });
		}
	});
}

function publishPage(cid,p) {
	$('#listForm').ajaxSubmit({
		dataType:  'json',
		type: 'POST',
		data: { task: 'pages.publish' , 'cid[]':cid, publish: p},
		beforeSubmit: function()	{
			$.blockUI({ message: "Processing..." });
		},
		success: function(res)	{
			$.unblockUI();
			if(res.success)	{
				var link = '';
				window.location.reload();
			}	else	{
				
			}
		}
	});
}

/* --------------------------------------------------- Menu -------- */
function addElement() {
	$('#formModal').block({ message: "Processing..." });
	$('#form-form').submit();
}

function copyElement() {
	//$('#copyModal').block({ message: "Processing..." });
	//$('#copy-form').submit();
	
	if (document.adminForm.boxchecked.value==0){
		alert('Please first make a selection from the list');
	} else {
		//console.log($('#copy-page').val().replace(/l/g,''));
		$('#adminForm').ajaxSubmit({
			data: {task: 'menus.copy', target_id: $('#copy-target').val(), page_id: $('#copy-page').val().replace(/l/g,'') },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				$('#copyModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
			},
			success: function(res)	{
				$('#copyModal').unblock();
				//$.unblockUI();
				$.pnotify({
			        title: res.title,
			        text: res.content,
			        type: res.success?'success':'error'
			    });
				
				window.location.reload();
				//$('#groupformModal').modal('hide');
			}
		});
	
	}
}

function move() {
	if (document.adminForm.boxchecked.value==0){ 
		alert('Please first make a selection from the list');
	} else {
		$('#adminForm').ajaxSubmit({
			data: {
				task: 'menus.move', 
				target_id: $('#move-target').val(),
				page_id:$('#move-page').val().replace(/l/g,'')
			},
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				$('#moveformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
			},
			success: function(res)	{
				$('#moveformModal').unblock();
				//$.unblockUI();
				$.pnotify({
			        title: res.title,
			        text: res.content,
			        type: res.success?'success':'error'
			    });
				
				window.location.reload();
				//$('#groupformModal').modal('hide');
			}
		});
	
	}
}

function removeElement(cid) {
	$('#adminForm').ajaxSubmit({
		dataType:  'json',
		type: 'POST',
		data: { task: 'menus.remove' , 'cid[]':cid},
		beforeSubmit: function()	{
			$('#confirmModal').block({ message: "Processing..." });
		},
		success: function(res)	{
			$('#confirmModal').unblock();
			if(res.success)	{
				$('#confirmModal').modal('hide');
				if(cid.length < 1 ) {
					cid = $('#adminForm input:checkbox').fieldValue();
				}
				
				jQuery.each(cid,function(i,item) {
					$('#li_'+item).remove();
				});
				
			}	else	{
				
			}
			
			$.pnotify({
		        title: res.title,
		        text: res.content,
		        type: res.success?'success':'error'
		    });
		}
	});
}

/* --------------------------------------------------- Common -------- */
function publish(cid,p) {
	$('#adminForm').ajaxSubmit({
		dataType:  'json',
		type: 'POST',
		data: { task: 'pages.publish' , 'cid[]':cid, publish: p},
		beforeSubmit: function()	{
			$.blockUI({ message: "Processing..." });
		},
		success: function(res)	{
			$.unblockUI();
			if(res.success)	{
				var link = '';
				window.location.reload();
			}	else	{
				
			}
			
		}
	});
}

function change() {
	var cid = new Array();
	cid.push($('#changeformid').val());

	$('#adminForm').ajaxSubmit({
		data: {task: 'menus.change', type: $('#change-type').val(), 'cid[]':cid },
		dataType:  'json',
		type: 'POST',
		beforeSubmit: function()	{
			$('#changeformModal').block({ message: "Processing..." });
			//$.blockUI({ message: "Processing..." });
		},
		success: function(res)	{
			$('#changeformModal').unblock();
			//$.unblockUI();
			$.pnotify({
		        title: res.title,
		        text: res.content,
		        type: res.success?'success':'error'
		    });
			
			window.location.reload();
			//$('#groupformModal').modal('hide');
		}
	});
}

function groupEl() {
	if (document.adminForm.boxchecked.value==0){ 
		alert('Please first make a selection from the list');
	} else {
		$('#group-form').ajaxSubmit({
			data: {task: 'menus.group', 'cid[]': $('#adminForm :checkbox').fieldValue() },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				$('#groupformModal').block({ message: "Processing..." });
			},
			success: function(res)	{
				$('#groupformModal').unblock();
				$.pnotify({
			        title: res.title,
			        text: res.content,
			        type: res.success?'success':'error'
			    });
				
				window.location.reload();
				//$('#groupformModal').modal('hide');
			}
		});
	}
}

function ungroupEl() {
	if (document.adminForm.boxchecked.value==0){ 
		alert('Please first make a selection from the list');
	} else {
		$('#adminForm').ajaxSubmit({
			data: {task: 'menus.ungroup', target_id: 0 },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#ungroupformModal').block({ message: "Processing..." });
				$.blockUI({ message: "Processing..." });
			},
			success: function(res)	{
				//$('#ungroupformModal').unblock();
				$.unblockUI();
				$.pnotify({
			        title: res.title,
			        text: res.content,
			        type: res.success?'success':'error'
			    });
				
				window.location.reload();
				//$('#groupformModal').modal('hide');
			}
		});
	}
}

Joomla.submitbutton = function(task) {
	var func = null; var p = 0;
	if((task == 'item.publish')) {
		p = 1;
	} 
	
	if(task == 'item.delete') {
		if (document.adminForm.boxchecked.value==0){ 
			alert('Please first make a selection from the list');
		} else {
			$('#confirmModal').modal('show');
			$('#confirmYes').click(function()	{
				removeElement([]);
			});
		};
	} else {
		if (document.adminForm.boxchecked.value==0 && (task=='item.publish' || task=='item.unpublish' ) ){ 
			alert('Please first make a selection from the list');
		} else {
			publish([],p);
		}
	}
}

var app = angular.module('myApp', []);

function wholepage($scope) {
	$scope.list = getList();
	//$scope.list = getPageChildren();
	//console.log($scope.list.length);
	$scope.addSubmit = function() {
		if($scope.add_title.length < 1) {
			vex.dialog.alert('Title is required!');
			return;
		}
		$('#addform').ajaxSubmit({
			data: {  },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list = res.list;
					$scope.add_title='';
					$scope.$apply();
				}
				NProgress.done();
			}
		});
	}
	
	$scope.exportPage = function(idx) {
		var obj = $scope.list[idx];
		var link = 'index.php?option=com_onepage&task=pages.exportPage&id='+obj.id;
		window.open(link,'_blank');
	}
	
	$scope.importPage = function() {
		$('#importForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.list = res.list;
					});
					
					$('#importForm .fileinput').fileinput('clear');
					$('#importModal').modal('hide');
				}
				
				NProgress.done();
			}
		});
	}
	
	$scope.deleteEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
				$('#adminForm').ajaxSubmit({
					data: { task:'pages.remove', cid:[obj.id] },
					dataType:  'json',
					type: 'POST',
					beforeSubmit: function()	{
						//$('#changeformModal').block({ message: "Processing..." });
						//$.blockUI({ message: "Processing..." });
						NProgress.start();
					},
					success: function(res)	{
						if(res.success) {
							$scope.list.splice(idx,1);
							$scope.$apply();
						}
						
						NProgress.done();
					}
				});
				}
			}
		});
	}
	
	$scope.deleteAll = function() {
		//console.log($scope.list[idx]);
		//$('#confirmModal').modal('show');
		//$('#confirmYes').click(function()	{
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
					$('#adminForm').ajaxSubmit({
						data: { task:'pages.remove', 'parent_id':1 },
						dataType:  'json',
						type: 'POST',
						beforeSubmit: function()	{
							//$('#changeformModal').block({ message: "Processing..." });
							//$.blockUI({ message: "Processing..." });
							NProgress.start();
						},
						success: function(res)	{
							if(res.success) {
								$scope.list = res.list;
								$scope.$apply();
							}
							NProgress.done();
							//$('#confirmModal').modal('hide');
						}
					});
				}
				/**/
			}
		});
			
		//});
	}
	
	$scope.publishEntry = function(idx) {
		//console.log($scope.list[idx]);
		$('#adminForm input:checkbox').attr('checked',false);
		
		var obj = $scope.list[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'pages.publish', cid:[obj.id],'published':-1 },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});
	}
	
	$scope.publishAll = function(p) {
		//console.log($scope.list[idx]);
		//$('#confirmModal').modal('show');
		//$('#confirmYes').click(function()	{
			$('#adminForm').ajaxSubmit({
				data: { task:'pages.publish', 'parent_id':1,'published':p },
				dataType:  'json',
				type: 'POST',
				beforeSubmit: function()	{
					//$('#changeformModal').block({ message: "Processing..." });
					//$.blockUI({ message: "Processing..." });
					NProgress.start();
				},
				success: function(res)	{
					if(res.success) {
						$scope.list = res.list;
						$scope.$apply();
						$('#adminForm input[name="checkall-toggle"]').attr('checked',false);
					}
					NProgress.done();
					//$('#confirmModal').modal('hide');
				}
			});
		//});
	}
	
	$scope.edit = function(idx) {
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&view=prototype&id="+obj.id;
	}
	
	$scope.config = function(idx) {
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&view=prototype&layout=config&id="+obj.id;
	}
	
	$scope.ignoreVersion = function(idx) {
		$('#adminForm').ajaxSubmit({
			data: { task:'ignoreVersion' },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$('#notification').hide();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});
	}
}