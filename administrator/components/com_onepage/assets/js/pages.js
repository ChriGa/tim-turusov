$(document).ready(function() {
	//$.pnotify.defaults.history = false;
	//$.pnotify.defaults.delay = 3000;
	
	vex.defaultOptions.className = 'vex-theme-os';
	
	// Confirm Modal
	$('#confirmModal').on('show', function() {
		// do something…
	}).on('hidden', function() {
		// do something…
		$("#confirmYes").off('click');
	});
	
	
	
	// Page
	$("#move-page").on('change',function() {
		var scope = $(this).scope();
		scope.pageid = $(this).val();
		scope.$apply();
		
	});
	
	$("#copy-page").on('change',function() {
		var scope = $(this).scope();
		scope.copypageid = $(this).val();
		scope.$apply();
		
	});
	
	$('.pageremove').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var cid = new Array();
		cid.push(data);
		
		$('#listForm input:checkbox').attr('checked',false);
		$('#confirmModal').modal('show');
		$('#confirmYes').click(function()	{
			removePage(cid);
		});
	});
	
	$('.pagepublish').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var p = me.data('publish');
		p = (p == 1)?0:1;
		var cid = new Array();
		cid.push(data);
		
		$('#listForm input:checkbox').attr('checked',false);
		publish(cid,p);
	});
	
	
	// Menu Item
	$('.itemremove').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var cid = new Array();
		cid.push(data);
		
		$('#adminForm input:checkbox').attr('checked',false);
		$('#confirmModal').modal('show');
		$('#confirmYes').click(function()	{
			removeElement(cid);
		});
	});
	
	$('.itempublish').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
		var p = me.data('publish');
		p = (p == 1)?0:1;
		var cid = new Array();
		cid.push(data);
		
		$('#adminForm input:checkbox').attr('checked',false);
		
		publish(cid,p);
		//return false;
	});
	
	$('.itemchange').on('click',function()	{
		var me = $(this);
		var data = me.data('id');
	
		
		
		$('#changeformid').val(data);
		
		$('#adminForm input:checkbox').attr('checked',false);
		$('#changeformModal').modal('show');

	});
	
    // Tooltip
    $('.btntip').tooltip({});
    
    if(needUpdate()) {
    	$('#adminForm').ajaxSubmit({
    		data: { task:'updateTpls' },
    		dataType:  'json',
    		type: 'POST',
    		beforeSubmit: function()	{
    			NProgress.start();
    		},
    		success: function(res)	{
    			NProgress.done();
    		}
    	});
    }
    
   /* $('.nestable').nestable({ 
    	rootClass: 'nestable',
    	listNodeName: 'ul',
    	maxDepth: 10
    	//expandBtnHTML: '<div class="span1"><button data-action="expand">Expand</button></div>',
    	//collapseBtnHTML: '<div class="span1"><button data-action="collapse">Collapse</button></div>'
    });*/
});


var app = angular.module('myApp', []);

function wholepage($scope) {
	$scope.list = getList();
	//$scope.list = getPageChildren();
	//console.log($scope.list.length);
	$scope.addSubmit = function() {
		if($scope.add_title.length < 1) {
			vex.dialog.alert('Title is required!');
			return;
		}
		$('#addform').ajaxSubmit({
			data: {  },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list = res.list;
					$scope.add_title='';
					$scope.$apply();
				}
				NProgress.done();
			}
		});
	}
	
	$scope.refreshTpls = function() {
		$('#adminForm').ajaxSubmit({
    		data: { task:'updateTpls' },
    		dataType:  'json',
    		type: 'POST',
    		beforeSubmit: function()	{
    			NProgress.start();
    		},
    		success: function(res)	{
    			NProgress.done();
    		}
    	});
	}
	
	$scope.exportPage = function(idx) {
		var obj = $scope.list[idx];
		var link = 'index.php?option=com_onepage&task=pages.exportPage&id='+obj.id;
		window.open(link,'_blank');
	}
	
	$scope.duplicate = function(idx) {
		var obj = $scope.list[idx];
		$('#adminForm').ajaxSubmit({
    		data: { 'task':'pages.duplicate', 'id':obj.id },
    		dataType:  'json',
    		type: 'POST',
    		beforeSubmit: function()	{
    			NProgress.start();
    		},
    		success: function(res)	{
    			NProgress.done();
    			
    			if(res.success) {
					$scope.$apply(function() {
						$scope.list = res.list;
						$scope.add_title='';
					});
				}
    		}
    	});
	}
	
	$scope.importPage = function() {
		$('#importForm').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.list = res.list;
					});
					
					$('#importForm .fileinput').fileinput('clear');
					$('#importModal').modal('hide');
				}
				
				NProgress.done();
			}
		});
	}
	
	$scope.deleteEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
				$('#adminForm').ajaxSubmit({
					data: { task:'pages.remove', cid:[obj.id] },
					dataType:  'json',
					type: 'POST',
					beforeSubmit: function()	{
						//$('#changeformModal').block({ message: "Processing..." });
						//$.blockUI({ message: "Processing..." });
						NProgress.start();
					},
					success: function(res)	{
						if(res.success) {
							$scope.list.splice(idx,1);
							$scope.$apply();
						}
						
						NProgress.done();
					}
				});
				}
			}
		});
	}
	
	$scope.deleteAll = function() {
		//console.log($scope.list[idx]);
		//$('#confirmModal').modal('show');
		//$('#confirmYes').click(function()	{
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
					$('#adminForm').ajaxSubmit({
						data: { task:'pages.remove', 'parent_id':1 },
						dataType:  'json',
						type: 'POST',
						beforeSubmit: function()	{
							//$('#changeformModal').block({ message: "Processing..." });
							//$.blockUI({ message: "Processing..." });
							NProgress.start();
						},
						success: function(res)	{
							if(res.success) {
								$scope.list = res.list;
								$scope.$apply();
							}
							NProgress.done();
							//$('#confirmModal').modal('hide');
						}
					});
				}
				/**/
			}
		});
			
		//});
	}
	
	$scope.publishEntry = function(idx) {
		//console.log($scope.list[idx]);
		$('#adminForm input:checkbox').attr('checked',false);
		
		var obj = $scope.list[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'pages.publish', cid:[obj.id],'published':-1 },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});
	}
	
	$scope.publishAll = function(p) {
		//console.log($scope.list[idx]);
		//$('#confirmModal').modal('show');
		//$('#confirmYes').click(function()	{
			$('#adminForm').ajaxSubmit({
				data: { task:'pages.publish', 'parent_id':1,'published':p },
				dataType:  'json',
				type: 'POST',
				beforeSubmit: function()	{
					//$('#changeformModal').block({ message: "Processing..." });
					//$.blockUI({ message: "Processing..." });
					NProgress.start();
				},
				success: function(res)	{
					if(res.success) {
						$scope.list = res.list;
						$scope.$apply();
						$('#adminForm input[name="checkall-toggle"]').attr('checked',false);
					}
					NProgress.done();
					//$('#confirmModal').modal('hide');
				}
			});
		//});
	}
	
	$scope.edit = function(idx) {
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&task=prototype.edit&id="+obj.id;
	}
	
	$scope.config = function(idx) {
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&view=prototype&layout=config&m=1&id="+obj.id;
	}
	
	$scope.ignoreVersion = function(idx) {
		$('#adminForm').ajaxSubmit({
			data: { task:'ignoreVersion' },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$('#notification').hide();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});
	}
}