$(document).ready(function() {
	/*$('.summernote').summernote({
	  height: 300,   //set editable area's height
	  focus: true,    //set focus editable area after Initialize summernote
	  codemirror: { // codemirror options
		  theme: 'monokai',
		  mode:  "text/html"
	  }
	});*/
	
	var editor = CodeMirror.fromTextArea(document.getElementById("codemirror"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/html'
	});
	
	$('#codemirror').data('CodeMirrorInstance', editor);
	
	var editor = CodeMirror.fromTextArea(document.getElementById("csscode"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/html'
	});
	
	$('#csscode').data('CodeMirrorInstance', editor);
	
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		e.target; // activated tab
		var id = $(e.target).attr('href');
		var activatedEditor = $(id).children('textarea').data('CodeMirrorInstance');
		activatedEditor.refresh();
	})
});

var app = angular.module('myApp', []);

function wholepage($scope) {
	$scope.list = getList();
	$scope.add_title = '';
	$scope.save = function() {
		var editor = $('#codemirror').data('CodeMirrorInstance');
		editor.save();
		$('#csscode').data('CodeMirrorInstance').save();
		
		$('#addForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				//$('#jscss .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.deleteEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
				$('#adminForm').ajaxSubmit({
					data: { task:'page.remove', id:obj.id },
					dataType:  'json',
					type: 'POST',
					beforeSubmit: function()	{
						//$('#changeformModal').block({ message: "Processing..." });
						//$.blockUI({ message: "Processing..." });
						NProgress.start();
					},
					success: function(res)	{
						if(res.success) {
							$scope.list.splice(idx,1);
							$scope.$apply();
						}
						
						NProgress.done();
					}
				});
				}
			}
		});
	}
	
	$scope.edit = function(idx) {
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&view=page&layout=edit&id="+obj.id;
	}
}

function sub($scope) {
	$scope.list = [{}];
	$scope.add = function() {
		$scope.list.push({});
	}
}