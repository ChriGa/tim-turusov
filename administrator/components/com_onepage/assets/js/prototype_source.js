$(document).ready(function() {
	
	var editor = CodeMirror.fromTextArea(document.getElementById("codemirror"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/html'
	});
	
	$('#codemirror').data('CodeMirrorInstance', editor);
});


var app = angular.module('myApp', []);

function wholepage($scope) {
	
}
