$(document).ready(function() {
	//vex.defaultOptions.className = 'vex-theme-os';
	var type = getType();
	$('.jstips').tooltip({placement:'bottom'});
	
	var editor = CodeMirror.fromTextArea(document.getElementById("csscode"), {
	    lineNumbers: true,
	    lineWrapping: true,
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'monokai',
	    mode: 'text/'+type
	});
	
	$('#csscode').data('CodeMirrorInstance', editor);
	//editor.replaceSelection('Wa haha , I got it!');
});

var app = angular.module('myApp', []);

function wholepage($scope) {
	$scope.csssnip = '';
	$scope.jssnip = '';
	$scope.type = getType();
	$scope.save = function() {
		var editor = $('#csscode').data('CodeMirrorInstance');
		editor.save();
		$('#csscode').data('CodeMirrorInstance').save();
		
		$('#addForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				//$('#jscss .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.addCssSnip = function() {
		var editor = $('#csscode').data('CodeMirrorInstance'),
			txt='';
		if($scope.csssnip == 'xs') {
			txt="@media (max-width: 767px) {\r\r}";
			
		} else if($scope.csssnip == 'sm') {
			txt="@media (min-width: 768px) and (max-width: 991px) {\r\r}";
		} else if($scope.csssnip == 'md') {
			txt="@media (min-width: 992px)  {\r\r}";
		}
		
		editor.replaceSelection(txt);
	}
	
	$scope.addJsSnip = function() {
		var editor = $('#csscode').data('CodeMirrorInstance'),
			txt='';
		if($scope.jssnip == 'or') {
			txt="$(document).ready(function() {\r\r});";
			
		}
		
		editor.replaceSelection(txt);
	}
	
}