$(document).ready(function() {
	
	//vex.defaultOptions.className = 'vex-theme-os';
	$.fn.editable.defaults.mode = 'inline';
	
	NProgress.configure({ showSpinner: true });
	$(window).load();
	
	var getPageId = function() {return 1;}
	
	$('.xeditable').editable({
        url: 'index.php?option=com_onepage',
        ajaxOptions: {
            type: 'post',
            dataType: 'json'
        },
        params: {task:'saveConfig' }
    });
	
	$('#iconForm').ajaxForm({
		dataType:  'json',
		type: 'POST',
		beforeSubmit: function()	{
			//$('#changeformModal').block({ message: "Processing..." });
			//$.blockUI({ message: "Processing..." });
			NProgress.start();
		},
		success: function(res)	{
			if(res.success) {
				
			}
			$('#iconForm .fileinput').fileinput('clear');
			NProgress.done();
		}
	});
});

var app = angular.module('myApp', []);

function wholepage($scope) {
	
}