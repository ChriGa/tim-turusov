$(document).ready(function() {
	vex.defaultOptions.className = 'vex-theme-os';
	
	$('.jstips').tooltip({placement:'bottom'});
	
});

var app = angular.module('myApp', ['summernote','ui.nestedSortable']);

function wholepage($scope) {
	$scope.list = getList();
	$scope.add_title = '';
	$scope.pageid = 0;
	$scope.selected = 0;
	$scope.elparams = {};
	
	$scope.changePage = function() {
		window.location.href="index.php?option=com_onepage&view=page&id="+$scope.pageid;
	}
	
	$scope.add = function() {
		if($scope.add_title.length < 1) {
			vex.dialog.alert('Title is required!');
			return;
		}
		$('#addForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list.push(res.entry);
					$scope.add_title='';
					$scope.$apply();
				}
				$('#jscss .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.publishEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'page.publish', id:obj.id,'published':-1 },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});	
	}
	
	$scope.deleteEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
				$('#adminForm').ajaxSubmit({
					data: { task:'page.remove', id:obj.id },
					dataType:  'json',
					type: 'POST',
					beforeSubmit: function()	{
						//$('#changeformModal').block({ message: "Processing..." });
						//$.blockUI({ message: "Processing..." });
						NProgress.start();
					},
					success: function(res)	{
						if(res.success) {
							$scope.list.splice(idx,1);
							$scope.$apply();
						}
						
						NProgress.done();
					}
				});
				}
			}
		});
	}
	
	$scope.edit = function(idx) {
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&view=page&layout=edit&id="+obj.id;
	}
	
	$scope.openConfig = function(idx) {
		var obj = $scope.list[idx];
		$scope.selected = idx;
		$scope.elparams = obj.params;
		$scope.elparams.id = obj.id;
		$scope.elparams.title = obj.title;
		$('#elConfigModal').modal('show');
	}
	
	$scope.saveConfig = function() {
		$('#elConfigForm').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					var obj = $scope.list[$scope.selected];
					obj.params = $scope.elparams;
					$scope.list[$scope.selected] = obj;
					$scope.$apply();
				}
				$('#elConfigForm .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.editCode = function(id) {
		window.location.href="index.php?option=com_onepage&view=page&layout=edit&id="+id;
	}
	
	$scope.sortableOptions = {
	    accept: function(modelData, sourceItemScope, targetScope, destIndex) {
	      return true;
	    },
	    itemRemoved: function(scope, modelData, sourceIndex) {

	    },
	    itemAdded: function(scope, modelData, destIndex) {

	    },
	    itemMoved: function(sourceScope, modelData, sourceIndex, destScope, destIndex) {

	    },
	    orderChanged: function(scope, modelData, sourceIndex, destIndex) {
	    	var a = [];
	    	jQuery.each(scope.list,function(idx,item) {
	    		a.push(item.id);
	    	});
	    	$('#adminForm').ajaxSubmit({
				dataType:  'json',
				type: 'POST',
				url: 'index.php',
				data: {ids: a, task: 'page.sort'},
				beforeSubmit: function()	{
					//$('#changeformModal').block({ message: "Processing..." });
					//$.blockUI({ message: "Processing..." });
					NProgress.start();
				},
				success: function(res)	{
					if(res.success) { }
					NProgress.done();
				}
			});
	    },
	    itemClicked: function(modelData) {

	    },
	    start: function(scope, modelData, elements) {

	    },
	    move: function(scope, modelData, elements) {

	    },
	    stop: function(scope, modelData, elements) {

	    }
	};
}

function editor($scope) {
	$scope.item = {};
	$scope.sncfg = {
			height: 150,
			focus: true,
			toolbar: [
			    ['style', ['style']],
			    ['style', ['bold', 'italic', 'underline', 'clear']],
			    ['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['height', ['height']],
			    ['insert', ['link', 'picture', 'video']],
			    ['table', ['table']],
			    ['view', ['fullscreen']],
			    ['help', ['help']]
			]
		};
	$scope.saveChild = function(item) {
		var form = $('#form-'+item.id);
		form.ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data: {content: $scope.item.content},
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				NProgress.done();
			}
		});
	}
	
	$scope.imageUpload = function(files, editor, welEditable) {
		console.log('image upload:', files, editor, welEditable);
	}
}

function image($scope) {
	$scope.item = {};
	$scope.saveChild = function(item) {
		var form = $('#form-'+item.id);
		form.ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				$('#form-'+item.id+' .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
}

function carousel($scope) {
	$scope.item = {};
	$scope.item.children = [];
	$scope.sncfg = {
		height: 150,
		focus: true,
		toolbar: [
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']]
		]
	};
	
	$scope.saveChild = function(item) {
		var form = $('#form-'+item.id);
		form.ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				$('#form-'+item.id+' .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.addChild = function(item) {
		var form = $('#form-'+item.id);
		form.ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data: {task: 'page.addChild'},
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.item.children.push(res.entry);
					$scope.$apply();
				}
				$('#form-'+item.id+' .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
}

function thumbnails($scope) {
	$scope.item = {};
	$scope.item.children = [];
	$scope.sncfg = {
		height: 150,
		focus: true,
		toolbar: [
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']]
		]
	};
	
	$scope.saveChild = function(item) {
		var form = $('#form-'+item.id);
		form.ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				$('#form-'+item.id+' .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.addChild = function(item) {
		var form = $('#form-'+item.id);
		form.ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data: {task: 'page.addChild'},
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.item.children.push(res.entry);
					$scope.$apply();
				}
				$('#form-'+item.id+' .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
}


function elitem($scope) {
	$scope.elitem = {};
	$scope.sncfg = {
		height: 150,
		focus: true,
		toolbar: [
		    ['style', ['style']],
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']]
		]
	};
	
	$scope.saveChild = function(item) {
		var form = $('#form-'+item.id);
		form.ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			data: {caption: $scope.elitem.params.caption},
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					
				}
				$('#form-'+item.id+' .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
	
	$scope.publishEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.item.children[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'page.publish', id:obj.id,'published':-1 },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.item.children[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
				//$('#confirmModal').modal('hide');
			}
		});	
	}
	
	$scope.removeChild = function(idx) {
		var obj = $scope.item.children[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
				$('#adminForm').ajaxSubmit({
					data: { task:'page.removeChild', id: obj.id },
					dataType:  'json',
					type: 'POST',
					beforeSubmit: function()	{
						//$('#changeformModal').block({ message: "Processing..." });
						//$.blockUI({ message: "Processing..." });
						NProgress.start();
					},
					success: function(res)	{
						if(res.success) {
							$scope.item.children.splice(idx,1);
							$scope.$apply();
						}
						
						NProgress.done();
					}
				});
				}
			}
		});
	}
	
	$scope.changeOrder = function(id,change) {
		$('#adminForm').ajaxSubmit({
			data: { task:'page.changeOrder', id: id, change: change},
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.item.children = res.list;
					$scope.$apply();
				}
				
				NProgress.done();
			}
		});
	}
}