$(document).ready(function() {
	//$.pnotify.defaults.history = false;
	//$.pnotify.defaults.delay = 3000;
	// Confirm Modal
	
	// Alert Box
	vex.defaultOptions.className = 'vex-theme-os';
	// Progress
	NProgress.configure({ showSpinner: true });
	
	$('#confirmModal').on('show.bs.modal', function() {
		// do something…
	}).on('hide.bs.modal', function() {
		// do something…
		$("#confirmYes").off('click');
	});
    
	//PageTransitions().init();
	$('.datepicker').pickadate({format: 'yyyy-mm-dd'});
	
	$('.htips').tooltip();$('.jstips').tooltip();
	// init angular
	var scope = $('#adminForm').scope();
	//scope.pageChanged();
});

var app = angular.module('myApp', ['ui.bootstrap']);

app.controller('wholepage', ['$scope', function($scope) {
	$scope.list = [];
	$scope.totalItems = 0;
	$scope.currentPage = 1;
	$scope.itemsPerPage = 10;
	$scope.maxSize=10;
	$scope.search='';
	
	$scope.pageChanged = function(page) {
		data = { task:'templates.getList',limit:$scope.itemsPerPage };
		if(page > 0)	{
			data.limitstart = (page-1) * $scope.itemsPerPage;
		}
		data.filter_search = $scope.search;
		$('#adminForm').ajaxSubmit({
			url: 'index.php?option=com_onepage',
			data: data,
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				$scope.list = res.list;
				$scope.totalItems = res.total;
				$scope.currentPage = res.cp;
				$scope.$apply();
				
				//console.log();
				NProgress.done();
			}
		});
	};
	
	$scope.clearSearch = function() {
		if($scope.search.length < 1) return;
		$scope.search='';
		$scope.pageChanged(0);
	}
	
	$scope.getTotalPages = function() {
		var totalPages = $scope.itemsPerPage < 1 ? 1 : Math.ceil($scope.totalItems / $scope.itemsPerPage);
	    return Math.max(totalPages || 0, 1);
	}
	
	$scope.clean = function() {
		var data = { task:'templates.clean' };
		$('#adminForm').ajaxSubmit({
			data: data,
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function(){
						$scope.pageChanged($scope.currentPage);
					})
				}
				NProgress.done();
			}
		});
	}
	
	
	$scope.add = function() {
		$('#addform').ajaxSubmit({
			data: { task:'templates.add' },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list = res.list;
					$scope.totalItems = res.total;
					$scope.currentPage = res.cp;
					$scope.$apply();
				}
				
				NProgress.done();
			}
		});
	}
	
	$scope.edit = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		window.location.href="index.php?option=com_onepage&view=template&id="+obj.id;
		/*var obj = $scope.list[idx];
		delete obj.$$hashKey;
		$('#editform').loadJSON(obj);
		$('#carousel-user').carousel('next');*/
	}

	
	$scope.returnMain = function() {
		$('#editform').clearForm();
		$('#carousel-user').carousel('prev');
	}
	
	$scope.deleteEntry = function(idx) {
		//console.log($scope.list[idx]);
		var obj = $scope.list[idx];
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
				$('#adminForm').ajaxSubmit({
					data: { task:'templates.remove', cid:[obj.id] },
					dataType:  'json',
					type: 'POST',
					beforeSubmit: function()	{
						//$('#changeformModal').block({ message: "Processing..." });
						//$.blockUI({ message: "Processing..." });
						NProgress.start();
					},
					success: function(res)	{
						if(res.success) {
							$scope.list = res.list;
							$scope.totalItems = res.total;
							$scope.currentPage = res.cp;
							$scope.$apply();
						}
						
						NProgress.done();
					}
				});
				}
			}
		});
	}
	
	$scope.deleteAll = function() {
		//console.log($scope.list[idx]);
		//$('#confirmModal').modal('show');
		//$('#confirmYes').click(function()	{
		vex.dialog.confirm({
			message: 'Are you sure to delete the item(s)?',
			callback: function(value) {
				if(value === true) {
					$('#adminForm').ajaxSubmit({
						data: { task:'templates.remove', 'parent_id':1 },
						dataType:  'json',
						type: 'POST',
						beforeSubmit: function()	{
							//$('#changeformModal').block({ message: "Processing..." });
							//$.blockUI({ message: "Processing..." });
							NProgress.start();
						},
						success: function(res)	{
							if(res.success) {
								$scope.list = res.list;
								$scope.totalItems = res.total;
								$scope.currentPage = res.cp;
								$scope.$apply();
							}
							NProgress.done();
							//$('#confirmModal').modal('hide');
						}
					});
				}
				/**/
			}
		});
			
		//});
	}
	
	$scope.publishEntry = function(idx) {
		//console.log($scope.list[idx]);
		$('#adminForm input:checkbox').attr('checked',false);
		
		var obj = $scope.list[idx];
		$('#adminForm').ajaxSubmit({
			data: { task:'templates.publish', cid:[obj.id],'published':-1 },
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.list[idx].published = 1 - obj.published;
					$scope.$apply();
				}
				NProgress.done();
			}
		});
	}
	
}]);