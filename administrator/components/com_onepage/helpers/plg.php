<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access
defined('_JEXEC') or die;

class OnepageHelperPlg
{	
	static public function getTpl($item)
	{
		JPluginHelper::importPlugin('opeltype',$item->type);
		$dispatcher = jsGetDispatcher();
		$list = $dispatcher->trigger('getDefaultTpl',array($item));
		return $list;
	}
	
	static public function preloadScriptSrc($type = 'js',$nodes)
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$codes = array();
		
		foreach($nodes as $node)
		{
			if($type == 'js')
			{
				$dispatcher->trigger('preloadJsSrc',array(&$codes,$node));
			}
			elseif($type == 'css')
			{
				$dispatcher->trigger('preloadCssSrc',array(&$codes,$node));
			}
		}
		
		
		$codes = implode("\r\n",$codes);
		return $codes;
	}
	
	static public function getGroupType()
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$list = $dispatcher->trigger('getGroupType',array());
		$list = jsArrayNoEmpty($list);
		sort($list);
		return $list;
	}
	
	static public function getItemType()
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$list = $dispatcher->trigger('getItemType',array());
		$list = jsArrayNoEmpty($list);
		sort($list);
		return $list;
	}
	
	static public function getRowType()
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$list = $dispatcher->trigger('getRowType',array());
		$list = jsArrayNoEmpty($list);
		sort($list);
		return $list;
	}
}
