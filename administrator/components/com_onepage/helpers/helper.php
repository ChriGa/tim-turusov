<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access
defined('_JEXEC') or die;

class OnepageHelper extends jsAppHelper
{
	public static $extension = 'com_onepage';

	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	$vName	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */
	
	public static function createMenu()
	{
		$view = JRequest::getCmd('view');
		$menus = array();
		$menus[] = array('title'=>JText::_('ONEPAGE_MENU_PAGES'),'default'=>true,'view'=>'pages'
		,'path' => 'index.php?option=com_onepage&view=pages'
		);
/*
		$menus[] = array('title'=>JText::_('DLOCKER_MENU_USER'),'default'=>false,'view'=>'users'
		,'path' => 'index.php?option=com_onepage&view=users'
		);
		$menus[] = array('title'=>'Go To (JS) Money','default'=>false,'view'=>'test'
		,'path' => 'index.php?option=com_onepage'
		);*/
		$html = parent::_createMenu($menus,$view);
		return $html;
	}
	
	public static function checkTitle($title,$i = 0)
	{
		$oTitle = $title;
		$title = empty($i)?$title:$title." {$i}";
		
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
		$query->select( '`title`' );
		$query->from('`#__onepage_page`');
		$query->where("`title` = ".$db->quote($title));
		$obj = jsDB::loadObj($query,'obj');
		if(!empty($obj))
		{
			return self::checkTitle($oTitle,$i+1);
		}
		else
		{
			return $title;
		}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param	int		The category ID.
	 * @param	int		The article ID.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions($categoryId = 0, $articleId = 0)
	{
		// Reverted a change for version 2.5.6
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($articleId) && empty($categoryId)) {
			$assetName = 'com_content';
		}
		elseif (empty($articleId)) {
			$assetName = 'com_content.category.'.(int) $categoryId;
		}
		else {
			$assetName = 'com_content.article.'.(int) $articleId;
		}

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
	
	public static function initPage()
	{
		//js
		JHtml::script('media/system/js/core.js');
		JHtml::script('media/jsbt3/js/basic.js');
		//JHtml::script('media/jsbt3/js/nprogress.js');
		
		// css
		JHtml::stylesheet('media/jsbt3/css/basic.css');
		JHtml::stylesheet('media/jsbt3/css/nprogress.css');
		JHtml::stylesheet('media/jsbt3/css/theme.css');
		JHtml::stylesheet('administrator/components/com_onepage/assets/css/com.css');
	}

	
	
	/**
	* Applies the content tag filters to arbitrary text as per settings for current user group
	* @param text The string to filter
	* @return string The filtered string
	*/
	public static function filterText($text)
	{
		// Filter settings
		$config		= JComponentHelper::getParams('com_config');
		$user		= JFactory::getUser();
		$userGroups	= JAccess::getGroupsByUser($user->get('id'));

		$filters = $config->get('filters');

		$blackListTags			= array();
		$blackListAttributes	= array();

		$customListTags			= array();
		$customListAttributes	= array();

		$whiteListTags			= array();
		$whiteListAttributes	= array();

		$noHtml				= false;
		$whiteList			= false;
		$blackList			= false;
		$customList			= false;
		$unfiltered			= false;

		// Cycle through each of the user groups the user is in.
		// Remember they are included in the Public group as well.
		foreach ($userGroups as $groupId)
		{
			// May have added a group but not saved the filters.
			if (!isset($filters->$groupId)) {
				continue;
			}

			// Each group the user is in could have different filtering properties.
			$filterData = $filters->$groupId;
			$filterType	= strtoupper($filterData->filter_type);

			if ($filterType == 'NH') {
				// Maximum HTML filtering.
				$noHtml = true;
			}
			elseif ($filterType == 'NONE') {
				// No HTML filtering.
				$unfiltered = true;
			}
			else {
				// Black, white or custom list.
				// Preprocess the tags and attributes.
				$tags			= explode(',', $filterData->filter_tags);
				$attributes		= explode(',', $filterData->filter_attributes);
				$tempTags		= array();
				$tempAttributes	= array();

				foreach ($tags as $tag)
				{
					$tag = trim($tag);

					if ($tag) {
						$tempTags[] = $tag;
					}
				}

				foreach ($attributes as $attribute)
				{
					$attribute = trim($attribute);

					if ($attribute) {
						$tempAttributes[] = $attribute;
					}
				}

				// Collect the black or white list tags and attributes.
				// Each lists is cummulative.
				if ($filterType == 'BL') {
					$blackList				= true;
					$blackListTags			= array_merge($blackListTags, $tempTags);
					$blackListAttributes	= array_merge($blackListAttributes, $tempAttributes);
				}
				elseif ($filterType == 'CBL') {
					// Only set to true if Tags or Attributes were added
					if ($tempTags || $tempAttributes) {
						$customList				= true;
						$customListTags			= array_merge($customListTags, $tempTags);
						$customListAttributes	= array_merge($customListAttributes, $tempAttributes);
					}
				}
				elseif ($filterType == 'WL') {
					$whiteList				= true;
					$whiteListTags			= array_merge($whiteListTags, $tempTags);
					$whiteListAttributes	= array_merge($whiteListAttributes, $tempAttributes);
				}
			}
		}

		// Remove duplicates before processing (because the black list uses both sets of arrays).
		$blackListTags			= array_unique($blackListTags);
		$blackListAttributes	= array_unique($blackListAttributes);
		$customListTags			= array_unique($customListTags);
		$customListAttributes	= array_unique($customListAttributes);
		$whiteListTags			= array_unique($whiteListTags);
		$whiteListAttributes	= array_unique($whiteListAttributes);

		// Unfiltered assumes first priority.
		if ($unfiltered) {
			// Dont apply filtering.
		}
		else {
			// Custom blacklist precedes Default blacklist
			if ($customList) {
				$filter = JFilterInput::getInstance(array(), array(), 1, 1);

				// Override filter's default blacklist tags and attributes
				if ($customListTags) {
					$filter->tagBlacklist = $customListTags;
				}
				if ($customListAttributes) {
					$filter->attrBlacklist = $customListAttributes;
				}
			}
			// Black lists take third precedence.
			elseif ($blackList) {
				// Remove the white-listed attributes from the black-list.
				$filter = JFilterInput::getInstance(
					array_diff($blackListTags, $whiteListTags), 			// blacklisted tags
					array_diff($blackListAttributes, $whiteListAttributes), // blacklisted attributes
					1,														// blacklist tags
					1														// blacklist attributes
				);
				// Remove white listed tags from filter's default blacklist
				if ($whiteListTags) {
					$filter->tagBlacklist = array_diff($filter->tagBlacklist, $whiteListTags);
				}
				// Remove white listed attributes from filter's default blacklist
				if ($whiteListAttributes) {
					$filter->attrBlacklist = array_diff($filter->attrBlacklist);
				}

			}
			// White lists take fourth precedence.
			elseif ($whiteList) {
				$filter	= JFilterInput::getInstance($whiteListTags, $whiteListAttributes, 0, 0, 0);  // turn off xss auto clean
			}
			// No HTML takes last place.
			else {
				$filter = JFilterInput::getInstance();
			}

			$text = $filter->clean($text, 'html');
		}

		return $text;
	}
	
	public static function addCustomSubmenu()
	{
		$vName = JRequest::getCmd('view','pages');
		$layout = JRequest::getCmd('layout');
		
		jsJoomlaSubmenu::beginMenu('Pages');
		jsJoomlaSubmenu::addMenuItem(
			'Pages',
			'index.php?option=com_onepage&view=pages',
			$vName == 'pages'
		);
		
		jsJoomlaSubmenu::addMenuItem(
			'Top Navigation Bar',
			'index.php?option=com_onepage&view=hf',
			$vName == 'hf'
		);
		
		jsJoomlaSubmenu::addMenuItem(
			'Footer',
			'index.php?option=com_onepage&view=hf&layout=footer',
			($vName == 'hf' && $layout == 'footer')
		);
		
		jsJoomlaSubmenu::endMenu();
		
		/*jsJoomlaSubmenu::addEntry(
		JText::_('COM_ONEPAGE_MENU_MENUS'),
				'index.php?option=com_onepage&view=menus',
		$vName == 'menus');*/
	
		jsJoomlaSubmenu::addEntry(
		JText::_('COM_ONEPAGE_MENU_CSS'),
		'index.php?option=com_onepage&view=customcss',
		$vName == 'customcss','new');
	
	
		jsJoomlaSubmenu::addEntry(
		JText::_('COM_ONEPAGE_MENU_CONFIG'),
				'index.php?option=com_onepage&view=config',
		$vName == 'config','new'
		);
	
		jsJoomlaSubmenu::addEntry(
		JText::_('COM_ONEPAGE_MENU_HELP'),
						'index.php?option=com_onepage&view=help',
		$vName == 'help','new'
		);
	}
	
	public static function getImgPath($html = false)
	{
		if($html)
		{
			//return  '../components/com_onepage/bootstrap/site_images';
			return '../images/onepage/';
		}
		else
		{
			//return  JPATH_COMPONENT_SITE.'/bootstrap/site_images';
			return JPATH_SITE.'/images/onepage/';
		}
		
	}
	
	public static function initCom()
	{
		$db = JFactory::getDbo();	
		
		$tConfig = jsAppConfig::getInstance();
		if (!$tConfig->get('mode',0))
		{
			$tConfig->set('mode',2);
			$tConfig->set('app_id','639356178');
		}
		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('`#__jsapp_default`');
		$query->where("`app`='onepage'");
		$query->where("`key`='topmenu_published'");
		$db->setQuery($query);
		$rec = $db->loadObject();
		
		if( empty($rec) )
		{
			$tConfig->set('topmenu_published',1);
			$tConfig->set('topmenu_barcolor','black');
			$tConfig->set('topmenu_fixed',1);
			$tConfig->set('topmenu_showpng',0);
			$tConfig->set('topmenu_title','OnePage');
			
			$tConfig->set('footer_published',1);
			$tConfig->set('footer_adv',1);
			$tConfig->set('footer_csscls','');
			
			$content = '<div class="row">
			<div class="span3 col-md-3">
			<h3 class="footer-title">Address</h3>
			<address>
			  <strong>Corporation Name</strong><br>
			  Addr Line1<br>
			  Addr Line2, Post Code<br>
			  <abbr title="Phone">P:</abbr> (123) 456-7890
			</address>
			  
			<address>
			  <strong>Contact@J-SOHO</strong><br>
			  <a href="mailto:#">j-soho@hotmail.com</a>
			</address>
			  
			<p class="pvl"> </p>
			<a class="footer-brand" href="http://www.j-soho.com" target="_blank"> 2013@J-SOHO  </a></div>
			
			<div class="span3 col-md-3">
			  <a href="http://designmodo.com/shop/?u=105" target="_blank" class="thumbnail">
			    <img src="components/com_onepage/bootstrap/site_images/flatui_300_250.jpg" border="0"/>
			    <strong> 
			    FlatUI Pro.
			    </strong>
			    <p> Prettify the page easily.</p>
			  </a>
			  
			</div>
			  
			<!-- /span8 -->
			<div class="span6 col-md-6">
			<div class="footer-banner">
			<h3 class="footer-title">You should know</h3>
			<ul>
			<li><a href="http://fortawesome.github.io/Font-Awesome">Font Icon with @Font-Awesome</a></li>
			<li><a href="http://twitter.github.io/bootstrap/">Bootstrap Based HTML/CSS/JS Layout</a></li>
			  <li>Code licensed under Apache License v2.0</li>
			</ul>
			</div>
			</div>
			</div>';
			
			$tConfig->set('footer_content',$content);
		}
		
		$hp = $tConfig->get('home',0);
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_menu');
		$query->where("`id` = '{$hp}'");
		$home = jsDB::loadObj($query);
		
		if(empty($home))
		{
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__onepage_menu');
			$query->where("`level` = '3'");
			$query->where("`published` = '1'");
			$query->order(' `lft` asc limit 1');
			$child = jsDB::loadObj($query);
			
			$tConfig->set('home',jsGetValue($child,'id'));
		}
		
				
		// Init Menu
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__menu_types');
		$query->where("`menutype` = 'onepage'");
		$mt = jsDB::loadObj($query);
		
		if(empty($mt))
		{
			$vals = array();
			$vals['menutype'] = 'onepage';
			$vals['title'] = 'OnePage Menu';
			$vals['description'] = 'All OnePage will show here!';
			jsDB::insert('#__menu_types', $vals);
		}
		
		// Create Menu
		if(!$hp)
		{
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__extensions');
			$query->where("`name` = 'com_onepage'");
			$query->where("`type` = 'component'");
			$com = jsDB::loadObj($query);
			
			$tEl = JTable::getInstance('Menu','OnepageTable');
			$tEl->load($hp);
				
			JTable::addIncludePath(dirname(JPATH_COMPONENT_ADMINISTRATOR).'/com_menus/tables');
			$menu = JTable::getInstance('Menu','MenusTable');
			$menu->setLocation(1, 'last-child');
			$data = array();
			$data['menutype'] = 'onepage';
			$data['title'] = 'OnePage Home';
			$data['alias'] = 'onepage-home';//JString::increment(str_replace(' ','-',strtolower($data['title'])), 'dash');
			$data['parent_id'] = 1;
			$data['link'] = 'index.php?option=com_onepage&view=home';
			$data['type'] = 'component';
			$data['level'] = 1;
			$data['language'] = '*';
			$data['component_id'] = jsGetValue( $com,'extension_id');
			$data['published'] = 1;
			$params = '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}';
			$params = jsJson::decode($params);
			
			$data['params'] = jsJson::encode($params);
			$menu->bind($data);
			$menuCreated = $menu->store();
			if($menuCreated)
			{
				$tConfig->set('home_menu_id',$menu->id);
			}
		}
		
		
		
		
		
		
		return true;
	}
	
	static public function isEditable($item)
	{
		$types = array('item','gmap','contactform');
		//$types[] = 'clock';
		//$types[] = 'sociallike';
		
		$type = $item->type;
		if(in_array($type,$types))
		{
			return true;
		}
		
		
		return false;
	}
	
	static public function isDeprecated($type)
	{
		if(in_array($type,array('navbar','hellounit','footer')))
		{
			return true;
		}
	
		return false;
	}
	
	static public function filterId($id)
	{
		$tConfig = jsAppConfig::getInstance();
		$id = ($id <2)?false:$id;
		return $id;
	}
	
	
	static public function getItemTpl($item)
	{
		$row = $item;
		$params  = $row->params;
		$title = (!empty($row->title))?$row->title.$row->id:$row->type.'_'.$row->id;
		$id = onepageHelperFront::genId($title);
		
		$return = array();
		$return[] = $row;
		$return[] = $params;
		$return[] = $id;
		
		$children = array();
		$return[] = $children;
		
		return $return;
	}
	
	
	static public function getGroupType()
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$list = $dispatcher->trigger('getGroupType',array());
		$list = jsArrayNoEmpty($list);
		sort($list);
		return $list;
	}
	
	static public function getItemType()
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$list = $dispatcher->trigger('getItemType',array());
		$list = jsArrayNoEmpty($list);
		sort($list);
		return $list;
	}
	
	static public function getTypes()
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$list = $dispatcher->trigger('getOPElementType',array());
		$list = jsArrayNoEmpty($list);
		
		return $list;
	}
	
	static public function isItem($item)
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$bool = false;
		$list = $dispatcher->trigger('isItem',array($item->type,&$bool));
		return $bool;
	}
	
	static public function isGroup($item)
	{
		JPluginHelper::importPlugin('opeltype');
		$dispatcher = jsGetDispatcher();
		$bool = false;
		$list = $dispatcher->trigger('isGroup',array($item->type,&$bool));
		return $bool;
	}
	
	static public function logEditPath($pid,$eid)
	{
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_log');
		$query->where("`pid` = '{$pid}'");
		$db->setQuery($query);
		$obj = $db->loadObject();
			
		if(!empty($obj))
		{
			$query = $db->getQuery(true);
			$query->update('#__onepage_log');
			$query->set("`eid`='{$eid}'");
			$query->where("`id` = '{$obj->id}'");
			
		}
		else
		{
			$query = $db->getQuery(true);
			$query->insert('#__onepage_log');
			$query->columns(array('pid','eid'));
			$query->values(array("'{$pid}','{$eid}'"));
		}
		
		$db->setQuery($query);
		return $db->query();
	}
	
	static public function clearLogPath($pid)
	{
		$db = jsDB::instance();
		$vals = array();
		$vals['pid'] = $pid;
		
		return jsDB::delete('#__onepage_log', $vals);
	}
	
	static public function getEditLog($pid)
	{
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_log');
		$query->where("`pid` = '{$pid}'");
		$db->setQuery($query);
		$obj = $db->loadObject();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_page');
		$query->where("`id` = '{$obj->eid}'");
		$db->setQuery($query);
		$exists = $db->loadObject();
		
		//
		if( empty($obj) || empty($exists) )
		{
			// get the first child node
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__onepage_page');
			$query->where("`parent_id` = '{$pid}'");
			$query->order("`lft` ASC LIMIT 1");
			$db->setQuery($query);
			$obj = $db->loadObject();
			$obj->eid = $obj->id;
		}
		
		return $obj;
	}
	
	static public function getTpls($type)
	{
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_tpl');
		$query->where("`type` = '{$type}'");
		$objs = jsDB::loadObjs($query,'obj');
	
		
	
		return $objs;
	}
}
