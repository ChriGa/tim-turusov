RENAME TABLE `#__onepage_menu` TO `#__onepage_page`;
ALTER TABLE  `#__onepage_page` ADD  `html` TEXT NULL DEFAULT NULL AFTER  `content`;
ALTER TABLE  `#__onepage_page` ADD  `js` TEXT NULL DEFAULT NULL AFTER  `html`;
ALTER TABLE  `#__onepage_page` ADD  `css` TEXT NULL DEFAULT NULL AFTER  `js`;
ALTER TABLE  `#__onepage_page` ADD  `md` INT( 2 ) NOT NULL DEFAULT  '4' AFTER  `css`;
ALTER TABLE  `#__onepage_page` ADD  `ver` VARCHAR( 7 ) NOT NULL DEFAULT  '1.4.0';
UPDATE `#__onepage_page` SET `level` = `level`-1;
UPDATE `#__onepage_page` SET `ver` = '1.3.0';