CREATE TABLE IF NOT EXISTS `#__onepage_tpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(20) NOT NULL DEFAULT 'item',
  `html` text,
  `css` text,
  `js` text,
  `code` varchar(50) DEFAULT NULL,
  `from` varchar(30) NOT NULL DEFAULT 'system',
  `required` varchar(20) NOT NULL DEFAULT '1.4.1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000;