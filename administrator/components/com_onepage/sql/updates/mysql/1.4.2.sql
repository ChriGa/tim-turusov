ALTER TABLE  `#__onepage_tpl` ADD  `enable_js` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `js`;
ALTER TABLE  `#__onepage_tpl` ADD  `published` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `type`;
ALTER TABLE  `#__onepage_tpl` ADD  `author` INT( 11 ) NOT NULL DEFAULT  '0' AFTER  `required`;