--
-- Dumping data for table `#__onepage_default`
--

CREATE TABLE IF NOT EXISTS `#__jsapp_default` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) DEFAULT NULL,
  `value` text,
  `app` varchar(20) NOT NULL DEFAULT 'onepage',
  `default` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

--
-- Dumping data for table `#__onepage_default`
--

CREATE TABLE IF NOT EXISTS `#__onepage_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `showtitle` int(1) NOT NULL DEFAULT '1',
  `code` varchar(30) NOT NULL,
  `path` varchar(50) NOT NULL DEFAULT 'none' COMMENT 'The computed path of the menu item based on the alias field.',
  `type` varchar(30) NOT NULL,
  `content` text,
  `html` text,
  `js` text,
  `css` text,
  `md` int(2) NOT NULL DEFAULT '4',
  `published` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'The relative ordering of the menu item in the tree.',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '1' COMMENT 'Nested set rgt.',
  `test` int(2) NOT NULL DEFAULT '4',
  `ver` varchar(7) NOT NULL DEFAULT '1.4.0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__onepage_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `eid` int(11) DEFAULT '0',
  `default` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__onepage_tpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(20) NOT NULL DEFAULT 'item',
  `published` INT( 1 ) NOT NULL DEFAULT  '0',
  `html` text,
  `css` text,
  `itemcss` TEXT NULL DEFAULT NULL,
  `js` text,
  `mobile` text,
  `tablet` text,
  `enable_js` INT( 1 ) NOT NULL DEFAULT  '0',
  `code` varchar(50) DEFAULT NULL,
  `from` varchar(30) NOT NULL DEFAULT 'system',
  `required` varchar(20) NOT NULL DEFAULT '1.4.1',
  `author` INT( 11 ) NOT NULL DEFAULT  '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000;