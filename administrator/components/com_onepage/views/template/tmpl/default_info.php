<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
?>

<div class="panel-group" id="accordion-main">
	<div class="panel panel-info" style="margin-top:10px;">
		<div class="panel-heading ">
			<h4 class="panel-title">General Information</h4>
		</div>
		<div id="apinfo" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Title</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="title" ng-model="item.title"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-4 control-label">Required</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="required" ng-model="item.required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-4 control-label">Use JS</label>
					<div class="col-md-8">
						<select class="form-control" name="enable_js"  ng-model="item.enable_js">
							<option value="0">Use Default JS</option>
							<option value="1">Use Custom JS</option>
							<option value="2">Disabled All</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-4 control-label">Code</label>
					<div class="col-md-8">
						<textarea class="form-control" name="code" ng-model="item.code"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="panel panel-info" style="margin-top:10px;">
		<div class="panel-heading ">
			<h4 class="panel-title">Script</h4>
		</div>
		
		<div id="apinfo" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="form-group">
					<div class="col-md-10 col-md-offset-1">
						<div class="input-group">
						  <span class="input-group-addon">CSS</span>
						  <select class="form-control" name="published"  ng-model="csssnip">
							<option value="">-- Select --</option>
							<option value="xs">Media-Mobile</option>
							<option value="sm">Media-Tablet</option>
							<option value="md">Media-Desktop</option>
						  </select>
						  <span class="input-group-btn">
					      	<button type="button" class="btn btn-success btn-block" ng-click="addCssSnip()"><span class="fa fa-plus " style="margin-left:10px;margin-right:10px;"></span> </button>
					      </span>
						</div>
						
					</div>
				</div>
				<hr/>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-1">
						<button class="btn btn-warning btn-block" type="button" ng-click="restore()">Restore To Default</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
