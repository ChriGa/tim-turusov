<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

?>

<style>
.CodeMirror {height:500px;}
</style>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Html</a></li>
  <li><a href="#tab2" role="tab" data-toggle="tab">Type CSS</a></li>
  <li><a href="#tab6" role="tab" data-toggle="tab">Item CSS</a></li>
  <li class="hidden"><a href="#tab3" role="tab" data-toggle="tab">JS</a></li>
  <li class=""><a href="#tab4" role="tab" data-toggle="tab">Mobile</a></li>
  <li class=""><a href="#tab5" role="tab" data-toggle="tab">Tablet</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  	<div class="tab-pane active" id="tab1">
		<textarea id="html" style="height:500px;" name="html"   ><?php //echo $this->item->html;?></textarea>
	</div>
  	<div class="tab-pane" id="tab2">
		<textarea id="csscode" style="height:500px;" name="css"   ><?php echo $this->item->css;?></textarea>
	</div>
	<div class="tab-pane" id="tab6">
		<textarea id="itemcsscode" style="height:500px;" name="itemcss"   ><?php echo $this->item->itemcss;?></textarea>
	</div>
  	<div class="tab-pane hidden" id="tab3">
		<textarea id="jscode" style="height:500px;" name="js"   ><?php echo $this->item->js;?></textarea>
	</div>
	
	<div class="tab-pane " id="tab4">
		<textarea id="mobile" style="height:500px;" name="mobile"   ><?php echo $this->item->mobile;?></textarea>
	</div>
	
	<div class="tab-pane " id="tab5">
		<textarea id="tablet" style="height:500px;" name="tablet"   ><?php echo $this->item->tablet;?></textarea>
	</div>
</div>



