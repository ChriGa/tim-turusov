<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

JHtml::script('media/jsbt3/js/form.js');
JHtml::script('media/jsbt3/js/blockui.js');

JHtml::script('media/jsbt3/js/jasny-bootstrap.min.js');
JHtml::stylesheet('media/jsbt3/css/jasny-bootstrap.min.css');


JHtml::script('administrator/components/com_onepage/assets/js/plugins/codemirror.js');
JHtml::script("administrator/components/com_onepage/assets/js/plugins/javascript.js");
JHtml::script("administrator/components/com_onepage/assets/js/plugins/css.js");
JHtml::script("administrator/components/com_onepage/assets/js/plugins/xml.js");
JHtml::script('administrator/components/com_onepage/assets/js/plugins/active-line.js');
JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/codemirror.css');
JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/monokai.css');

$document = JFactory::getDocument();
$document->addScript("index.php?option=com_onepage&task=template.generateJS&id={$this->item->id}&f=".uniqid());

//JHtml::script('administrator/components/com_onepage/assets/js/plugins/ui-codemirror.js');
JHtml::script('administrator/components/com_onepage/assets/js/template_edit.js');

JPluginHelper::importPlugin('opeltype',$this->item->type);
?>
				
<div ng-app="myApp">
	<div ng-controller="wholepage" class="theme">
		<?php echo $this->loadTemplate('header');?>
		<div class="container" style="width:100%">
			<div class="row" style="padding-top:30px;">
				<form id="elform" class="form-horizontal" role="form" action="index.php">
					<div class="col-md-8">
						<?php echo $this->loadTemplate('editor');?>
					</div>
					<div class="col-md-4">
						<?php echo $this->loadTemplate('panel');?>
						<?php echo $this->loadTemplate('info');?>
					</div>
					<input type="hidden" name="option" value="com_onepage"/>
					<input type="hidden" name="task" value="template.save"/>
					<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
				</form>
				
				<form id="aform" action="index.php">
					<input type="hidden" name="option" value="com_onepage"/>
				</form>
			</div>
		</div>
		
	</div>
</div>