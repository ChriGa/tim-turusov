<?php
/**
* @version     1.0 +
* @package     J-SOHO - com_onepage
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on May-2013
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013- J-SOHO
*/
defined('_JEXEC') or die("Direct Access Not Allowed");


$groups = OnepageHelper::getGroupType();
$items = OnepageHelper::getItemType();
?>

<form id="addform" class="form-inline" role="form" ng-submit="add()">
	<div class="form-group">
		<label class="label-control">Type:</label>
	</div>
	
  	<div class="form-group">
	    <select class="form-control dropdown" id="addform_title" name="type">
	    <optgroup label="Level2 - Group Layout">
	    <?php foreach($groups as $k => $group): ?>
	    	<option value="<?php echo jsGetValue($group,'value'); ?>"><?php echo jsGetValue($group,'text'); ?></option>
	    <?php endforeach; ?>
	    </optgroup>
	    
	    <optgroup label="Level3 - Item Element">
	    <?php foreach($items as $k => $item): ?>
	    	<option value="<?php echo jsGetValue($item,'value'); ?>"><?php echo jsGetValue($item,'text'); ?></option>
	    <?php endforeach; ?>
	    </optgroup>
	   
	    </select>
 	</div>
  
  <button type="submit" class="btn btn-primary"><span class="fa fa-plus "></span> Add </button>
  
  <input type="hidden" name="option" value="com_onepage">
  <input type="hidden" name="task" value="templates.add">
</form>