<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
defined('_JEXEC') or die("Direct Access Not Allowed");

$config = jsAppConfig::getInstance();
?>


<div id="settingModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="settingModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?php echo JText::_('SETTING');?></h3>
  </div>
  <div class="modal-body">
  		
<form class="form-horizontal" action="<?php echo JRoute::_('index.php?option=com_onepage'); ?>" method="post" id="setting-form">
	<input type="hidden" name="task" value="saveConfig" />

		<div class="control-group">
			<label class="control-label">Mode</label>
			<div class="controls">
				<select name="mode" class="input-xlarge">
					<option value="1" <?php echo ($config->get('mode') == 1)?'selected':''; ?>>(Beginer)I can write an article in Joomla.</option>
					<option value="2" <?php echo ($config->get('mode') == 2)?'selected':''; ?>>I am a web designer.</option>
				</select>
			</div>
		</div>
		
</form>

	</div>
  <div class="modal-footer">
    <button class="btn btn-success" onClick="$('#setting-form').submit();" ><?php echo JText::_('Save');?></button>
    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('Cancel');?></button>
  </div>
</div>