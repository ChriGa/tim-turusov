<?php
/**
* @version     1.0 +
* @package     J-SOHO - com_onepage
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on May-2013
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013- J-SOHO
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
$tConfig = jsAppConfig::getInstance();
?>

<div style="height: 30px;"> </div>
<div class="row ">
<div class="col-md-4"><?php echo $this->loadTemplate('addform');?></div>
<div class="col-md-8 text-right">
	<button  class="btn btn-info" data-toggle="modal" data-target="#importModal"><?php echo JText::_('JS_IMPORT');?></button>
	<button  class="btn btn-default" ng-click="publishAll(1)"><?php echo JText::_('JS_PUBLISH');?></button>
	<button  class="btn btn-default" ng-click="publishAll(0)"><?php echo JText::_('JS_UNPUBLISH');?></button>
	<button  class="btn btn-danger" ng-click="deleteAll()"><span class="fa fa-trash-o "><?php echo JText::_('JS_DEL');?></span></button>
</div>
</div>

<form action="<?php echo JRoute::_('index.php?option=com_onepage');?>" method="post" name="adminForm" id="adminForm">

	<table class="table table-hover jslist">
		<thead>
			<tr>
				<th width="3%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th>
					<?php echo JText::_('JS_TITLE'); ?>
				</th>
				
				<th width="15%" class="text-center">
					<?php echo JText::_('JS_PREVIEW'); ?>
				</th>
			
			
				<th width="10%" class="text-center">
					<?php echo JText::_('JS_STATUS'); ?>
				</th>
				
				<th width="150" class="text-center">
					<?php echo JText::_('JS_ACTIONS'); ?>
				</th>
				
		
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">
				</td>
			</tr>
		</tfoot>
		<tbody ng-init="host='<?php echo JUri::root(); ?>'">
			<tr ng-repeat="item in list" class="row{{$index%2}}">
				<td class="center" >
					<input type="checkbox" id="cb{{$index}}" name="cid[]" value="{{item.id}}" onclick="Joomla.isChecked(this.checked);">
				</td>
				<td >{{item.title}}</td>
				<td class="text-center"><a ng-href="{{host}}index.php?option=com_onepage&view=page&id={{item.id}}" target="_blank"><?php echo JText::_('JS_PREVIEW'); ?></a></td>
			
				<td class="text-center">
					<a ng-click="publishEntry($index)">
					<i class="fa fa-check-circle fa-lg htips" style="color: #51A351" ng-show="item.published" data-toggle="tooltip" data-placement="bottom" title="<?php echo JText::_('JS_CLICK_2_UNPUBLISH'); ?>"></i>
					<i class="fa fa-times-circle fa-lg htips" style="color: #F44646" ng-hide="item.published" data-toggle="tooltip" data-placement="bottom" title="<?php echo JText::_('JS_CLICK_2_PUBLISH'); ?>"></i>
					</a>
				</td>

				<td >
					<a ng-click="exportPage($index)" class="btn btn-default btn-xs"><i class="fa fa-share "></i></a>
					<a ng-click="duplicate($index)" class="btn btn-default btn-xs"><i class="fa fa-copy "></i></a>
					<a ng-click="config($index)" class="btn btn-warning btn-xs"><i class="fa fa-gear "></i></a>
					<a ng-click="edit($index)" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
					<a ng-click="deleteEntry($index)" class="btn btn-danger btn-xs"><span class="fa fa-times"></span></a>
				</td>
			</tr>
			
		</tbody>
	</table>


	<div>
		<input type="hidden" name="boxchecked" value="0">
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php //echo JHtml::_('form.token'); ?>
	</div>
</form>