<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

/*JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');*/

//
$model = $this->getModel();

OnepageHelper::initPage();

$assetPath = 'administrator/com_onepage/assets';
JHtml::stylesheet('media/jsbt3/css/vex.css');
JHtml::stylesheet('media/jsbt3/css/vex-theme-os.css');
JHtml::stylesheet($assetPath.'/css/com.css');

JHtml::script('media/jsbt3/js/form.js');
JHtml::script('media/jsbt3/js/blockui.js');
JHtml::script('media/jsbt3/js/vex.combined.min.js');

JHtml::script('media/jsbt3/js/jasny-bootstrap.min.js');
JHtml::stylesheet('media/jsbt3/css/jasny-bootstrap.min.css');

$document = JFactory::getDocument();
$document->addScript("index.php?option=com_onepage&task=pages.generateJS&f=".uniqid());
JHtml::script('administrator/components/com_onepage/assets/js/pages.js');
//jsExit($this->menus);
?>
<style type="text/css">
#system-message-container {
padding-top: 0px;
}
</style>

<script>
var needUpdate = function() {return <?php echo $this->config->get('updatetpl',1);?>; };
</script>

<div ng-app="myApp">
	<div ng-controller="wholepage" class="theme">
		<?php echo $this->loadTemplate('header');?>
		<div class="container">
			<?php echo $this->loadTemplate('list');?>
		</div>
		
		<?php if($this->has_update):?>
		<?php echo $this->loadTemplate('version');?>
		<?php endif;?>
		
		<?php echo $this->loadTemplate('modal');?>
	</div>
</div>