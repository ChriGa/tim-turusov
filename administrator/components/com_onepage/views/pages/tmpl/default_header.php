<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
defined('_JEXEC') or die("Direct Access Not Allowed");
?>
<nav class="navbar navbar-default " role="navigation" style="margin-bottom: 0px;">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand htips exit" data-toggle="tooltip" data-placement="bottom" title="<?php echo JText::_('JS_BACK2JOOMLA'); ?>" style="background-color:#D2322D;color:#fff;" href="index.php"><span class="fa fa-angle-double-left fa-lg"></span></a>
		<a class="navbar-brand" href="#"><?php echo JText::_('COM_ONEPAGE').' '.$this->config->get('version'); ?></a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav ">
			<?php echo OnepageHelper::createMenu(); ?>
			
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php if($this->has_update):?>
			<li id="notification">
				<a class="" data-toggle="modal" data-target="#versionModal"><span class="badge" style="background-color:#D9534F;"><i class="fa fa-bell-o fa-lg"></i></span> </a>
		        
			</li> 
			<?php endif;?>
			<li><a ng-click="refreshTpls()" class="htips" data-toggle="tooltip" data-placement="left" title="<?php echo JText::_('Update Templates'); ?>" target="_blank"><span class="fa fa-refresh fa-lg"></span></a></li> 
			<li><a href="<?php echo JS_RATEME_LINK;?>" class="htips" data-toggle="tooltip" data-placement="left" title="<?php echo JText::_('JS_RATEME'); ?>" target="_blank"><span class="fa fa-star fa-lg"></span></a></li> 
			<li><a href="mailto:<?php echo JS_CONTACT_EMAIL;?>?subject=Digital Locker" class="htips" data-toggle="tooltip" data-placement="left" title="<?php echo JText::_('JS_GIVE_FEEDBACK'); ?>"><span class="fa fa-envelope fa-lg"></span></a></li> 
			<li><a href="http://j-soho.com/updatenews/6-onepage-v1-4-0-beta-document" class="htips" data-toggle="tooltip" data-placement="left" title="Document" target="_blank"><span class="fa fa-question fa-lg"></span></a></li> 
			<li><a href="index.php?option=com_onepage&view=config" class="htips" data-toggle="tooltip" data-placement="left" title="<?php echo JText::_('JS_CONFIG'); ?>"><span class="fa fa-gears fa-lg text-success" ></span></a></li> 
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>