<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
?>

<!-- Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="importModalLabel">Import Data</h4>
      </div>
      <div class="modal-body" >
      	<form id="importForm" class="form-horizontal" role="form" ng-submit="importPage()">
			
			<div class="form-group">
			  	<label class="col-sm-3 control-label">File</label>
		  		<div class="col-sm-9">
		  			<div class="fileinput fileinput-new" data-provides="fileinput">
					  <div class="input-group">
					    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
					    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="pack"></span>
					    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
					  </div>
					</div>
		  		</div>
			</div>
			
			<div class="form-group">
			  	<label class="col-sm-3 control-label">Replace?</label>
		  		<div class="col-sm-9">
		  			<select class="form-control" name="id">
		  				<option><?php echo JText::_('JS_NONE');?></option>
						<option value="{{ropt.id}}" ng-repeat="ropt in list">{{ropt.title}}</option>
						
					</select>
		  		</div>
			</div>
			
			<div class="form-group">
			  	<label class="col-sm-3 "></label>
		  		<div class="col-sm-9">
		  			<div class="alert alert-danger fade in">
				      <p>Please note that the original content will be removed if you use the <strong>Replace</strong> function.</p>
				      
				    </div>
		  		</div>
			</div>
			
			
		  	<button type="submit" class="btn btn-primary btn-block"><span class="fa fa-save "></span> <?php echo JText::_('JS_SAVE'); ?></button>
		  
			<input type="hidden" name="option" value="com_onepage">
			<input type="hidden" name="task" value="pages.importPage">
		</form>
      </div>
    </div>
  </div>
</div>