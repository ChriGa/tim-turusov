<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
defined('_JEXEC') or die("Direct Access Not Allowed");
?>
<div class="modal fade" id="versionModal" tabindex="-1" role="dialog" aria-labelledby="versionModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="versionModalLabel"><?php echo JText::_('COM_ONEPAGE').' - '.$this->config->get('latest_version'); ?> </h4>
      </div>
      <div class="modal-body">
       	<?php echo $this->config->get('version_desc'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger">Ignore This Version</button>
      </div>
    </div>
  </div>
</div>