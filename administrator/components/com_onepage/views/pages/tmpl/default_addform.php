<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
defined('_JEXEC') or die("Direct Access Not Allowed");
?>

<form id="addform" class="form-inline" role="form" ng-submit="addSubmit()">
  <div class="form-group">
    <label class="sr-only" for="addform_title"><?php echo JText::_('JS_TITLE'); ?></label>
    <input type="text" class="form-control" id="addform_title" placeholder="<?php echo JText::_('JS_TITLE'); ?>" name="title" ng-model="add_title">
  </div>
  
  <button type="submit" class="btn btn-primary"><span class="fa fa-plus "></span> <?php echo JText::_('JS_ADD'); ?></button>
  
  <input type="hidden" name="option" value="com_onepage">
  <input type="hidden" name="task" value="pages.add">
</form>