<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;
class OnepageViewPages extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return	void
	 */
	public function display($tpl = null)
	{

		JRequest::setVar('tmpl','component');
		//OnepageHelper::initPage();
		//JHTML::script("components/com_onepage/bootstrap/js/jquery-ui.custom.min.js");
		
		$this->config = jsAppConfig::getInstance('onepage');
		
		$model = $this->getModel();
		$this->items = $this->get('Items');
	
		$showUpdate = onepageHelper::checkVersion($this->config);
		if($showUpdate !== false)
		{
			$this->config = $showUpdate;
			$this->assignRef('has_update', $showUpdate);
		}
		else 
		{
			$this->assignRef('has_update', $showUpdate);
		}
			
		
		
		parent::display($tpl);
	}
	
}
