<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access.
defined('_JEXEC') or die;
$cp = $this->cp;
?>
<nav class="navbar navbar-default " role="navigation" style="margin-bottom: 0px;">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		
		<a class="navbar-brand" target="_blank" href="<?php echo JUri::root().'index.php?option=com_onepage&view=page&id='.$cp->id;?>" style="color:#428BCA" >{{item.title}} [<?php echo JText::_('JS_'.$this->item->type)?>]</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
			
		</ul>
		
		

		<button type="button" class=" btn btn-default navbar-btn "  ng-click="showPanel('map')">Jump To:</button>
		
		<button type="button" class=" btn btn-default navbar-btn" ng-click="showPanel('create')">Create Next!</button>
		
		<form class=" hidden navbar-form navbar-left " role="search" ng-submit="changePage()">
	        <div class="form-group" ng-init="pageid = '<?php echo $this->item->id;?>'">
	        	<label>Jump To:</label>
	          <select class="form-control" name="page" ng-model="pageid">
	          	<?php foreach($this->pitems as $page):?>
	          	<option value="<?php echo $page->id;?>" <?php echo onepageHelper::isEditable($page)?'':'disabled';?>>
	          	<?php echo onepageHelper::isEditable($page)?str_repeat('--',$page->level-3).$page->title:'    '.$page->title.'    ';?>
	          	</option>
	          	<?php endforeach;?>
	          </select>
	          <button class="btn btn-default">Go</button>
	        </div>
		</form>
		<ul class="nav navbar-nav navbar-right">
		
			<li class="hidden">
				<a class="jstips" href="index.php?option=com_onepage&view=prototype&layout=source&id=<?php echo $cp->id;?>" 
				data-toggle="tooltip" data-placement="left" title="Source Code Editor">
				<span class="fa fa-code fa-lg"></span>
				</a>
			</li>
			
			
			<li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Action(s) <b class="caret"></b></a>
	          <ul class="dropdown-menu">
		          
	          	<?php if($this->item->level != 3):?>
	          	<li><a href="#" ng-click="showPanel('type')">Change Type</a></li>
	            <?php endif;?>
	            <li><a href="#" ng-click="del()">Delete</a></li>
	            <li ><a href="#" ng-click="duplicate()">Duplicate</a></li>
	          	<li ng-show="canShowEmbed()"><a href="#" ng-click="showEmbed()">Embedded in...</a></li>
	         
	        	<li><a href="index.php?option=com_onepage&view=prototype&id=<?php echo $cp->id;?>">Layout Setting</a></li>
	      
	          </ul>
	        </li>
	        
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Open </a>
	          <ul class="dropdown-menu">
		      	<li><a target="_blank" href="<?php echo "index.php?option=com_onepage&view=script&type=css&id={$this->cp->id}"?>">Open CSS Editor</a></li>
		        <li><a target="_blank" href="<?php echo "index.php?option=com_onepage&view=script&type=js&id={$this->cp->id}"?>">Open JS Editor</a></li>
		        
	          </ul>
	        </li>
	        <li>
				<a class="jstips" href="index.php?option=com_onepage&view=prototype&id=<?php echo $cp->id;?>&layout=config" 
				data-toggle="tooltip" data-placement="left" title="Page Configuration">
				<span class="fa fa-gear fa-lg text-success"></span>
				</a>
			</li>
			<li>
				<a href="index.php?option=com_onepage&view=pages" id="returnLink"><span class="fa fa-sign-out fa-lg text-info"></span> <strong class="text-info"><?php echo JText::_('JS_BACK2MAIN');?></strong></a>
			</li>
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>

