<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
?>

<div class="panel panel-default">
<div class="panel-heading"> Script Setting </div>
<div class="panel-body">
	<form class="form-horizontal" action="index.php" role="form" id="jsconfig">
	
		<div class="form-group">
			<label class="col-sm-4 control-label">In the Bottom</label>
			<div class="col-sm-8">
				<select class="form-control" name="js_bottom" ng-model="item.params.js_bottom">
					<option value=""> No </option>
					<option value="1"> Yes </option>
				</select>
			</div>
		</div>

		<button class="btn btn-primary btn-block" type="submit">Save</button>
		<input type="hidden" name="option" value="com_onepage"/>
		<input type="hidden" name="task" value="prototype.savejsconfig"/>
		<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
	</form>
</div>
</div>


<div class="panel panel-default">
<div class="panel-heading"> Upload Form </div>
<div class="panel-body">
	<form class="form-horizontal" action="index.php" role="form" id="jscss">
	
		<div class="fileinput fileinput-new" data-provides="fileinput">
		  <div class="input-group">
		    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
		    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="file"></span>
		    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
		  </div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
			<div class="checkbox">
			<label>
			<input class="" type="checkbox" value="1" name="overwrite"/> Overwrite?
			</label>
			</div>
			</div>
		</div>

		<button class="btn btn-primary btn-block" type="submit">Upload</button>
		<input type="hidden" name="option" value="com_onepage"/>
		<input type="hidden" name="task" value="prototype.uploadjscss"/>
		<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
	</form>
</div>
</div>