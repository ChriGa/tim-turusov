<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$rows = OnepageHelperPlg::getRowType();
$groups = OnepageHelperPlg::getGroupType();
$items = OnepageHelperPlg::getItemType();
?>

<div class="container" ng-show="sc">
	<div class="row">
		<div class="col-md-11">
			<input class="form-control createtitle" placeholder="TITLE ..." ng-model="createtitle"/>
		</div>
		<div class="col-md-1 text-right">
			<button class="btn btn-danger" ng-click="showPanel('')" style="margin-top:30px;"> Close </button>
		</div>
	</div>
	
	<hr/>
	<div class="row" ng-hide="embed">
		<div class="col-md-12">
			<h3 style="margin-top:0px;">Level 1 Element:</h3>
			
			<?php foreach($rows as $item):?>
				<button class="btn btn-default" data-type="" ng-click="createEl(1,'<?php echo $item['value'];?>')"><?php echo $item['text'];?></button>
			<?php endforeach;?>
		</div>
	</div>
	<?php if( !in_array($this->item->type, array('nav','sidebar')) ):?>
	<div class="row">
		<div class="col-md-12">
			<h3>Level 2 Element:</h3>
			
			<div style="display:inline-block">
		<?php foreach($groups as $item):?>
			<button class="btn btn-default" data-type="" ng-click="createEl(2,'<?php echo $item['value'];?>')"><?php echo $item['text'];?></button>
		<?php endforeach;?>
			<div class="text-center" ng-hide="embed"><i class="fa fa-plus" style="color:#3E95F1;"></i></div>
			</div>
			<div ng-hide="embed">
		<?php foreach($items as $item):?>
			<button class="btn btn-default btn-create" data-type="" ng-click="createEl(2,'<?php echo $item['value'];?>')"><?php echo $item['text'];?></button>
		<?php endforeach;?>
			</div>
		</div>
	</div>
	
	<?php if($this->item->level != 3):?>
	<div class="row" ng-hide="embed">
		<div class="col-md-12">
			<h3>Level 3 Element:</h3>
		<?php foreach($items as $item):?>
		<button class="btn btn-default btn-create" data-type="" ng-click="createEl(3,'<?php echo $item['value'];?>')"><?php echo $item['text'];?></button>
		<?php endforeach;?>
		</div>
	</div>
	<?php endif;?>
	
	<?php endif;?>

	<form method="POST" id="createForm" action="index.php">
		<input type="hidden" name="option" value="com_onepage"/>
		<input type="hidden" name="task" value="prototype.createLevelEl"/>
		<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
	</form>
</div>