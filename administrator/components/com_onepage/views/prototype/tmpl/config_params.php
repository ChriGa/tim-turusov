<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$params = $this->item->params;

$yes = array();
$yes[] = array('value'=>1,'text'=>JText::_('JS_YES'));
$yes[] = array('value'=>0,'text'=>JText::_('JS_NO'));
$yes = htmlspecialchars( jsJSON::encode($yes) );
?>
<form class="form-horizontal" method="POST" ng-submit="saveInfo()" id="infoForm"> 
		
			<div class="form-group">
				<label class="control-label col-md-3">Title</label>
				<div class="col-md-9">
				<input name="title" ng-model="item.title" class="form-control" placeholder="Default 3">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Single Page</label>
				<div class="col-md-9">
				<select name="single" ng-model="item.params.single" class="form-control">
					<option value="1">Yes</option>
					<option value="">No</option>
				</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Page Webapp Icon</label>
				<div class="col-md-9">
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="input-group">
					    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
					    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="file"></span>
					    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
					  </div>
					</div>
				</div>
			</div>
		
			<div class="text-right">
				<button class="btn btn-primary" type="submit">Save</button>
					<input type="hidden" name="option" value="com_onepage"/>
					<input type="hidden" name="task" value="prototype.saveInfo"/>
					<input type="hidden" name="id" value="{{item.id}}"/>
			</div>
		</form>