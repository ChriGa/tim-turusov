<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

?>

<div class="panel panel-info ">
	<div class="panel-heading">
		{{groupUnit.title}}
		<div class="pull-right">
			<button class="btn btn-xs btn-default" ng-click="changeOrder($index,-1)" ng-hide="$first">
				<i class="fa fa-arrow-up" ></i>
			</button>
			<button class="btn btn-xs btn-default" ng-click="changeOrder($index,1)" ng-hide="$last">
				<i class="fa fa-arrow-down" ></i>
			</button>
			<a class=" btn btn-xs btn-default" ng-click="showMap(groupUnit)"><i class="fa fa-arrows-alt"></i></a>
			<a class="btn btn-xs btn-primary" ng-hide="showNav(rowUnit)" ng-click="edit($index)" ><i class="fa fa-pencil"></i></a>
			<a class="btn btn-xs btn-warning hidden" ng-click="selectType($index)" data-toggle="tooltip" title="Edit Source Code">{{groupUnit.type}}</a>

			<a class="btn btn-xs btn-primary hidden" ng-click="edit($index)"><i class="fa fa-pencil"></i></a>
			<a class="btn btn-xs btn-default" ng-click="publishEntry($index)">
				<i class="fa fa-check" ng-show="groupUnit.published"></i>
				<i class="fa fa-ban" ng-hide="groupUnit.published"></i>
			</a>
			
			<a class="btn btn-xs btn-danger hidden" ng-click="remove($index)"><i class="fa fa-times"></i></a>
		</div>
	</div>
</div>
