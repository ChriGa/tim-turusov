<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

JHtml::script('media/jsbt3/js/form.js');
JHtml::script('media/jsbt3/js/blockui.js');

//JHtml::script('media/jsbt3/js/bootstrap-editable.min.js');
//JHtml::stylesheet('media/jsbt3/css/bootstrap-editable.css');

JHtml::script('media/jsbt3/js/jasny-bootstrap.min.js');
JHtml::script('media/jsbt3/js/vex.combined.min.js');
JHtml::script('media/jsbt3/js/switchery.js');


JHtml::stylesheet('media/jsbt3/css/jasny-bootstrap.min.css');
JHtml::stylesheet('media/jsbt3/css/vex.css');
JHtml::stylesheet('media/jsbt3/css/vex-theme-os.css');
JHtml::stylesheet('media/jsbt3/css/switchery.css');


//JHtml::script('administrator/components/com_onepage/assets/js/plugins/angular-summernote.js');
//JHtml::script('administrator/components/com_onepage/assets/js/plugins/angular-nested-sortable.min.js');

JHtml::script('administrator/components/com_onepage/assets/js/plugins/codemirror.js');
JHtml::script('administrator/components/com_onepage/assets/js/plugins/xml.js');
JHtml::script('administrator/components/com_onepage/assets/js/plugins/active-line.js');

JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/codemirror.css');
//JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/monokai.css');

JHtml::script('media/jsbt3/js/summernote.min.js');
JHtml::stylesheet('media/jsbt3/css/summernote.css');
//JHtml::stylesheet('media/jsbt3/css/summernote-bs3.css');

$document = JFactory::getDocument();
$document->addScript("index.php?option=com_onepage&task=prototype.generateJS&layout=edit&id={$this->item->id}&f=".uniqid());

JHtml::stylesheet('administrator/components/com_onepage/assets/css/com.css');


$controller = ($this->item->type == 'item')?'elitem':'elitem';//$this->item->type;

JHtml::stylesheet('media/jsbt3/css/switchery.min.css');
JHtml::script('media/jsbt3/js/switchery.min.js');
JHtml::script('administrator/components/com_onepage/assets/js/plugins/ui-switchery.js');

// load lang
$groups = OnepageHelper::getGroupType();
$items = OnepageHelper::getItemType();
?>

  
<div ng-app="myApp">

	<div ng-controller="wholepage" class="theme">

		
		<?php echo $this->loadTemplate('header');?>
		
		<div class="container" style="width:100%" ng-show="(sp=='')">
		<div class="row" style="padding-top:30px;" ng-controller="<?php echo $controller;?>">
			<form id="elform" class="form-horizontal" role="form" action="index.php">
			<div class="col-md-8"><?php echo $this->loadTemplate('edit_'.$this->item->type);?></div>
			<div class="col-md-4">
				
				<?php echo $this->loadTemplate('panel');?>
				<?php echo $this->loadTemplate('info');?>
			
			</div>
			</form>
			
			<form id="aform" action="index.php">
			<input type="hidden" name="option" value="com_onepage"/>
			</form>
		</div>
		</div>
		


		<?php echo $this->loadTemplate('create');?>	
		<?php echo $this->loadTemplate('map');?>
		<?php echo $this->loadTemplate('type');?>
		<?php echo $this->loadTemplate('move');?>
	</div>
</div>

<?php JHtml::script('administrator/components/com_onepage/assets/js/prototype_edit.js');?>