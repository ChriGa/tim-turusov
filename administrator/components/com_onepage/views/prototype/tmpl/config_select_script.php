<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
?>
<form id="jsform" action="index.php" method="POST">
<div class="panel panel-default">
	<div class="panel-heading"> Select JS <button class="btn btn-primary btn-xs pull-right" type="submit"> <i class="fa fa-save"> </i></button></div>
	
	<table class="table">
    	<tbody>
    		<tr ng-repeat="item in js">
    			<td width="20"> <input type="checkbox"  name="value[]" value="{{item.name}}" ng-checked="item.checked"> </td>
    			<td>{{item.name}}</td>
    		</tr>
    	</tbody>
	</table>
	
</div>

<input type="hidden" name="option" value="com_onepage"/>
<input type="hidden" name="task" value="prototype.saveJsCss"/>
<input type="hidden" name="name" value="js_files"/>
<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
</form>

<form id="cssform" action="index.php" method="POST">
<div class="panel panel-default">
	<div class="panel-heading"> Select CSS <button class="btn btn-primary btn-xs pull-right" type="submit"> <i class="fa fa-save"> </i></button></div>
	
	<table class="table">
    	<tbody>
    		<tr ng-repeat="item in css">
    			<td width="20"> <input type="checkbox"  name="value[]" value="{{item.name}}" ng-checked="item.checked"> </td>
    			<td>{{item.name}}</td>
    		</tr>
    	</tbody>
	</table>
	
</div>

<input type="hidden" name="option" value="com_onepage"/>
<input type="hidden" name="task" value="prototype.saveJsCss"/>
<input type="hidden" name="name" value="css_files"/>
<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
</form>