<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

JHtml::script('media/jsbt3/js/form.js');
JHtml::script('media/jsbt3/js/blockui.js');

JHtml::script('media/jsbt3/js/bootstrap-editable.min.js');
JHtml::script('media/jsbt3/js/jasny-bootstrap.min.js');

JHtml::stylesheet('media/jsbt3/css/bootstrap-editable.css');
JHtml::stylesheet('media/jsbt3/css/jasny-bootstrap.min.css');

JHtml::script('administrator/components/com_onepage/assets/js/plugins/codemirror.js');
JHtml::script('administrator/components/com_onepage/assets/js/plugins/xml.js');
JHtml::script('administrator/components/com_onepage/assets/js/plugins/active-line.js');

JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/codemirror.css');
JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/monokai.css');

JHtml::script('media/jsbt3/js/summernote.min.js');
JHtml::stylesheet('media/jsbt3/css/summernote.css');

$document = JFactory::getDocument();
$postString = array();
$postString['option']='com_onepage';
$postString['task']='prototype.generateJS';
$postString['id']=$this->item->id;
$postString['layout'] = 'config';
$postString['f']= uniqid();
$document->addScript('index.php?'.http_build_query($postString));
JHtml::script('administrator/components/com_onepage/assets/js/pageconfig.js');


/*$formItem = (array)$this->item;
$formItemParams = (array)$formItem['params'];
unset($formItem['params']);
$formData = array_merge($formItem,$formItemParams);*/

?>




<div ng-app="myApp">
	<div ng-controller="wholepage" class="theme">
		<?php echo $this->loadTemplate('header');?>
		
		<div style="height:30px;"> </div>
		<div class=" container" ng-controller="wholepage">
			
			<div class="row">
				<div class="col-md-12 tabs-left">
					<ul class="nav nav-tabs ">
						<li class="active"><a href="#tab2" data-toggle="tab">JS & CSS</a></li>
						<li ><a href="#tab1" data-toggle="tab">Information</a></li>
						<li ><a href="#tab3" data-toggle="tab">Google Font</a></li>
					</ul>
										
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="tab2" ><?php echo $this->loadTemplate('script'); ?></div>
						<div class="tab-pane" id="tab1" ><?php echo $this->loadTemplate('params'); ?></div>
						<div class=" tab-pane" id="tab3" ><?php echo $this->loadTemplate('font'); ?></div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>