<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access.
defined('_JEXEC') or die;
$cp = $this->getModel()->getCurrentPage($this->item->id);
?>
<nav class="navbar navbar-default " role="navigation" style="margin-bottom: 0px;">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		
		<a class="navbar-brand" target="_blank" href="<?php echo JUri::root().'index.php?option=com_onepage&view=page&id='.$this->item->id;?>" style="color:#428BCA" ng-init="ptitle='<?php echo $this->item->title;?>'">{{ptitle}}</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
			
		</ul>
		<form class="navbar-form navbar-left" role="search" ng-submit="changePage()">
	        <div class="form-group" ng-init="pageid = '<?php echo $this->item->id;?>'">
	        	<label>Jump To:</label>
	          <select class="form-control" name="page" >
	          	<?php foreach($this->pitems as $page):?>
	          	<option value="<?php echo $page->id;?>"><?php echo $page->title;?></option>
	          	<?php endforeach;?>
	          </select>
	          <button class="btn btn-default">Go</button>
	        </div>
		</form>
		<ul class="nav navbar-nav navbar-right">
		
		<li>
			<a href="index.php?option=com_onepage&view=prototype&layout=edit&id=<?php echo $this->item->id;?>" id="returnLink"><span class="fa fa-level-up fa-lg text-info"></span> <strong class="text-info"><?php echo JText::_('JS_BACK');?></strong></a></li>
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>
