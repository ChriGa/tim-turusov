<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$params = $this->item->params;

$rows = $this->item->getPublishedChildren(false);
$checked = jsGetValueNo0($this->item->params,'row_show',array());
//$rows = htmlspecialchars( jsJSON::encode($rows) );
?>
<form class="form" method="POST" ng-submit="submit()" id="fontForm"> 


    	<button class="btn btn-success" type="button" ng-click="addFontLink()">ADD</button>
    	<button class="btn btn-primary" >Save</button>
    	<ul class="list-unstyled">
			<li ng-repeat="link in item.params.font_links" style="margin-top: 10px;">
				<div class="row">
				<div class="col-md-6">
				<input type="text" name="font_links[]" class="form-control " value="{{link.link}}"/>
				</div>
				<div class="col-md-1"><button class="btn btn-danger" type="button" ng-click="delLink($index)">x</button></div>
				</div>
			</li>

		</ul>
	

<div class="text-right">
		
		<input type="hidden" name="option" value="com_onepage"/>
		<input type="hidden" name="task" value="prototype.saveFont"/>
		<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
</div>
</form>