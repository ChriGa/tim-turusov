<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$cp = $this->getModel()->getCurrentPage($this->item->id);
$p = JTable::getInstance('Page','onepageTable');
$p->load($cp->id);
$items = $this->getModel()->getNodes($p);

$odd = array(); $even = array();
foreach($items as $k => $item)
{
	if($k%2)
	{
		$even[] = $item;
	}
	else
	{
		$odd[] = $item;
	}
}

$isItem = OnepageHelper::isItem($this->item);
?>

<div class="container" ng-show="smove" style="padding-bottom: 20px;">

<div class="row">
	<h2>Move To:</h2><hr/>
	<div class="col-md-2 ">
		<div>
			<i class="fa fa-square" style="color:#BEA1CC"></i> Editing
		</div>
		<div>
			<i class="fa fa-square" style="color:#D9534F"></i> Section
		</div>
		<div>
			<i class="fa fa-square" style="color:#F0AD4E"></i> Level2 Element
		</div>
		<div>
			<i class="fa fa-square" style="color:#39B3D7"></i> Level3 Element
		</div>
	</div>
	<div class="col-md-8 ">
<?php 
	if($this->item->level > 3):
	foreach($items as $first):
		$fc = $this->getModel()->getNodes($first);
		$active = ($first->id == $this->item->id)?true:false;
?>
	
	<div class="map-unit first " style="margin-top:0px">
		<div class="btn btn-danger btn-block btn-lg <?php echo $active?'uactive':'';?>" ng-click="moveTo(<?php echo $first->id?>,$event)" style="text-align: left;padding-left: 10px"><?php echo $first->title;?></div>	
		<div class="row" style="padding:0 10px">
			<?php 
			if($this->item->level > 4 || ($this->item->level == 4 && $isItem)):
				foreach($fc as $second):
					$isGroup = OnepageHelper::isGroup($second);
					if(!$isGroup) continue;
					$sc = $this->getModel()->getNodes($second);
					$active = ($second->id == $this->item->id)?true:false;
					$params = $second->params;
					$offset = jsGetValueNo0($params, 'offset',0);
					$wrap_cls = $offset?" col-md-offset-{$offset}":'';
			?>
				<div class="col-md-<?php echo $second->md;?> <?php echo $wrap_cls;?>" style="">
					<div class="map-unit second " >
						<div class="btn btn-warning btn-block btn-lg <?php echo $active?'uactive':'';?>" ng-click="moveTo(<?php echo $second->id?>,$event)"><?php echo $second->title;?></div>	
						
					</div>
				</div>
			
			<?php endforeach;endif;?>
		</div>	
	</div>
	<?php endforeach;endif;?>
	</div>
</div>

<hr/>
		<div class="row">
			<div class="col-md-12 text-right">
				<button class="btn btn-default btn-lg" ng-click="showPanel('')">Close</button>
			</div>
		</div>
		
		
	<form method="POST" id="moveForm" action="index.php">
		<input type="hidden" name="option" value="com_onepage"/>
		<input type="hidden" name="task" value="prototype.moveTo"/>
		<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
	</form>
</div>