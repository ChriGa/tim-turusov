<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$list = onepageHelper::getGroupType();
?>



<form id="addForm" class="form" role="form" ng-submit="add()">
  <div class="form-group hidden">
  	<input class="form-control" type="text" name="title" ng-model="title" placeholder="<?php echo JText::_('JS_TITLE');?>"/>
  </div>
  <div class="form-group hidden">
  	<select class="form-control" id="" name="type" ng-model="type">
  		<option value="row">Row</option>
  		<?php foreach($list as $a):?>
	  		<option value="<?php echo $a['value'];?>"><?php echo $a['text'];?></option>
	  	<?php endforeach;?>
  	</select>
  </div>
  
  <button type="submit" class="hidden btn btn-primary btn-block"><span class="fa fa-plus "></span> <?php echo JText::_('JS_ADD'); ?></button>
  
  <button class="btn btn-primary btn-block" type="button" data-toggle="button" ng-click="enableSort()"> Sort </button>
  
  <input type="hidden" name="option" value="com_onepage">
  <input type="hidden" name="task" value="page.add">
  <input type="hidden" name="id" value="<?php echo $this->item->id;?>">
</form>