<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$list = onepageHelper::getGroupType();
$list1 = onepageHelper::getItemType();
?>

<!-- Modal -->
<div class="modal fade" id="itemGroupModal" tabindex="-1" role="dialog" aria-labelledby="itemGroupModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="itemGroupModalLabel">Change Type - {{elparams.title}}</h4>
      </div>
      <div class="modal-body" >
      	<form id="itemGroupForm" class="form-horizontal" role="form" ng-submit="changeItemType()">
		  	<div class="form-group">
			  	<label class="col-sm-4">Title</label>
		  		<div class="col-sm-8">
		  			<input type="text" class="form-control"  placeholder="<?php echo JText::_('JS_TITLE'); ?>" name="title" ng-model="rtItem.title">
		  		</div>
			</div>
		  	<div class="form-group">
			  	<label class="col-sm-4"><?php echo JText::_('JS_TYPE');?></label>
		  		<div class="col-sm-8">
		  			<select class="form-control" name="type" ng-model="rtItem.type">
		  				<option disabled> ----- Group Type ----- </option>
		  			<?php foreach($list as $a):?>
				  		<option value="<?php echo $a['value'];?>"><?php echo $a['text'];?></option>
				  	<?php endforeach;?>
				  		<option disabled> ----- Item Type -----</option>
				  	<?php foreach($list1 as $a):?>
				  		<option value="<?php echo $a['value'];?>"><?php echo $a['text'];?></option>
				  	<?php endforeach;?>
				  	</select>
		  		</div>
			</div>
	
		  	<div ng-include=" rtItem.type+'.html' "></div>
		  	
		  <button type="submit" class="btn btn-primary btn-block"><span class="fa fa-save "></span> <?php echo JText::_('JS_SAVE'); ?></button>
		  
		  <input type="hidden" name="option" value="com_onepage">
		  <input type="hidden" name="task" value="prototype.changeType">
		  <input type="hidden" name="id" value="{{rtItem.id}}">
		</form>
      </div>
    </div>
  </div>
</div>