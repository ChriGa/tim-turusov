<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

?>

<div class="panel panel-warning ">
	<div class="panel-heading">
		<div><div class="pull-left">{{rowUnit.title}}</div>
		<div class="text-right">
			<button class="btn btn-xs btn-default" ng-click="changeOrder(rowUnit.id,-1)" ng-hide="rowUnit.first">
				<i class="fa fa-arrow-up" ></i>
			</button>
			<button class="btn btn-xs btn-default" ng-click="changeOrder(rowUnit.id,1)" ng-hide="rowUnit.last">
				<i class="fa fa-arrow-down" ></i>
			</button>
		
			<a class=" btn btn-xs btn-default" ng-click="showMap(rowUnit)"><i class="fa fa-arrows-alt"></i></a>
			<a class="btn btn-xs btn-default" ng-click="publishEntry(rowUnit.id)">
				<i class="fa fa-check" ng-show="rowUnit.published"></i>
				<i class="fa fa-ban" ng-hide="rowUnit.published"></i>
			</a>
			<a class=" btn btn-xs btn-danger" ng-click="remove(rowUnit.id)"><i class="fa fa-times"></i></a>
		</div>
		</div>
	</div>
	
	<div class="panel-body" style="position:relative;" ng-controller="group" ng-init="groupUnits = rowUnit.children;groupId=rowUnit.id">
		
		<form id="" class="form-horizontal" role="form" action="index.php">
			<select class="form-control input-sm" ng-model="rowUnit.md" ng-change="changeGrid($index)">
				<?php for($i=3;$i<=12;$i++):?>
				<option value="<?php echo $i;?>"><?php echo $i;?></option>
				<?php endfor;?>
			</select>
		</form>
		
		<div ng-repeat="groupUnit in groupUnits" style="margin-top:10px;">
			<div ng-include="'groupUnit.html' "></div>
		</div>
	</div>
</div>
