<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$tpls = OnepageHelper::getTpls($this->item->type);
?>

<div class="panel-group" id="accordion-main">
	<div class="panel panel-info" style="margin-top:10px;">
		<div class="panel-heading ">
			<a data-toggle="collapse" data-parent="#accordion-main" href="#apinfo">
	        	<h4 class="panel-title">General Information</h4>
	        </a>
		</div>
		<div id="apinfo" class="panel-collapse collapse in">
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-4 control-label">Title</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="title" ng-model="item.title"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Published?</label>
				<div class="col-md-8">
					<select class="form-control" name="published"  ng-model="item.published">
						<option value="1"><?php echo JText::_('JS_YES');?></option>
						<option value="0"><?php echo JText::_('JS_NO');?></option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">ID</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="wrap_id" placeholder="op-{{item.type}}-{{item.id}}" ng-model="item.params.wrap_id"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Class</label>
				<div class="col-md-8">
					<textarea class="form-control" name="wrap_cls" placeholder="Class in the wrap div" ng-model="item.params.wrap_cls"></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Template</label>
				<div class="col-md-8">
					<select class="form-control" name="html"  ng-model="item.html">
						<option value="">Default</option>
						<?php foreach($tpls as $tpl):?>
						<option value="<?php echo $tpl->code;?>"><?php echo $tpl->title;?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
			
			<?php if($this->item->level == 4):?>
			<div class="form-group">
				<label class="col-md-4 control-label">Col-md-</label>
				<div class="col-md-8">
					<select class="form-control" name="md"  ng-model="item.md">
						<?php for($i=3;$i<=12;$i++):?>
						<option value="<?php echo $i;?>"><?php echo $i;?></option>
						<?php endfor;?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label">Offset-</label>
				<div class="col-md-8">
					<select class="form-control" name="offset"  ng-model="item.params.offset">
						<?php for($i=0;$i<=4;$i++):?>
						<option value="<?php echo $i;?>"><?php echo $i;?></option>
						<?php endfor;?>
					</select>
				</div>
			</div>
			<?php endif;?>
			
			<input type="hidden" name="sm" value="{{item.md}}"/>
			<input type="hidden" name="xs" value="{{item.md}}"/>
			<input type="hidden" name="lg" value="{{item.md}}"/>
		</div>
		</div>
	</div>
	
	<div class="panel panel-info" style="margin-top:10px;">
		<div class="panel-heading ">
			<a data-toggle="collapse" data-parent="#accordion-main" href="#apanimate">
	        	<h4 class="panel-title">Animation</h4>
	        </a>
		</div>
		<div id="apanimate" class="panel-collapse collapse ">
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-4 control-label">Animation</label>
				<div class="col-md-8">
					<select class="form-control" name="animation"  ng-model="item.params.animation">
						<option value="">None</option>
						<optgroup label="Bouncing Entrances">
				          <option value="bounceIn">bounceIn</option>
				          <option value="bounceInDown">bounceInDown</option>
				          <option value="bounceInLeft">bounceInLeft</option>
				          <option value="bounceInRight">bounceInRight</option>
				          <option value="bounceInUp">bounceInUp</option>
				        </optgroup>
						<optgroup label="Fading Entrances">
				          <option value="fadeIn">fadeIn</option>
				          <option value="fadeInDown">fadeInDown</option>
				          <option value="fadeInDownBig">fadeInDownBig</option>
				          <option value="fadeInLeft">fadeInLeft</option>
				          <option value="fadeInLeftBig">fadeInLeftBig</option>
				          <option value="fadeInRight">fadeInRight</option>
				          <option value="fadeInRightBig">fadeInRightBig</option>
				          <option value="fadeInUp">fadeInUp</option>
				          <option value="fadeInUpBig">fadeInUpBig</option>
				        </optgroup>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Delay (s)</label>
				<div class="col-md-8">
					<div class="input-group">
					  <input type="text" class="form-control" name="delay" ng-model="item.params.delay"/>
					  <span class="input-group-addon">s</span>
					</div>
					
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
