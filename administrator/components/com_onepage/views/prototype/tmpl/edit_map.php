<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
$cp = $this->getModel()->getCurrentPage($this->item->id);
$p = JTable::getInstance('Page','onepageTable');
$p->load($cp->id);
$items = $this->getModel()->getNodes($p);

$odd = array(); $even = array();
foreach($items as $k => $item)
{
	if($k%2)
	{
		$even[] = $item;
	}
	else
	{
		$odd[] = $item;
	}
}
?>

<style>

</style>

<div class="container" ng-show="sm" style="padding-bottom: 20px;">

<div class="row">
	<h2>Jump To:</h2><hr/>
	<div class="col-md-2 ">
		<div>
			<i class="fa fa-square" style="color:#BEA1CC"></i> Editing
		</div>
		<div>
			<i class="fa fa-square" style="color:#D9534F"></i> Section
		</div>
		<div>
			<i class="fa fa-square" style="color:#F0AD4E"></i> Level2 Element
		</div>
		<div>
			<i class="fa fa-square" style="color:#39B3D7"></i> Level3 Element
		</div>
	</div>
	<div class="col-md-8 ">

	
	<div class="map-unit first " style="margin-top:0px" ng-repeat="row in list">
		<div class="btn btn-danger btn-block btn-lg {{activeClass(row)}}" ng-click="mapClick(row.id,$event)" style="text-align: left;padding-left: 10px">{{row.title}}</div>	
			<div class="row" style="padding:0 10px" ng-repeat="subrow in row.children">
			
				<div class="col-md-{{second.md}} col-md-offset-{{second.params.offset}}" ng-class="" ng-repeat="second in subrow">
					<div class="map-unit second " >
						<div class="btn btn-warning btn-block btn-lg {{activeClass(second)}}" ng-click="mapClick(second.id,$event)">{{second.title}}</div>
									
						<div class="row">
							<div class="col-md-12" ng-repeat="third in second.children">
								<div class="map-unit third " >
									<div class="btn btn-info btn-block btn-lg {{activeClass(third)}}" ng-click="mapClick(third.id,$event)">{{third.title}}</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
	</div>

	</div>
	
	<div class="col-md-2">
		<button class="btn btn-default btn-lg btn-block" ng-click="showPanel('')">Close</button>
	</div>
</div>

</div>