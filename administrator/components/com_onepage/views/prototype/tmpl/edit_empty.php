<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO 
 */
// no direct access
defined('_JEXEC') or die;

?>

<style>
.emptyPage {
	height:400px;
	background-color:#1F8DD1;
	color:#fff;
	padding-top: 100px;
}

.emptyPage .text{
	background-color:#1C7EBB;
	padding:50px;
}

.emptyPage h1{
	font-size:50x;
	
}
</style>

<div class="emptyPage">
	<div class="text">
	<h1 class="text-center">
		No Need To Set Parameters
	</h1>
	</div>
</div>
