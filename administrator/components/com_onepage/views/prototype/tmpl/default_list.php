<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

// load Template
foreach($this->plugins as $result)
{
	//$this->addTemplatePath(JPATH_PLUGINS."/opeltype/{$result['value']}/tpl");
	//echo $this->loadTemplate('tpl_'.$result['value']);
}
?>
<?php echo $this->loadTemplate('template'); ?>

<style>
.panel-danger > .panel-heading {
	color: #fff;
	background-color: #D9534F;
	border-color: #D9534F;
}

.panel-warning > .panel-heading {
	color: #fff;
	background-color: #F0AD4E;
	border-color: #EEA236;
}

.panel-info > .panel-heading {
	color: #fff;
	background-color: #5BC0DE;
	border-color: #5BC0DE;
}
</style>

<div class="row" id="panel" ui-tree="treeOptions">
	<ul class="col-md-12 list-unstyled" style="padding-bottom:30px;"  ng-model="list" ui-tree-nodes="">
		
		<li ng-repeat="item in list" ui-tree-node >
			
			<div class="panel panel-danger" >
				<div class="panel-heading" >
					<div class="noselect">
					<i ui-tree-handle class="fa fa-sort " style="color:#fff" ng-show="sorted"></i> {{item.id}} {{item.title}}
					<div class="pull-right">
						<a class="btn btn-xs btn-success hidden" ng-click="openConfig($index)"><i class="fa fa-gear"></i></a>
						<a class="hidden btn btn-xs btn-warning" ng-click="selectType($index)">{{item.type}}</a>
						<a class="btn btn-xs btn-default jstips hidden" data-toggle="collapse" href="#el-panelbody-{{item.id}}" data-toggle="tooltip" data-placement="bottom" title="Show/Hide"><i class="fa fa-minus"></i></a>
						
						<a class="btn btn-xs btn-default jstips" ng-click="publishEntry($index)" data-toggle="tooltip" title="Publish/Unpublish">
							<i class="fa fa-check" ng-show="item.published"></i>
							<i class="fa fa-ban" ng-hide="item.published"></i>
						</a>
						<a class="btn btn-xs btn-warning jstips hidden" ng-click="edit($index)" data-toggle="tooltip" title="Edit Source Code"><i class="fa fa-code"></i></a>
						<a class="hidden btn btn-xs btn-danger" ng-click="removeI($index)"><i class="fa fa-times"></i></a>
					</div>
					</div>
				</div>
				<div class="panel-collapse collapse in" id="el-panelbody-{{item.id}}" ng-hide="sorted">
				<div class="panel-body" style="position:relative;padding-top:20px;" ng-controller="row" ng-init="rowUnits = item.children;rowId=item.id">
					<nav class="hidden  navbar navbar-default" role="navigation" style="position:absolute;top:0;left:0;right:0;min-height:30px;padding-right:15px;">

					<div class="pull-right">
						<button class="btn btn-xs btn-success" style="margin:4px 0px;" data-toggle="collapse" ng-click="add()">
							<i class="fa fa-plus"></i>
						</button>
			 			<button class="btn btn-xs btn-success hidden" style="margin:4px 0px;">
							<i class="fa fa-save"></i>
						</button>
			  		</div>
			
					</nav>
					
					<div class="row" ng-repeat="rows in rowUnits">
						<div class="col-md-{{rowUnit.md}} col-md-offset-{{rowUnit.params.offset}} " id="unit_{{rowUnit.id}}" ng-repeat="rowUnit in rows">
							<div ng-include="'unit.html' "></div>
						
						</div>
					</div>
				</div>
				</div>
			</div>
		</li>
		
	</ul>
	<form id="adminForm" action="index.php">
		<input type="hidden" name="option" value="com_onepage"/>
	</form>
</div>