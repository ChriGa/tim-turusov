<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;
/*
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

//
$document = JFactory::getDocument();
JHtml::script('administrator/components/com_onepage/helpers/js/page.js');


$formItem = (array)$this->item;
$formItemParams = (array)$formItem['params'];
unset($formItem['params']);
$formData = array_merge($formItem,$formItemParams);
*/
//JHtml::script('media/jsbt3/js/jquery-ui.min.js');
JHtml::script('media/jsbt3/js/angular-sanitize.min.js');
JHtml::script('media/jsbt3/js/form.js');
JHtml::script('media/jsbt3/js/blockui.js');

//JHtml::script('media/jsbt3/js/bootstrap-editable.min.js');
JHtml::script('media/jsbt3/js/jasny-bootstrap.min.js');
JHtml::script('media/jsbt3/js/vex.combined.min.js');

//JHtml::stylesheet('media/jsbt3/css/bootstrap-editable.css');
JHtml::stylesheet('media/jsbt3/css/jasny-bootstrap.min.css');
JHtml::stylesheet('media/jsbt3/css/vex.css');
JHtml::stylesheet('media/jsbt3/css/vex-theme-os.css');

//JHtml::script('media/jsbt3/js/summernote.min.js');
//JHtml::stylesheet('media/jsbt3/css/summernote.css');
//JHtml::script('administrator/components/com_onepage/assets/js/plugins/angular-summernote.js');
JHtml::script('administrator/components/com_onepage/assets/js/plugins/angular-ui-tree.min.js');
//JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/angular-ui-tree.min.css');

$document = JFactory::getDocument();
$document->addScript("index.php?option=com_onepage&task=prototype.generateJS&id={$this->item->id}&f=".uniqid());

JHtml::stylesheet('administrator/components/com_onepage/assets/css/com.css');
JHtml::script('administrator/components/com_onepage/assets/js/prototype.js');
?>
<script>
var getReturn = function() {return '<?php echo $this->return;?>';}
</script>
  
<div ng-app="myApp">

	<div ng-controller="wholepage" class="theme">
		<?php echo $this->loadTemplate('header');?>
		<?php echo $this->loadTemplate('move');?>
		<div class="container" style="width:100%" ng-hide="sm">
		<div class="row" style="padding-top:30px;">
			<div class="col-md-1"></div>
			<div class="col-md-8"><?php echo $this->loadTemplate('list');?></div>
			<div class="col-md-3">
				<?php echo $this->loadTemplate('panel');?>
			</div>
		</div>
		</div>
		
		<?php echo $this->loadTemplate('modal');?>
		<?php echo $this->loadTemplate('rowtype');?>
		<?php echo $this->loadTemplate('itemtype');?>
		<?php echo $this->loadTemplate('grouptype');?>
	</div>
</div>