<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

$groups = OnepageHelper::getGroupType();
$items = OnepageHelper::getItemType();
$isGroup =false;
?>

<div class="container" ng-show="st">
	
	<div class="row">
		<div class="row">
			<div class="col-md-11"><h2>Change Type</h2></div>
			<div class="col-md-1 text-right">
				<button class="btn btn-danger" ng-click="showPanel('')" style="margin-top:20px;"> Close </button>
			</div>
		</div>
		<hr/>
		<div class="row">
		<?php if($this->item->level == 4):?>
			<div class="col-md-12">
				<h3>Group Type</h3>
			<?php foreach($groups as $item): if($item['value'] == $this->item->type) {$isGroup = true;continue;}?>
				<button class="btn btn-default btn-create"  ng-click="changeType('<?php echo $item['value'];?>')"><?php echo $item['text'];?></button>
			<?php endforeach;?>
			</div>
		<?php endif;?>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<h3>Item Type</h3>
			<?php foreach($items as $item): if($item['value'] == $this->item->type) {continue;}?>
			<button class="btn btn-default btn-create" ng-click="changeType('<?php echo $item['value'];?>')"><?php echo $item['text'];?></button>
			<?php endforeach;?>
			</div>
		</div>
	
		<form method="POST" id="typeForm" action="index.php">
			<input type="hidden" name="option" value="com_onepage"/>
			<input type="hidden" name="task" value="prototype.changeElType"/>
			<input type="hidden" name="id" value="<?php echo $this->item->id;?>"/>
		</form>
	</div>
</div>