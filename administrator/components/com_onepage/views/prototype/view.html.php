<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;
class OnepageViewPrototype extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return	void
	 */
	public function display($tpl = null)
	{
		
		OnepageHelper::initPage();
		
		$id = JRequest::getInt('id');
		$model = $this->getModel();
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		//$tEl->rebuild(1);
		
		$tEl->load($id);
		$this->item = $tEl;
		
		$layout = JRequest::getCmd('layout','default');
		$this->setLayout($layout);

		if($layout == 'default')
		{
			//$dispather = jsGetDispatcher();
			//JPluginHelper::importPlugin('opeltype');
			$this->plugins = OnepageHelper::getTypes();
		}
		elseif($layout == 'edit')
		{
			$this->addTemplatePath(JPATH_PLUGINS."/opeltype/{$tEl->type}/tpl");
			$this->pitems = $model->getPageItems($id);
			
			$this->cp = $model->getCurrentPage($id);
			OnepageHelper::logEditPath($this->cp->id, $id);
		}
		elseif($layout == 'source')
		{
			$this->addTemplatePath(JPATH_PLUGINS."/opeltype/{$tEl->type}/tpl");
			$this->pitems = $model->getPageItems($id);
		}
		else 
		{
			$this->cp = $model->getCurrentPage($id);
		}
		
		$pages = $model->getPages();
		$this->assignRef('pages', $pages);
		
		$uri  = JFactory::getURI();
		$this->return = base64_encode( $uri->toString() );
		//js
		/*JHtml::script($com.DS.'tinymce'.DS.'tinymce.min.js');
		JHtml::script($com.DS.'js'.DS.'jquery.form.js');
		JHtml::script($com.DS.'js'.DS.'jquery.blockUI.js');
		JHtml::script($com.DS.'js'.DS.'jquery.loadJSON.js');
		
		JHtml::script($com.DS.'js'.DS.'modernizr.custom.js');
		JHtml::script($com.DS.'js'.DS.'jquery.pin.min.js');
		JHtml::script($com.DS.'js'.DS.'grid.js');*/
		//JHtml::stylesheet($com.DS.'css'.DS.'theme.css');
		//JHtml::stylesheet('administrator'.DS.'components'.DS.'com_onepage'.DS.'views/tmpl/css'.DS.'jquery.shadow.css');
		//$this->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR.'/views/tmpl');
		
		parent::display($tpl);
	}

	protected function init()
	{
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_menu');
		$query->where('`level` = "1"');
		$obj = jsDB::loadObj($query,'obj');
		
		$updated = true;
		$tEl = JTable::getInstance('Menu','OnepageTable');
		if( empty($obj) )
		{
			/*$tEl->set('title', 'Root');
			$tEl->set('level', 1);
			$tEl->set('parent_id', 0);
			$tEl->setLocation( 0, 'last-child' );
			$updated = $tEl->store();*/
		
			$query = $db->getQuery(true);
			$query->insert('#__onepage_menu');
			$query->columns('`title`,`level`,`parent_id`,`lft`,`rgt`');
			$query->values("'Root',1,0,0,1");
			$db->setQuery($query);
			$db->query();
			$tEl->load(1);
		}
		else
		{
			$tEl->load($obj->id);
		}
		
		if(!$updated)
		{
			return;
		}
		
		
		$parent_id = $tEl->getRootId();
		//
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_menu');
		$query->where('`parent_id` = "'.$parent_id.'"');
		$query->where('`level` = "2"');
		$objs = jsDB::loadObjs($query,'obj');
		
		$tEl = JTable::getInstance('Language','OnepageTable');
		if(count($objs) < 1)
		{
			
			$tEl->setLocation( $parent_id, 'last-child' );
			$tEl->set('title', 'English');
			$tEl->set('parent_id', $parent_id);
			$tEl->set('level', 2);
			$tEl->set('code', 'en');
			$updated = $tEl->store();
			
			if($updated)
			{
				$tEl->createDefaultBlocks();
			}
			
		}
		else
		{
			
		}
		
		return;
	}
	
	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		//$canDo	= ContentHelper::getActions($this->state->get('filter.category_id'));
		$user		= JFactory::getUser();
		JToolBarHelper::title(JText::_('COM_CONTENT_ARTICLES_TITLE'), 'article.png');

		JToolBarHelper::addNew('menu.add');
		JToolBarHelper::editList('menu.edit');
		JToolBarHelper::divider();
		JToolBarHelper::publish('articles.publish', 'JTOOLBAR_PUBLISH', true);
		JToolBarHelper::unpublish('articles.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		JToolBarHelper::divider();
		JToolBarHelper::deleteList('', 'articles.delete', 'JTOOLBAR_EMPTY_TRASH');
		JToolBarHelper::help('JHELP_CONTENT_ARTICLE_MANAGER');
	}
	
	public function generateView()
	{
		
	}
}
