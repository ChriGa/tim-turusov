<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
// No direct access.
defined('_JEXEC') or die;

OnepageHelper::initPage();

$com = 'administrator/components/com_onepage/assets/';

JHtml::script('media/jsbt3/js/form.js');
JHtml::script('media/jsbt3/js/blockui.js');

JHtml::script('media/jsbt3/js/bootstrap-editable.min.js');
JHtml::stylesheet('media/jsbt3/css/bootstrap-editable.css');

JHtml::script('media/jsbt3/js/jasny-bootstrap.min.js');
JHtml::stylesheet('media/jsbt3/css/jasny-bootstrap.min.css');

JHtml::script($com.'/js/config.js');




//$plgs = $this->getModel()->getPlugins();
?>

<div class=" theme" id="config" ng-app="myApp">
<?php echo $this->loadTemplate('header');?>
	<div style="height:30px;"> </div>
	<div class=" container" ng-controller="wholepage">
	<div class="row">
		<div class="col-md-12 tabs-left">
			<ul class="nav nav-tabs ">
				<li class="active"><a href="#tab1" data-toggle="tab"><?php echo JText::_('JS_GLOBAL_SETTING'); ?></a></li>
				<li><a href="#tab2" data-toggle="tab"><?php echo JText::_('3RD Party API'); ?></a></li>
				<li class=""><a href="#tab3" data-toggle="tab"><?php echo JText::_('Social Information'); ?></a></li>
			</ul>
								
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="tab1" ><?php echo $this->loadTemplate('tab_global');?></div>
				<div class="tab-pane" id="tab2" style="padding-top:0px;">
					<?php echo $this->loadTemplate('tab_ga');?>
					<?php echo $this->loadTemplate('tab_mc');?>
					<?php echo $this->loadTemplate('tab_mixpanel');?>
				</div>
				<div class="tab-pane " id="tab3">
					<?php echo $this->loadTemplate('tab_social');?>
				</div>
			</div>
			
		</div>
	</div>
	
</div>
</div>