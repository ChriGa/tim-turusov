<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// No direct access.
defined('_JEXEC') or die;

$tConfig = jsAppConfig::getInstance();

?>

<div class="control-group">
	<label  class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_BOOTSTRAP_VERSION');?>:</label>
	<div class="controls">
		<select name="bootstrap_version">
			<option value="3" <?php echo ($tConfig->get('bootstrap_version',2) == 3)?'selected':'';?>>3.0</option>
		</select>
	</div>
</div>


<div class="control-group">
	<label  class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_DISABLE_THEME');?>:</label>
	<div class="controls">
		<select name="disable_theme">
			<option value="1" <?php echo $tConfig->get('disable_theme',0)?'selected':'';?>><?php echo JText::_('JYES');?></option>
			<option value="0" <?php echo $tConfig->get('disable_theme',0)?'':'selected';?>><?php echo JText::_('JNO');?></option>
		</select>
	</div>
</div>

<div class="control-group">
	<label  class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_BOOTSTRAPTHEME');?>:</label>
	<div class="controls">
		<input type="file" name="theme">
	</div>
</div>

<div class="control-group">
	<label  class="control-label"><?php echo JText::_('COM_ONEPAGE_LABEL_FAVICON');?>:</label>
	<div class="controls">
		<input type="file" name="favicon">
	</div> 
</div>

<div class="alert alert-info">
	<i class="icon-info-sign"></i> 
	<span>
		<?php echo JText::_('COM_ONEPAGE_TEXT_FAVICON_TIPS');?>
	</span> 
</div> 