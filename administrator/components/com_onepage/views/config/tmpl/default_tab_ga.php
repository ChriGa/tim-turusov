<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
// No direct access.
defined('_JEXEC') or die;

$tConfig = $this->config;
$uri = JFactory::getURI();

$yes = array();
$yes[] = array('value'=>1,'text'=>JText::_('JS_YES'));
$yes[] = array('value'=>0,'text'=>JText::_('JS_NO'));
$yes = htmlspecialchars( jsJSON::encode($yes) );
?>
<div class="row" >
	<div class="col-md-12" >
		<table class="table">
			<thead>
			<tr >
				<th colspan="2"><h3><?php echo JText::_('JS_GOOGLE_ANALYTICS');?></h3></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td width="30%"><?php echo JText::_('JS_ENABLE');?>?</td>
				<td>
					<a class="xeditable" id="ga_enable" data-type="select" data-pk="1" data-value="<?php echo $tConfig->get('ga_enable');?>" data-source="[{value: 1, text: '<?php echo JText::_('JYES');?>'}, {value: 0, text: '<?php echo JText::_('JNO');?>'}]" ></a>
				</td>
			</tr>
			
			<tr>
				<td><?php echo JText::_('JS_CODE');?>?</td>
				<td>
					<a class="xeditable" id="ga_code" data-type="text" data-pk="1"><?php echo  $tConfig->search('ga_code');?></a>
				</td>
			</tr>
			
			<tr>
				<td><?php echo JText::_('JS_WEBSITE');?>?</td>
				<td>
					<a class="xeditable" id="ga_website" data-type="text" data-pk="1"><?php echo $tConfig->search('ga_website',JURI::root());?></a>
				</td>
			</tr>
			</tbody>
		</table>
		
		
	</div>
</div>		