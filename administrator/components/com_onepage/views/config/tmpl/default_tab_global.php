<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
// No direct access.
defined('_JEXEC') or die;

$tConfig = jsAppConfig::getInstance('dlocker');

$yes = array();
$yes[] = array('value'=>1,'text'=>JText::_('JS_YES'));
$yes[] = array('value'=>0,'text'=>JText::_('JS_NO'));
$yes = htmlspecialchars( jsJSON::encode($yes) );
?>
<div class="row" >
	<div class="col-md-12" >
		<form class="form-horizontal" method="POST" action="index.php" id="iconForm">
			<div class="form-group">
				<label class="control-label col-md-3">Favicon(.ico only)</label>
				<div class="col-md-5">
					<div class="fileinput fileinput-new" data-provides="fileinput">
					  <div class="input-group">
					    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
					    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="favicon"></span>
					    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
					  </div>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Apple Webapp Icon (144 x 144)</label>
				<div class="col-md-5">
					<div class="fileinput fileinput-new" data-provides="fileinput">
					  <div class="input-group">
					    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
					    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="appicon144"></span>
					    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
					  </div>
					</div>
				</div>
			</div>
			
			<div class="form-group hidden">
				<label class="control-label col-md-3">Enable Default Style</label>
				<div class="col-md-5" ng-init="enable_default_style='<?php echo $this->config->get('enable_default_style',1);?>'">
				<select name="enable_default_style" ng-model="enable_default_style" class="form-control">
					<option value="1">Yes</option>
					<option value="">No</option>
				</select>
				</div>
			</div>
			
			
			
			<div class="form-group">
				<div class="col-md-8 text-right">
					<button class="btn btn-primary">Save</button>
				</div>
			</div>
			
			<input type="hidden" name="option" value="com_onepage"/>
			<input type="hidden" name="task" value="uploadIcon"/>
		</form>
		
		
	</div>
</div>		