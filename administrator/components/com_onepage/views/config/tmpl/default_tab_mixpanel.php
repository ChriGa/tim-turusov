<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
// No direct access.
defined('_JEXEC') or die;

$tConfig = $this->config;

?>
<div style="height:10px;" ></div>
<div class="row" >
	<div class="col-md-12" >
		<table class="table">
			<thead>
			<tr >
				<th colspan="2"><h3><?php echo JText::_('JS_MIXPANEL');?></h3></th>
			</tr>
			</thead>
			<tbody>
		
			<tr>
				<td width="30%"><?php echo JText::_('JS_APIKEY');?>(Token)</td>
				<td>
					<a class="xeditable" id="mixpanel_apikey" data-type="text" data-pk="1" ><?php echo  $tConfig->get('mixpanel_apikey');?></a>
				</td>
			</tr>
			
			</tbody>
		</table>
		
		
	</div>
</div>		