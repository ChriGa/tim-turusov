<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */
// No direct access.
defined('_JEXEC') or die;

$tConfig = $this->config;
$uri = JFactory::getURI();

$yes = array();
$yes[] = array('value'=>1,'text'=>JText::_('JS_YES'));
$yes[] = array('value'=>0,'text'=>JText::_('JS_NO'));
$yes = htmlspecialchars( jsJSON::encode($yes) );
?>
<div class="row" >
	<div class="col-md-12" >
		<table class="table">
			<thead>
			<tr >
				<th colspan="2"><h3><?php echo JText::_('Legacy Setting');?></h3></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td width="30%"><?php echo JText::_('Top Bar Published');?>?</td>
				<td>
					<a class="xeditable" id="topmenu_published" data-type="select" data-pk="1" data-value="<?php echo $tConfig->get('topmenu_published');?>" data-source="<?php echo $yes;?>" ></a>
				</td>
			</tr>
			
			<tr>
				<td><?php echo JText::_('Top Bar Fixed');?>?</td>
				<td>
					<a class="xeditable" id="topmenu_fixed" data-type="select" data-pk="1" data-value="<?php echo $tConfig->get('topmenu_fixed');?>" data-source="<?php echo $yes;?>"></a>
				</td>
			</tr>
			
			<tr>
				<td width="30%"><?php echo JText::_('Footer Published');?>?</td>
				<td>
					<a class="xeditable" id="footer_published" data-type="select" data-pk="1" data-value="<?php echo $tConfig->get('footer_published');?>" data-source="<?php echo $yes;?>" ></a>
				</td>
			</tr>
			</tbody>
		</table>
		
		
	</div>
</div>		