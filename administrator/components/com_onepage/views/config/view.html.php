<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
defined('_JEXEC') or die;

/**
 */
class OnepageViewConfig extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$id = JRequest::getInt('id');
		
		//$this->setLayout('edit');
		


		JRequest::setVar('tmpl','component');
		
		
		//$model = $this->getModel('');
		//$this->item = $model->getItem($id);
		//$this->form		= $this->get('Form');
		// Initialiase variables.
		/*$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');
		//$this->canDo	= ContentHelper::getActions($this->state->get('filter.category_id'));

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}*/

		//$this->addToolbar();
		$this->config = jsAppConfig::getInstance('onepage');
		
		//$this->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR.'/views/tmpl');
		
		
		//js
		//JHtml::script($com.DS.'tinymce'.DS.'tinymce.min.js');
		
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		JRequest::setVar('hidemainmenu', true);
		$user		= JFactory::getUser();
		$userId		= $user->get('id');
		$isNew		= true;//($this->item->id == 0);
		//$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $userId);
		//$canDo		= ContentHelper::getActions($this->state->get('filter.category_id'), $this->item->id);
		
		JToolBarHelper::title(JText::_('COM_CONTENT_PAGE_'.($checkedOut ? 'VIEW_ARTICLE' : ($isNew ? 'ADD_ARTICLE' : 'EDIT_ARTICLE'))), 'article-add.png');

		// Built the actions for new and existing records.

		// For new records, check the create permission.
		JToolBarHelper::apply('article.apply');
		JToolBarHelper::save('article.save');
		JToolBarHelper::save2new('article.save2new');
	
		JToolBarHelper::save2copy('menu.save2copy');
		JToolBarHelper::cancel('article.cancel', 'JTOOLBAR_CLOSE');
		JToolBarHelper::divider();
		JToolBarHelper::help('JHELP_CONTENT_ARTICLE_MANAGER_EDIT');
	}
}
