<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

JHtml::script('media/jsbt3/js/form.js');
JHtml::script('media/jsbt3/js/blockui.js');


JHtml::script('media/jsbt3/js/jasny-bootstrap.min.js');
JHtml::stylesheet('media/jsbt3/css/jasny-bootstrap.min.css');


JHtml::script('administrator/components/com_onepage/assets/js/plugins/codemirror.js');
JHtml::script("administrator/components/com_onepage/assets/js/plugins/{$this->type}.js");
JHtml::script('administrator/components/com_onepage/assets/js/plugins/active-line.js');



JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/codemirror.css');
JHtml::stylesheet('administrator/components/com_onepage/assets/css/plugins/monokai.css');

//JHtml::stylesheet('media/jsbt3/css/summernote-bs3.css');
JHtml::script('administrator/components/com_onepage/assets/js/plugins/ui-codemirror.js');



$document = JFactory::getDocument();
//$document->addScript("index.php?option=com_onepage&task=page.generateJS&id={$this->item->id}&f=".uniqid());

JHtml::stylesheet('administrator/components/com_onepage/assets/css/com.css');
JHtml::script('administrator/components/com_onepage/assets/js/script.js');
?>
<script>
var getType = function() {return '<?php echo $this->type;?>';}
</script>
<div ng-app="myApp">

	<div ng-controller="wholepage" class="theme">
		<?php echo $this->loadTemplate('header');?>
		
		<div class="container" style="width:100%">
		<form id="addForm" class="form" role="form" ng-submit="save()">
			<div class="tablediv">
				<div class="left"><?php echo $this->loadTemplate('content');?></div>
				<div class="right" style="width:350px">
					<?php echo $this->loadTemplate('panel');?>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>