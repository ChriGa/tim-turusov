<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

?>

  
  
  <button type="submit" class="btn btn-primary btn-block btn-lg"><span class="fa fa-save "></span> <?php echo JText::_('JS_SAVE'); ?></button>
  
  
  <div class="panel-group" ng-show="type=='css'">
	<div class="panel panel-info" style="margin-top:10px;">
		<div class="panel-heading ">
			<h4 class="panel-title">Snippets</h4>
		</div>
		
		<div class="panel-body">
			<div class="form-group">
				
				<div class="col-md-8">
					<select class="form-control" name="published"  ng-model="csssnip">
						<option value="">-- Select --</option>
						<option value="xs">Media-Mobile</option>
						<option value="sm">Media-Tablet</option>
						<option value="md">Media-Desktop</option>
					</select>
				</div>
				<div class="col-md-3 text-right">
					<button type="button" class="btn btn-success btn-block" ng-click="addCssSnip()"><span class="fa fa-plus "></span> </button>
				</div>
			</div>
			
			
		</div>
	</div>
	</div>
  
	<div class="panel-group"  ng-show="type=='javascript'">
		<div class="panel panel-info" style="margin-top:10px;">
			<div class="panel-heading ">
				<h4 class="panel-title">Snippets</h4>
			</div>
			
			<div class="panel-body">
				<div class="form-group">
					
					<div class="col-md-8">
						<select class="form-control" name="published"  ng-model="jssnip">
							<option value="">-- Select --</option>
							<option value="or">OnReady</option>
						</select>
					</div>
					<div class="col-md-3 text-right">
						<button type="button" class="btn btn-success btn-block" ng-click="addJsSnip()"><span class="fa fa-plus "></span> </button>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
  
  <input type="hidden" name="option" value="com_onepage">
  <input type="hidden" name="task" value="script.saveCode">
  <input type="hidden" name="type" value="<?php echo $this->type;?>">
  <input type="hidden" name="id" value="<?php echo $this->item->id;?>">


