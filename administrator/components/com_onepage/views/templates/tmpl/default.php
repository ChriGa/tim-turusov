<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');

$com = 'media/jsbt3';
$com = 'media/jsbt3';
JHtml::stylesheet($com.'/css/vex.css');
JHtml::stylesheet($com.'/css/vex-theme-os.css');

JHtml::script($com.'/js/form.js');
JHtml::script($com.'/js/blockui.js');
JHtml::stylesheet($com.'/css/picker.min.css');
JHtml::script($com.'/js/picker.min.js');
JHtml::script($com.'/js/vex.combined.min.js');
JHtml::script($com.'/js/uibootstrap-pagination-tpls.min.js');

JHtml::script('administrator/components/com_onepage/assets/js/templates.js');

/*$model = $this->getModel();

$user		= JFactory::getUser();
$userId		= $user->get('id');
//$listOrder	= $this->escape($this->state->get('list.ordering'));
//$listDirn	= $this->escape($this->state->get('list.direction'));
$saveOrder	= $listOrder == 'a.ordering';*/

?>

<div ng-app="myApp" style="height: 100%;">

	<div ng-controller="wholepage" class="theme height-100">

		<div id="carousel-user" class="carousel slide" data-ride="carousel" data-interval="false">

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<div class="mt-page">
						<?php echo $this->loadTemplate('header'); ?>
						<div class="container" style="margin-top:30px;">
							<?php echo $this->loadTemplate('table'); ?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>	