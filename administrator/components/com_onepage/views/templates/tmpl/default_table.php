<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */
// no direct access
defined('_JEXEC') or die;

//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');

//

?>

<div class="row">
	<div class="col-md-9 text-left">
		<?php echo $this->loadTemplate('addform'); ?>
	</div>
	<div class="col-md-3 hidden">
		<input type="text" class="form-control" placeholder="Search Email Or Invoice ID" ng-model="search" ng-keypress="pageChanged(0)">
		<button  class="btn btn-lg btn-link fieldBtn" ng-click="clearSearch()"><span class="fa fa-times-circle "> </span></button>
	</div>
</div>

<form method="post" name="adminForm" id="adminForm">
	
	<div class="clr"> </div>

	<table class="table table-hover jslist" ng-init="pageChanged(0)">
		<thead>
			<tr>
				<th width="3%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th width="30" >
					<?php echo JText::_('ID'); ?>
				</th>
				
				<th class="text-left">
					<?php echo JText::_('JS_Title'); ?>
				</th>
				
				<th width="200" class="text-center">
					<?php echo JText::_('Type'); ?>
				</th>
				
				<th width="100" class="text-center">
					<?php echo JText::_('Published'); ?>
				</th>
				
				
				<th width="10%" class="text-center">
					<?php echo JText::_('JS_ACTIONS'); ?>
				</th>
				
		
			</tr>
		</thead>
		
		<tbody ng-init="host='<?php echo JUri::root(); ?>'">
			<tr ng-repeat="item in list" class="row{{$index%2}}">
				<td class="center" >
					<input type="checkbox" id="cb{{$index}}" name="cid[]" value="{{item.id}}" onclick="Joomla.isChecked(this.checked);">
				</td>
				<td >{{item.id}}</td>
				<td class="text-left">{{item.title}}</td>
				<td class="text-center">{{item.type}}</td>
				
				<td class="text-center">
					<a ng-click="publishEntry($index)">
					<i class="fa fa-check-circle fa-lg htips" style="color: #51A351" ng-show="item.published" data-toggle="tooltip" data-placement="bottom" title="<?php echo JText::_('JS_CLICK_2_UNPUBLISH'); ?>"></i>
					<i class="fa fa-times-circle fa-lg htips" style="color: #F44646" ng-hide="item.published" data-toggle="tooltip" data-placement="bottom" title="<?php echo JText::_('JS_CLICK_2_PUBLISH'); ?>"></i>
					</a>
				</td>
				
				<td class="text-center">
					<button type="button" class="btn btn-primary btn-xs" ng-click="edit($index)"><i class="fa fa-pencil"></i></button>
					<button type="button" class="btn btn-danger btn-xs" ng-click="deleteEntry($index)"><i class="fa fa-times"></i></button>
				</td>
			</tr>
			
		</tbody>
		<tfoot >
			<tr>
			<td colspan="20" class="text-center" >
				<pagination total-items="totalItems" page="currentPage" on-select-page="pageChanged(page)" items-per-page="itemsPerPage" max-size="maxSize" rotate="false"></pagination>
				
			</td>
			</tr>
		</tfoot>
	</table>


	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php //echo JHtml::_('form.token'); ?>
	</div>
</form>
