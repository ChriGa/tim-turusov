<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 */
class OnepageModelPages extends jsModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'ordering', 'a.ordering'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$session = JFactory::getSession();

		

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

	
		
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		
		$level = $this->getUserStateFromRequest($this->context.'.filter.level', 'filter_level', 0, 'int');
		$this->setState('filter.level', $level);

		// List state information.
		parent::populateState('a.lft', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 *
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.published');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$user	= JFactory::getUser();

		$this->setState('list.limit',0);
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
			)
		);
		
		$id = JRequest::getInt('id');
		$id = ($id < 3)?3:$id;
		$id = 2;
		$query->from('`#__onepage_page` AS a');

		$query->where("a.parent_id = {$id}");
		
		//$query->where("a.level = 4 ");
		$query->order(" a.lft ASC ");
		
		// echo nl2br(str_replace('#__','jos_',$query));
		return $query;
	}

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6.1
	 */
	public function getItems()
	{
		$items	= parent::getItems();
		
		foreach($items as $k => $item)
		{
		
			$items[$k] = $item;
		}
		
		return $items;
	}
	
	
	public function copyChildren($id,$children = array())
	{
		foreach($children as $k => $child)
		{
			$itemid = $child->id;
			unset($child->id);
		
			$newChild = JTable::getInstance('Menu','OnepageTable');
			//$obj->lft = 0;$obj->rgt = 1;
			$newChild->parent_id = $id;
			$newChild->setLocation($id, 'last-child');
			$newChild->bind($child);
			
			if($newChild->store())
			{
				if($newChild->type == 'image' && JFile::exists(OnepageHelper::getImgPath()."/image_{$itemid}.png"))
				{
					JFile::copy(OnepageHelper::getImgPath()."/image_{$itemid}.png", OnepageHelper::getImgPath()."/image_{$newChild->id}.png");
				}
				
				// get Children
				$tChild = JTable::getInstance('Menu','OnepageTable');
				$tChild->load($itemid);
				$cChildren = $tChild->getChildren(null,false);
				$this->copyChildren($newChild->id,$cChildren);
			}
		}
	}
	
	public function getChildren($id)
	{
		$tEl = JTable::getInstance('Menu','OnepageTable');
		$menus = $tEl->getChildren($id,false);
		if(empty($menus))
		{
			$menus = array();
		}
		foreach($menus as $k => $item)
		{
			$item->up = 0;
			$item->down = 0;
			$item->isEl = OnepageHelper::isElement($item->type);
			if(count($menus) == 1)
			{
				$item->up = 0;
				$item->down = 0;
			}
			elseif($i == 0)
			{
				$item->up = 0;
				$item->down = 1;
			}
			elseif( $i == count($menus) - 1 )
			{
				$item->up = 1;
				$item->down = 0;
			}
			else
			{
				$item->up = 1;
				$item->down = 1;
			}
			
			$i++;
			
			$menus[$k] = $item;
		}
		
		return $menus;
	}
	
	public function getTree($id)
	{
		$tEl = JTable::getInstance('Menu','OnepageTable');
		$tree = $tEl->getChildren($id);
		
		// clean it
		foreach($tree as $k => $item)
		{
			$obj = new stdClass();
			$obj->id = $item->id;
			$obj->title = str_repeat('..', ($item->level-4)).$item->title;
			$obj->isEl = OnepageHelper::isElement($item->type)?'e':'b';
			$tree[$k] = $obj;
		}
		
		return $tree;
	}
	
	public function iterateExport($node,$path)
	{
		// copy Image
		$params = $node->params;
		
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype');
		$data = '';
		$dispatcher->trigger( 'export',array(&$node,$path) );
		
		$pageArray = $node->getProperties();
		unset($pageArray['lft']);
		unset($pageArray['rgt']);
		unset($pageArray['js']);
		unset($pageArray['css']);
	
		$pageArray['children'] = array();
		$children = $node->getChildren(null,false);
		foreach($children as $k => $child)
		{
			$tEl = JTable::getInstance('Page','OnepageTable');
			$tEl->load($child->id);
			$childArray = $this->iterateExport($tEl,$path);
			$pageArray['children'][] = $childArray;
		}
	
		return $pageArray;
	}
	
	public function iterateExport1($node,$path)
	{
		$pageArray = $node->getProperties();
		
		unset($pageArray['lft']);
		unset($pageArray['rgt']);
		unset($pageArray['js']);
		unset($pageArray['css']);
	
		// copy Image
		$params = $node->params;
		$dst = $path.'images/';
		$imgPath = '';
		if( $node->type == 'item' )
		{
			$imgPath = OnepageHelper::getImgPath().jsGetValueNo0($params,'image','no.png');
			$dst .= jsGetValueNo0($params,'image','no.png');
			
		}
		elseif( $node->type == 'row')
		{
			// section background
			$bgImage = jsGetValueNo0($params,'bg_image','none.png');
			$imgPath = OnepageHelper::getImgPath().$bgImage;
			$dst .= $bgImage;
		}
		
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}
		
		$pageArray['children'] = array();
		$children = $node->getChildren(null,false);
		foreach($children as $k => $child)
		{
			$tEl = JTable::getInstance('Page','OnepageTable');
			$tEl->load($child->id);
			$childArray = $this->iterateExport($tEl,$path);
			$pageArray['children'][] = $childArray;
		}
		
		return $pageArray;
	}
	
	public function importData($node,$parent_id,$path,&$js,&$css)
	{
		$db = jsDB::instance();
		$node_id = jsGetValue($node,'id');;
		$children = jsGetValue($node,'children',array());
		$params = jsGetValue($node,'params');
		$oid = OnepageHelperEl::id($node);
		
		unset($node->id);
		unset($node->children);
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$node = (array)$node;
		$node['parent_id'] = $parent_id;
	
		$tEl->setLocation($parent_id, 'last-child');
		$tEl->bind($node);
		
		if( $tEl->type == 'page' )
		{
			$tEl->title = onepageHelper::checkTitle($tEl->title,0);
			//$tEl->store();
			
			// we have to do something to update the navigation;
		}
		
		$updated = $tEl->store();
	
		if(!$updated)
		{
			return false;
		}
	
		// JS & CSS
		$nid = OnepageHelperEl::id($tEl);
		if( !empty($js) && ( $oid != $nid ) )
		{
			$js = str_replace($oid, $nid, $js);
		}
		
		if( !empty($css) && ( $oid != $nid ) )
		{
			$css = str_replace($oid, $nid, $css);
		}
		
		
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype');
		$data = '';
		$dispatcher->trigger( 'import',array($node,&$tEl,$path) );
		$tEl->store();
		foreach($children as $child)
		{
			$updated = $this->importData($child,$tEl->id,$path,$js,$css);
				
			if(!$updated)
			{
				return false;
			}
		}
	
		return $tEl->id;
	}
	
	public function getList($id = 2)
	{
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
		
		$query->select( '`id`,`title`,`published`' );
		
		$query->from('`#__onepage_page`');
		
		if($id)
		{
			$query->where("`parent_id` = {$id}");
		}
		
		$query->order(" `lft` DESC ");
		$objs = jsDB::loadObjs($query,'obj');
		
		return $objs;
	}
}
