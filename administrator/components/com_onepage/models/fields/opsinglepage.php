<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

/**
 * Supports a modal article picker.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_content
 * @since       1.6
 */
class JFormFieldOPSinglePage extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'Modal_Article';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	protected function getInput()
	{
	
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__onepage_page');
		$query->where('`parent_id` = "2"');
		$query->where('`published` = "1"');
		$query->order(' `lft` ASC');
		
		$db->setQuery($query);
		$objs = $db->loadObjectList();
	
		$options = array();
		foreach($objs as $obj)
		{
			$options[]	= JHtml::_('select.option', $obj->id, $obj->title);
		}
		
		$html = array();
		
		$html[] = JHtml::_('select.genericlist', $options, $this->name, '', 'value', 'text', $this->value, $this->id);
		
		//$html[] = '<input type="hidden" id="'.$this->id.'"/>';

		return implode("\n", $html);
	}
}
