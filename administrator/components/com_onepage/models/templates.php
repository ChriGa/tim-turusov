<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 */
class onepageModelTemplates extends jsModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$session = JFactory::getSession();


		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		

		// List state information.
		parent::populateState('a.lft', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 *
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.published');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$user	= JFactory::getUser();

		//$this->setState('list.limit',10);
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
			)
		);
		
		$query->from('`#__onepage_tpl` AS a');
		//$query->where("`type` = 'invoice'");
		$query->order(" a.`id` DESC ");
		
		// Filter by search in title or menutype
		if ($search = trim($this->getState('filter.search')))
		{
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where("( a.email LIKE {$search}) OR (a.invoice_id LIKE {$search})");
		}
		
		return $query;
	}

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6.1
	 */
	public function getItems()
	{
		$items	= parent::getItems();
		
		if(!empty($items))
		{
			foreach($items as $k => $item)
			{
				$item->type = JText::_('JS_'.$item->type);
				$items[$k] = $item;
			}
		}
		else
		{
			$items = array();
		}
	
		return $items;
	}
	
	
	
	public function getTree($id)
	{
		$tEl = JTable::getInstance('Product','jsmoneyTable');
		$tree = $tEl->getChildren($id,false);
		//$tpls = OnepagevHelper::getTpls();
		// clean it
		foreach($tree as $k => $item)
		{
			$obj = new stdClass();
			$obj->id = $item->id;
			$obj->published = $item->published;
			$obj->title = $item->title;//str_repeat('..', ($item->level-4)).$item->title;
			//$obj->isEl = OnepagevHelper::isElement($item->type)?'e':'b';
			//$obj->tpl = $tpls[$item->tplcode]->title;
			$tree[$k] = $obj;
		}
		
		return $tree;
	}
	
	public function getProducts()
	{
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('`#__jsmoney_product`');
		$query->where('`level` = 1');
		$query->orderby(' `title` ASC');
		
		$objs = jsDB::loadObjs($query,'obj');
		$newObjs = array();
		foreach ($objs as $k => $obj)
		{
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('`#__jsmoney_product`');
			$query->where('`parent_id` = '.$obj->id);
			$query->order(' `lft` ASC');
			
			$children = jsDB::loadObjs($query,'obj');
			if(!empty($children))
			{
				$obj->children = $children;
				$newObjs[] = $obj;
			}
			//$obj->children = $children;
			//$objs[$k] = $obj;
		}
	
		return $newObjs;
	}
}
