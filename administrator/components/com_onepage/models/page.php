<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access
defined('_JEXEC') or die;

/**
 */
class OnepageModelPage extends jsModelList
{
	public function getList($id)
	{
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
	
		$query->select( '`id`,`title`,`published`,`params`,`type`,`content`' );
		$query->from('`#__onepage_page`');
		$query->where("parent_id = {$id}");
		$query->order(" lft ASC ");
		$objs = jsDB::loadObjs($query,'obj');
	
		foreach($objs as $k => $obj)
		{
			$objs[$k]->params = jsJSON::decode($obj->params); 
			$objs[$k]->children = $this->getList($obj->id);
		}
		return $objs;
	}
	
	public function getPages()
	{
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
	
		$query->select( '`id`,`title`' );
		$query->from('`#__onepage_page`');
		$query->where("parent_id = 2");
		$query->order(" lft ASC ");
		$objs = jsDB::loadObjs($query,'obj');
	
		return $objs;
	}
	
	protected function getListQuery()
	{
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$user	= JFactory::getUser();
	
		$this->setState('list.limit',0);
		// Select the required fields from the table.
		$query->select(
		$this->getState(
					'list.select',
					'a.*'
		)
		);
	
		$id = JRequest::getInt('id');
		$id = ($id < 3)?3:$id;
		$id = 2;
		$query->from('`#__onepage_page` AS a');
	
		$query->where("a.parent_id = {$id}");
	
		//$query->where("a.level = 4 ");
		$query->order(" a.lft ASC ");
		// echo nl2br(str_replace('#__','jos_',$query));
		return $query;
	}
}
