<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access
defined('_JEXEC') or die;

/**
 */
class OnepageModelPrototype extends jsModelList
{
	public function getList($id)
	{
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
	
		$query->select( '`id`,`title`,`published`,`params`,`type`,`content`,`md`,`showtitle`,`level`' );
		$query->from('`#__onepage_page`');
		$query->where("parent_id = {$id}");
		$query->order(" lft ASC ");
		$objs = jsDB::loadObjs($query,'obj');
	
		foreach($objs as $k => $obj)
		{
			$objs[$k]->params = jsJSON::decode($obj->params); 
			$objs[$k]->children = $this->getList($obj->id);
		}
		return $objs;
	}
	
	public function getListWithRow($id)
	{
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
	
		$query->select( '`id`,`title`,`published`,`params`,`type`,`content`,`md`,`showtitle`,`level`' );
		$query->from('`#__onepage_page`');
		$query->where("parent_id = {$id}");
		$query->order(" lft ASC ");
		$objs = jsDB::loadObjs($query,'obj');
	
		foreach($objs as $k => $obj)
		{
			if($obj->level == 3)
			{
				$objs[$k]->children = $this->getRowList($obj->id);
			}
			else 
			{
				$objs[$k]->children = $this->getListWithRow($obj->id);
			}
			$objs[$k]->params = jsJSON::decode($obj->params);
			
		}
		return $objs;
	}
	
	public function getRowList($id)
	{
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
	
		$query->select( '`id`,`title`,`published`,`params`,`type`,`content`,`md`,`showtitle`,`level`' );
		$query->from('`#__onepage_page`');
		$query->where("parent_id = {$id}");
		$query->order(" lft ASC ");
		$objs = jsDB::loadObjs($query,'obj');
	
		$col = 0;$a = array();$nObjs = array();$i = 0;
		foreach($objs as $k => $obj)
		{
			$obj->params = jsJSON::decode($obj->params);
			$col += ($obj->md + jsGetValueNo0($obj->params,'offset',0));
			
			if( $col > 12 )
			{
				$i++;$col = 0;
				$a = array();
			}
			
			$obj->first = ($k == 0)?true:false;
			$obj->last = ($k == count($objs)-1)?true:false;
			
			$obj->children = $this->getListWithRow($obj->id);
			
			$a[] = $obj;
			$nObjs[$i] = $a;
		}
		$nObjs = array_values($nObjs);
		return $nObjs;
	}
	
	public function getPages()
	{
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
	
		$query->select( '`id`,`title`' );
		$query->from('`#__onepage_page`');
		$query->where("parent_id = 2");
		$query->order(" lft ASC ");
		$objs = jsDB::loadObjs($query,'obj');
	
		return $objs;
	}
	
	public function getPageItems($id)
	{
		$obj = $this->getCurrentPage($id);
		
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
		$query->select( "`id`,CONCAT(  REPEAT('|__', `level`-2), `title`) As `title1`,`title`,`type`,`level`" );
		$query->from('`#__onepage_page`');
		$query->where("`lft` > {$obj->lft}");
		$query->where("`rgt` < {$obj->rgt}");
		$query->order("`lft` ASC");
		$objs = jsDB::loadObjs($query,'obj');

		return $objs;
	}
	
	public function addChild($parent_id,$type='item')
	{
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->setLocation($parent_id, 'last-child');
		$tEl->set('type',$type);
		JPluginHelper::importPlugin('opeltype',$tEl->type);
		$dispatcher = jsGetDispatcher();
		$results = $dispatcher->trigger( 'saveEl',array(&$tEl,$post) );
		$updated = $tEl->store();
	
		return $tEl;
	}
	
	public function duplicateChildren($old,$new,$path)
	{
		
		/*$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->setLocation($item->id, 'after');
		$tEl->bind($item->getProperties());
		$tEl->set('id',0);
		$updated = $tEl->store();*/
		
		$children = $old->getChildren(null,false);
		foreach($children as $child)
		{
			$tChild = JTable::getInstance('Page','OnepageTable');
			$tChild->bind($child);
			
			$node = JTable::getInstance('Page','OnepageTable');
			$node->bind($child);
			$node->set('id',0);
			$node->set('parent_id',$new->id);
			$node->setLocation($new->id, 'last-child');
			$updated = $node->store();
			
			// update params id
			$node->set('title',"{$node->type}-{$node->id}");
			$node->params['wrap_id'] = OnepageHelperEl::id($node);
			$node->store();
			
			$path = OnepageHelper::getImgPath();
			$dispatcher = jsGetDispatcher();
			JPluginHelper::importPlugin('opeltype');
			$data = '';
			$dispatcher->trigger( 'import',array($tChild,$node,$path) );
			
			$this->duplicateChildren($tChild,$node,$path);
		}
	}
	
	public function getCurrentPage($id)
	{
	
		$tEl = JTable::getInstance('page','onepageTable');
		$tEl->load($id);
		
		$db = jsDB::instance();
		$query	= $db->getQuery(true);
	
		$query->select( '*' );
		$query->from('`#__onepage_page`');
		$query->where("parent_id = 2");
		$query->where("lft <= {$tEl->lft}");
		$query->where("rgt >= {$tEl->rgt}");
		$obj = jsDB::loadObj($query,'obj');
		
		$tEl->bind($obj);
		return $tEl;
	}
	
	protected function getListQuery()
	{
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$user	= JFactory::getUser();
	
		$this->setState('list.limit',0);
		// Select the required fields from the table.
		$query->select(
		$this->getState(
					'list.select',
					'a.*'
		)
		);
	
		$id = JRequest::getInt('id');
		$id = ($id < 3)?3:$id;
		$id = 2;
		$query->from('`#__onepage_page` AS a');
	
		$query->where("a.parent_id = {$id}");
	
		//$query->where("a.level = 4 ");
		$query->order(" a.lft ASC ");
		// echo nl2br(str_replace('#__','jos_',$query));
		return $query;
	}
	
	
	public function getNodes($p)
	{
		$children = $p->getChildren($p->id,false);
		
		$a = array();
		foreach($children as $child)
		{
			$nChild = JTable::getInstance('Page','onepageTable');
			$nChild->bind($child);
			$nChild->set( 'tpl', $nChild->type );
				
			if($nChild->type == 'item')
			{
				$path = JPATH_PLUGINS."/opeltype/{$p->type}/tpl/default_{$p->type}_item.php";
				if(JFile::exists($path))
				{
					$nChild->set( 'tpl', $p->type.'_'.$nChild->type );
				}
			}
			else
			{
				// compatible with old version.
				$path = JPATH_PLUGINS."/opeltype/{$p->type}/tpl/default_{$nChild->type}.php";
	
				if(!JFile::exists($path))
				{
					$nChild->set( 'tpl', 'item' );
	
				}
			}
	
			$a[] = $nChild;
		}
		return $a;
	}
}
