<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

defined('_JEXEC') or die;

define('JS_RATEME_LINK', 'http://extensions.joomla.org/extensions/style-a-design/design/24546');
define('JS_CONTACT_EMAIL', 'j-soho@hotmail.com');
define('JS_APP_ID', 2);

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

// Add
JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');

$lang = JFactory::getLanguage();
$lang->load('jskit',JPATH_ADMINISTRATOR);
$lang->load('com_onepage.sys',JPATH_ADMINISTRATOR);

if(!class_exists("jsLoader"))
{
	echo "The extension is not installed completely";exit;
}

jsImportHelper('onepage');
jsImportHelper('onepage',1);
jsImportHelperOther('onepage','el');
jsImportHelperOther('onepage','plg',0);

JRequest::setVar('tmpl','component');
$controller = JControllerLegacy::getInstance('Onepage');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
