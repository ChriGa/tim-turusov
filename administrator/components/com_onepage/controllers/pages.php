<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Articles list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since	1.6
 */
class OnepageControllerPages extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param	array	$config	An optional associative array of configuration settings.

	 * @return	ContentControllerArticles
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		// Articles default form can come from the articles or featured view.
		// Adjust the redirect view on the value of 'view' in the request.
		
		parent::__construct($config);
	}

	public function execute($task)
	{
		parent::execute($task);
	}
	
	public function add()
	{
		$parent_id = JRequest::getInt('parent_id',3);
		$parent_id = OnepageHelper::filterId($parent_id);
		
		$parent_id = 2;
		$vals = array();
		$vals['title'] = JRequest::getString('title');
		$vals['path'] = 'body';
		$vals['showtitle'] = 1;
		//$vals['type'] = JRequest::getCmd('type');
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->setLocation($parent_id, 'last-child');
		
		$tEl->bind($vals);
		$updated = $tEl->store();
		
		if($updated)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			$model = $this->getModel('Pages');
			$result['list'] = $model->getList($tEl->parent_id);
			
			$child = JTable::getInstance('Page','OnepageTable');
			$child->setLocation($tEl->id, 'last-child');
			
			$vals = array();
			$vals['title'] = "Hi, I'm Row";
			$vals['type']  = 'row';
			$params = array('height'=>'0','width'=>'0');
			$params['has_wrap'] = 0;
			$params['showtitle'] = 1;
			$params['wrap_cls'] = '';
			$params['bg_color'] = '';
			$params['hide_bg_image'] = 0;
			$vals['params']  = $params;
			$child->bind($vals);
			$dispatcher = jsGetDispatcher();
		
			$post = JRequest::get('post');
			JPluginHelper::importPlugin('opeltype',$child->type);
			$dispatcher->trigger( 'saveEl',array(&$child,$post) );
			$updated = $child->store();
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ERROR');
		}
		
		jsExit(jsJSON::encode($result));
	}
	
	public function remove()
	{
		$ids = JRequest::getVar('cid',array(),'post','array');
		JArrayHelper::toInteger($ids);
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$finished = true;
		
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype');
		
		foreach($ids as $id)
		{
			$all = $tEl->getTree($id);
			foreach($all as $obj)
			{
				
				$node = JTable::getInstance('Page','OnepageTable');
				$node->bind($obj);
				
				$dispatcher->trigger( 'removeEl',array($node) );
			}
			
			
			//$tEl->load($id);
			//$path = $tEl->path;
			if(!$tEl->delete($id,true))
			{
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= $tEl->getError();
				$finished = false;
				break;
			}
		}
		
		if($finished)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			
		}
		
		$model = $this->getModel('pages');
		$result['list']= $model->getList(2);
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	
	public function publish()
	{
		$publish = JRequest::getInt('publish');
		$ids = JRequest::getVar('cid',array(),'post','array');
		JArrayHelper::toInteger($ids);
		$tEl = JTable::getInstance('Page','OnepageTable');
		$finished = true;
		$parent_id = 2;
		
		$published = JRequest::getInt('published');
		//$this->setRedirect('index.php?option=com_onepage&view=menus');
		foreach($ids as $id)
		{
			$tEl->load($id);
			if($published < 0)
			{
				if($tEl->published)
				{
					$tEl->set('published',0);
				}
				else
				{
					$tEl->set('published',1);
				}
			}
			else
			{
				$tEl->set('published',$published);
			}
			
			if(!$tEl->store())
			{
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= $tEl->getError();
				$finished = false;
				break;
			}
		}
		
		if($finished)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
		}
		
		if(!empty($parent_id))
		{
			$model = $this->getModel('pages');
			$result['list']= $model->getList($parent_id);
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	
	
	
	public function move()
	{
		$ids = JRequest::getVar('cid',array());
		JArrayHelper::toInteger($ids);
		$target = JRequest::getInt('target_id');
		$target = ($target <3)?3:$target;
		if(count($ids) < 1) exit;
		
		$tTarget = JTable::getInstance('Menu','OnepageTable');
		$tTarget->load($target);
		if(OnepageHelper::isElement($tTarget))
		{
			$result['success']= false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
	
			$result = jsJson::encode($result);
			jsExit($result);
		}
		// allow the same level only.
		
		$tEl = JTable::getInstance('Menu','OnepageTable');
		foreach($ids as $id)
		{
			$tEl->load($id);
			$tEl->moveByReference($target,'last-child');
		}
		
		$updated = true;
		if($updated)
		{
			$result['success']= true;
			$result['title'] = JText::_('SUCCESS');
			$result['content'] = JText::_('SUCCESS');
		}
		else
		{
			$result['success']= false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
		}
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function change()
	{
		$result = array();
		$ids = JRequest::getVar('cid',array());
		JArrayHelper::toInteger($ids);
		
		if(count($ids) < 1) exit;
		$id = $ids[0];
		
		$tChild = JTable::getInstance('Menu','OnepageTable');
		$tChild->load($id);
		
		$type = JRequest::getCmd('type');
		$tChild->set('type',$type);
		$tChild->store();
		
		$result['success']= true;
		$result['title'] = JText::_('SUCCESS');
		$result['content'] = JText::_('SUCCESS');

		$result = jsJson::encode($result);
		jsExit($result);
	}
	

	public function generateJs()
	{
		header('Content-Type: text/javascript');
		$id = JRequest::getInt('id');
	
		$model = $this->getModel('Pages');
		
		$list = $model->getList(2);
	
		
		/*$home = new stdClass();
		foreach($list as $k => $obj)
		{
			if($obj->id == $page_id)
			{
				$home = $obj;
				unset($list[$k]);
				break;
			}
		}
	
		if(!empty($home))
		{
			array_unshift($list,$home);
		}*/
		$output = '';
		$output.= 'var getList = function() {';
		$output.= 'return '.jsJSON::encode($list).';';
		$output.='};';
		
		/*$output.= 'var getPageChildren = function() {';
		$output.= 'return '.jsJSON::encode($children).';';
		$output.='};';*/
	
		/*$output.= 'var getList = function() {';
		$output.= 'return '.jsJSON::encode($children).';';
		$output.='};';
	
		$output.= 'var getAddList = function() {';
		$output.= 'return '.jsJSON::encode($model->getTree($id)).';';
		$output.='};';*/
	
		jsExit($output);
	}
	
	/**
	* @deprecated after 1.4
	*/
	protected function iterateChildren($id)
	{
		$model = $this->getModel('menus');
		$children = $model->getChildren($id);
		foreach($children as $k => $child)
		{
			$nChild = array();
			$nChild['id'] = $child->id;
			$nChild['title'] = $child->title;
			$nChild['parent_id'] = $child->parent_id;
			$nChild['type'] = $child->type;
			
			if($child->level > 4) {
				$border_css = ($child->level > 5)?'child-level'.($child->level-5):'';
				$border_css = ($child->level > 9)?'child-level5':$border_css;
				$nChild['border_css'] = $border_css;
			} else {
				$border_css = '';
			}
			$nChild['header_size'] = min( array($child->level-1, 5) );
			
			$nChild['children'] = $this->iterateChildren($child->id);
			$children[$k] = $nChild;
		}
		
		return $children;
	}
	
	public function duplicate()
	{
		jimport('joomla.filesystem.archive');
	
		$result = array();
		$title = JRequest::getString('title');
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		$title = $tEl->title;
		$params = $tEl->params;
	
		// we create a folder in tmp for zip
		$tmp = JPATH_COMPONENT_ADMINISTRATOR.'/tmp/';
		$tmp = JApplication::getCfg('tmp_path').'/onepage/';
		JFolder::delete($tmp);
		$updated = JFolder::create($tmp);
		if(!$updated)
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ONEPAGE_ACTION_ERROR_NOMENU');
	
			$result = jsJson::encode($result);
			jsExit($result);
		}
	
		JFolder::create($tmp.'images');
		JFolder::create($tmp.'js');
		JFolder::create($tmp.'css');
	
		// we have a json file, js, css & images
		// First, build the json file.
		$model = $this->getModel('pages');
		$json = $model->iterateExport($tEl,$tmp);
		$jsonString = jsJSON::encode(array($json));
		JFile::write($tmp."page.json", $jsonString);
	
		// Second, build the js && CSS
		JFile::write($tmp."page.js", $tEl->js);
		JFile::write($tmp."page.css", $tEl->css);
	
		$path = JPATH_SITE.'/media/com_onepage/css/custom/';
		$cssFiles = jsGetValueNo0($params, 'css_files','');
		if(!empty($cssFiles))
		{
			$cssFiles = explode(',',$cssFiles);
			foreach($cssFiles as $file)
			{
				JFile::copy($path."{$file}",$tmp."css/{$file}");
			}
		}
	
		$path = JPATH_SITE.'/media/com_onepage/js/custom/';
		$jsFiles = jsGetValueNo0($params, 'js_files','');
		if(!empty($jsFiles))
		{
			$jsFiles = explode(',',$jsFiles);
			foreach($jsFiles as $file)
			{
				JFile::copy($path."{$file}",$tmp."js/{$file}");
			}
		}
	
		$unzip = $tmp;
		// copy css sheet & js
		$files = JFolder::files($unzip."css/");
		$path = JPATH_SITE.'/media/com_onepage/css/custom/';
		if(JFolder::exists($unzip."css/"))
		{
			foreach($files as $file)
			{
				JFile::copy($unzip."css/{$file}", $path.$file);
			}
		}
		
	
		$files = JFolder::files($unzip."js/");
		$path = JPATH_SITE.'/media/com_onepage/js/custom/';
		if(JFolder::exists($unzip."js/"))
		{
			foreach($files as $file)
			{
				JFile::copy($unzip."js/{$file}", $path.$file);
			}
		}
		// Finish Export
	
		// read the json data & move images
		
		$model = $this->getModel('pages');
		$page = JTable::getInstance('Page','OnepageTable');
	
		$string = JFile::read($unzip.'page.json');
		$data = jsJSON::decode($string);
	
		$updated = true;
		$id = JRequest::getInt('id');
		
		$js = JFile::read($unzip."page.js");
		$css = JFile::read($unzip."page.css");
		
		foreach($data as $child)
		{
			// create page here first
			$updated = $model->importData($child,2,$unzip."images/",$js,$css);
			if(!$updated)
			{
				break;
			}
			$id = $updated;
		}
		
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$tEl->set('js',$js);
		$tEl->set('css',$css);
		$tEl->store();
		
		JFolder::delete($unzip);
	
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		$result['list'] = $model->getList($tEl->parent_id);
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function exportPage()
	{
		jimport('joomla.filesystem.archive');
	
		$result = array();
		$title = JRequest::getString('title');
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		$title = $tEl->title;
		$params = $tEl->params;
	
		// we create a folder in tmp for zip
		$tmp = JPATH_COMPONENT_ADMINISTRATOR.'/tmp/';
		
		JFolder::delete($tmp);
		$updated = JFolder::create($tmp);
		if(!$updated)
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ONEPAGE_ACTION_ERROR_NOMENU');

			$result = jsJson::encode($result);
			jsExit($result);
		}

		JFolder::create($tmp.'images');
		JFolder::create($tmp.'js');
		JFolder::create($tmp.'css');
	
		// we have a json file, js, css & images
		// First, build the json file.
		$model = $this->getModel('pages');
		$json = $model->iterateExport($tEl,$tmp);
		$jsonString = jsJSON::encode(array($json));
		JFile::write($tmp."page.json", $jsonString);
	
		// Second, build the js && CSS
		JFile::write($tmp."page.js", $tEl->js);
		JFile::write($tmp."page.css", $tEl->css);
	
		$path = JPATH_SITE.'/media/com_onepage/css/custom/';
		$cssFiles = jsGetValueNo0($params, 'css_files','');
		if(!empty($cssFiles))
		{
			$cssFiles = explode(',',$cssFiles);
			foreach($cssFiles as $file)
			{
				JFile::copy($path."{$file}",$tmp."css/{$file}");
			}
		}
	
		$path = JPATH_SITE.'/media/com_onepage/js/custom/';
		$jsFiles = jsGetValueNo0($params, 'js_files','');
		if(!empty($jsFiles))
		{
			$jsFiles = explode(',',$jsFiles);
			foreach($jsFiles as $file)
			{
				JFile::copy($path."{$file}",$tmp."js/{$file}");
			}
		}
		
		/* Brand Icon
		$brand = jsGetValueNo0($params,'brandicon','none.png');
		$dst = $tmp."/images/".$brand;
		$imgPath = OnepageHelper::getImgPath().$brand;
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}*/
	
	
		// Zip, Then delete the folder
		$dFiles = array();
		$files = JFolder::files($tmp,'.',true,true);
		
		foreach($files as $file)
		{
			$dFile = array();
			$dFile['name'] = str_replace($tmp, '', $file);
			$dFile['data'] = JFile::read($file);
				
			$dFiles[] = $dFile;
		}
	
		$zip = JArchive::getAdapter('zip');
		$zipped = $zip->create($tmp."/opPages.zip",$dFiles);
	
		$title = (empty($title))?'opPages':$title;
		$zipPath = $tmp.'/opPages.zip';
		if($zipped)
		{
			header("Cache-Control: public, must-revalidate");
			header('Cache-Control: pre-check=0, post-check=0, max-age=0');
			header("Pragma: no-cache");
			header("Expires: 0");
			header("Content-Description: File Transfer");
			//header("Expires: Sat, 30 Dec 1990 07:07:07 GMT");
			header("Accept-Ranges: bytes");
	
			$fileSize= filesize($zipPath);
			$httpRange= 0;
			$newFileSize= $fileSize -1;
			// Default values! Will be overridden if a valid range header field was detected!
			$resultLenght= (string) $fileSize;
			$resultRange= "0-".$newFileSize;
	
			header("Content-Length: ".$resultLenght);
			header("Content-Range: bytes ".$resultRange.'/'.$fileSize);
			header("Content-Type: application/zip");
			header('Content-Disposition: attachment; filename="'.$title.'.zip"');
			header("Content-Transfer-Encoding: binary\n");
			ob_end_flush();
			//$fp= @ fopen($zipPath, 'rb');
			@ readfile($zipPath);
			JFile::delete($zipPath);
			JFolder::delete(JPATH_COMPONENT_ADMINISTRATOR.'/tmp');
		}
	
		exit;
	}
	
	public function importPage()
	{
		jimport('joomla.filesystem.archive');
	
		$result = array();
		$pack = JRequest::getVar('pack', null, 'files', 'array');
		$tmpPath = JApplication::getCfg('tmp_path');
		$unzip = $tmpPath.'/onepage_page/';
		$db = jsDB::instance();
		
		//
		$type = JRequest::getCmd('type');
		$zip = JArchive::getAdapter('zip');
		$updated = $zip->extract($pack['tmp_name'],$unzip);
	
		if(!$updated)
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ONEPAGE_ACTION_ERROR_UNZIP_FAILED');
			
			$result = jsJson::encode($result);
			jsExit($result);
		}
	
		// copy css sheet & js
		$files = JFolder::files($unzip."css/");
		$path = JPATH_SITE.'/media/com_onepage/css/custom/';
		if(JFolder::exists($unzip."css/"))
		{
			foreach($files as $file)
			{
				JFile::copy($unzip."css/{$file}", $path.$file);
			}
		}
		
	
		$files = JFolder::files($unzip."js/");
		$path = JPATH_SITE.'/media/com_onepage/js/custom/';
		if(JFolder::exists($unzip."js/"))
		{
			foreach($files as $file)
			{
				JFile::copy($unzip."js/{$file}", $path.$file);
			}
		}
	
	
		// read the json data & move images
		$model = $this->getModel('pages');
		$page = JTable::getInstance('Page','OnepageTable');
	
		$string = JFile::read($unzip.'page.json');
		$data = jsJSON::decode($string);
	
		$updated = true;
		$id = JRequest::getInt('id');
		
		$js = JFile::read($unzip."page.js");
		$css = JFile::read($unzip."page.css");
		
		if(empty($id))
		{
			foreach($data as $child)
			{
				// create page here first
				$updated = $model->importData($child,2,$unzip."images/",$js,$css);
				if(!$updated)
				{
					break;
				}
				$id = $updated;
			}
		}
		else
		{
			$page->load($id);
			
			$dispatcher = jsGetDispatcher();
			JPluginHelper::importPlugin('opeltype');
			
			$all = $page->getChildren(null);
			foreach($all as $obj)
			{
				$node = JTable::getInstance('Page','OnepageTable');
				$node->bind($obj);
		
				$dispatcher->trigger( 'removeEl',array($node) );
				$page->delete($node->id,false);
			}
			
			foreach(jsGetValueNo0($data[0],'children') as $child)
			{
				
				// create page here first
				$updated = $model->importData($child,$id,$unzip."images/",$js,$css);
			}
		}
		
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$tEl->set('js',$js);
		$tEl->set('css',$css);
		$tEl->store();
		
		JFolder::delete($unzip);
	
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		$result['list'] = $model->getList($tEl->parent_id);
		$result = jsJson::encode($result);
		jsExit($result);
	}
}
