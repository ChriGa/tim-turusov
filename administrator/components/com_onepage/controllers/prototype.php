<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 * @since       1.6
 */
class OnepageControllerPrototype extends JControllerForm
{
	/**
	 * Class constructor.
	 *
	 * @param   array  $config  A named array of configuration variables.
	 *
	 * @since	1.6
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	public function changeOrder()
	{
		$id = JRequest::getInt('id');
		$direction = JRequest::getInt('change');
	
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		if($direction < 0)
		{
			$updated = $tEl->orderUp($id);
		}
		else
		{
			$updated = $tEl->orderDown($id);
		}
	
		$result['success']= false;
		$result['title']= JText::_('ERROR');
		$result['content']= $tEl->getError();
	
		if($updated)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
	
			$model = $this->getModel('prototype');
			if($tEl->level == 4)
			{
				$result['list']= $model->getRowList($tEl->parent_id);
			}
			else
			{
				$result['list']= $model->getListWithRow($tEl->parent_id);
			}
			
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function sort()
	{
		$ids = JRequest::getVar('ids',array());
		JArrayHelper::toInteger($ids);
		if(count($ids) < 1) exit;
	
		// allow the same level only.
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$parent_id = 0 ;
		foreach($ids as $id)
		{
			$tEl->load($id);
			$tEl->moveByReference($tEl->parent_id,'last-child');
		}
	
		$updated = true;
		if($updated)
		{
			$result['success']= true;
			$result['title'] = JText::_('SUCCESS');
			$result['content'] = JText::_('SUCCESS');
		}
		else
		{
			$result['success']= false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
		}
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	
	public function uploadJsCss()
	{
		$result = array();
		$overwrite = JRequest::getInt('overwrite');
		$files = JRequest::get('files');
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		foreach($files as $k => $file)
		{
			// upload file to template folder
			$ext = strtolower( JFile::getExt($file['name']) );
			
			if(!in_array($ext,array('js','css','JS','CSS')))
			{
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ERROR');
				jsExit(jsJSON::encode($result));
			}
			
			$folderPath = JPATH_SITE."/media/com_onepage/".strtolower($ext)."/custom/";
			if(!$overwrite)
			{
				if(JFile::exists($folderPath.$file['name']))
				{
					$md5 = md5(uniqid(rand(0, 99)."_"));
					$uniq = substr($md5, 0,5);
					$file['name'] = basename($file['name'],'.css')."-{$uniq}.".JFile::getExt($file['name']);
				}
			}
			
			if ( !JFile::upload($file['tmp_name'], $folderPath.$file['name'] ))
			{
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ERROR');
				jsExit(jsJSON::encode($result));
			}
		}
		
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		$result['js']= $this->loadSrcFiles('js',$tEl);
		$result['css']= $this->loadSrcFiles('css',$tEl);
		jsExit(jsJSON::encode($result));
	}
	
	protected function loadSrcFiles($type,$node)
	{
		$folderPath = JPATH_SITE."/media/com_onepage/".strtolower($type)."/custom/";
		
		$files = JFolder::files($folderPath,".{$type}");
		$files = (empty($files))?array():$files;
		
		$params = $node->params;
		$s = jsGetValueNo0($params, $type.'_files','');
		$s = empty($s)?array('xx'):explode(',',$s);
		$a = array();
		foreach($files as $file)
		{
			$i = array('name'=>$file);
			$i['checked'] = (in_array($file,$s))?true:false;
			$a[] = $i;
		}
		return $a;
	}
	
	public function saveJsCss()
	{
		$db = jsDB::instance();
		// Error in upload
	
		$id = JRequest::getInt('id');
		$name = JRequest::getCmd('name');
		$val = JRequest::getVar('value',array());
		JArrayHelper::toString($val);
		$val = implode(',', $val);
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$params = $tEl->params;
		$params[$name] = $val;
		
		$tEl->set('params',$params);
		$tEl->store();
	
		$result = array();
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		$result['js']= $this->loadSrcFiles('js',$tEl);
		$result['css']= $this->loadSrcFiles('css',$tEl);
		jsExit(jsJSON::encode($result));
	}

	public function generateJS()
	{
		$output = '';
		$id = JRequest::getInt('id');
		$layout = JRequest::getCmd('layout');
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		//$model = $this->getModel('products');
		//$tree = $model->getTree($id);
		
		$output = '';
		$output.= 'var getId = function() {';
		$output.= 'return '.$id.';';
		$output.= '}';
		
		
		if(empty($layout))
		{
			$model = $this->getModel('prototype');
			//$list = $model->getList($id);
			$list = $model->getListWithRow($id);//
			$output.= "\n";
			$output.= 'var getList = function() {';
			$output.= 'return '.jsJSON::encode($list).';';
			$output.= '}';
		}
		elseif($layout == 'edit')
		{
			$tEl->html = jsGetValueNo0($tEl, 'html','');
			$output.= "\n";
			$output.= 'var getInfo = function() {';
			$output.= 'return '.jsJSON::encode($tEl->getProperties()).';';
			$output.= '}';
			
			
			$model = $this->getModel('prototype');
			$cp = $model->getCurrentPage($id);
			$list = $model->getListWithRow($cp->id);//
			$output.= "\n";
			$output.= 'var getList = function() {';
			$output.= 'return '.jsJSON::encode($list).';';
			$output.= '}';
		}
		elseif($layout == 'config')
		{
			$js = $this->loadSrcFiles('js',$tEl);
			
			$output.= "\n";
			$output.= 'var getJS = function() {';
			$output.= 'return '.jsJSON::encode($js).';';
			$output.= '}';
			
			
			$css = $this->loadSrcFiles('css',$tEl);
			
			$output.= "\n";
			$output.= 'var getCSS = function() {';
			$output.= 'return '.jsJSON::encode($css).';';
			$output.= '}';
			
			$output.= "\n";
			$output.= 'var getInfo = function() {';
			$output.= 'return '.jsJSON::encode($tEl->getProperties()).';';
			$output.= '}';
		}
		
		$groups = OnepageHelper::getGroupType();
		$g = array();
		foreach($groups as $group)
		{
			$g[] = $group['value'];
		}
		$output.= "\n";
		$output.= 'var getGroupType = function() {';
		$output.= 'return '.jsJSON::encode($g).';';
		$output.= '}';
		
		$groups = OnepageHelper::getItemType();
		$g = array();
		foreach($groups as $group)
		{
			$g[] = $group['value'];
		}
		$output.= "\n";
		$output.= 'var getItemType = function() {';
		$output.= 'return '.jsJSON::encode($g).';';
		$output.= '}';
		header('Content-Type: text/javascript');
		jsExit($output);
	}
	
	public function add()
	{
		$id = JRequest::getInt('id');
		$title = JRequest::getString('title');
		$type = JRequest::getCmd('type','editor');
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->setLocation($id, 'last-child');
		
		$vals = array();
		$vals['title'] = $title;
		$vals['type']  = 'row';
		$params = array('height'=>'0','width'=>'0');
		$params['has_wrap'] = 0;
		$params['showtitle'] = 1;
		$params['wrap_cls'] = '';
		$params['bg_color'] = '';
		$params['hide_bg_image'] = 0;
		$vals['params']  = $params;
		$tEl->bind($vals);
		
		if($tEl->store())
		{
			/*$tChild = JTable::getInstance('Page','OnepageTable');
			$tChild->setLocation($tEl->id, 'last-child');
			
			$vals = array();
			$vals['title'] = $title;
			$vals['type']  = $type;
			$tChild->bind($vals);
			
			$post = JRequest::get('post');
			$dispatcher = jsGetDispatcher();
			JPluginHelper::importPlugin('opeltype',$tChild->type);
			$dispatcher->trigger( 'saveEl',array(&$tChild,$post) );
			
			$tChild->store();*/
			
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			//$model = $this->getModel('page');
			//$list= $model->getList($id);
			$result['entry'] = $tEl->getProperties();
			$result['entry']['children'] = array();
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function addChild()
	{
		$id = JRequest::getInt('id');
		$title = JRequest::getString('title');
		$type = JRequest::getCmd('type','item');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->setLocation($id, 'last-child');
		
		$vals = array();
		$vals['title'] = $title;
		$vals['type']  = 'item';
		$vals['md']  = 4;
		$tEl->bind($vals);
		
		$tp = JTable::getInstance('Page','OnepageTable');
		$tp->load($id);
		
		$post = JRequest::get('post');
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype',$tEl->type);
		$results = $dispatcher->trigger( 'saveEl',array(&$tEl,$post) );
	
		$result = array();
		if($tEl->store())
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			//$model = $this->getModel('page');
			//$list= $model->getList($id);
			$result['entry'] = $tEl->getProperties();
			$result['entry']['children'] = array();
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function publish()
	{
		$id = JRequest::getInt('id');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		if($tEl->published)
		{
			$tEl->set('published',0);
		}
		else
		{
			$tEl->set('published',1);
		}
		$updated = $tEl->store();
		
		$result = array();
		if($updated)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			
			$model = $this->getModel('prototype');
			if($tEl->level == 4)
			{
				$result['list']= $model->getRowList($tEl->parent_id);
			}
			else
			{
				$result['list']= $model->getListWithRow($tEl->parent_id);
			}
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ERROR');
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	/**
	* @ Deprecated
	*/
	public function remove()
	{
		$id = JRequest::getInt('id');
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$finished = true;
		
		if(!$tEl->delete($id,true))
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			
		}
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function saveSrc()
	{
		// Error in upload
		$result = array();
		$result['success']= true;
		
		$id = JRequest::getInt('id');
		$title = JRequest::getString('title');
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$params = $tEl->params;
		
		$html = JRequest::getString('html','','post',JREQUEST_ALLOWHTML);
		$css = JRequest::getString('css','','post',JREQUEST_ALLOWHTML);
		$tEl->set('content',$html);
		$tEl->set('css',$css);
		
		if(!$tEl->check())
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
			$result = jsJson::encode($result);
			jsExit($result);
		}
		
		if($tEl->store())
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function saveInfo()
	{
		/*$id = JRequest::getInt('id');
		$name = JRequest::getCmd('name');
		$val = JRequest::getString('value','post','');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$vals = array();
		$vals[$name] = $val;
	
		$tEl->bind($vals);
		$tEl->store();*/
		
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$vals = array();
		$vals['title'] = JRequest::getString('title');
		$vals['params'] = $tEl->params;
		$vals['params']['single'] = JRequest::getCmd('single');
		$tEl->bind($vals);
		$tEl->store();
		
		$image = JRequest::getVar('file', null, 'files');
		if(!empty($image))
		{
			if( strtolower(JFile::getExt($image['name']) ) != 'png')
			{
				$result['content'] = JText::_('ONEPAGE_ERROR_ICO_ONLY');
				jsExit(jsJSON::encode($result));
			}
		
			$path = JPATH_SITE."/images/onepage/page{$tEl->id}_appicon144.png";
		
			if ( !JFile::upload($image['tmp_name'], $path ))
			{
				// Error in upload
				$result['content'] = JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				jsExit(jsJSON::encode($result));
			}
				
			$img = new jsImage($path);
			$img->resize('144', '144')->toFile($path);
			$img->resize('114', '114')->toFile( dirname($path)."/page{$tEl->id}_appicon114.png" );
			$img->resize('72', '72')->toFile( dirname($path)."/page{$tEl->id}_appicon72.png" );
			$img->resize('57', '57')->toFile( dirname($path)."/page{$tEl->id}_appicon57.png" );
		}
	
		$result = array();
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		jsExit(jsJSON::encode($result));
	}
	
	
	public function saveJSConfig()
	{
		$id = JRequest::getInt('id');
		$val = JRequest::getCmd('js_bottom');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$params = $tEl->params;
		$params['js_bottom'] = $val;
		
		$tEl->set('params',$params);
		$tEl->store();
	
		$result = array();
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		jsExit(jsJSON::encode($result));
	}
	
	// deprecated
	public function saveElConfig()
	{
		// Error in upload
		$result = array();
		$result['success']= true;
	
		$id = JRequest::getInt('id');
		$title = JRequest::getString('title');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		$title = JRequest::getString('title');
		$showtitle = JRequest::getInt('showtitle');
		$tEl->set('title',$title);
		$tEl->set('showtitle',$showtitle);
		
		$params = $tEl->params;
		$params['has_wrap'] = JRequest::getInt('has_wrap');
		$params['height'] = JRequest::getInt('height');
		$params['bg_color'] = JRequest::getCmd('bg_color');
		$params['hide_bg_image'] = JRequest::getInt('hide_bg_image');
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$params['wrap_id'] = JRequest::getString('wrap_id');
		// Image
		$image = JRequest::getVar('bg_image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$filePath = OnepageHelper::getImgPath()."bg_image_{$tEl->id}.{$imgExt}";
		
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');		
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			$params['bg_image'] = "bg_image_{$tEl->id}.{$imgExt}";
		}
		
		$tEl->params = $params;
		if($tEl->store())
		{
			$result['success']= true;
			$result['entryTitle']= $tEl->title;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
				
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	/**
	 * @ Use in V1.40
	 */
	public function saveItem()
	{
		// Error in upload
		$result = array();
		$result['success']= true;
	
		$id = JRequest::getInt('id');
		$v = JRequest::getInt('v');
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		$title = JRequest::getString('title');
		$wrap_id = JRequest::getString('wrap_id');
		$wrap_cls = JRequest::getString('wrap_cls');
		$unit = JRequest::getInt('md',12);
		$html = JRequest::getString('html');
		
		$vals = array();
		$vals['title'] = $title;
		$vals['md'] = $unit;
		$vals['html'] = $html;
		$vals['published'] = JRequest::getInt('published');
		$vals['params'] = $tEl->params;
		$vals['params']['wrap_id'] = $wrap_id;
		$vals['params']['wrap_cls'] = $wrap_cls;
		$vals['params']['offset'] = JRequest::getInt('offset');
		$vals['params']['animation'] = JRequest::getCmd('animation');
		$vals['params']['delay'] = JRequest::getFloat('delay');
		$tEl->bind($vals);
		
		$post = JRequest::get('post');
		$dispatcher = jsGetDispatcher();
		
		if($tEl->type == 'item')
		{
			/*$parent = JTable::getInstance('Page','OnepageTable');
			$parent->load($tEl->parent_id);
			JPluginHelper::importPlugin('opeltype',$parent->type);
			$results = $dispatcher->trigger( 'saveChild',array(&$tEl,$post) );
			*/
			JPluginHelper::importPlugin('opeltype',$tEl->type);
			$results = $dispatcher->trigger( 'saveEl',array(&$tEl,$post) );
		}
		else
		{
			JPluginHelper::importPlugin('opeltype',$tEl->type);
			$results = $dispatcher->trigger( 'saveEl',array(&$tEl,$post) );
		}
		
		
		$updated = $tEl->store();
		
		if($updated)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			
			$model = $this->getModel('prototype');
			$cp = $model->getCurrentPage($tEl->id);
			$result['list']  = $model->getListWithRow($cp->id);//
			
			
			switch ($v)
			{
				case(1):
					$model = $this->getModel('prototype');
					$node = $model->duplicateChild($tEl);
					$result['link']= "index.php?option=com_onepage&view=prototype&layout=edit&id={$node->id}";
					break;
					
			}
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ERROR');
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	/**
	 * @ Deprecated
	 * It's in Layout Setting
	 */
	public function removeItem()
	{
		// Error in upload
		$id = JRequest::getInt('id');
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		$level = $tEl->level;
		$parent_id = $tEl->parent_id;
		$model = $this->getModel('prototype');
		
		if(!$tEl->delete($id,true))
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			
			if( $level==4 )
			{
				$result['list']= $model->getRowList($parent_id);
			}
			else
			{
				$result['list']= $model->getListWithRow($parent_id);
			}
			
		}
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function deleteItem()
	{
		// Error in upload
		$id = JRequest::getInt('id');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$finished = true;
		
		$model = $this->getModel('prototype');
		$cp = $model->getCurrentPage($id);
		
		if($tEl->type == 'row')
		{
			// check if it is the last one
			$db = jsDB::instance();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__onepage_page');
			$query->where("`parent_id` = '{$tEl->parent_id}'");
			$query->order("`lft` ASC");
			$objs = jsDB::loadObjs($query,'obj');
			
			if(count($objs) <= 1)
			{
				// error
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= 'Last One';
			}
			else
			{
				$lft = $tEl->lft - 1;
				$updated = $tEl->delete($id,true);
				
				if($updated)
				{
					if( $id == $objs[0]->id)
					{
						$obj = $objs[1];
					}
					else
					{
						$db = jsDB::instance();
						$query = $db->getQuery(true);
						$query->select('*');
						$query->from('#__onepage_page');
						$query->where("`lft` = '{$lft}' OR `rgt` = '{$lft}'");
						$db->setQuery($query);
						$obj = $db->loadObject();
					}
					
					$result['success']= true;
					$result['title']= JText::_('SUCCESS');
					$result['content']= JText::_('SUCCESS');
					$result['id']= $obj->id;
					
					OnepageHelper::logEditPath($cp->id, $obj->id);
				}
				else
				{
					$result['success']= false;
					$result['title']= JText::_('ERROR');
					$result['content']= $tEl->getError();
				}
			}
		}
		else
		{
			$lft = $tEl->lft - 1;
			$updated = $tEl->delete($id,true);
			
			if($updated)
			{
				// check
				$db = jsDB::instance();
				$query = $db->getQuery(true);
				$query->select('*');
				$query->from('#__onepage_page');
				$query->where("`lft` = '{$lft}' OR `rgt` = '{$lft}'");
				$db->setQuery($query);
				$obj = $db->loadObject();
				
				$result['success']= true;
				$result['title']= JText::_('SUCCESS');
				$result['content']= JText::_('SUCCESS');
				$result['id']= $obj->id;
				
				OnepageHelper::logEditPath($cp->id, $obj->id);
			}
			else
			{
				
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= $tEl->getError();
				
			}
		}
	
		//
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype');
		$dispatcher->trigger( 'removeEl',array( $tEl ) );
		
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function changeType()
	{
		$id = JRequest::getInt('id');
		$title = JRequest::getString('title');
		$type = JRequest::getCmd('type');
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		
		
		$post = JRequest::get('post');
		JPluginHelper::importPlugin('opeltype',$tEl->type);
		$dispatcher = jsGetDispatcher();
		
		$ot = $tEl->type."-{$tEl->id}";
		
		if( $tEl->title == $ot )
		{
			$tEl->set('title',$type."-{$tEl->id}");
		}
		
		
		$tEl->set('type',$type);
		$results = $dispatcher->trigger( 'quickSaveEl',array(&$tEl,$post) );
		
		if(!$tEl->store())
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$result['success']= true;
			$result['itemtitle']= $tEl->title;
			$result['type']= $type;
			$result['showtitle']= $tEl->showtitle;
			$result['params']= $tEl->params;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
				
		}
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function changeElType()
	{
		$id = JRequest::getInt('id');
		$type = JRequest::getCmd('type');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		$oType = $tEl->type;
		$ot = $oType."-{$tEl->id}";
		if( $tEl->title == $ot )
		{
			$tEl->set('title',$type."-{$tEl->id}");
		}
		
		$post = JRequest::get('post');
		JPluginHelper::importPlugin('opeltype',$tEl->type);
		$dispatcher = jsGetDispatcher();
		$tEl->set('type',$type);
		$results = $dispatcher->trigger( 'saveEl',array(&$tEl,$post) );
		
		// Check if it is a group and new type is not.
		JPluginHelper::importPlugin('opeltype');
		$isItem = false;
		$dispatcher->trigger( 'isItem',array($oType,&$isItem) );
		
		$nIsItem = false;
		$dispatcher->trigger( 'isItem',array($type,&$nIsItem) );
		
		if($nIsItem && !$isItem)
		{
			$children = $tEl->removeChildren();
			
		}
		
		
	
		if( !$tEl->store() )
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function changeGrid()
	{
		$id = JRequest::getInt('id');
		$md = JRequest::getInt('md');
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		$tEl->set('md',$md);
	
		if(!$tEl->store())
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
	
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array())
	{
		// Initialise variables.
		$user = JFactory::getUser();
		$categoryId = JArrayHelper::getValue($data, 'catid', JRequest::getInt('filter_category_id'), 'int');
		$allow = null;

		if ($categoryId)
		{
			// If the category has been passed in the data or URL check it.
			$allow = $user->authorise('core.create', 'com_content.category.' . $categoryId);
		}

		if ($allow === null)
		{
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd();
		}
		else
		{
			return $allow;
		}
	}

	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Initialise variables.
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		$userId = $user->get('id');

		// Check general edit permission first.
		if ($user->authorise('core.edit', 'com_content.article.' . $recordId))
		{
			return true;
		}

		// Fallback on edit.own.
		// First test if the permission is available.
		if ($user->authorise('core.edit.own', 'com_content.article.' . $recordId))
		{
			// Now test the owner is the user.
			$ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
			if (empty($ownerId) && $recordId)
			{
				// Need to do a lookup from the model.
				$record = $this->getModel()->getItem($recordId);

				if (empty($record))
				{
					return false;
				}

				$ownerId = $record->created_by;
			}

			// If the owner matches 'me' then do the test.
			if ($ownerId == $userId)
			{
				return true;
			}
		}

		// Since there is no asset tracking, revert to the component permissions.
		return parent::allowEdit($data, $key);
	}

	
	
	protected function generateNewTitle($parent_id, $alias, $title)
	{
		// Alter the title & alias
		$table =  JTable::getInstance('Menu','OnepageTable');
		while ($table->load(array('alias' => $alias, 'parent_id' => $parent_id)))
		{
			if ($title == $table->title)
			{
				$title = JString::increment($title);
			}
			$alias = JString::increment($alias, 'dash');
		}
	
		return array($title, $alias);
	}
	
	
	
	public function getImg()
	{
		$id = JRequest::getInt('id');
		$tEl = JTable::getInstance('Page','onepageTable');
		$tEl->load($id);
		$name = JRequest::getCmd('name');
		$filepath = onepageHelper::getImgPath(false).jsGetValueNo0($tEl->params,'image','no.png');
		
		$placeholder = false;
		if(!JFile::exists($filepath))
		{
			$placeholder = true;
			$filepath = JPATH_SITE.'/media/com_onepage/img/placeholder.png';
		}
		
		header('Content-Type: image/png');
		
		if($placeholder)
		{
			$imginfo = getimagesize($filepath);
			$srcWidth = $imginfo[0];
			$srcHeight = $imginfo[1];
				
			$img = imagecreatefrompng($filepath);
			imagepng($img);
			ImageDestroy($img);
			/*$destWidth = 100;$destHeight = 60;
			$dst_img = imagecreatetruecolor($destWidth, $destHeight);
			imagecopyresized($dst_img, $img, 0, 0, 0, 0, $destWidth, $destHeight, $srcWidth, $srcHeight);
			imagepng($dst_img);
				
			ImageDestroy($img);
			ImageDestroy($dst_img);*/
		}
		else
		{
			$img = new jsImage($filepath);
			$imginfo = getimagesize($filepath);
			$srcWidth = $imginfo[0];
			$srcHeight = $imginfo[1];
			
			$img->toFile(null);
			$img->destroy(null);
		}
		
		
		exit;
	}
	
	public function createLevelEl()
	{
		$result = array();
		$result['success']= true;
		
		$id = JRequest::getInt('id');
		$lv = JRequest::getInt('lv');
		$type = JRequest::getCmd('type');
		
		$model = $this->getModel('prototype');
		$node = JTable::getInstance('Page','OnepageTable');
		$node->load($id);
		
		//
		$vals = array();
		$vals['type'] = $type;
		$vals['md'] = 4;
		$vals['params'] = array();
		$vals['params']['offset'] = 0;
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->bind($vals);
		
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype');
		
		switch($lv)
		{
			case(1): // row
				$cp = $model->getCurrentPage($id);
				
				$tEl->setLocation($cp->id, 'last-child');
				$tEl->set('type',$type); // force to be row
				break;
				
			case(2): // 
				$parent_id = $node->parent_id;
				if($node->level-2 > $lv) // new group
				{
					$node->load($node->parent_id);
					$parent_id = $node->parent_id;
				}
				elseif($node->level-2 < $lv) // child group
				{
					$parent_id = $node->id;
				}
				
				$tEl->setLocation( $parent_id, 'last-child' );
			
				break;
				
			case(3):
				$parent_id = $node->parent_id;
				$isItem = false;
				$results = $dispatcher->trigger( 'isItem',array($node->type,&$isItem) );
				
				if( ($node->level-2 == $lv)  )// new sibling
				{
					$parent_id = $node->parent_id;
				}
				elseif($node->level == 4) // child group
				{
					if( $isItem )
					{
						$parent_id = $node->parent_id;
					}
					else
					{
						$parent_id = $node->id;
					}
					
				}
				elseif($node->level == 3 ) // child group
				{
					$parent_id = $node->id;
				}
				
				$tEl->setLocation( $parent_id, 'last-child' );
				break;
		}
		
		$post = JRequest::get('post');
		$dispatcher->trigger( 'saveEl',array(&$tEl,$post) );
		
		if(!$tEl->store())
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$title = JRequest::getString('title',$tEl->type."-{$tEl->id}");
			$tEl->set('title',$title);
			
			$tEl->store();
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			$result['id']= $tEl->id;
		}
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function embed()
	{
		$result = array();
		$result['success']= true;
	
		$id = JRequest::getInt('id');
		$type = JRequest::getCmd('type');
	
		$model = $this->getModel('prototype');
		$node = JTable::getInstance('Page','OnepageTable');
		$node->load($id);
	
		//
		$vals = array();
		$vals['type'] = $type;
		$vals['md'] = $node->md;
		$vals['parent_id'] = $node->parent_id;
		$vals['params'] = array();
		$vals['params']['offset'] = 0;
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->setLocation( $node->id, 'before' );
		$tEl->bind($vals);
	
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype',$tEl->type);
	
		$post = JRequest::get('post');
		$dispatcher->trigger( 'saveEl',array(&$tEl,$post) );

		if(!$tEl->store())
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$tEl->set('title',$tEl->type."-{$tEl->id}");
			$tEl->store();
			
			// Move the node
			$node->moveByReference($tEl->id,'last-child');
			
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			$result['id']= $node->id;
		}
			
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function edit()
	{
		$id = JRequest::getInt('id');
		$entry = OnepageHelper::getEditLog($id);
		
		$url = "index.php?option=com_onepage&view=prototype&layout=edit&id={$entry->eid}";
		$this->setRedirect($url);
	}
	
	public function saveFont()
	{
		$id = JRequest::getInt('id');
		$font_links = JRequest::getVar('font_links',array());
		$font_links = jsArrayNoEmpty($font_links);
		JArrayHelper::toString($font_links);
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		$params = $tEl->params;
		$new = array();
		foreach($font_links as $link)
		{
			$new[] = array('link'=>$link);
		}
		$params['font_links'] = $new;
		
		$vals = array();
		$vals['params'] = $params;
	
		$tEl->bind($vals);
		$tEl->store();
		
		$result = array();
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		jsExit(jsJSON::encode($result));
	}
	
	public function moveTo()
	{
		$id = JRequest::getInt('id');
		$target = JRequest::getInt('target');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
		
		$tEl->moveByReference($target,'last-child');
	
		if(!$tEl->store())
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= $tEl->getError();
		}
		else
		{
			$result['success']= true;
			$result['itemtitle']= $tEl->title;
			//$result['type']= $type;
			$result['showtitle']= $tEl->showtitle;
			$result['params']= $tEl->params;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
		}
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function duplicate()
	{
		$id = JRequest::getInt('id');
		$target = JRequest::getInt('target');
	
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
	
		
		
		$node = JTable::getInstance('Page','OnepageTable');
		$node->setLocation($tEl->id, 'after');
		$node->bind($tEl->getProperties());
		
		$node->set('id',0);
		$updated = $node->store();
		$node->set('title',$node->type."-{$node->id}");
		$updated = $node->store();
		//
		$path = OnepageHelper::getImgPath();
		$dispatcher = jsGetDispatcher();
		JPluginHelper::importPlugin('opeltype');
		$data = '';
		$dispatcher->trigger( 'import',array($tEl,$node,$path) );
		
		$model = $this->getModel('prototype');
		$model->duplicateChildren($tEl,$node,$path);
		
		//$result['link']= "index.php?option=com_onepage&view=prototype&layout=edit&id={$node->id}";
	
		$result['success']= true;
		$result['itemtitle']= $tEl->title;
		$result['type']= $tEl->type;
		$result['showtitle']= $tEl->showtitle;
		$result['params']= $tEl->params;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		$result['id'] = $node->id;
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
}
