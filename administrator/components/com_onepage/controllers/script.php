<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 * @since       1.6
 */
class OnepageControllerScript extends JControllerForm
{
	/**
	 * Class constructor.
	 *
	 * @param   array  $config  A named array of configuration variables.
	 *
	 * @since	1.6
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	public function saveCode()
	{
		$db = jsDB::instance();
		// Error in upload
	
		$id = JRequest::getInt('id');
		$type = JRequest::getCmd('type');
		$code = JRequest::getString('css');
		
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($id);
	
		if($type == 'css')
		{
			$tEl->set('css',$code);
		}
		else
		{
			$tEl->set('js',$code);
		}
		
		$tEl->store();
	
		$result = array();
		$result['success']= true;
		$result['title']= JText::_('SUCCESS');
		$result['content']= JText::_('SUCCESS');
		jsExit(jsJSON::encode($result));
	}
	
	public function generateJS()
	{
		$output = '';
		$id = JRequest::getInt('id');
		$layout = JRequest::getCmd('layout');
	
		//$model = $this->getModel('products');
		//$tree = $model->getTree($id);
	
		$output = '';
		$output.= 'var getId = function() {';
		$output.= 'return '.$id.';';
		$output.= '}';
	
	
		if(empty($layout))
		{
			$model = $this->getModel('page');
			$list = $model->getList($id);
			$output.= "\n";
			$output.= 'var getList = function() {';
			$output.= 'return '.jsJSON::encode($list).';';
			$output.= '}';
		}
	
		header('Content-Type: text/javascript');
		jsExit($output);
	}
}
