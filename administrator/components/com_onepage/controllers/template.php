<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
/**
 * Articles list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since	1.6
 */
class onepageControllerTemplate extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param	array	$config	An optional associative array of configuration settings.

	 * @return	ContentControllerArticles
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		// Articles default form can come from the articles or featured view.
		// Adjust the redirect view on the value of 'view' in the request.
		
		parent::__construct($config);
	}

	
	
	public function save()
	{
		$vals = array();
		
		
		$id = JRequest::getInt('id');
		
		$vals['title'] = JRequest::getString('title');
		$vals['html'] = JRequest::getString('html','','post',JREQUEST_ALLOWRAW);
		$vals['js'] = JRequest::getString('js','','post',JREQUEST_ALLOWRAW);
		$vals['css'] = JRequest::getString('css','','post',JREQUEST_ALLOWRAW);
		$vals['itemcss'] = JRequest::getString('itemcss','','post',JREQUEST_ALLOWRAW);
		$vals['mobile'] = JRequest::getString('mobile','','post',JREQUEST_ALLOWRAW);
		$vals['tablet'] = JRequest::getString('tablet','','post',JREQUEST_ALLOWRAW);
		$vals['enable_js'] = JRequest::getInt('enable_js');
		$vals['code'] = JRequest::getString('code');
		
		$tEl = JTable::getInstance('Tpl','onepageTable');
		$tEl->load($id);
		
		$tEl->bind($vals);
		$updated = $tEl->store();
		
		if($updated)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			$result['id']= $tEl->id;
			$result['code']= $tEl->code;
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ERROR');
		}
		jsExit(jsJSON::encode($result));
	}
	
	public function generateJS()
	{
		$output = '';
		$id = JRequest::getInt('id');
		$layout = JRequest::getCmd('layout');
		$tEl = JTable::getInstance('Tpl','OnepageTable');
		$tEl->load($id);
		//$model = $this->getModel('products');
		//$tree = $model->getTree($id);
	
		$output = '';
		$output.= 'var getId = function() {';
		$output.= 'return '.$id.';';
		$output.= '}';
	
	
		$output.= "\n";
		$output.= 'var getInfo = function() {';
		$output.= 'return '.jsJSON::encode($tEl->getProperties()).';';
		$output.= '}';
			
		$output.= "\n";
		
		$output.= 'var getTpl = function() {';
		$output.= 'return '.jsJSON::encode( OnepageHelperPlg::getTpl($tEl) ).';';
		$output.= '}';
		
		header('Content-Type: text/javascript');
		jsExit($output);
	}
}
