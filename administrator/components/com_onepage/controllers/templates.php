<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
/**
 * Articles list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since	1.6
 */
class onepageControllerTemplates extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param	array	$config	An optional associative array of configuration settings.

	 * @return	ContentControllerArticles
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		// Articles default form can come from the articles or featured view.
		// Adjust the redirect view on the value of 'view' in the request.
		
		parent::__construct($config);
	}

	public function execute($task)
	{	
		parent::execute($task);
	}
	

	/**
	 * Proxy for getModel.
	 *
	 * @param	string	$name	The name of the model.
	 * @param	string	$prefix	The prefix for the PHP class name.
	 *
	 * @return	JModel
	 * @since	1.6
	 */
	/*public function getModel($name = 'Article', $prefix = 'ContentModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}*/
	
	protected function _correctDate($date = null)
	{
		$now = jsGetDateTime();
		$d = date_create($now);
		$nowTime = $d->format('H:i:s');
		
		if(strlen($date) == 10)
		{
			$date .= " {$nowTime}";
		}
		elseif(strlen($date) > 10)
		{
			//$date = $date;
		}
		else
		{
			$date = $now;
		}
		return $date;
	}
	
	public function add()
	{
		$db = jsDB::instance();
		$result = array();
		$vals = array();
		
		
		//$vals['id'] = JRequest::getInt('id');
		
		$vals['type'] = JRequest::getCmd('type');
		$vals['title'] = $vals['type']." - Template";
		$vals['create_date'] = jsGetDateTime();
		
		$tEl = JTable::getInstance('Tpl','onepageTable');
		
		$tEl->bind($vals);
		$updated = $tEl->store();
		
		if($updated)
		{
			$tpls = OnepageHelperPlg::getTpl($tEl);
			$tpl = $tpls[0];
			$tEl->bind($tpl);
			$tEl->store();
			
			$result = $this->_getList();
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			$result['id']= $tEl->id;
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ERROR');
		}
		jsExit(jsJSON::encode($result));
	}
	
	
	
	public function remove()
	{
		// just hide
		$ids = JRequest::getVar('cid',array(),'post','array');
		$delIds = $ids;

		$db = jsDB::instance();
		$updated = true;
		foreach($delIds as $id)
		{
			$updated = jsDB::delete('#__onepage_tpl', array('id'=>$id));
			if(!$updated) break;
		}
		
		if($updated)
		{
			$result = $this->_getList();
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
			
		}
		else
		{
			$result['success']= false;
			$result['title']= JText::_('ERROR');
			$result['content']= JText::_('ERROR');
		}
		
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	// deprecated
	public function changeOrder()
	{
		$id = JRequest::getInt('id');
		$direction = JRequest::getInt('move');
		
		$tEl = JTable::getInstance('Menu','OnepageTable');
	
		if($direction < 0)
		{
			$updated = $tEl->orderUp($id);
		}
		else
		{
			$updated = $tEl->orderDown($id);
		}
		
		
		$this->setRedirect('index.php?option=com_onepage&view=menus');
	}
	
	public function publish()
	{
		$publish = JRequest::getInt('publish');
		$ids = JRequest::getVar('cid',array(),'post','array');
		JArrayHelper::toInteger($ids);
		$tEl = JTable::getInstance('Tpl','OnepageTable');
		$finished = true;
		$parent_id = 2;
		
		$published = JRequest::getInt('published');
		//$this->setRedirect('index.php?option=com_onepage&view=menus');
		foreach($ids as $id)
		{
			$tEl->load($id);
			if($published < 0)
			{
				if($tEl->published)
				{
					$tEl->set('published',0);
				}
				else
				{
					$tEl->set('published',1);
				}
			}
			else
			{
				$tEl->set('published',$published);
			}
			
			if(!$tEl->store())
			{
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= $tEl->getError();
				$finished = false;
				break;
			}
		}
		
		if($finished)
		{
			$result['success']= true;
			$result['title']= JText::_('SUCCESS');
			$result['content']= JText::_('SUCCESS');
		}
	
	
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	
	public function resort()
	{
		$ids = JRequest::getVar('el_ids',array());

		if(count($ids) < 1) exit;
		
		// allow the same level only.
		
		$tEl = JTable::getInstance('Menu','OnepageTable');
		$parent_id = 0 ;
		foreach($ids as $id)
		{
			$id = (int)str_replace('item_','',$id);
			$tEl->load($id);
			$tEl->moveByReference($tEl->parent_id,'last-child');
		}
	
		$updated = true;
		if($updated)	
		{
			$result['success']= true;
			$result['title'] = JText::_('SUCCESS');
			$result['content'] = JText::_('SUCCESS');
		}
		else
		{
			$result['success']= false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
		}
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function move()
	{
		$ids = JRequest::getVar('cid',array());
		JArrayHelper::toInteger($ids);
		$target = JRequest::getInt('target_id');
		$target = ($target <3)?3:$target;
		if(count($ids) < 1) exit;
		
		$tTarget = JTable::getInstance('Menu','OnepageTable');
		$tTarget->load($target);
		if(OnepageHelper::isElement($tTarget))
		{
			$result['success']= false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
	
			$result = jsJson::encode($result);
			jsExit($result);
		}
		// allow the same level only.
		
		$tEl = JTable::getInstance('Menu','OnepageTable');
		foreach($ids as $id)
		{
			$tEl->load($id);
			$tEl->moveByReference($target,'last-child');
		}
		
		$updated = true;
		if($updated)
		{
			$result['success']= true;
			$result['title'] = JText::_('SUCCESS');
			$result['content'] = JText::_('SUCCESS');
		}
		else
		{
			$result['success']= false;
			$result['title'] = JText::_('ERROR');
			$result['content'] = JText::_('ERROR');
		}
		$result = jsJson::encode($result);
		jsExit($result);
	}
	
	public function generateJs()
	{
		header('Content-Type: text/javascript');
		$id = JRequest::getInt('id');
		$id = 1;
		$model = $this->getModel('users');
		$tree = $model->getItems();
		
		$cp = $model->getState('limit.start',0);
		$limit = $model->getState('list.limit');
		$total = $model->getTotal();
		$cp = max(0, (int) (ceil($total / $limit) - 1));
		
		$output = '';
		$output.= 'var getList = function() {';
		$output.= 'return '.jsJSON::encode($tree).';';
		$output.='}';
		$output.="\r\n";
		$output.= 'var getCurrentPage = function() {';
		$output.= 'return '.$cp.';';
		$output.='}';
		$output.="\r\n";
		$output.= 'var getTotal = function() {';
		$output.= 'return '.$total.';';
		$output.='}';
		
		jsExit($output);
	}
	
	public function getList()
	{
		header('Content-Type: text/javascript');
		//$page = JRequest::getInt('page');
		$result = $this->_getList();
		
		$output = jsJSON::encode($result);
		jsExit($output);
	}
	
	protected function _getList()
	{
		$groups = OnepageHelper::getGroupType();
		$items = OnepageHelper::getItemType();
		
		$model = $this->getModel('templates');
		//$model->setState('list.start',min($page-1,0)*10);
		//jsExit($model->getState('list.start'));
		
		
		$limit = $model->getState('list.limit');
		//$model->setState('list.start',10);
		$limitStart = $model->getStart();
		$total = $model->getTotal();
		if($limitStart == $total)
		{
			$limitStart -= $limit;
			$model->setState('list.start',$limitStart);
		}
		$cp = max(0, (int) (ceil( ($limitStart+1) / $limit)));
		
		$tree = $model->getItems();
		
		$result = array();
		$result['list'] = $tree;
		$result['cp'] = $cp;
		$result['total'] = $total;
		
		return $result;
	}
	
	public function clean()
	{
		$now = jsGetDateTime();
		$valid = new JDate($now." -7 day");
		
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->update('`#__jsmoney_order`');
		$query->set("`status`='record'");
		$query->where("`status`='pending'");
		$query->where("`create_date`< '{$valid}'");
		
		jsDB::query($query);
		$result = array();
		$result['success'] = true;
		
		jsExit(jsJSON::encode($result));
	}
}
