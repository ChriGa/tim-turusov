<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.filesystem.folder' );

class pkg_OnepageInstallerScript
{
	public $com = 'com_onepage';
	public $img_folder = 'onepage';
	public $table = '#__onepage_page';
	public $manifest;
	
	function preflight($type, $parent)
	{
		if($parent->manifest->version == '1.4.0')
		{
			$this->fixBugsv14();
		}
	}
	
	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent)
	{
		
		if($type != 'uninstall' )
		{
			$this->manifest = $parent->manifest;
			//$this->fixBugsv14();
			$this->init();
			
			// Enable Plugin - Forced to enable since 1.3.1
			$db = JFactory::getDbo();
			$query = " UPDATE `#__extensions`"
					." SET `enabled` = 1"
					." WHERE `name` = 'plg_system_onepagecss'"
					;
			$db->setQuery($query);
			$db->query();
			
			$query = " UPDATE `#__extensions`"
					." SET `enabled` = 1"
					." WHERE `folder` = 'opeltype' AND `type`='plugin'"
					;
			$db->setQuery($query);
			$db->query();
			
			// Update the SQL `#__onepage_default`
			$this->initExt();
			$this->initLang();
			$this->initTpl();
			
			$lang = JFactory::getLanguage();
			$lang->load('com_onepage');
			
			echo JText::_('COM_ONEPAGE_PACKAGE_POSTFLIGHT_MSG');
		
		}
	}
	
	protected function initExt()
	{
		// check if table `jsapp_default` exists
		$db = JFactory::getDbo();
		$tables = $db->getTableList();
		$app = JFactory::getApplication('admin');
		$dbprefix = $app->getCfg('dbprefix');
			
		if(in_array("{$dbprefix}jsapp_default",$tables))
		{
			if(in_array("{$dbprefix}onepage_default",$tables))
			{
				$query = $db->getQuery(true);
				$query->select('*');
				$query->from('`#__onepage_default`');
				$db->setQuery($query);
				$objs = $db->loadObjectList();
				
				$query = $db->getQuery(true);
				$query->insert('`#__jsapp_default`');
				$query->columns( '`key`,`value`,`app`' );
				foreach($objs as $k => $obj)
				{
					$query->values("'{$obj->key}','{$obj->value}','onepage'");	
				}
				$db->setQuery($query);
				$db->query();
				
				$query = 'RENAME TABLE  `#__onepage_default` TO  `#__onepage_default_deprecated` ';
				$db->setQuery($query);
				$db->query();
			}
		}
		elseif(in_array("{$dbprefix}onepage_default",$tables))
		{
			$query = 'RENAME TABLE  `#__onepage_default` TO  `#__jsapp_default` ';
			$db->setQuery($query);
			$db->query();
			
			$query = "ALTER TABLE  `#__jsapp_default` ADD  `app` VARCHAR( 20 ) NOT NULL DEFAULT  'onepage' AFTER  `default`";
			$db->setQuery($query);
			$db->query();
		}
		
		//
		$file = JPATH_SITE."/components/com_onepage/views/home/tmpl/default.xml";
		if(JFile::exists($file))
		{
			JFile::move($file, JPATH_SITE."/components/com_onepage/views/home/tmpl/default.txt");
		}
		
		$file = JPATH_SITE."/components/com_onepage/views/single/tmpl/default.xml";
		if(JFile::exists($file))
		{
			JFile::move($file, JPATH_SITE."/components/com_onepage/views/single/tmpl/default.txt");
		}
		return true;
	}
	
	protected function init()
	{
		$db = JFactory::getDbo();
	
		// create image folder
		$imageFolderPath = JPATH_SITE.'/images/'.$this->img_folder;
		if(!JFolder::exists($imageFolderPath))
		{
			JFolder::create($imageFolderPath);
			JFile::copy(dirname($imageFolderPath).'/index.html',$imageFolderPath.'/index.html');
		}
	
		// cover latest jskit
		$libPath = JPATH_LIBRARIES.'/jskit';
		$comKitPath = JPATH_ADMINISTRATOR."/components/{$this->com}/jskit";
		if(JFolder::copy($comKitPath,$libPath,'',true))
		{
			JFolder::delete($comKitPath);
		}
	
		// cover latest js & css
		$libPath = JPATH_SITE.'/media/jsbt3';
		$comKitPath = JPATH_SITE."/components/{$this->com}/media/jsbt3";
		if(JFolder::copy($comKitPath,$libPath,'',true))
		{
			JFolder::delete($comKitPath);
			JFolder::delete(dirname($comKitPath));
		}
	
		// Have Nested Table? Insert Root
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from("`{$this->table}`");
		$query->where('`level` = "0"');
		$db->setQuery($query);
		$obj = $db->loadObject();
	
		if( empty($obj) )
		{
			$query = $db->getQuery(true);
			$query->insert("`{$this->table}`");
			$query->columns('`title`,`level`,`parent_id`,`lft`,`rgt`');
			$query->values("'Root',0,0,0,3");
			$db->setQuery($query);
			$db->query();
			
			//JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_onepage/tables/');
			
			$query = $db->getQuery(true);
			$query->insert("`{$this->table}`");
			$query->columns('`title`,`level`,`parent_id`,`lft`,`rgt`,`code`');
			$query->values("'English',1,1,1,2,'en'");
			$db->setQuery($query);
			$db->query();
		}
		
		// update the version
		$manifest = $this->manifest;
		//$mainfest = jsJSON::decode($comItem->manifest_cache);
		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__jsapp_default');
		$query->where("`app` = '{$this->img_folder}'");
		$query->where("`key` = 'version'");
		$db->setQuery($query);
		$obj = $db->loadObject();
			
		if(!empty($obj))
		{
			$query = $db->getQuery(true);
			$query->update('#__jsapp_default');
			$query->set("`value`='{$manifest->version}'");
			$query->where("`app` = '{$this->img_folder}'");
			$query->where("`key` = 'version'");
			$db->setQuery($query);
			$db->query();
		}
		else
		{
			$query = $db->getQuery(true);
			$query->insert('#__jsapp_default');
			$query->columns(array('`app`','`key`','`value`'));
			$query->values(array("'{$this->img_folder}','version','{$manifest->version}'"));
			$db->setQuery($query);
			$db->query();
		}
		
		$date = JFactory::getDate();
		$now = $date->toSql(true);
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__jsapp_default');
		$query->where("`app` = '{$this->img_folder}'");
		$query->where("`key` = 'version_date'");
		$db->setQuery($query);
		$obj = $db->loadObject();
			
		if(!empty($obj))
		{
			$query = $db->getQuery(true);
			$query->update('#__jsapp_default');
			$query->set("`value`='{$now}'");
			$query->where("`app` = '{$this->img_folder}'");
			$query->where("`key` = 'version_date'");
			$db->setQuery($query);
			$db->query();
		}
		else
		{
			$query = $db->getQuery(true);
			$query->insert('#__jsapp_default');
			$query->columns(array('`app`','`key`','`value`'));
			$query->values(array("'{$this->img_folder}','version_date','{$now}'"));
			$db->setQuery($query);
			$db->query();
		}
	
		//
		$query = $db->getQuery(true);
		$query->update('#__jsapp_default');
		$query->set("`value`='1'");
		$query->where("`app` = '{$this->img_folder}'");
		$query->where("`key` = 'updatetpl'");
		$db->setQuery($query);
		$db->query();
		return;
	}
	
	protected function initLang()
	{
		// copy language to frontend
		$updated = array();
		$com = $this->com;
		$src = JPATH_SITE."/components/{$com}/language/";
		$dest = JPATH_SITE.'/language/';
		$folders = JFolder::folders($src);
		foreach($folders as $folder)
		{
			$updated[] = JFile::copy($src."{$folder}/{$folder}.$com.ini",$dest."{$folder}/{$folder}.$com.ini");
		}
		$updated = array_unique($updated);
		if(count($updated) > 1)
		{
			// Error
		}
		else
		{
			// Delete Language Folder
			JFolder::delete($src);
		}
	}
	
	protected function initTpl()
	{
		// fixed the database
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__template_styles');
		$query->where("`template` = 'jsbootstrap3'");
		$db->setQuery($query);
		$obj = $db->loadObject();
		
		if(empty($obj))
		{
			$params = '{"leftColumnWidth":"3","rightColumnWidth":"4","logo_file":"images\/joomla_black.gif","logo_link":"index.php","logo_width":"173","logo_height":"26"}';
			$params = $db->quote($params);
			$query = $db->getQuery(true);
			$query->insert('#__template_styles');
			$query->columns(array('template','client_id','home','title','params'));
			$query->values("'jsbootstrap3',0,0,'(JS) Bootstrap3 - Default',{$params}");
			$db->setQuery($query);
			$db->query();
		}
		
		// update them
		$comKitPath = JPATH_SITE.'/templates/jsbootstrap3/html/';
		JFolder::delete($comKitPath.'com_content');
		if(JVERSION >= 3)
		{
			JFolder::copy($comKitPath.'com_content3', $comKitPath.'com_content');
		}
		else
		{
			JFolder::copy($comKitPath.'com_content2', $comKitPath.'com_content');
		}
	}
	
	protected function fixBugsv14()
	{
		$db = JFactory::getDbo();
		$tables = $db->getTableList();
		$app = JFactory::getApplication('admin');
		$dbprefix = $app->getCfg('dbprefix');
		
		if(in_array("{$dbprefix}onepage_menu",$tables) && !in_array("{$dbprefix}onepage_page",$tables))
		{
			$query = "RENAME TABLE `#__onepage_menu` TO `#__onepage_page`;";
			$db->setQuery($query);
			$db->query();
		}
		elseif(!in_array("{$dbprefix}onepage_menu",$tables) && !in_array("{$dbprefix}onepage_page",$tables))
		{
			return ;
		}
		
		$tables = $db->getTableList();
		if(in_array("{$dbprefix}onepage_page",$tables))
		{
			$columns = $db->getTableColumns('#__onepage_page');
			
			if(!array_key_exists('html', $columns))
			{
				$query = "ALTER TABLE  `#__onepage_page` ADD  `html` TEXT NULL DEFAULT NULL AFTER  `content`";
				$db->setQuery($query);
				$db->query();
			}
			
			if(!array_key_exists('js', $columns))
			{
				$query = "ALTER TABLE  `#__onepage_page` ADD  `js` TEXT NULL DEFAULT NULL AFTER  `html`";
				$db->setQuery($query);
				$db->query();
			}
			
			if(!array_key_exists('css', $columns))
			{
				$query = "ALTER TABLE  `#__onepage_page` ADD  `css` TEXT NULL DEFAULT NULL AFTER  `js`";
				$db->setQuery($query);
				$db->query();
			}
			
			
			if(!array_key_exists('md', $columns))
			{
				$query = "ALTER TABLE  `#__onepage_page` ADD  `md` INT( 2 ) NOT NULL DEFAULT  '4' AFTER  `css`";
				$db->setQuery($query);
				$db->query();
			}
			
			if(!array_key_exists('ver', $columns))
			{
				$query = "ALTER TABLE  `#__onepage_page` ADD  `ver` VARCHAR( 7 ) NOT NULL DEFAULT  '1.3.0'";
				$db->setQuery($query);
				$db->query();
			}
			
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__onepage_page');
			$query->where("`id` = 1");
			$db->setQuery($query);
			$obj = $db->loadObject();
			
			if($obj->level == 1)
			{
				$query = "UPDATE `#__onepage_page` SET `level` = `level`-1;";
				$db->setQuery($query);
				$db->query();
				
				//$query = "UPDATE `#__onepage_page` SET `type` = 'item' WHERE `type` IN ('article', 'editor','image');";
				//jsDB::query($query);
			}
		}
		
		$query = "CREATE TABLE IF NOT EXISTS `#__onepage_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `eid` int(11) DEFAULT '0',
  `default` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8";
		$db->setQuery($query);
		$db->query();
	}
}