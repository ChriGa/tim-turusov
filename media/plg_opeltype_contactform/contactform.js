$('#[wrap_id]').ajaxForm({
	dataType:  'json',
	type: 'POST',
	beforeSubmit: function()	{NProgress.start();
		/*-- Mixpanel --*/mixpanel.track( "ContactForm.Send.[wrap_id]");/*-- Mixpanel --*/
	},
	success: function(res)	{ 
		if(res.success) {	
			noty({text: '<i class=\"fa fa-check-circle\"></i> '+res.content,type: 'success', timeout: 3000 }); 
		} else {
			noty({text: '<i class=\"fa fa-times-circle\"></i> '+res.content,type: 'error', timeout: 3000 }); 
		}
		NProgress.done();
	}
});

$('#[wrap_id]').parsley().subscribe('parsley:field:error', function (fieldInstance) {
var errors = window.ParsleyUI.getErrorsMessages(fieldInstance);
var eString = errors[0];
fieldInstance.$element.closest('.form-group').removeClass('has-success').addClass('has-error').children('.form-control-feedback').html('*'+eString);
});

$('#[wrap_id]').parsley().subscribe('parsley:field:success', function (fieldInstance) {
fieldInstance.$element.closest('.form-group').removeClass('has-error').addClass('has-success').children('.form-control-feedback').html('');
});