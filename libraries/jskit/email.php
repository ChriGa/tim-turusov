<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsEmail
{
	protected static $instance;
	public $subject = null;
	public $body = null;
	
	public static function instance()
	{
		if(!self::$instance instanceof jsEmail)
		{
			self::$instance = new jsEmail();
		}
		
		return self::$instance;
	}

	public function addVariable($key,$val)
	{
		$this->subject = str_replace("[{$key}]",$val,$this->subject);
		$this->body = str_replace("[{$key}]",$val,$this->body);
	}
	
	public function addVariables($vals = array())
	{
		foreach($vals as $k => $v)
		{
			$this->addVariabe($k,$v);
		}
	}
	
	protected function send($subject, $body,$address,$attachment = array(),$attname = array())
	{
		$mail = JFactory::getMailer();
		$mail->addRecipient($address);
		$mail->setSubject($subject);
		$mail->setBody($body);
		$mail->IsHTML(true);
		
		if(!empty($attachment))
		{
			$attname = empty($attname)?'':$attname;
			$mail->addAttachment($attachment,$attname);
		}
		return $mail->Send();
	}
	
	public function sendEmailWithAttachment($emailAddress = null,$attachment = array(),$attname = array())
	{
		//$email = self::getDoc($email_id,'obj');
		if(empty($emailAddress))
		{
			return false;
		}
		return $this->send($this->subject, $this->body,$emailAddress,$attachment,$attname);
	}
	
	function sendEmail($emailAddress = null)
	{
		//$email = self::getDoc($email_id,'obj');
		if(empty($emailAddress))
		{
			return false;
		}
		return $this->send($this->subject, $this->body,$emailAddress);
	}
	
	function sendToGroup($email,$group,$force2Send = false)
	{
		$db = jsDB::instance();

		if(is_array($group))
		{
			$group = implode(',',$group);
		}
	
		$where = array();
		if(!$force2Send)
		{
			$where[] = "`sendEmail` =1";
		}
	
		$where[] = "g.group_id IN ( {$group} )";
		$where = oseDB::implodeWhere($where);
		$query = " SELECT u.* FROM `#__users` AS u "
				." INNER JOIN `#__user_usergroup_map` AS g ON g.user_id = u.id"
				. $where
				;

		$objs = jsDB::loadList($query,'obj');

		foreach($objs as  $obj)
		{
			$this->sendEmail($obj->email);
		}

		return true;
	}
}