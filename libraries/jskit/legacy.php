<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

if( !class_exists('JModelLegacy') && (JVERSION < 3 ) )
{
	class JModelLegacy extends JModel
	{
		
	}
}

if( !class_exists('JControllerLegacy') && (JVERSION < 3 ) )
{
	class JControllerLegacy extends JController
	{

	}
}

if( !class_exists('JViewLegacy') && (JVERSION < 3 ) )
{
	class JViewLegacy extends JView
	{

	}
}

if(!defined('DS'))
{
	define('DS',DIRECTORY_SEPARATOR);
}