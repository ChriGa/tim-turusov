<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsCart extends JObject
{
	function __construct()
	{
		// check if it is user mode
		$user = JFactory::getUser();
		if($user->guest)
		{
			// only get session
			$session = JFactory::getSession();
			$items = $session->get('jsCartItems',array());
		}
		else
		{
			
		}
	}
	
	public function add($sku)
	{
		$this->items[$sku] = $sku;
	}
}