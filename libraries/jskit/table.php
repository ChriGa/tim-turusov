<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die(";)");

class jsTable extends JTable
{
	public function bind($array, $ignore = array())
	{
		$updated = parent::bind($array,$ignore);
	
		if(jsGetValue($array,'params'))
		{
			$this->params = jsGetValueNo0($array,'params',array());
		}
		else
		{
			$this->params = jsGetValueNo0($this,'params',array());
		}
		
		if( is_string( $this->params) )
		{
			$this->params = jsJson::decode($this->params,true);
		}
	
		return $updated;
	}
	
	function store($updateNulls = false)
	{
		$params = $this->params;
		if( is_array( $this->params) || is_object( $this->params) )
		{
			$this->params = jsJson::encode($this->params);
		}
		$updated = parent::store($updateNulls);
		$this->params = $params;
		return $updated;
	}
	
	public function loadByCode($code)
	{
		$db = $this->_db;
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from("`{$this->_tbl}`");
		$query->where("`code`=".$db->quote($code));
		$item = jsDB::loadObj($query);
		if(!empty($item))
		{
			$this->bind($item);
		}
	
	}
}