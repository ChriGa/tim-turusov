<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsAppHelper extends JObject
{
	static protected function _createMenu($menus,$view)
	{
		$html = '';
		foreach($menus as $menu)
		{
			$title= jsGetValueNo0($menu, 'title','');
			$mView = jsGetValue($menu, 'view');
			$default = jsGetValueNo0($menu, 'default',false);
			$attr = jsGetValueNo0($menu, 'attr','');
			$path = jsGetValueNo0($menu, 'path','');
			
			$html.= '<li';
			$active = false;
			if($view == $mView || ($default && empty($view)))
			{
				$html .= ' class="active"';
				$active = true;
			}
			$html .= ' >';
			
			if($active)
			{
				$html .= "<a href=\"#\" {$attr}>".$title.'</a>';
			}
			else
			{
				$html .= "<a href=\"{$path}\" {$attr}>".$title.'</a>';
			}
			
			$html .= '</li>';
		}
		
		return $html;
	}
	
	static protected function _isBt3Template()
	{
		$app = JFactory::getApplication();
		$current = $app->getTemplate();
		
		$tpls = array('jsbootstrap3');
		return in_array($current,$tpls);
	}
	
	public static function checkVersion(&$config)
	{
		$db = jsDB::instance();
		
		// exclude, j-soho.com
		$uri = JFactory::getURI();
		//$prefix = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
		$prefix = $uri->root();
		if( in_array($prefix,array('http://j-soho.com')) )
		{
			return false;
		}
		
		if(!$config->get('check_update'))
		{
			return false;
		}
		
		$check = false;
		$check_date = $config->get('check_date',false);
		
		if(empty($check_date))
		{
			$check = true;
			
			$now = jsGetDate($check_date);
			$nowTime = $now->format('Y-m-d',true);
			$config->set('check_date',$nowTime);
		}
		else
		{
			// check one time each day
			$d = jsGetDate($check_date);
			$cd = $d->format('Y-m-d');
			
			$now = jsGetDate($check_date);
			$nowTime = $now->format('Y-m-d');
			if($cd != $nowTime)
			{
				$check = true;
			}
			
			$config->set('check_date',$nowTime);
		}
		
		if($check)
		{
			$not = jsNotification::getInstance();
			$latest = $not->call('product',array('id'=>JS_APP_ID));
			
			if($latest === false || !$latest->success)
			{
				return false;
			}
			
			// update the update info.
			$config->set('version_title',$latest->title);
			$config->set('latest_version',$latest->version);
			$config->set('latest_version_date',$latest->update_date);
			$config->set('update_link',$latest->link);
			$config->set('check_date',$nowTime);
			$config->set('version_desc',$latest->desc);
		}	
		
		/*elseif(  !$config->getNo0('ignore_version',false) )
		{
			$config->set('ignore_version','0.0.0');
			return false;
		}*/
		if( $config->get('ignore_version') == $config->get('latest_version')  )
		{
			// sometimes we do not change the version, but make some bugs fixed
			if($config->get('ignore_version_date') == $config->get('latest_version_date'))
			{
				return false;
			}
			else
			{
				// mark it as another version.
				$config->set('ignore_version','0.0.0');
			}
		}
		elseif( $config->get('latest_version') == $config->get('version') )
		{	
			// sometimes we do not change the version, but make some bugs fixed
			if( $config->get('version_date') > $config->get('latest_version_date') )
			{
				return false;
			}
		}
	
		//$config->set('version_date',$latest->update_date);
		
		return true;
	}
	
	public static function checkDevice()
	{
		$device = 'desktop';
		if(JVERSION < 3)
		{
			$appWeb = new jsWebClient();
				
			if ($appWeb->mobile )
			{
				if(in_array($appWeb->platform,array(5,22)))
				{
					$device = 'tablet';
				}
				else
				{
					$device = 'mobile';
				}
			}
		}
		else
		{
			$appWeb = JFactory::getApplication('web');
				
			if ($appWeb->client->mobile )
			{
				if(in_array($appWeb->client->platform,array(5,22)))
				{
					$device = 'tablet';
				}
				else
				{
					$device = 'mobile';
				}
					
			}
		}
		return $device;
	}
	
	public static function getAcyMailingList()
	{
		$db = jsDB::instance();
	
		$tables = $db->getTableList();
		$app = JFactory::getApplication('admin');
		$dbprefix = $app->getCfg('dbprefix');
	
		$list = array();
	
		if(in_array("{$dbprefix}acymailing_list",$tables))
		{
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__acymailing_list');
			$query->where("`published` = 1");
			$list = jsDB::loadObjs($query);
		}
	
		return $list;
	}
}