<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsAppConfig extends JObject
{
	protected $table= '#__jsapp_default';
	protected $db = '';
	protected static $app = '';
	protected static $items = array();
	static protected $instances = array();
	function __construct($app)
	{
		$this->app = $app;
		$db = $this->db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from("`{$this->table}`");
		$query->where("`app` = '{$app}'");
		$db->setQuery($query);
		$this->items = $db->loadObjectList('key');
	}
	
	public static function getInstance($app='onepage')
	{
		if( empty(self::$instances[$app]) ) // !self::$instances[$app] instanceof jsAppConfig
		{
			$instance = new jsAppConfig($app);
			self::$instances[$app] = $instance;
		}
		
		return self::$instances[$app];
	}
	
	function get($key,$default='')
	{
		$item = jsGetValue($this->items, $key);
		if(empty($item))
		{
			return $default;
		}
		else
		{
			return $item->value;
		}
	}
	
	function getNo0($key,$default='')
	{
		$item = jsGetValue($this->items, $key);
		if(empty($item) || !jsGetValueNo0($item,'value',false))
		{
			return $default;
		}
		else
		{
			return jsGetValueNo0($item,'value',false);
		}
	}
	
	
	function set($key,$value)
	{
		$item = jsGetValue($this->items, $key,'');
		if(empty($item))
		{
			$vals = array();
			$vals['key'] = $key;
			$vals['value'] = $value;
			$vals['app'] = $this->app;
			$updated =  jsDB::insert($this->table, $vals);
		}
		else
		{
			
			$vals = array();
			$vals['key'] = $key;
			$vals['value'] = $value;
			$updated =  jsDB::update($this->table, 'key', $vals);
		}
		
		if($updated)
		{
			$new = new stdClass();
			$new->key = $key;
			$new->value = $value;
			$this->items = jsSetValue($this->items, $key,$new);
			
		}
		
		return $updated;
	}
	
	function search($key,$value = null)
	{
		// first search in current app;
		$value = $this->get($key);
		if(empty($value))
		{
			$db = $this->db;
			$query = $db->getQuery(true);
			$query->select('`value`');
			$query->from($this->table);
			$query->where("`key` = '{$key}'");
			$query->order(' `id` DESC LIMIT 1 ' );
			$db->setQuery($query);
			
			$value = $db->loadResult();
		}
		return $value;
	}
}