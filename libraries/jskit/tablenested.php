<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die(";)");

jimport('joomla.database.tablenested');
class jsTableNested extends JTableNested
{
	public function bind($array, $ignore = array())
	{
		$updated = parent::bind($array,$ignore);
	
		if(jsGetValue($array,'params'))
		{
			$this->params = jsGetValueNo0($array,'params',array());
		}
		else
		{
			$this->params = jsGetValueNo0($this,'params',array());
		}
		
		if( is_string( $this->params) )
		{
			$this->params = jsJson::decode($this->params,true);
		}
	
		return $updated;
	}
	
	function store($updateNulls = false)
	{
		$params = $this->params;
		if( is_array( $this->params) || is_object( $this->params) )
		{
			$this->params = jsJson::encode($this->params);
		}
		$updated = parent::store($updateNulls);
		$this->params = $params;
		return $updated;
	}
	
	function getChildren($pk = null,$recursive=true,$diagnostic = false)
	{
		// Initialise variables.
		$k = $this->_tbl_key;
		$pk = (is_null($pk)) ? $this->$k : $pk;
		if($recursive)
		{
			// Get the node and children as a tree.
			$query = $this->_db->getQuery(true);
			$select = ($diagnostic) ? 'n.'.$k.', n.parent_id, n.level, n.lft, n.rgt' : 'n.*';
			$query->select($select);
			$query->from($this->_tbl.' AS n, '.$this->_tbl.' AS p');
			$query->where('n.lft > p.lft AND n.lft < p.rgt');
			$query->where('p.'.$k.' = '.(int) $pk);
			$query->order('n.lft');
			$this->_db->setQuery($query);
			$tree = $this->_db->loadObjectList();
			
			// Check for a database error.
			if ($this->_db->getErrorNum())
			{
				$e = new JException(JText::sprintf('JLIB_DATABASE_ERROR_GET_TREE_FAILED', get_class($this), $this->_db->getErrorMsg()));
				$this->setError($e);
				return false;
			}
		}
		else
		{
			// Get the node and children as a tree.
			$query = $this->_db->getQuery(true);
			$select = ($diagnostic) ? 'n.'.$k.', n.parent_id, n.level, n.lft, n.rgt' : 'n.*';
			$query->select($select);
			$query->from($this->_tbl.' AS n, '.$this->_tbl.' AS p');
			$query->where('n.lft > p.lft AND n.lft < p.rgt');
			$query->where('p.'.$k.' = '.(int) $pk);
			$query->where('n.level=p.level+1');
			$query->order('n.lft');
			$this->_db->setQuery($query);
			$tree = $this->_db->loadObjectList();
				
			// Check for a database error.
			if ($this->_db->getErrorNum())
			{
				$e = new JException(JText::sprintf('JLIB_DATABASE_ERROR_GET_TREE_FAILED', get_class($this), $this->_db->getErrorMsg()));
				$this->setError($e);
				return false;
			}
		}
		
	
		return $tree;
	}
	
	function removeChildren($pk = null)
	{
		// Initialise variables.
		$k = $this->_tbl_key;
		$pk = (is_null($pk)) ? $this->$k : $pk;
		
		// Lock the table for writing.
		if (!$this->_lock()) {
			// Error message set in lock method.
			return false;
		}
		
		// If tracking assets, remove the asset first.
		if ($this->_trackAssets)
		{
			$name		= $this->_getAssetName();
			$asset		= JTable::getInstance('Asset');
		
			// Lock the table for writing.
			if (!$asset->_lock()) {
				// Error message set in lock method.
				return false;
			}
		
			if ($asset->loadByName($name)) {
				// Delete the node in assets table.
				if (!$asset->delete(null, $children)) {
					$this->setError($asset->getError());
					$asset->_unlock();
					return false;
				}
				$asset->_unlock();
			}
			else {
				$this->setError($asset->getError());
				$asset->_unlock();
				return false;
			}
		}
		
		// Get the node by id.
		if (!$node = $this->_getNode($pk))
		{
			// Error message set in getNode method.
			$this->_unlock();
			return false;
		}
		
		// Should we delete all children along with the node?
		// Delete the node and all of its children.
		$query = $this->_db->getQuery(true);
		$query->delete();
		$query->from($this->_tbl);
		$query->where('lft > '.(int) $node->lft.' AND lft <'.(int) $node->rgt);
		$this->_runQuery($query, 'JLIB_DATABASE_ERROR_DELETE_FAILED');
	
		// Compress the left values.
		/*$width = (int) $node->width;
		$width -= 2;
		$query = $this->_db->getQuery(true);
		$query->update($this->_tbl);
		$query->set('lft = lft - '.$width);
		$query->where('lft > '.(int) $node->rgt);
		$this->_runQuery($query, 'JLIB_DATABASE_ERROR_DELETE_FAILED');*/
	
		// Compress the right values.
		$width = (int) $node->width;
		$width -= 2;
		$query = $this->_db->getQuery(true);
		$query->update($this->_tbl);
		$query->set('rgt = rgt - '.$width);
		$query->where('rgt > '.(int) $node->rgt);
		$this->_runQuery($query, 'JLIB_DATABASE_ERROR_DELETE_FAILED');
		// Unlock the table for writing.
		$this->_unlock();
		
		return true;
	}
}