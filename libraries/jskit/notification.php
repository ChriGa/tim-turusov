<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die(";)");

class jsNotification extends JObject
{
	protected static $app = '';
	protected static $items = array();
	static protected $instances = array();
	
	private $api_endpoint = 'https://j-soho.com/index.php?option=com_dlocker';
	//private $api_endpoint = 'http://127.0.0.1/workshop_j3/index.php?option=com_dlocker';
	private $verify_ssl   = false;
	
	public function __construct($app,$endpoint,$ssl)
	{
		$this->app = $app;
		$this->config = jsAppConfig::getInstance($app);
		
		if(!empty($endpoint))
		{
			$this->api_endpoint = $endpoint;
		}
		
		$this->verify_ssl = $ssl;
	}
	
	public static function getInstance($app = 'onepage',$endpoint = '', $ssl = false)
	{
		if( empty(self::$instances[$app])  ) //|| !self::$instances[$app] instanceof jsNotification
		{
			$instance = new jsNotification($app,$endpoint,$ssl);
			self::$instances[$app] = $instance;
		}
	
		return self::$instances[$app];
	}
	
	
	// PHP SIMPLE CALL
	public function call($method, $args=array())
	{
		return $this->_raw_request($method, $args);
	}
	
	private function _raw_request($method, $args=array())
	{
		//$args['apikey'] = $this->api_key;
		$url = $this->api_endpoint.'&task='.$method;
		$postFields = http_build_query($args);
		
		if (function_exists('curl_init') && function_exists('curl_setopt')){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields );
			$result = curl_exec($ch);
			curl_close($ch);
			
		}else{
			$json_data = json_encode($args);
			$result = file_get_contents($url, null, stream_context_create(array(
				    'http' => array(
				        'protocol_version' => 1.1,
				        'user_agent'       => 'PHP-MCAPI/2.0',
				        'method'           => 'POST',
				        'header'           => "Content-type: application/x-www-form-urlencoded\r\n".
				                              "Connection: close\r\n" .
				                              "Content-length: " . strlen($postFields) . "\r\n",
				        'content'          => $postFields,
			),
			)));
		}
		
		return $result ? jsJSON::decode($result) : false;
	}
}