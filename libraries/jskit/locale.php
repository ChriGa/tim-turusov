<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsLocale extends JObject
{
	protected $country= '#__jsapp_country';
	protected $city= '#__jsapp_city';
	protected $db = '';
	function __construct($app)
	{
		$this->db = jsDB::instance();
	}
	
	public static function getInstance($app='onepage')
	{
		if( !self::$instances[$app] instanceof jsAppConfig ) 
		{
			$instance = new jsAppConfig($app);
			self::$instances[$app] = $instance;
		}
		
		return self::$instances[$app];
	}
	
	public function getCountry($code)
	{
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($this->country);
		
		if(strlen($code) == 2)
		{
			$query->where("`code2` = ".$db->quote($code));
		}
		else
		{
			$query->where("`code3` = ".$db->quote($code));
		}
		$item = jsDB::loadObj($query,'obj');
		return $item;
	}
	
	public static function getCountryList()
	{
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('`#__jsapp_country`');
		$query->order('`title` ASC');
		$items = jsDB::loadObjs($query,'obj');
		return $items;
	}
}