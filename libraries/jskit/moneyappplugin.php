<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;
jsImportHelperOther('jsmoney','route');

class jsMoneyAppPlugin extends JPlugin
{
	protected $error;
	protected $name = 'common';
	protected $is_cc = false;
	protected $config;
	protected static $items = array();
	//protected static $i;
	
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	
		$lang = JFactory::getLanguage();
		//$lang->load('plg_jsmpm_common.sys',JPATH_ADMINISTRATOR);
		//$lang->load('plg_jsmpm_'.$this->name.'.sys',JPATH_ADMINISTRATOR);
		$this->config = jsAppConfig::getInstance('jsmoney');
	}
	
	public function isFree($id,$app)
	{
		if($app != $this->name)	{
			return true;
		}
	
		if(empty($this->items[$id]))
		{
			$db = jsDB::instance();
			
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('`#__jsmoney_product`');
			$query->where("`pid` = '{$id}'");
			$query->where("`app` = '{$app}'");
			$query->where("`level` = 1");
			$query->where("`published` = 1");
			$query->order("`id` DESC LIMIT 1");
			$item = jsDB::loadObj($query,'obj');
			
			$this->items[$id] = $item;
		}
		else
		{
			$item = $this->items[$id];
		}
		
		return (empty($item))?true:false;
	}
	
	public function getProduct($id,$app)
	{
		if($app != $this->name)	{
			return true;
		}
	
		return $this->_getProduct($id);
	}
}