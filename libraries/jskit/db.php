<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsDB
{
	public static function instance()
	{
		return JFactory::getDBO();
	}

	public static function query($query)
	{
		$db = self::instance();
		$db->setQuery($query);
		return $db->query();
	}
	
	public static function update($table,$keyId,$updateValues)
	{
		$db = self::instance();
		$query = $db->getQuery(true);
		
		$keyValue = $updateValues[$keyId];
		unset( $updateValues[$keyId] );
		
		$query->update( $db->quoteName($table) );
		foreach($updateValues as $k => $v)
		{
			$query->set($db->quoteName($k)."=".$db->quote($v));
		}
		$query->where( $db->quoteName($keyId)." = ".$db->quote($keyValue) );
		$db->setQuery($query);
		return $db->query();
	}

	public static function insert($table,$vals)
	{
		$db = self::instance();
		$query = $db->getQuery(true);
		
		$nVals = array();
		foreach($vals as $k => $v)
		{
			$nVals[$db->quoteName($k)] = $db->quote($v);
		}
		
		$query->insert( $db->quoteName($table) );
		$query->columns(array_keys($nVals));
		$values = array_values($nVals);
		$query->values(implode(',',$values));
		
		$db->setQuery($query);
		return $db->query();
	}

	public static function delete($table,$uniqueKeys)
	{
		$db = self::instance();
		$query = $db->getQuery(true);
		
		foreach($uniqueKeys as $k => $v)
		{
			$query->where($db->quoteName($k)."=".$db->quote($v));
		}
		
		$query->delete( $db->quoteName($table) );
		
		$db->setQuery($query);
		return $db->query();
	}

	public static function loadObjs($query,$type = 'array',$key = null)
	{
		$db = self::instance();
		$db->setQuery($query);
		
		switch($type)
		{
			case('obj'):
				return $db->loadObjectList($key);
				break;
	
			default:
				return $db->loadAssocList($key);
			break;
		}
	}
	
	public static function loadObj($query,$type = 'array',$key = null)
	{
		$db = self::instance();
		$db->setQuery($query);
	
		switch($type)
		{
			case('obj'):
				return $db->loadObject();
				break;
	
			default:
				return $db->loadAssoc();
			break;
		}
	}
	
	public static function loadResult($query)
	{
		$db = self::instance();
		$db->setQuery($query);
	
		return $db->loadResult();
	}
	
	/*
	public static function loadItem($type = 'array',$key = null)
	{
		$db = oseDB::instance();
	
		switch($type)
		{
			case('obj'):
				return $db->loadObject();
				break;
	
			default:
				return $db->loadAssoc();
			break;
		}
	}
	
	public static function loadList($type = 'array',$key = null)
	{
		$db = self::instance();

		switch($type)
		{
			case('obj'):
				return $db->loadObjectList($key);
			break;

			default:
				return $db->loadAssocList($key);
			break;
		}
	}

	public static function loadItem($type = 'array',$key = null)
	{
		$db = self::instance();

		switch($type)
		{
			case('obj'):
				return $db->loadObject();
			break;

			default:
				return $db->loadAssoc();
			break;
		}
	}

	public function query($unlock = false)
	{
		$testMode = 1;

		$db = oseDB::instance();

		if(!$db->query())
		{
			if($testMode == 1)
			{
				oseExit($db->getErrorMsg());
			}

			if($unlock)
			{
				oseDB::unlock();
			}

			return false;
		}

		return true;
	}

	public static function lock($query)
	{
		$db = oseDB::instance();
		$db->setQuery("LOCK TABLE ". $query);
		return $db->query();
	}

	public static function unlock()
	{
		$db = oseDB::instance();
		$query = "UNLOCK TABLES";
		$db->setQuery($query);
		return $db->query();
	}

	public static function implodeWhere($where = array())
	{
		$where = ( count( $where ) ? ' WHERE (' . implode( ') AND (', $where ) . ')' : '' );
		return $where;
	}*/

}