<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;
jsImportHelperOther('jsmoney','route');
jsImportTable('jsmoney','billinginfo');
class jsMoneyPlugin extends JPlugin
{
	protected $error;
	protected $name = 'common';
	protected $is_cc = false;
	protected $config;
	//protected static $i;
	
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	
		$lang = JFactory::getLanguage();
		$lang->load('plg_jsmpm_common.sys',JPATH_ADMINISTRATOR);
		$lang->load('plg_jsmpm_'.$this->name.'.sys',JPATH_ADMINISTRATOR);
		$this->config = jsAppConfig::getInstance('jsmoney');
	}
	
	// $tp => Parent Node
	public function jsPurchase($pm,$order)
	{ 
		if($pm != $this->name) { 
			return;
		}
		
		$tConfig = jsAppConfig::getInstance('jsmoney');
		
		$pms = jsJSON::decode($tConfig->get('pms','[]'));
		if(in_array($pm, $pms))
		{
			//
			if($this->getPm($tConfig,$order) === false)
			{
				return false;
			}
		}
		elseif($pm == $tConfig->get('primary_cc'))
		{
			//
			if($this->getCCPm($tConfig,$order) === false)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
		// v0.9.4 save billing
		$billing = $this->_getBilling();
		if($order->uid)
		{
			// update billing
			$user = JTable::getInstance('billinginfo','jsmoneyTable');
			$user->load($order->uid);
			$user->bind($billing);
			$user->store();
		}
		$order->params['billing'] = $billing;
		
		$note = JRequest::getString('note');
		if(!empty($note))
		{
			$order->set('note',$note);
		}
		
		return $this->_purchase($order);
	}
	
	protected function _purchase($order)
	{
		
	}
	
	public function getInfo()
	{
		$i = new stdClass();
		$i->text = $i->title = JText::_('JSMPM_'.$this->name);
		$i->value = $this->name;
		$i->is_cc = $this->is_cc;
		return $i;
	}
	
	// $jm => JSMONEY PRODUCT
	public function getPm($config,$order)
	{
		$p = $this->getInfo();
		$pms = jsJSON::decode($config->getNo0('pms','[]'),true);
		$ppm = $config->get('primary_pm','paypal');
		if( !in_array($p->value, $pms) )
		{
			return false;
		}
		
		if(!empty($order))
		{
			if( count($order->get('items')) > 1 && $this->single)
			{
				return false;
			}
		}
		else
		{
			// some pm requirment product info, but it occurs in check payment.
		}
		
		
		if($this->is_cc) {return false;}
		
		$p->primary = false;
		if($ppm == $p->value)
		{
			$p->primary = true;
		}
		
		$this->i = $p;
	
		return $this->_getPm($config,$order);
	}
	
	public function getCCPm($config,$jm)
	{
		$p = $this->getInfo();
		//$pms = jsJSON::decode($config->get('pms','[]'),true);
		$ppm = $config->get('primary_cc','braintree');
		if( $p->value != $ppm )
		{
			return false;
		}
	
		if(!$p->is_cc) {
			return false;
		}
	
		$p->primary = false;
		if($ppm == $p->value)
		{
			$p->primary = true;
		}
	
		$this->i = $p;
	
		return $this->_getPm($config,$jm);
	}
	
	public function execPayment($pm,$order,$p = array())
	{
		if($pm != $this->name)
		{
			return false;
		}
		return $this->_execPayment($order,$p);
	}
	
	protected function _sendEmail($order)
	{
		// notice client
		$product = '';
		foreach($order->get('ps') as $n)
		{
			$node = $n;
			$parent = JTable::getInstance('Product','jsmoneyTable');
			$parent->load($node->parent_id);
			//$product = $n;
			//$title = $product->title;
			//$product = jsmoneyHelperFront::getProduct($parent->app,$parent->pid);
			break;
		}
		
		$uri = JFactory::getURI();
		$prefix = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
		$ilink = $prefix.JRoute::_(jsmoneyHelperRoute::getInvoiceRoute($order->invoice_id));
		//JUri::root().'index.php?option=com_jsmoney&view=invoice&id='.$order->invoice_id;
		$config = jsAppConfig::getInstance('jsmoney');
		$email = jsEmail::instance();
		
		if(!$config->getNo0('email_order_approved',false))
		{
			
			$email->subject = 'Order Approved';
			$email->body = 'You have purchase item(s) from [company].'."<br/>";
			$email->body.= 'Invoice ID:[invoice],'."<br/>"
			.' you can view invoice details <a href="[ilink]">here</a>.';
			
			$email->addVariable('company', $config->get('company_title') );
			$email->addVariable('invoice', $order->invoice_id );
			$email->addVariable('ilink', $ilink );
			//$email->addVariable('item_title', $title );
			
		}
		else
		{
			$tEl = JTable::getInstance('Email','jsmoneyTable');
			$tEl->loadByCode($config->getNo0('email_order_approved'));
			
			
			$email->subject = $tEl->title;
			$email->body = $tEl->html;
			
			$params = array('order'=>$order);
			$params = array('ilink'=>$ilink);
			
			$dispatcher = jsGetDispatcher();
			JPluginHelper::importPlugin('jsmemail',$tEl->type);
			$dispatcher->trigger('parseVariables',array(&$email,$tEl,$params));
		}
		
		$email->sendEmail($order->email);
		if($config->getNo0('cc_order_approved',false))
		{
			$email->sendEmail($config->get('alternative_email',$contact));
		}
		
		$tConfig = jsAppConfig::getInstance('jsmoney');
		$app	= JFactory::getApplication();
		$contact = $app->getCfg('mailfrom');
		$params = $order->params;
		
		$email->subject = "New Order - [sku] - [name]";
		$email->body = "Client: [name]"."<br/>";
		$email->body.= 'Email :[email],'."<br/>";
		$email->body.= 'Price :[price] [currency] ,'."<br/>";
		$email->body.= 'Payment Method :[pm].'."<br/>";
		
		$email->addVariable('sku', $product->sku.'.'.$node->sku );
		$email->addVariable('name', jsGetValue($params,'first_name','').' '.jsGetValue($params,'last_name','') );
		$email->addVariable('email', $order->email );
		$email->addVariable('price', $order->price );
		$email->addVariable('pm', $order->pm );
		$email->addVariable('currency', strtoupper($order->currency) );
		$email->sendEmail($tConfig->get('alternative_email',$contact));
		return true;
	}
	
	public function getTpl($tConfig)
	{
		$pms = jsJSON::decode($tConfig->get('pms','[]'),true);
		if(in_array($this->name,$pms))
		{
			if(method_exists($this, '_getTpl'))
			{
				return $this->_getTpl($tConfig);
			}
			else
			{
				return '';
			}
			
		}
		else
		{
			return false;
		}
	}
	
	protected function _getBilling()
	{
		$firstName = JRequest::getString('first_name');
		$lastName = JRequest::getString('last_name');
		$company = JRequest::getString('company');
		$line1 = JRequest::getString('line1');
		$city = JRequest::getString('city');
		$state = JRequest::getString('state');
		$country = JRequest::getString('country');
		$postal = JRequest::getCmd('postal_code');
		
		$billing = array();
		$billing['firstName'] = $firstName;
		$billing['lastName'] = $lastName;
		$billing['company'] = $company;
		$billing['streetAddress'] = $line1;
		$billing['locality'] = $city;
		$billing['region'] = $state;
		$billing['countryCodeAlpha2'] = $country;
		$billing['postalCode'] = $postal;
		$billing = jsArrayNoEmpty($billing);
		
		$a = array();
		$a['first_name'] = jsGetValueNo0($billing,'firstName');
		$a['last_name'] = jsGetValueNo0($billing,'lastName');
		$a['company'] = jsGetValueNo0($billing,'company');
		$a['line1'] = jsGetValueNo0($billing,'streetAddress');
		$a['city'] = jsGetValueNo0($billing,'locality');
		$a['state'] = jsGetValueNo0($billing,'region');
		$a['country_code'] = jsGetValueNo0($billing,'countryCodeAlpha2');
		$a['postal_code'] = jsGetValueNo0($billing,'postalCode');
		$a = jsArrayNoEmpty($a);
		return $a;
	}
}