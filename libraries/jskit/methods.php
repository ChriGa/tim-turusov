<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
function jsExit($msg = null)
{
	if(!empty($msg))
	{
		$ajax = false;
		if($ajax)
		{
			ob_get_clean();
		}
		print_r( $msg );
	}

	exit;
}

function jsGetValue($item,$key,$default = null)
{
	$isObj = is_object($item);

	if($isObj)
	{
		return !isset($item->{$key})?$default:$item->{$key};
	}
	else
	{
		return !isset($item[$key])?$default:$item[$key];
	}
}

function jsGetValueNo0($item,$key,$default = null)
{
	$isObj = is_object($item);

	if($isObj)
	{
		return empty($item->{$key})?$default:$item->{$key};
	}
	else
	{
		return empty($item[$key])?$default:$item[$key];
	}
}

function jsSetValue($item,$key,$value)
{
	$isObj = is_object($item);

	if($isObj)
	{
		$item->{$key} = $value;
	}
	else
	{
		$item[$key] = $value;
	}

	return $item;
}

class jsJSON
{
	static public function encode($arr)
	{
		$data = json_encode($arr);
		return $data;
	}

	static public function decode($json,$assoc = false)
	{
		$data = json_decode($json,$assoc);
		return $data;
	}
}

function jsGetDateTime($format = "Y-m-d H:i:s")
{
	$date = JFactory::getDate();

	$jversion = new JVersion;
	$version = $jversion->getShortVersion();
	if($version >= 2.5)
	{
		$app = JFactory::getApplication();
		$tz = $app->getCfg('offset');
		
		$date = JFactory::getDate('now',$tz);
		return $date->toSql(true);
	}
	elseif( $version >= '1.6' )
	{
		$format =  "Y-m-d H:i:s";
		$date = JFactory::getDate(JHTML::Date($date, $format, false));
		return $date->toMySQL();
	}
	else
	{
		$date = JFactory::getDate(JHTML::Date($date->_date, "%Y-%m-%d %H:%M:%S"));
		return $date->toMySQL();
	}
}

/**
 * @only work for version 2.5+
 */
function jsGetDate($time = 'now', $tzOffset= null)
{
	$app = JFactory::getApplication();
	$tz = $app->getCfg('offset');
	
	if( empty($tzOffset) )
	{
		$tzOffset = $tz;
	}
	
	$date = JFactory::getDate($time,$tzOffset);
	return $date;
}


// Deprecated
function getDispatcher()
{
	return jsGetDispatcher();
}

function jsGetDispatcher()
{
	$dispatch = (JVERSION >=3)?JEventDispatcher::getInstance():JDispatcher::getInstance();

	return $dispatch;
}

function jsCurrencySymbol($code = 'usd',$size = '')
{
	$c = array();
	$c['usd'] = 'fa-usd';
	$c['eur'] = 'fa-eur';
	$c['gbp'] = 'fa-gbp';
	$c['won'] = 'fa-krw';
	$c['cny'] = 'fa-cny';
	$c['jpy'] = 'fa-jpy';
	
	return '<span class="fa '.$c[strtolower($code)].' '.$size.'"></span>';
}

function jsArrayNoEmpty($array)
{
	foreach($array as $k => $v)
	{
		if(empty($v))
		{
			unset($array[$k]);
		}
	}

	return $array;
}

function jsGetFilterJSCSS($front = true,$res = 'js')
{
	if($front)
	{
		if(JVERSION >= 3)
		{
			$css[] = 'template.css';
			$css[] = 'position.css';
			$css[] = 'layout.css';
			$css[] = 'print.css';
			$css[] = 'general.css';
			$css[] = 'personal.css';
			$css[] = 'system.css';
			$css[] = 'nature.css';
		}
		else
		{
			$css[] = 'template.css';
			$css[] = 'position.css';
			$css[] = 'layout.css';
			$css[] = 'print.css';
			$css[] = 'general.css';
			$css[] = 'personal.css';
			$css[] = 'system.css';
		}
		
		if(JVERSION >= 3)
		{
			$js[] = 'core.js';
			$js[] = 'jui/js/jquery';
			$js[] = 'jui/js/bootstrap';
			$js[] = 'caption.js';
		}
		else
		{
			$js[] = 'core.js';
			$js[] = 'caption.js';
		}
		
		
	}
	else
	{
		$css[] = 'template.css';
		$css[] = 'modal.css';
		$css[] = 'system.css';
		if(JVERSION >= 3)
		{
		
		}
		else
		{
			//$css[] = 'system.css';
		}
		
		if(JVERSION >= 3)
		{
			$js[] = 'multiselect.js';
			$js[] = 'mootools';
			$js[] = 'jui/js/jquery';
			$js[] = 'jui/js/bootstrap';
			$js[] = 'template.js';
			$js[] = 'modal.js';
			$js[] = 'validate.js';
			//$js[] = 'caption.js';
		}
		else
		{
			$js[] = 'multiselect.js';
			$js[] = 'mootools';
			$js[] = 'validate.js';
			$js[] = 'modal.js';
		}
	}
	
	if($res == 'js')
	{
		return $js;
	}
	else
	{
		return $css;
	}
}

function jsWrapLink($ellink)
{
	if(!empty($ellink) && strpos($ellink,'index.php' ) !== false)
	{
		return JRoute::_($ellink);
	}

	if(!empty($ellink) &&  (substr($ellink,0,4) != 'http') )
	{
		if(strpos($ellink, '.') > 0)
		{
			$ellink = 'http://'.$ellink;
		}
		else
		{
			$ellink = JUri::base(). $ellink;
		}
	}

	return $ellink;
}

function jsImportHelper($com,$front = 0)
{
	$com = strtolower($com);
	$path = (!empty($front))?JPATH_SITE:JPATH_ADMINISTRATOR;
	$path.= "/components/com_{$com}/helpers/helper.php";
	$className = ucfirst($com).'Helper';
	$className .= ($front == 1)?'Front':'';
	
	if(!class_exists($className)  && JFile::exists($path))
	{
		require_once $path;
	}
	return null;
}

function jsImportHelperOther($com,$plg = 'route',$front = 1)
{
	$com = strtolower($com);
	$path = ($front)?JPATH_SITE:JPATH_ADMINISTRATOR;
	$path.= "/components/com_{$com}/helpers/{$plg}.php";
	$className = ucfirst($com).'Helper'.ucfirst($plg);
	if(!class_exists($className) && JFile::exists($path))
	{
		require_once $path;
	}
	return null;
}

function jsImportTable($com,$name)
{
	$com = strtolower($com);
	$name = strtolower($name);
	$path = JPATH_ADMINISTRATOR;
	$path.= "/components/com_{$com}/tables/{$name}.php";
	$className = ucfirst($com).'Table'.ucfirst($name);

	if(!class_exists($className)  && JFile::exists($path))
	{
		require_once $path;
	}
	return null;
}