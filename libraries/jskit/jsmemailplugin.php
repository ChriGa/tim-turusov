<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsJsmemailPlugin extends JPlugin
{
	
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	protected function _addStyle($email,$tEl)
	{
		$style = '<style>';
		$style.= JFile::read(JPATH_SITE.'/media/jsbt3/css/bootstrap.min.css');
		$style.= $tEl->css;
		$style.= '</style>';
		$email->body = $style.$email->body;
		return $email;
	}
}