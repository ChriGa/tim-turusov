<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;

class jsOpelPlugin extends JPlugin
{
	protected $error;
	protected $name;
	protected static $i;
	protected $isLevel1 = false;
	protected $isGroup = false;
	protected $isItem = false;
	protected $level = array();
	protected $html;
	protected $appconfig;
	protected $layout;
	protected $isEditable = true;
	
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	
		$lang = JFactory::getLanguage();
		$lang->load('plg_opeltype_common.sys',JPATH_ADMINISTRATOR);
		$lang->load('plg_opeltype_'.$this->name.'.sys',JPATH_ADMINISTRATOR);
		
		$this->appconfig = jsAppConfig::getInstance('onepage');
		
		$this->layout = onepageHelperFront::checkDevice();
	}
	
	public function parseTpl(&$html,$item,$children = array())
	{
		if($item->type != $this->name)
		{
			return ;
		}
		$this->html = $html;
		$useGroupFunc = ( count($children)>0 || $this->isGroup || $item->type == 'row' );
		if( method_exists($this,'_parseGroupTpl') && $useGroupFunc ) {
			$html =  $this->_parseGroupTpl($item,$children);
		}
		elseif( method_exists($this,'_parseTpl') )
		{
			$html =  $this->_parseTpl($item);
			$cHtml = implode("\r\n",$children);
			$html = str_replace('[children]',$cHtml,$html);
		}
		else
		{
			//return $html;
		}
	}
	
	public function addVariable($key,$val)
	{
		$this->html = str_replace("[{$key}]",$val,$this->html);
	}
	
	public function getOPElementType()
	{
		$a = array();
		$a['value'] = $this->name;
		$a['text'] = JText::_('JS_'.$this->name);
		$a['isGroup'] = $this->isGroup;
		$a['isItem'] = $this->isItem;
		$a['isEditable'] = $this->isEditable;
		return $a;
	}
	
	public function getRowType()
	{
		if(!$this->isLevel1)
		{
			return false;
		}
		return $this->getOPElementType();
	}
	
	public function getGroupType()
	{
		if(!$this->isGroup)
		{
			return false;
		}
		return $this->getOPElementType();
	}
	
	public function getItemType()
	{
		if(!$this->isItem)
		{
			return false;
		}
		return $this->getOPElementType();
	}
	
	public function isGroup($type,&$bool)
	{
		if($type != $this->name)
		{
			return ;
		}
		
		$bool = $this->isGroup;
		return;
	}
	
	public function isItem($type,&$bool)
	{
		if($type != $this->name)
		{
			return ;
		}
		
		$bool = $this->isItem;
		return;
	}
	
	protected function _parseCommonParams( $item )
	{
		$params = $item->params;
		
		$wrap_cls = jsGetValueNo0($params,'wrap_cls','');
		$offset = jsGetValueNo0($params, 'offset',0);
		$animation = jsGetValueNo0($params, 'animation',false);
		$wrap_cls.= $offset?" col-md-offset-{$offset}":'';
		$wrap_cls.= $animation?" wow {$animation}":'';
		
		$wrap_id = OnepageHelperEl::id($item);
	 
		$this->addVariable('id', $item->id);
		$this->addVariable('wrap_cls',  $wrap_cls );
		$this->addVariable('title', $item->title);
		$this->addVariable('wrap_id',  $wrap_id );
		
		if( $item->level == 4 )
		{
			$this->addVariable('md', jsGetValueNo0($item, 'md',12));
		}
		elseif($item->level == 5)
		{
			$this->addVariable('md', 12);
		}
		
		// Animation
		$this->addVariable('delay', jsGetValueNo0($params, 'delay',0));
		$this->addVariable('wow_offset', jsGetValueNo0($params, 'wow_offset',150));
		
		return $params;
	}
	
	public function export( &$item,$path )
	{
		if($item->type != $this->name)
		{
			return ;
		}
	
		$data = '';
		if(method_exists($this,'_export')) {
			$data =  $this->_export($item,$path);
		}
		else
		{
			//return $html;
		}
		
		return $data;
	}
	
	public function import( $old,&$new,$path )
	{
		if($new->type != $this->name)
		{
			return ;
		}
	
		$data = '';
		if(method_exists($this,'_import')) {
			$data =  $this->_import($old,$new,$path);
			//if($new->type == 'module') jsExit($new->params);
		}
		else
		{
			//return $html;
		}
	
		return $data;
	}
	
	public function removeEl( $node )
	{
		if( $node->type != $this->name )
		{
			return ;
		}
	
		$data = '';
		if(method_exists($this,'_removeEl')) {
			return $this->_removeEl( $node );
		}
		else
		{
			//return $html;
			return false;
		}
	
	}
	
	public function getCSS( &$code,$node )
	{
		if( $node->type != $this->name )
		{
			return ;
		}
		
		if( $this->appconfig->get('enable_default_style') || (jsGetValue($node, 'tpl') == '') )
		{
			if(JFile::exists(JPATH_SITE."/media/plg_opeltype_{$this->name}/{$this->name}.css"))
			{
				$txt = JFile::read(JPATH_SITE."/media/plg_opeltype_{$this->name}/{$this->name}.css");
				$code["op-{$this->name}-default"] = $txt;
			}
		}
		
		$data = '';
		if(method_exists($this,'_getCSS')) {
			$code = $this->_getCSS($code, $node );
		}
		
		return true;
	
	}
	
	public function getDefaultTypeCSS( &$code,$node )
	{
		if( $node->type != $this->name )
		{
			return ;
		}
	
		if( $this->appconfig->get('enable_default_style') || (jsGetValue($node, 'tpl') == '') )
		{
			if(JFile::exists(JPATH_SITE."/media/plg_opeltype_{$this->name}/{$this->name}.css"))
			{
				$txt = JFile::read(JPATH_SITE."/media/plg_opeltype_{$this->name}/{$this->name}.css");
				$code["op-{$this->name}-default"] = $txt;
			}
		}
	
		return true;
	}
	
	public function getItemCSS( &$code,$node )
	{
		if( $node->type != $this->name )
		{
			return ;
		}
		
		
		if(method_exists($this,'_getItemCSS')) {
			$code = $this->_getItemCSS($code, $node );
		}
	
		return true;
	
	}
	
	public function getJS( &$code,$node )
	{
		if( $node->type != $this->name )
		{
			return ;
		}
	
		$data = '';
		if(method_exists($this,'_getJS')) {
			$code = $this->_getJS( $code, $node );
		}
		
		return true;
	}
	
	public function getDefaultTpl($node)
	{
		$html = '';
		$js = array();
		$css = array();
	
		$path = JPATH_PLUGINS."/opeltype/{$this->name}/";
		$path.= "tpl/default_{$this->name}.php";
		
		$a = array();
		if(JFile::exists($path))
		{
			ob_start();
			include $path;
			$html = ob_get_contents();
			ob_end_clean();
		}
		
		//$node = new stdClass();
		//$node->type = $this->name;
		$this->getCSS($css, $node);
		$css = implode("\r\n",$css);
		
		$this->getJS($js, $node);
		$js = implode("\r\n",$js);
		
		$a['html'] = $html;
		$a['js'] = $js;
		$a['css'] = $css;
		$a['itemcss'] = '';
		if(JFile::exists(JPATH_SITE."/media/plg_opeltype_{$this->name}/style.css"))
		{
			$a['itemcss'] = JFile::read(JPATH_SITE."/media/plg_opeltype_{$this->name}/style.css");
		}
		return $a;
	}
	
	// deprecated need to be updated
	protected function removeShowTag($all = true)
	{
		if($all)
		{
			$this->html = preg_replace("/<!-- show -->(\s|.)*<!-- show -->/i", "", $this->html);
		}
		else
		{
			$this->html = str_replace(array('<!-- show -->','</!-- show -->'), '', $this->html);
		}
		
		return;
	}
	
	public function preloadCssSrc( &$codes,$node )
	{
		static $loaded;
		if( $node->type != $this->name )
		{
			return ;
		}
		
		if(empty($loaded))
		{
			$loaded = false;
		}
		
		$data = '';
		if(method_exists($this,'_preloadCssSrc')) {
			$codes = $this->_preloadCssSrc($codes, $node );
		}
	
		$loaded = true;
		return true;
	}
	
	public function preloadJsSrc( &$codes,$node )
	{
		static $loaded;
		if( $node->type != $this->name )
		{
			return ;
		}
	
		if(empty($loaded))
		{
			$loaded = false;
		}
	
		$data = '';
		if(method_exists($this,'_preloadJsSrc')) {
			$codes = $this->_preloadJsSrc($codes, $node );
		}
	
		$loaded = true;
		return true;
	}
}