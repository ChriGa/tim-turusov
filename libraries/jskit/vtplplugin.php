<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die;
jimport('joomla.filesystem.folder');
class jsVtplPlugin extends JPlugin
{
	protected $name;
	public function tplSetup()
	{ 
		// find media
		$mf = JPATH_PLUGINS."/opvtpl/{$this->name}/frontend/media";
		if(JFolder::exists($mf))
		{
			if(JFolder::copy($mf,JPATH_SITE."/media/opvtpl/{$this->name}",'',true) === true)
			{
				JFolder::delete($mf);
			}
		}
	}
	
	function getTplInfo()
	{
		$folder = new stdClass();
		$folder->folder = $this->name;
		return $folder;
	}
}