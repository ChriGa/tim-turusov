<?php
/**
* @version     1.0 +
* @package     J-SOHO - jsKit
* @author      J-SOHO {@link  http://www.j-soho.com}
* @author      Created on Jan-2014
* @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  @Copyright Copyright (C) 2013-2014 J-SOHO
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
jimport('joomla.application.component.modellist');
class jsModelList extends JModelList
{
	protected $items;
	protected $pagination;
	protected $state;
	
	public function getPagination()
	{
		// Get a storage key.
		$store = $this->getStoreId('getPagination');
	
		// Try to load the data from internal storage.
		if (isset($this->cache[$store]))
		{
			return $this->cache[$store];
		}
	
		// Create the pagination object.
		jimport('joomla.html.pagination');
		$limit = (int) $this->getState('list.limit') - (int) $this->getState('list.links');
		$page = new jsJoomlaPagination($this->getTotal(), $this->getStart(), $limit);
	
		// Add the object to the internal cache.
		$this->cache[$store] = $page;
	
		return $this->cache[$store];
	}

	
}