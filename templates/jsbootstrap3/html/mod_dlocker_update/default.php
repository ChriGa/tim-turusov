<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$cls = $params->get('rightside',1)?'navbar-right':'';

JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_dlocker/tables');
$tn = JTable::getInstance('Product','DLockerTable');
$tp = JTable::getInstance('Product','DLockerTable');
$panelCls = $params->get('panel_style','default');
?>

<div class="panel panel-<?php echo $panelCls; ?>" id="dlocker_update_<?php echo $module->id;?>">
	<div class="panel-heading"><?php echo $module->title?></div>
	
	
	<ul class="list-group ">
	<?php 
		$i=0;
		foreach ($list as $item) : 
		if($i==5) break;
		$tn->load($item->vid);
		$tp->load($item->pid);
		$i++;
	?>
		<a class="list-group-item" href="<?php echo JRoute::_( DlockerHelperRoute::getProductRoute($item->pid) ); ?>">
			<span class="badge noradius badge-pink"><?php echo $item->update_date; ?></span>
			<?php echo $tp->title; ?>-<small><?php echo $tn->title; ?></small>
		</a>
	<?php endforeach; ?>
	</ul>
</div>
