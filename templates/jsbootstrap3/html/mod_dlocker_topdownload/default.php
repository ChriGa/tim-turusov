<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$cls = $params->get('rightside',1)?'navbar-right':'';
$panelCls = $params->get('panel_style','default');
?>

<div class="panel panel-<?php echo $panelCls; ?>" id="dlocker_update_<?php echo $module->id;?>">
	<div class="panel-heading"><?php echo $module->title?></div>
	
	<ul class="list-group ">
	<?php foreach ($list as $i => $item) :  ?>
		<a class="list-group-item" href="<?php echo JRoute::_( DlockerHelperRoute::getProductRoute($item->id) ); ?>">
			<span class="badge badge-info">Details</span>
			<?php echo $item->title; ?>
		</a>
	<?php endforeach; ?>
	</ul>
</div>
