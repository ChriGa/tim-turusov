<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;?>
<?php
// Create a shortcut for params.
$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
$canEdit = $this->item->params->get('access-edit');
JHtml::_('behavior.framework');
JHtml::stylesheet('com_content/com.css',false,true);

$date = new JDate($this->item->checked_out_time);
?>
<?php if ($this->item->state == 0) : ?>
	<span class="label label-warning"><?php echo JText::_('JUNPUBLISHED'); ?></span>
<?php endif; ?>
<?php $useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
	|| $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author') ); ?>
	
<article id="post-<?php echo $this->item->id; ?>" class="jsbt3_blog_post">
	
	<div class="date_of_post">
		<div class="daymonth">
			<span class="day_post"><?php echo $date->__get('day');?></span>
			<span class="month_post"><?php echo $date->format('M', true);?></span>
		</div>
		<div class="year_post"><?php echo $date->__get('year');?></div>
	</div>
	
	
	
	<div class="post-content">
		<div class="post-header">
			<h1 class="post-title">
				<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->id,$this->item->catid));?>" title="Permalink to RoockLab" rel="bookmark"><?php echo $this->item->title;?></a>
			</h1>
		</div><!-- .entry-header -->
	
		<div class="entry-content">
			<?php echo JLayoutHelper::render('joomla.content.intro_image', $this->item); ?>


			<?php if (!$params->get('show_intro')) : ?>
				<?php echo $this->item->event->afterDisplayTitle; ?>
			<?php endif; ?>
			
			
			
			<?php echo $this->item->event->beforeDisplayContent; ?> <?php echo $this->item->introtext; ?>
			
			
			
			
			<?php if ($useDefList) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'below')); ?>
			<?php  endif; ?>
			
			<?php echo $this->item->event->afterDisplayContent; ?>
		</div><!-- .entry-content -->
	
		<footer class="entry-meta">
<?php if ($params->get('show_readmore') && $this->item->readmore) :
	if ($params->get('access-view')) :
		$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
	else :
		$menu = JFactory::getApplication()->getMenu();
		$active = $menu->getActive();
		$itemId = $active->id;
		$link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
		$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
		$link = new JUri($link1);
		$link->setVar('return', base64_encode($returnURL));
	endif; ?>

	<p class="readmore"><a class="btn" href="<?php echo $link; ?>"> <span class="icon-chevron-right"></span>

	<?php if (!$params->get('access-view')) :
		echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
	elseif ($readmore = $this->item->alternative_readmore) :
		echo $readmore;
		if ($params->get('show_readmore_title', 0) != 0) :
		echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
		endif;
	elseif ($params->get('show_readmore_title', 0) == 0) :
		echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
	else :
		echo JText::_('COM_CONTENT_READ_MORE');
		echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
	endif; ?>

	</a></p>

<?php endif; ?>
		</footer><!-- .entry-meta -->
	</div>
</article>