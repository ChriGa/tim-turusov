<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;


list($tItem,$params,$id,$children) = $this->fetchVars();

// Thumbnails Parameters
$columns = jsGetValueNo0($params,'columns',3);
$div_style = jsGetValue($params,'csscls');

$fixed_width = jsGetValue($params,'fixed_width');
$fixed_height = jsGetValueNo0($params,'fixed_height',jsGetValueNo0($tItem,'height',0));
$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);

$i = 0;
?>

<div class="col-md-12 opheight">
<div id="<?php echo $id;?>"  class="opthumbnails opheight <?php echo $div_style;?>">
	
	<?php 
		foreach($children as $k => $child):
			//if(!$item->published) continue;
			$child->height = $fixed_height;
			$this->item = $child;
			//$childParams  = jsJSON::decode($child->params);
			$childParams  = $child->params;
			
			$childUnit = jsGetValueNo0($childParams,'parent_unit','1');
			
	?>
	<?php if($i == 0):?>
		<div class="row" style="padding-bottom:10px;">
			
	<?php endif;?>		
	
			<div class="col-md-<?php echo min( array(12/$columns*$childUnit,12) ) ;?>">
				<div class="thumbnail">
					<div class="row opheight" style="<?php echo $style;?>">
						<?php echo $this->loadTemplate($child->get('tpl'));?>
					</div>
				</div>
			</div>
				
	<?php  $i += $childUnit;?>
	
	<?php 	if(($i >= $columns) || ($k == count($children)-1) ):  $i=0;?>
			
		</div>
	<?php  endif; ?>	
	<?php  endforeach; ?>
</div>
</div>
