<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$tConfig = jsAppConfig::getInstance();

//$params  = jsJSON::decode($this->item->params);
$brandtype = jsGetValue($params,'brandtype',0);

$color = $tConfig->get('topmenu_barcolor');
$fixed = $tConfig->get('topmenu_fixed');
$title = $tConfig->get('topmenu_title');
$showpng = $tConfig->get('topmenu_showpng');

$class = ($color == 'white')?' ':' navbar-inverse';
$class.= ($fixed)?' navbar-fixed-top':' ';

//$uiHtml = $this->getModel()->generateNavbarUi($this->items);
$content = '';
if( JFile::exists( OnepageHelper::getImgPath().'/logo.png') && $showpng )
{
$content = '<img src="'.OnepageHelper::getImgPath(true).'/logo.png"';
$content.= ' width="'.jsGetValue($params,'logowidth').'" ';
$content.= ' height="'.jsGetValue(params,'logoheight').'"> ';
}
$content.= $title;

$pages = $this->getModel()->getPages();

$app		= JFactory::getApplication();
$menus		= $app->getMenu();
$menu = $menus->getActive();

$uri = JUri::getInstance();
$uriPath = $uri->getPath();
$curi = JUri::current();

?>

<div class="navbar navbar-default <?php echo $class;?> onepagenavbar" id="onepage-navbar"  role="navigation">
	<div class="" >
		<div class="container">
			<div style="width:100%;height:70px;">
				<img id="image_17" class="img-responsive " src="/workshop/images/onepage/image_17.png" style="width: 100%; ">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
				<span class="sr-only">Toggle navigation</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span>
			</button>
			<?php if(!empty($content)): ?>
			<a class="navbar-brand" href="#" style="text-decoration:none;">
			<?php echo $content;?>
			</a>
			<?php endif;?>
		</div>
		
		<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
			
			<ul class="nav navbar-nav  navbar-right"  >
				<?php 
				foreach($pages as $k => $item):
				if(!$item->showtitle && $k != 0) continue;
				$link = JRoute::_('index.php?option=com_onepage&view=home&id='.$item->id.'&Itemid='.$menu->id);
				$link_cls = '';
				if($link == $uriPath) $link_cls='active';
				?>
				<li class="<?php echo $link_cls;?>">
				<a  href="<?php echo JRoute::_('index.php?option=com_onepage&view=home&id='.$item->id.'&Itemid='.$menu->id);?>">
				<?php echo $item->title;?>
				</a>
				</li>
				<?php endforeach;?>
			</ul>
		</nav>
	</div>
</div>

<?php if( $fixed ):?>
	<div class="navbarbelow"></div>
<?php endif;?>	