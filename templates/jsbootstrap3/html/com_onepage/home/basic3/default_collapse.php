<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// Accordian Parameters
$columns = jsGetValue($params,'columns','1');
$first_collapsed = jsGetValue($params,'first_collapsed');

$fixed_width = jsGetValue($params,'fixed_width');
$fixed_height = jsGetValueNo0($params,'fixed_height');
if($tItem->level == 4)
{
	$fixed_height = jsGetValueNo0($params,'fixed_height',400);
}

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);

//$i = 0;
$div_cls = jsGetValue($params,'csscls');
?>
<div class="col-md-12 opheight" >
<div class="panel-group opheight opcollapse <?php echo $div_cls;?>" id="<?php echo $id;?>">
	<?php 
		foreach($children as $k => $child):
			$child->height = $fixed_height;
			$this->item = $child;
		    $in = '';
			if($k == 0 && !$first_collapsed )
			{
				$in = 'in';
			}
			
	?>
			<div class="panel panel-default ">
				<div class="panel-heading">
					<a class="accordion-toggle collapsed" data-toggle="collapse"
						data-parent="#<?php echo $id;?>" href="#<?php echo $id.'_'.$k;?>"><?php echo $child->title;?></a>
				</div>
				<div id="<?php echo $id.'_'.$k;?>" class="panel-collapse collapse <?php echo $in;?> " >
					<div class="panel-body opheight"  style="<?php echo $style;?>">
						<div class="row opheight">
							<?php echo $this->loadTemplate($child->tpl);?>
						</div>
					</div>
				</div>
			</div>
	<?php  endforeach; ?>
	</div>		
</div>
