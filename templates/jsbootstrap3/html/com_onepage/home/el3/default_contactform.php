<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();


$fixed_width = jsGetValueNo0($params,'fixed_width');
$fixed_height = jsGetValueNo0($params,'fixed_height',0);

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);

$textarea_style = "height:140px;";
$div_style = jsGetValue($params,'csscls');

if(empty($fixed_height) && $tItem->level >4)
{
	$div_style .= ' opautoheight';
	//$div_style .= ' opautoheight';
	$fixed_height = jsGetValue($tItem,'height');
	$fixed_height = max($fixed_height,328);
	$textarea_height = $fixed_height - 20 - 140 - 28;
	$textarea_style = "height:{$textarea_height}px;";
}
elseif(!empty($fixed_height))
{
	$fixed_height = max($fixed_height,328);
	$textarea_height = $fixed_height - 20 - 140 - 28;
	$textarea_style = "height:{$textarea_height}px;";
}
$uri = JFactory::getURI();
?>
<div class="col-md-12 opheight">
<form class="form  opcontactform <?php echo $div_style;?> " id="<?php echo $id;?>" action="index.php?option=com_onepage" method="POST" style="<?php echo $style;?>" parsley-validate novalidate>
	<div class="row">
		
		<div class="form-group col-md-6">
			<label class="sr-only" for="contactform-<?php echo $tItem->id; ?>-firstname"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_FIRST');?></label>
			<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_FIRST');?>" name="firstname"  class="form-control required" id="contactform-<?php echo $tItem->id; ?>-email" required>
		</div>
		
		<div class="form-group col-md-6">
		    <label class="sr-only" for="contactform-<?php echo $tItem->id; ?>-lastname"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_LAST');?></label>
			<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_LAST');?>" name="lastname"  class="form-control " id="contactform-<?php echo $tItem->id; ?>-name" >
		</div>
	</div>
	
	<div class="row ">	
		<div class="form-group col-md-12">
			<label class="sr-only" for="contactform-<?php echo $tItem->id; ?>-email"><?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?></label>
			<input type="email" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?>" name="email"  class="form-control required" id="contactform-<?php echo $tItem->id; ?>-subject" required parsley-type="email">
		</div>
	</div>
	
	<div class="row ">	
		<div class="form-group col-md-12">
			<label class="sr-only" for="contactform-<?php echo $tItem->id; ?>-content"><?php echo JText::_('COM_ONEPAGE_LABEL_CONTENT');?></label>
			<textarea  style="<?php echo $textarea_style;?>"  placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_CONTENT');?>" name="message" id="contactform-<?php echo $tItem->id; ?>-content"  class="form-control required" ></textarea>
		</div>
	</div>
	
	<div class="row ">
		<div class="form-group col-md-12">
		<button class="btn btn-primary pull-right">Submit</button>
		</div>	
	</div>	
<input type="hidden" name="returnUrl" value="<?php echo base64_encode($uri);?>">
	<input type="hidden" name="task" value="contact.submit"> 
	<?php echo JHtml::_( 'form.token' );?>
	
</form>
</div>	