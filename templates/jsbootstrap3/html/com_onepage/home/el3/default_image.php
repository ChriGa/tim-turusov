<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// Image Parameters
$image_width = jsGetValue($params,'image_width',0);
$image_width_unit = jsGetValue($params,'image_width_unit','percent');
$image_width_unit = ($image_width_unit =='percent')?'%':$image_width_unit;
$image_height = jsGetValue($params,'image_height',0);
$image_height_unit = jsGetValue($params,'image_height_unit','percent');
$image_height_unit = ($image_height_unit =='percent')?'%':$image_height_unit;

$image_width_style = $image_width.$image_width_unit;
$image_height_style = $image_height.$image_height_unit;

$caption = jsGetValue($params,'caption','');
$image_type = jsGetValueNo0($params,'image_type','png');

$filePath = '';
if(JFile::exists(OnepageHelperFront::getImgPath()."/{$tItem->type}_{$tItem->id}.".$image_type))
{
	$filePath = OnepageHelperFront::getImgPath(true)."/{$tItem->type}_{$tItem->id}.{$image_type}";
}

$style = array();
if(!empty($image_width)) $style[] = "width: {$image_width_style}";
if(!empty($image_height)) $style[] = "height: {$image_height_style}";
$style = implode(';',$style);

$img_style = '';
$img_style = jsGetValue($params,'img_style');
$img_style = (empty($img_style))?'':'img-'.$img_style;



$div_style = jsGetValue($params,'csscls');
if($image_height_style == '100%' && $tItem->level >4)
{
	$div_style .= ' opautoheight';
}

$parent_type = jsGetValue($tItem,'parent_type','');
if($parent_type != 'vertical')
{
	//$div_style = ' col-md-12 '.$div_style;
}

$allow_popup = jsGetValue($params,'allow_popup',0);
$div_style .= ($allow_popup)?' venobox':'';

JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
?>

<div class="col-md-12 opheight" >
<?php if($filePath):?>


<div style="<?php echo $style;?>" class="opimg <?php echo $div_style;?> ">
<?php if($allow_popup):?>
<a class="mplightbox" href="<?php echo $filePath;?>" title="<?php echo (!empty($caption))?$caption:'';?>">
	<img id="<?php echo $id;?>" class="img-responsive <?php echo $img_style;?>" src="<?php echo $filePath;?>" >
</a>
<?php else:?>
<img id="<?php echo $id;?>" class="img-responsive <?php echo $img_style;?>" src="<?php echo $filePath;?>" >
<?php if(!empty($caption)):?>
<div class="img-caption"><?php echo $caption;?></div>
<?php endif;?>
<?php endif;?>
</div>
<?php endif;?>
</div>
