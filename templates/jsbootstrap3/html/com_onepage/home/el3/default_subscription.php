<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// we assume this layout contains only 2 elements
$child1 = jsGetValue($children, 0,array());
$child2 = jsGetValue($children, 1,array());

$fixed_width = jsGetValue($params,'fixed_width');
$fixed_height = jsGetValue($params,'fixed_height');

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);

$div_cls = jsGetValue($params,'csscls');
?>
<div class="col-md-12" >
<div class="subscribe-form"  id="<?php echo $id; ?>">
	
	<form class="form " action="<?php echo JRoute::_('index.php?option=com_onepage'); ?>" method="post" role="form">
		<div class="row">
			<div class="form-group col-md-2">
				<span>Subscribe It!</span>
			</div>
			
			<div class="form-group col-md-3">
				<input type="email" class="form-control" id="subscribe-<?php echo $tItem->id; ?>-email" name="data[subscriber][email]" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?>">
			</div>
			
			<div class="form-group col-md-3">
			    <label class="sr-only" for="subscribe-<?php echo $tItem->id; ?>-name"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?></label>
				<input type="email" class="form-control" id="subscribe-<?php echo $tItem->id; ?>-name" name="data[subscriber][name]" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?>">
			</div>
          
			<div class="form-group col-md-2">
			<button type="submit" class="btn btn-default btn-lg btn-block"><?php echo JText::_('COM_ONEPAGE_LABEL_SUBMIT');?></button>
			</div>
		</div>
		
		<input type="hidden" name="task" value="contact.subscribe" />
		<input type="hidden" name="data[subscriber][html]" value="1" />
		<input type="hidden" name="data[listsub][<?php echo jsGetValue($params,'subid','0');?>][status]" value="1" />
		<?php echo JHtml::_( 'form.token' );?>
	</form>
</div>
</div>