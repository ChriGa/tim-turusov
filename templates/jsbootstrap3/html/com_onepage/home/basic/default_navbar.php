<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
$params  = jsJSON::decode($this->item->params);
$color = jsGetValue($params,'barcolor','white');
$fixed = jsGetValue($params,'fixed',0);
$brandtype = jsGetValue($params,'brandtype',0);



$class = ' navbar-inverse';
$class.= ' navbar-fixed-top';

//$uiHtml = $this->getModel()->generateNavbarUi($this->items);
$fixed = 1;
$content = 'Back';

?>
<div class="navbar <?php echo $class;?> onepagenavbar" id="onepage-navbar">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse"> 
				<i class="icon-th-list"></i>
			</a> 
			
			<a class="brand" onclick="window.history.back();" style="text-decoration:none;">
			<?php echo $content;?>
			</a>
		</div>
	</div>
	<!-- /navbar-inner -->
</div>

<?php if( $fixed ):?>
	<div class="navbarbelow"></div>
<?php endif;?>	