<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

// This is Level 4 Row Template
list($row,$params,$id) = $this->fetchVars(4);
$showTitleOnBody = jsGetValue($params,'showtitleonbody',0);
$nocontainer = 0;//jsGetValueNo0($params,'nocontainer',0);

$fixed_height = jsGetValue($params,'fixed_height',0);


$style = array();
//if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) 
{
	if( !in_array($row->type,array('gmap','webvideo')) )
	{
		//$style[] = 'height:'.$fixed_height.'px';
	}
	
	if($row->type == 'article')
	{
		$style[] = 'overflow:auto';
	}
}
$style = implode(';',$style);

?>

<section id="block_<?php echo $id;?>" style="<?php echo $style;?>">
<?php if( !in_array($row->type,array('carousel') )  ):?>
	<div class="container">
<?php endif;?>

<?php if($showTitleOnBody  && !empty($row->title) ):?>
<h1 class="mbl pbl"><?php echo ''. $row->title . '';?></h1>
<?php endif;?>

<div class="row-fluid">
<?php echo $this->loadTemplate($row->tpl); ?>
</div>

<?php if( !in_array($row->type,array('carousel')) ):?>
	</div>
<?php endif;?>
</section>
