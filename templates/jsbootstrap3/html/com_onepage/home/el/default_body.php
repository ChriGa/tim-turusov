<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id) = $this->fetchVars(4);

$fixed_height = jsGetValue($params,'fixed_height');
$fixed_height = (empty($fixed_height))?0:$fixed_height;

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);

$div_style = jsGetValue($params,'csscls');


$parent_type = jsGetValue($tItem,'parent_type','');
if($parent_type != 'vertical')
{
	if(empty($fixed_height) && $tItem->level >4)
	{
		$div_style .= ' opheight';
	}
	$div_style .= ' span12';
}
?>

<?php if( $tItem->level == 4 ):?>
<div class="row-fluid">
<?php endif;?>

<div id="<?php echo $id;?>" class="oparticle <?php echo $div_style;?> " style="<?php echo $style;?>"><?php echo $tItem->content;?></div>

<?php if( $tItem->level == 4 ):?>
</div>
<?php endif;?>