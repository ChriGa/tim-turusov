<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

list($tItem,$params,$id,$children) = $this->fetchVars();

// we assume this layout contains only 2 elements
$child1 = jsGetValue($children, 0,array());
$child2 = jsGetValue($children, 1,array());

$fixed_width = jsGetValue($params,'fixed_width');
$fixed_height = jsGetValue($params,'fixed_height');

$style = array();
if(!empty($fixed_width)) $style[] = 'width:'.$fixed_width.'px';
if(!empty($fixed_height)) $style[] = 'height:'.$fixed_height.'px';
$style = implode(';',$style);

$div_cls = jsGetValue($params,'csscls');
?>

<div class="row-fluid" >
	<div class="span1">
		<img src="components/com_onepage/bootstrap/site_images/email.png" alt="Welcome" width="97" height="100">
	</div>
	<div class="span11">
		<form class="form-inline subscribe-form" action="<?php echo JRoute::_('index.php?option=com_onepage'); ?>" method="post" >
			
          
			<label><strong><?php echo JText::_('COM_ONEPAGE_LABEL_SUBSCRIBEIT');?></strong></label>
			<input type="text" name="data[subscriber][email]" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?>" required>
			<input type="text" name="data[subscriber][name]" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?>">
			<button type="submit" class="btn"><?php echo JText::_('COM_ONEPAGE_LABEL_SUBMIT');?></button>
			
			<input type="hidden" name="task" value="contact.subscribe" />
			<input type="hidden" name="data[subscriber][html]" value="1" />
			<input type="hidden" name="data[listsub][<?php echo jsGetValue($params,'subid','0');?>][status]" value="1" />
			<?php echo JHtml::_( 'form.token' );?>
		</form>
	</div>
</div>
