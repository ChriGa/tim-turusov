$(function () {
	//$('#system-message-container').hide();
	$('.jstips').popover();
	$('.pmimg').on('click',function() {
		var pm = $(this).data('pm');
		$('#pm').val(pm);
		if( $('#dlocker_email').parsley().validate() !== true ) {return;}
		
		$('#buyform').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				$.blockUI({ css: { 
		            border: 'none', 
		            padding: '10px', 
		            backgroundColor: '#transparent', 
		            color: '#fff' 
		        },message:'<span class="fa fa-spinner fa-spin fa-4x"></span>' });
			},
			success: function(res)	{
				if(res.type == '3rd') {
					window.location.href=res.link;
				} else {
					// error
					alert(res.msg);
				}
				$.unblockUI();
			}
		});
	});
	
	//$('a.preview').nivoLightbox();
	$('.pure-menu-horizontal a.preview').magnificPopup({'type':'image'});
	if(ccFirst()) {
		$('#gwparams').hide();
		$('#usecc').val(1);
	}	else {
		$('.ccparams').hide();
	}
	
	$('#usecc').easyDropDown({
		cutOff: 10,
		onChange: function(selected){
			// do something
			console.log(selected);
			if(selected.value == 1) {
				$('.ccparams').show();
				$('#gwparams').hide();
			}	else {
				$('.ccparams').hide();
				$('#gwparams').show();
			}
		}
	});
	
	$('#ccSubmit').on('click',function() {
		// Ajax
		if( !$('#buyform').parsley().validate() ) {return;}
		$('#buyform').ajaxSubmit({
			dataType:  'json',
			type: 'POST',
			beforeSubmit: function()	{
				$.blockUI({ css: { 
		            border: 'none', 
		            padding: '10px', 
		            backgroundColor: '#transparent', 
		            color: '#fff' 
		        },message:'<span class="fa fa-spinner fa-spin fa-4x"></span>' });
			},
			success: function(res)	{
				if(res.success && res.link) {
					// redirect payment success page
					window.location.href = res.link;
				}
				$.unblockUI();
			}
		});
	});
});
