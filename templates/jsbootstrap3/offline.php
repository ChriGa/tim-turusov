<?php 
    /*------------------------------------------------------------------------
# author    Gonzalo Suez
# copyright Copyright � 2012 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/
    defined( '_JEXEC' ) or die;
    // variables
    $app = JFactory::getApplication();
    $doc = JFactory::getDocument();
    $tpath = $this->baseurl.'/templates/'.$this->template;
    $this->setGenerator(null);
    // load sheets and scripts
    $doc->addStyleSheet($tpath.'/css/basic.css?v=1.0.0');
    $doc->addStyleSheet($tpath.'/css/offline.css?v=1.0.0');
    $doc->addScript($tpath.'/js/modernizr-2.6.2.js');
    ?><!doctype html>
<!--[if IEMobile]><html class="iemobile" lang="<?php  echo $this->language; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="<?php  echo $this->language; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="<?php  echo $this->language; ?>"> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js" lang="<?php  echo $this->language; ?>"> <!--<![endif]-->
<head>
  <jdoc:include type="head" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" /> <!-- mobile viewport optimized -->
  <link rel="apple-touch-icon-precomposed" href="<?php  echo $tpath; ?>/apple-touch-icon-57x57.png"> <!-- iphone, ipod, android -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php  echo $tpath; ?>/apple-touch-icon-72x72.png"> <!-- ipad -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php  echo $tpath; ?>/apple-touch-icon-114x114.png"> <!-- iphone retina -->
  <link href="<?php  echo $tpath; ?>/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" /> <!-- favicon -->
</head>
<body>
  <jdoc:include type="message" />
  
  <div id="frame" class="preloader">
  	  
	  <div class="preloader-table">
		  <div class="preloader-cell">
			  <div class="container">
				  <div class="row offline-text">
					  <div class="col-sm-6 col-sm-offset-3">
					  	<?php  include_once dirname(__FILE__)."/offline_panel.php"; ?>
					  </div>
				  </div>
			  </div>
		  </div>
		  <div class="powerby text-center">
			<a class="http://j-soho.com/onepage-v1-4" target="_blank">OnePage Design By J-SOHO</a>
		  </div>
	  </div>
  </div>
</body>
</html>