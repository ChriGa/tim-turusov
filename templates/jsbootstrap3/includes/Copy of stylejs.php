<?php
defined('_JEXEC') or die;
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();

// First, we delete the jui js
$filterJss = array('core.js','jui/js/jquery','jui/js/bootstrap');
foreach($doc->_scripts as $k => $v)
{
	foreach($filterJss as $jk => $jv)
	{
		if(strpos($k, $jv) !== false)
		{
			unset($doc->_scripts[$k]);
			break1;
		}
	}
}

// add javascript files
// JavaScript plugins (requires jQuery) 
$js = array('basic.js');
$addJS = array();$addCSS = array();
if(JFolder::exists('media/jsbt3'))
{
	$addJS["media/jsbt3/js/basic.js"]['mime'] = "text/javascript";
	$addJS["media/jsbt3/js/basic.js"]['defer'] = false;
	$addJS["media/jsbt3/js/basic.js"]['async'] = false;
	
	$addCSS["media/jsbt3/css/basic.css"]['mime'] = "text/css";
	$addCSS["media/jsbt3/css/basic.css"]['media'] = 'screen,print';
	$addCSS["media/jsbt3/css/basic.css"]['attribs'] = array();
}
else
{
	$addJS["templates/{$this->template}/js/basic.js"]['mime'] = "text/javascript";
	$addJS["templates/{$this->template}/js/basic.js"]['defer'] = false;
	$addJS["templates/{$this->template}/js/basic.js"]['async'] = false;
	
	$addCSS["templates/{$this->template}/css/basic.css"]['mime'] = "text/css";
	$addCSS["templates/{$this->template}/css/basic.css"]['media'] = null;
	$addCSS["templates/{$this->template}/css/basic.css"]['attribs'] = array();
}
$doc->_scripts = $addJS + $doc->_scripts;
$doc->_styleSheets = $addCSS + $doc->_styleSheets;

/*foreach($js as $k => $v) 
{
	$add["templates/{$this->template}/js/".$v]['mime'] = "text/javascript";
	$add["templates/{$this->template}/js/".$v]['defer'] = false;
	$add["templates/{$this->template}/js/".$v]['async'] = false;
}*/


JHtml::stylesheet('templates/'.$this->template.'/css/theme.css');


/*
JHtml::script('templates/'.$this->template . '/js/jquery-1.10.2.min.js');
JHtml::script('templates/'.$this->template . '/js/jquery-noconflict.js');
// Include all compiled plugins (below), or include individual files as needed 
JHtml::script('templates/'.$this->template . '/js/bootstrap.min.js');
// Add Stylesheets
JHtml::stylesheet('templates/'.$this->template.'/css/bootstrap.min.css');
JHtml::stylesheet('templates/'.$this->template.'/css/font-awesome.min.css');
JHtml::stylesheet('templates/'.$this->template.'/css/navbar.css');
JHtml::stylesheet('templates/'.$this->template.'/css/template.css');*/
?>