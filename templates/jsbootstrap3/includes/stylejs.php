<?php
defined('_JEXEC') or die;
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
jimport('joomla.filesystem.folder');
// First, we delete the jui js
$filterJss = array('core.js','jui/js/jquery','jui/js/bootstrap');
foreach($doc->_scripts as $k => $v)
{
	foreach($filterJss as $jk => $jv)
	{
		if(strpos($k, $jv) !== false)
		{
			unset($doc->_scripts[$k]);
			break;
		}
	}
}

// add javascript files
// JavaScript plugins (requires jQuery) 
$js = array('basic.js');
$addJS = array();$addCSS = array();
if(JFolder::exists('media/jsbt3'))
{
	$addJS["media/jsbt3/js/basic.js"]['mime'] = "text/javascript";

	$addJS["templates/{$this->template}/js/skrollr.min.js"]['mime'] = "text/javascript";

	$addJS["media/jsbt3/js/basic.js"]['defer'] = false;
	$addJS["media/jsbt3/js/basic.js"]['async'] = false;

	//$addJS["media/jsbt3/js/classie.js"]['mime'] = "text/javascript";
	
	$addCSS["media/jsbt3/css/basic.css"]['mime'] = "text/css";
	$addCSS["media/jsbt3/css/basic.css"]['media'] = 'screen,print';
	$addCSS["media/jsbt3/css/basic.css"]['attribs'] = array();
	$addCSS["templates/{$this->template}/css/overrides.css"]['override'] = array();


	//$addCSS["templates/{$this->template}/css/component.css"]['component'] = array();
	//$addCSS["templates/{$this->template}/css/demo.css"]['demo'] = array();

	// $addCSS["templates/{$this->template}/css/media.css"]['media'] = array();
	
	
}
else
{
	$addJS["templates/{$this->template}/js/basic.js"]['mime'] = "text/javascript";
	$addJS["templates/{$this->template}/js/basic.js"]['defer'] = false;
	$addJS["templates/{$this->template}/js/basic.js"]['async'] = false;
	
	$addCSS["templates/{$this->template}/css/basic.css"]['mime'] = "text/css";
	$addCSS["templates/{$this->template}/css/basic.css"]['media'] = null;
	$addCSS["templates/{$this->template}/css/basic.css"]['attribs'] = array();

}
$doc->_scripts = $addJS + $doc->_scripts;
$doc->_styleSheets = $addCSS + $doc->_styleSheets;

JHtml::stylesheet('templates/'.$this->template.'/css/theme.css');



?>