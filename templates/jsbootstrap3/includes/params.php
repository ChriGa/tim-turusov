<?php
	/*------------------------------------------------------------------------
# author    Gonzalo Suez
# copyright Copyright � 2013 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/
defined('_JEXEC') or die;
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();

// add javascript files
// JavaScript plugins (requires jQuery) 
/*
JHtml::script('templates/'.$this->template . '/js/jquery-1.10.2.min.js');
JHtml::script('templates/'.$this->template . '/js/jquery-noconflict.js');
// Include all compiled plugins (below), or include individual files as needed 
JHtml::script('templates/'.$this->template . '/js/bootstrap.min.js');
// Add Stylesheets
JHtml::stylesheet('templates/'.$this->template.'/css/bootstrap.min.css');
JHtml::stylesheet('templates/'.$this->template.'/css/font-awesome.min.css');
JHtml::stylesheet('templates/'.$this->template.'/css/navbar.css');
JHtml::stylesheet('templates/'.$this->template.'/css/template.css');*/
//JHtml::stylesheet();
// Variables
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$params = $app->getParams();
$headdata = $doc->getHeadData();
$menu = $app->getMenu();
$active = $app->getMenu()->getActive();
$pageclass = $params->get('pageclass_sfx');
$tpath = $this->baseurl.'/templates/'.$this->template;
// Parameter
$modernizr = $this->params->get('modernizr');
$fontawesome = $this->params->get('fontawesome');
$pie = $this->params->get('pie');
// Generator tag
$this->setGenerator(null);
// force latest IE & chrome frame
$doc->setMetadata('x-ua-compatible', 'IE=edge,chrome=1');

// Column widths
$leftcolgrid = $this->params->get('leftColumnWidth', 3);
$rightcolgrid= $this->params->get('rightColumnWidth', 4);
if ($this->countModules('left') == 0)
{
	$leftcolgrid  = "0";
}

if ($this->countModules('right') == 0)
{
	$rightcolgrid  = "0";
}

// Extension Setting
$blankPage = false;
$option = JRequest::getCmd('option');
if(in_array($option,array('com_onepage','com_onepagev')))
{
	// if page set onepage only, make it a blank template
	$blankPage = true;
}
?>