<?php
	/*------------------------------------------------------------------------
# author    Gonzalo Suez
# copyright Copyright � 2013 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die;

// path for appicon
$doc = JFactory::getDocument();
$doc->setMetaData('apple-mobile-web-app-capable','yes' );
$doc->setMetaData('viewport','width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' );
$doc->setMetaData('apple-mobile-web-app-status-bar-style','black' );
if( JFile::exists($tpath."/appicon144.png") )
{
	$doc->addHeadLink('templates/jsbt3/appicon144.png','apple-touch-icon-precomposed','rel',array('sizes'=>'144x144') );
	$doc->addHeadLink('templates/jsbt3/appicon114.png','apple-touch-icon-precomposed','rel',array('sizes'=>'114x114') );
	$doc->addHeadLink('templates/jsbt3/appicon72.png','apple-touch-icon-precomposed','rel',array('sizes'=>'72x72') );
	$doc->addHeadLink('templates/jsbt3/appicon57.png','apple-touch-icon-precomposed','rel',array('sizes'=>'57x57') );
}


?>

<head>
	<jdoc:include type="head" />
  <!--[if lte IE 8]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <style> 
        {behavior:url(<?php  echo $tpath; ?>/js/PIE.htc);}
      </style>
  <![endif]-->


</head>