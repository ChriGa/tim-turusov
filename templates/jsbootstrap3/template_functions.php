<?php

/**
 * Hier werden Hilfsfunktionen und Variablen definiert die Systemweit durch include
 * in der index.php verwendbar und aufrufbar sind
 * Author: CG
 **/

/**
 * Debuggingausgabe von Objekten und Arrays
 **/	
	function preprint($s, $where="") {
		print "<pre>";
			if ($where) print $where."<br/>";
			print_r($s);
		print "</pre>";
	}

/**
 * Meta Generator löschen und 089-webdesign eintragen
 **/
	unset($headMetaData["metaTags"]["standard"]["title"]); 
	$this->setGenerator('Chris Gabler-WEBDESIGN'); 


/*erweiterte Mobile Detection siehe: http://mobiledetect.net/*/

		require_once 'Mobile_Detect.php';
		$detect = new Mobile_Detect;

