<?php  
	/*------------------------------------------------------------------------
# author    Gonzalo Suez
# copyright Copyright © 2013 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die;

include 'includes/params.php';
include 'includes/stylejs.php';

?>
<!doctype html>
<html lang="en" >

<?php
include 'includes/head.php';
?>

<body>
<!--top-->
<?php  if($this->countModules('top')) : ?>
<div id="top">
<div class="container">
<div class="row">
<jdoc:include type="modules" name="top" style="none" />	
</div>
</div>
</div>
<!--top-->
<?php  endif; ?>

<div id="wrap">

<?php  if ($this->countModules('navigation')) : ?>
<!--Navigation-->
<div id="jsbt3-navigation">
    <div class="navbar navbar-default navbar-theme" role="navigation">
    	<div class="container">
    	<div class="navbar-header">
        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	        </button>
	        <?php if($this->params->get('logo_file')):  ?>
			<div id="brand">
				<a href="<?php  echo $this->params->get('logo_link')  ?>">
					 <img style="width:<?php  echo $this->params->get('logo_width')  ?>px; height:<?php  echo $this->params->get('logo_height')  ?>px; " src="<?php  echo $this->params->get('logo_file')  ?>" alt="Logo" />
				</a>
			</div>
			<?php endif;  ?>
        </div>
        <div class="navbar-collapse collapse">
			<jdoc:include type="modules" name="navigation" style="none" />
    	</div>
    	</div>
	</div>
</div>
<!--Navigation-->
<?php  endif; ?>

<!--fullwidth-->
<?php  if($this->countModules('fullwidth')) : ?>
<div id="fullwidth">
<div class="row">
<jdoc:include type="modules" name="fullwidth" style="block"/>
</div>
</div>
<?php  endif; ?>
<!--fullwidth-->
<!--Showcase-->
<?php  if($this->countModules('showcase')) : ?>
<div id="showcase">
<div class="container">
<div class="row">
<jdoc:include type="modules" name="showcase" style="block"/>
</div>
</div>
</div>
<?php  endif; ?>
<!--Showcase-->
<!--Feature-->
<?php  if($this->countModules('feature')) : ?>
<div id="feature">
<div class="container">
<div class="row">
<jdoc:include type="modules" name="feature" style="block" />	
</div>
</div>
</div>
<?php  endif; ?>
<!--Feature-->

<?php  if(!$blankPage): ?>
<!-- Content -->
<div class="container">
	<?php  if($this->countModules('breadcrumbs')) : ?>
	<div id="breadcrumbs">	
		<div class="row">
		<jdoc:include type="modules" name="breadcrumbs" style="block" />
		</div>
	</div>
	<?php endif; ?>
	
<div id="main" class="row show-grid">

	<!-- Left -->
	<?php if($this->countModules('left')) : ?>
	<div id="sidebar" class="col-sm-<?php echo $leftcolgrid; ?>">
		<jdoc:include type="modules" name="left" style="xhtml" />
	</div>
	<?php endif; ?>

	<!-- Component -->
<div id="container" class="col-sm-<?php echo (12-$leftcolgrid-$rightcolgrid); ?>">
	<!-- Content-top Module Position -->	
		<?php if($this->countModules('content-top')) : ?>
		<div id="content-top">
			<div class="row">
				<jdoc:include type="modules" name="content-top" style="block" />	
			</div>
		</div>
		<?php endif; ?>
	
		<div class="main-box">
			<jdoc:include type="component" />
			<jdoc:include type="message" />	
		</div>
	
		<!-- Below Content Module Position -->	
		<?php if($this->countModules('content-bottom')) : ?>
		<div id="content-bottom">
			<div class="row">
				<jdoc:include type="modules" name="content-bottom" style="block" />	
			</div>
		</div>
		<?php endif; ?>
	</div>

	<!-- Right -->
	<?php if($this->countModules('right')) : ?>
	<div id="sidebar-2" class="col-sm-<?php echo $rightcolgrid; ?>">
		<jdoc:include type="modules" name="right" style="xhtml" />
	</div>
	<?php endif; ?>
</div>
</div>
<!-- Content -->
<?php else: ?>
<!-- Content -->
	<jdoc:include type="component" />
	<jdoc:include type="message" />	
<!-- Content -->
<?php endif; ?>

<!-- bottom -->
<?php  if($this->countModules('bottom')) : ?>
<div id="bottom">
<div class="container">
<div class="row">
<jdoc:include type="modules" name="bottom" style="block" />
</div>
</div>
</div>
<?php  endif; ?>
<div id="push"></div>
<!-- bottom -->
</div>
<!-- footer -->
<?php  if($this->countModules('footer')) : ?>
<footer class="footer" id="footer">
<div class="container">
<div class="row">
<jdoc:include type="modules" name="footer" style="block" />
</div>
</div>
</footer>
<?php  endif; ?>
<!-- footer -->

<script type="text/javascript">
jQuery(function () {
	var bodyHeight = parseInt(jQuery("body").css("height").replace('px',''));
	if( bodyHeight < jQuery(window).height() )	{
		var dif = jQuery(document).height() - bodyHeight;
		var wrapHeight = parseInt(jQuery("#wrap").css("height").replace('px',''));
		jQuery('#wrap').css('min-height',dif+wrapHeight);
	}

	$(window).resize( function(){
		var bodyHeight = parseInt(jQuery("body").css("height").replace('px',''));
		if( bodyHeight < jQuery(window).height() )	{
			var dif = jQuery(document).height() - bodyHeight;
			var wrapHeight = parseInt(jQuery("#wrap").css("height").replace('px',''));
			jQuery('#wrap').css('min-height',dif+wrapHeight);
		}
	});
});


</script>
<script> <?php //google Analytics?>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-73442296-1', 'auto');
	ga('send', 'pageview');

</script>

<jdoc:include type="modules" name="debug" />	
<!-- page -->	


</body>
</html>
