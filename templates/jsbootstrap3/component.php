<?php 
	/*------------------------------------------------------------------------
# author    Gonzalo Suez
# copyright Copyright � 2013 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/
defined('_JEXEC') or die;

$app   = JFactory::getApplication();
$doc   = JFactory::getDocument();
// Add Stylesheets
include 'includes/params.php';
include 'includes/stylejs.php';


/*CG inculde template functions-mobile detect*/
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');

$doc = JFactory::getDocument();
$doc->setMetaData('apple-mobile-web-app-capable','yes' );
$doc->setMetaData('viewport','width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' );
$doc->setMetaData('apple-mobile-web-app-status-bar-style','black' );
if( JFile::exists($tpath."/appicon144.png") )
{
	$doc->addHeadLink('templates/jsbt3/appicon144.png','apple-touch-icon-precomposed','rel',array('sizes'=>'144x144') );
	$doc->addHeadLink('templates/jsbt3/appicon114.png','apple-touch-icon-precomposed','rel',array('sizes'=>'114x114') );
	$doc->addHeadLink('templates/jsbt3/appicon72.png','apple-touch-icon-precomposed','rel',array('sizes'=>'72x72') );
	$doc->addHeadLink('templates/jsbt3/appicon57.png','apple-touch-icon-precomposed','rel',array('sizes'=>'57x57') );
}
?>
<!DOCTYPE html>
<html xml:lang="<?php echo $this->language; ?> " lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<jdoc:include type="head" />

<!--[if lt IE 9]>
	<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
<![endif]-->

</head>
<body >
  <div id="overall">
    <jdoc:include type="message" />
    <jdoc:include type="component" />
  </div>

<?php /*skrollr nur einfuegen wenn UA nicht Mobile*/

	if(!$detect->isMobile())

		print	'<script type="text/javascript">
				    var s = skrollr.init();
				</script>';
 ?>
</body>
</html>

