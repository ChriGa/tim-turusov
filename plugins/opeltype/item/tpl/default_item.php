<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
/*$params = $this->item->params;
$allow_popup = jsGetValueNo0($params,'allow_popup',0);

if( !empty($allow_popup) )
{
	JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
	JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
}*/
?>
<div id="[wrap_id]" class="op-item col-md-[md] [wrap_cls]" data-wow-delay="[delay]s" data-wow-offset="[wow_offset]">
	<div class="op-item-inner">
		<!-- showimg -->
		<div class="opimage" id="op-image-[id]">
			<a class="[img_cls] " href="[popup_img_link]" title="[caption]" >
				<img class="[cls] "  src="[img_link]"  alt="[title]"/>
			</a>
		</div>
		<!-- showimg -->
		
		<!-- showtxt -->
		<div class="ophtml" id="op-html-[id]">[content]</div>
		<!-- showtxt -->
	</div>
</div>