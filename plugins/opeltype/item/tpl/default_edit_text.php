<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

?>

<div class="panel panel-primary">
	<div class="panel-heading haslink noselect" style="position:relative">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion"
				href="#html"> Html </a>
				<span style="position:absolute;right:0;top:8px;" class="hidden">
					<input type="checkbox" class="js-switch " name="enable_html1" value="1" ui-switch ng-model="item.params.enable_html1"/>
				</span>
		</h4>
	</div>
	<div id="html" class="panel-collapse collapse in">
		<div class="panel-body">
			<div class="form-group hidden">
				<label class="col-md-3 control-label">Position</label>
				<div class="col-sm-9">
					<select name="position" ng-model="item.params.position" class="form-control ">
						<option value="">Under The Image</option>
						<option value="up">Above The Image</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-12">
				<div class="summernote" name="content" ><?php echo $this->item->content;?></div>
			</div>
			</div>
		</div>
	</div>
</div>
	