<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeItem extends jsOpelPlugin
{
	protected $name = 'item';
	protected $isGroup = false;
	protected $isItem = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$node->set('params',$params);
		
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');		
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			$params['image'] = $imageFile;
		}
		
		$mImage = JRequest::getVar('mobile_image', null, 'files');
		$imgExt = JFile::getExt($mImage['name']);
		$imageFile = "mobile_image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		if(!empty($mImage))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			if ( !JFile::upload( $mImage['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			$params['mobile_image'] = $imageFile;
		}
		
		$filter = JFilterInput::getInstance();
		//$params['height'] = $filter->clean($post['height'],'int');
		//$params['width'] = $filter->clean($post['width'],'int');
		//$params['cls'] = $filter->clean($post['cls'],'string');
		$params['responsive'] = $filter->clean(jsGetValueNo0($post,'responsive','img-responsive'),'string');
		$params['caption'] = JRequest::getString('caption');
		$params['allow_popup'] = JRequest::getCmd('allow_popup','');//$filter->clean($post['allow_popup'],'cmd');
	
		if(!isset($post['wrap_id']))
		{
			$params['enable_image'] = 1;//jsGetValueNo0($post,'enable_image',0);
			$params['enable_html'] = 1;
		}
		else
		{
			$params['enable_image'] = JRequest::getInt('enable_image',0);//jsGetValueNo0($post,'enable_image',0);
			$params['enable_html'] = JRequest::getInt('enable_html',0);
		}
		
		$params['position'] = JRequest::getCmd('position');
		
		$params['ialign'] = JRequest::getString('ialign');
		
		//$params['enable_image'] = $filter->clean($params['enable_image']);
		//$params['enable_html'] = jsGetValue($post,'enable_html',1);
		//$params['enable_html'] = $filter->clean($params['enable_html'],'int');
		
		
		$node->set('params',$params);
		
		$content = JRequest::getString('content','','post',JREQUEST_ALLOWRAW);
		$node->set('content',$content);
		
		return true;
	}
	
	protected function _parseTpl($item)
	{
		//list($tItem,$params,$id,$children) = onepageHelper::getItemTpl($item);
		$params = $this->_parseCommonParams($item);
		// Html
		if(jsGetValueNo0($params, 'enable_html',false))
		{
			$this->addVariable('content', $item->content);
		}
		else
		{
			$this->addVariable('content', '');
		}
		
		// Image
		$image = jsGetValueNo0($params, 'image');
		$hasImage = JFile::exists(OnepageHelperFront::getImgPath().$image);
		
		$width = jsGetValueNo0($params, 'width',0);
		$height = jsGetValueNo0($params, 'height',0);
		$cls = jsGetValueNo0($params, 'cls','');
		$allow_popup = jsGetValueNo0($params,'allow_popup',0);
		
		if(!$width && !$height)
		{
			$cls.= ' '.jsGetValueNo0($params, 'responsive','');
		}
		else
		{
			$attr = (empty($width))?'':"width=\"{$width}\"";
			$attr.= (empty($height))?'':"height=\"{$height}\"";
		}
		
		$cls.= ' '.jsGetValueNo0($params, 'ialign','');
		
		$filePath = ($hasImage)?OnepageHelperFront::getImgPath(true).$image:'#';
		$pfilePath = ($hasImage && $allow_popup)?OnepageHelperFront::getImgPath(true).$image:'javascript:void(0)';
		$img_cls = ($hasImage && $allow_popup)?'mplightbox':'';
		
		$enable_image = jsGetValueNo0($params, 'enable_image',false);
		$enable_image = ($filePath=='#')?false:$enable_image;
		if($enable_image)
		{	
			$this->addVariable('popup_img_link', $pfilePath);
			$this->addVariable('caption', jsGetValueNo0($params, 'caption',''));
			$this->addVariable('cls', $cls);
			$this->addVariable('img_cls', $img_cls);
			//$this->addVariable('attr', $attr);
		}
		else
		{
			$this->html = preg_replace("/<!-- showimg -->(\s|.)*?<!-- showimg -->/i",'', $this->html);
		}
		
		if(!jsGetValueNo0($params, 'enable_html',false))
		{
			$this->html = preg_replace("/<!-- showtxt -->(\s|.)*?<!-- showtxt -->/i",'', $this->html);
		}
		
		$this->addVariable('img_link', $filePath);
		
		
		
		return $this->html;
	}
	
	protected function _getJS($code,$item)
	{
		$params = $item->params;
		$allow_popup = jsGetValueNo0($params,'allow_popup',0);
		
		
		$mixpanel_api = $this->appconfig->getNo0('mixpanel_apikey',false);
		
		$id = OnepageHelperEl::id($item);
		
		if(jsGetValueNo0($params, 'enable_image',false))
		{
			if($allow_popup)
			{
				$code['mplightbox'] = '$(".mplightbox").magnificPopup({type:"image" ';
				$code['mplightbox'] .= '});';
				
				if(!empty($mixpanel_api))
				{
					$code['mp-item-'.$item->id] = "$('.mplightbox').on('click',function(){
						mixpanel.track( \"Popup.Img.{$id}\");
					});";
				}
				
			}
		}
		
		return $code;
	}
	
	protected function _export($item,$path)
	{
		$params = $item->params;
		// section background
		$imgPath = OnepageHelper::getImgPath().jsGetValueNo0($params,'image','no.png');
		
		$dst = $path.'images/';
		$dst .= jsGetValueNo0($params,'image','no.png');
	
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}
	}
	
	protected function _import($old,$new,$path)
	{
		$params = jsGetValueNo0($old,'params',array());
	
		$imgPath = $path;//."/images/";
		$dst = OnepageHelper::getImgPath();
		$iImage = jsGetValueNo0($params,'image','none.png');
	
		$newFile = "image_{$new->id}.".JFile::getExt($iImage);
		if( JFile::exists($imgPath.$iImage) )
		{
			JFile::copy( $imgPath.$iImage, $dst.$newFile );
	
			$params = jsSetValue($params, 'image', $newFile);
			$new->set('params',$params);
			$new->store();
		}
	
	}
	
	protected function _removeEl( $node )
	{
		$params = $node->params;
		// section background
		$bgImage = jsGetValueNo0($params,'image','no.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
	
	
		if( JFile::exists($imgPath) )
		{
			JFile::delete( $imgPath );
		}
	}
	
	public function preloadJs(&$codes)
	{
		JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
	}
	
	public function preload(&$codes)
	{
		JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
	}
	
	protected function _preloadJsSrc($codes,$item = null)
	{
		$codes['magnific'] = JFile::read(JPATH_SITE."/media/jsbt3/js/jquery.magnific-popup.min.js");
		//JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
		return $codes;
	}
	
	protected function _preloadCssSrc($codes,$item = null)
	{
		$codes['magnific'] = JFile::read(JPATH_SITE."/media/jsbt3/css/magnific-popup.css");
		//JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
		return $codes;
	}
}
