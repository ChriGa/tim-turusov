<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
?>

<div class="panel-group" id="accordion">

	<div class="panel panel-primary">
		<div class="panel-heading haslink">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#html"> Parameters </a>
			</h4>
		</div>
		
			<div class="panel-body">
				<div class="form-group">
				  	<label class="col-sm-4">Style</label>
			  		<div class="col-sm-8">
			  			<select class="form-control" name="nav_style" ng-model="item.params.nav_style">
					  		<option value="">Tab</option>
				  			<option value="pill">Pill</option>
					  	</select>
			  		</div>
				</div>
			
			</div>
	</div>
	
	
</div>
<input type="hidden" name="option" value="com_onepage">
<input type="hidden" name="task" value="prototype.saveItem">
<input type="hidden" name="id" value="<?php echo $this->item->id;?>">

<script>
function elitem($scope) {
	$scope.previewimglink = 'index.php?option=com_onepage&task=prototype.getImg&id='+getId();
	$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
	
	$scope.submit = function(v) {
		var sHTML = $('.summernote').code();
		$('#elform').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'v':v,'content':sHTML},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
						$scope.$parent.list = res.list;
					});
				}
				$('#elform .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
}
</script>