<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$params = $this->item->params;
$bgImage = jsGetValueNo0($params,'bg_image',false);
$hideImage = jsGetValueNo0($params,'hide_bg_image',false)?true:false;
$addImageCls = (!$hideImage && JFile::exists(OnepageHelperFront::getImgPath().$bgImage ) );

$cls = jsGetValueNo0($params, 'cls','');
$allow_popup = jsGetValueNo0($params,'allow_popup',0);
$indicator = jsGetValueNo0($params,'indicators',0);
$navcontrol = jsGetValueNo0($params,'navcontrol',0);


$filePath = OnepageHelperFront::getImgPath(true).$image;

$tItem = $this->item;
$children = $tItem->get('children');
?>


<div class="col-md-[md] [wrap_cls]" >
	<div class="panel-group op-tab" id="[wrap_id]">
		[children]
	</div>
</div>


