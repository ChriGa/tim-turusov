<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

/*$params = $this->item->params;
$ns = jsGetValueNo0($params,'nav_style','tabs');
$n = ($ns == 'tabs')?'tab':'pill';

$tItem = $this->item;
$children = $tItem->get('children');*/
?>


<div class="col-md-[md] [wrap_cls]" data-wow-delay="[delay]s" data-wow-offset="[wow_offset]">
	<div class="op-tab " id="[wrap_id]">
		<ul class="nav nav-[tab_style]">
		<!-- Tab Link -->
			<li  class="[tab_cls]" >
				<a href="#op-tab-item-[tab_id]" data-toggle="[toggle]" >[tab_title]</a>
			</li>
		<!-- Tab Link -->
		</ul>
		
		<div class="tab-content">
		  [children]
		</div>
	</div>
</div>