<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeTab extends jsOpelPlugin
{
	protected $name = 'tab';
	protected $isGroup = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
			
		$params['indicators'] = JRequest::getInt('indicators');
		$params['navcontrol'] = JRequest::getInt('navcontrol');
		$node->set('params',$params);
	
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
			
		$params['nav_style'] = JRequest::getCmd('nav_style');
		$node->set('params',$params);
		
		return true;
	}
	
	public function saveChild(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');		
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			$params['image'] = $imageFile;
		}
		
		$filter = JFilterInput::getInstance();
		$params['cls'] = $filter->clean($post['cls'],'string');
		$caption = JRequest::getString('caption','','post',JREQUEST_ALLOWHTML);
		$params['caption'] = $caption;
		
		$node->set('params',$params);
		return true;
	}
	
	protected function _parseGroupTpl($item,$children)
	{
		$params = $this->_parseCommonParams($item);
		$ns = jsGetValueNo0($params,'nav_style','tabs');
		$n = ($ns == 'tabs')?'tab':'pill';
	
		$parent = OnepageHelperEl::id($item);
		$nodes = $item->get('children');
		$newChildren = array();
		foreach ($nodes as $k => $node)
		{
			if($this->layout != 'mobile')
			{
				$active = ($k == 0)?'active':'';
				$newChildren[] = "<div class=\"tab-pane {$active} clearfix op-tab-item \" id=\"op-tab-item-{$node->id}\">";
				$newChildren[] = $children[$k];
				$newChildren[] = "</div>";
			}
			else
			{
				$cls = "data-parent=\"#{$parent}\"";
				$active = 'in';//($k == 0)?'in':'';
				$fa = ($k == 0)?"fa-minus":'fa-plus';
				$newChildren[] = "<div class=\"panel panel-primary\">";
				$newChildren[] = "<div class=\"panel-heading\">";
				
				$newChildren[] =  "<a data-toggle=\"collapse\" {$cls}  href=\"#op-collapse-item-{$node->id}\" class=\"panel-title\"><h4 >{$node->title}</h4></a>";
				
				
				$newChildren[] = "</div>";
				
				$newChildren[] = "<div id=\"op-collapse-item-{$node->id}\" class=\"panel-collapse collapse op-collapse-item {$active}\">";
				$newChildren[] = '<div class="panel-body">';
				$newChildren[] = $children[$k];
				$newChildren[] = "</div></div></div>";
			}
			
		}
		
		// Indicators
		preg_match("/<!-- Tab Link -->(\s|.)*?<!-- Tab Link -->/i", $this->html,$matches);
		$li = $matches[0];
		if( count($nodes) > 0 )
		{
			$lis = array();
			foreach ($nodes as $k => $node)
			{
				$a = $li;
				$icls = ($k==0)?'active':'';
				$a = str_replace('[tab_id]',$node->id,$a);
				$a = str_replace('[tab_title]',$node->title,$a);
				$a = str_replace('[tab_cls]',$icls,$a);
				$a = str_replace('[toggle]',$n,$a);
				$lis[] = $a;
			}
			$lis = implode("\r\n",$lis);
				
			$this->html = str_replace($li, $lis, $this->html);
		}
		else
		{
			$this->html = str_replace($li, '', $this->html);
		}
		
		$cHtml = implode("\r\n",$newChildren);
		$this->addVariable('children',  $cHtml );
		$this->addVariable('tab_style',  $ns );
		
		return $this->html;
	}
	
	protected function _getJS($code,$item)
	{
		/*$params = $item->params;
		$allow_popup = jsGetValueNo0($params,'allow_popup',0);
		$ce = jsGetValueNo0($params,'clickevent',0);
	
		$mixpanel_api = $this->appconfig->getNo0('mixpanel_apikey',false);
		$id = OnepageHelperEl::id($item);*/
	
	
	
		//$code['mplightbox'] = '$(".mplightbox").magnificPopup({type:"image"}); ';
		return $code;
	}
}
