<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeSidebar extends jsOpelPlugin
{
	protected $name = 'sidebar';
	protected $isLevel1 = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
		$params['has_wrap'] = JRequest::getInt('has_wrap');
		$params['height'] = JRequest::getInt('height');
		$params['bg_color'] = JRequest::getCmd('bg_color');
		$params['hide_bg_image'] = JRequest::getInt('hide_bg_image');
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$params['wrap_id'] = JRequest::getString('wrap_id');
		$node->showtitle = JRequest::getInt('showtitle');
	
		// Image
		$image = JRequest::getVar('bg_image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$filePath = OnepageHelper::getImgPath()."bg_image_{$node->id}.{$imgExt}";
	
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			$params['bg_image'] = "bg_image_{$node->id}.{$imgExt}";
		}
		$node->params = $params;
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		////////////////////////////////////////////////////////
		$nav_show = JRequest::getVar('row_show',array());
		JArrayHelper::toInteger($nav_show);
		$nav_type = JRequest::getCmd('nav_type');
		$header_fixed = JRequest::getCmd('header_fixed');
		$brand_type = JRequest::getCmd('brand_type');
		
		$params = $node->params;
		$params['row_show'] = $nav_show;
		$params['nav_type'] = $nav_type;
		$params['header_fixed'] = $header_fixed;
		$params['brand_type'] = $brand_type;
		$params['header_align'] = JRequest::getCmd('header_align');
		
		$params['show_social'] = JRequest::getCmd('show_social');
		
		// Image
		$image = JRequest::getVar('brand', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "brand_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
	
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			$params['brandicon'] = $imageFile;
		}
		$node->params = $params;
	
		$content = JRequest::getString('content','','post',JREQUEST_ALLOWHTML);
	
		if($content == '<p><br /></p>')
		{
			$content = '';
		}
		$node->set('content',$content);
		
		// Here we have to update the showtitle
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($node->parent_id);
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->update('#__onepage_page');
		$query->set("`showtitle`=0");
		$query->where("`lft` > {$tEl->lft} AND `rgt` < {$tEl->rgt}");
		jsDB::query($query);
		
		if(!empty($nav_show))
		{
			$query = $db->getQuery(true);
			$query->update('#__onepage_page');
			$query->set("`showtitle`=1");
			$rids = implode(',',$nav_show);
			$query->where("`id` IN ({$rids})");
			jsDB::query($query);
		}
		
	
		return true;
	}
	
	protected function _parseTpl($item)
	{
		$params =  $this->_parseCommonParams($item);
	
		$this->addVariable('content', $item->content);
		
		$imgPath = OnepageHelperFront::getImgPath().jsGetValueNo0($params,'brandicon','no.png');
		$hasImage = JFile::exists($imgPath);
		$link = '';
		$brandType = jsGetValueNo0($params,'brand_type');
		$brand = '';
		
		if($brandType == 'image')
		{
			if($hasImage)
			{
				$link = OnepageHelperFront::getImgPath(true).jsGetValueNo0($params,'brandicon','no.png');
				$brand = "<img src='{$link}' alt='brand' title='Brand' class=\"img-responsive-height\">";
			}
		}
		elseif($brandType == 'title')
		{
			$brand = $item->title;
		}
		
		
		
		$this->addVariable('brand', $brand);
		return $this->html;
	}
	
	public function saveChild(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			$params['image'] = $imageFile;
		}
	
		$filter = JFilterInput::getInstance();
		$params['cls'] = $filter->clean($post['cls'],'string');
		$caption = JRequest::getString('caption','','post',JREQUEST_ALLOWHTML);
		$params['caption'] = $caption;
	
		$node->set('params',$params);
		return true;
	}
		
	/*public function getJS( &$code,$item )
	{
		$txt = "$('#menuToggle, .menu-close').on('click', function(){
				$('#menuToggle').toggleClass('active');
				$('body').toggleClass('body-push-toleft');
				if ( $( '#theMenu' ).hasClass( 'menu-open' ) ) {
			  		$( '#theMenu' ).css('z-index', 1100).removeClass('menu-open');
			 	} else {
			  		$( '#theMenu' ).css('z-index', 1130).addClass('menu-open');
			  	}
				//$('#theMenu').toggleClass( 'menu-open' );
			});";
		
		$code['menu'] = $txt;
		
		
		return true;
	}*/
	
	protected function _getJS($code,$item)
	{
		$params = $item->params;
		$allow_popup = jsGetValueNo0($params,'allow_popup',0);
		

	/*
	 * Original: 
	 */	
	/*$txt = "$('#menuToggle, .menu-close').on('click', function(){
						$('#menuToggle').toggleClass('active');
						$('body').toggleClass('body-push-toleft');
						if ( $( '#theMenu' ).hasClass( 'menu-open' ) ) {
					  		$( '#theMenu' ).css('z-index', 1100).removeClass('menu-open');
					 	} else {
					  		$( '#theMenu' ).css('z-index', 1130).addClass('menu-open');
					  	}
						//$('#theMenu').toggleClass( 'menu-open' );
					});";
		*/
		/*
		 * Überarbeitet-> $('body').toggleClass('body-push-toleft'); -entfernt !
		 */

		$txt = "$('#menuToggle, .menu-close').on('click', function(){
						$('#menuToggle').toggleClass('active');						
						if ( $( '#theMenu' ).hasClass( 'menu-open' ) ) {
					  		$( '#theMenu' ).css('z-index', 1100).removeClass('menu-open');
					 	} else {
					  		$( '#theMenu' ).css('z-index', 1130).addClass('menu-open');
					  	}
						//$('#theMenu').toggleClass( 'menu-open' );
					});";


		$code['menu'] = $txt;
	
		$id = OnepageHelperEl::id($item);
		$code['scrollspy'] = "$('body').scrollspy({ target: '.menu-wrap' });";
	
		return $code;
	}
	
	protected function _import($old,$new,$path)
	{
		//$tEl = JTable::getInstance('Page','OnepageTable');
		//$tEl->load($node->parent_id);
		
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from('#__onepage_page');
		$query->where("`parent_id` = '{$new->parent_id}'");
		$query->where("`showtitle` = '1'");
		$objs = jsDB::loadObjs($query,'obj');
		$rids = array();
		
		foreach($objs as $obj)
		{
			$rids[] = $obj->id;
		}
		$params = $new->params;
		$params = jsSetValue($params, 'row_show', $rids);
		$new->set('params',$params);
		$new->store();
	}
}
