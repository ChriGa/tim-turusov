<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
//list($tItem,$params,$id,$children) = $this->fetchVars();
//$tItem->set('children',$children);
$params = $this->item->params;
$rows = jsGetValueNo0($params,'row_show',array());
$show = jsGetValueNo0($params,'show_social');
$pi = array();
foreach($this->items as $item){
	if(!in_array($item->id,$rows)) {
		continue;
	}
	$pi[] = $item;
}
?>

<nav class="menu" id="theMenu">
	<div class="menu-wrap">
		<h3 class="logo"><a href="index.php">[title]</a></h3>
		<i class="fa fa-times menu-close"></i>
		<ul class="nav list-unstyled">

			<?/*Hier wurde manuel Menüpunkt about me eingetragen */?>

			<? //<li><a href="/about-me" alt="About Tim Turusov">About me</a></li> ?>

		<?php foreach($pi as $item): ?>
		<li>
		<a href="<?php echo '#'.OnepageHelperEl::id($item); ?>" class="smoothScroll"><?php echo $item->title; ?></a>
		</li>
		<?php endforeach; ?>
		</ul>
		<div>[content]</div>
		<hr class="hr-sidebar"/>
		
		<?php if( $show ): ?>
		<ul class="list-inline op-sidebar-social">
			<?php if($this->config->get('facebook')): ?>
			
			<li>
				<a class="op-sidebar-facebook" href="<?php echo $this->config->get('facebook'); ?>"><i class="fa fa-facebook fa-2x"></i></a>
			</li>
			
			<?php endif; ?>
			
			<?php if($this->config->get('twitter')): ?>
			<li>
				<a class="op-sidebar-twitter" href="<?php echo $this->config->get('twitter'); ?>"><i class="fa fa-twitter fa-2x"></i></a>
			</li>
			<?php endif; ?>
			
			<?php if($this->config->get('googleplus')): ?>
			<li>
				<a class="op-sidebar-googleplus" href="<?php echo $this->config->get('googleplus'); ?>"><i class="fa fa-google-plus fa-2x"></i></a>
			</li>
			<?php endif; ?>
		</ul>
		<?php endif; ?>
	</div>
	
	<!-- Menu button -->
	<div id="menuToggle" class=""><i class="fa fa-bars"></i></div>
</nav>
