<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
$rows = $this->cp->getPublishedChildren(false);
$checked = jsGetValueNo0($this->item->params,'row_show',array());
?>


<div class="panel-group" id="accordion">

	<div class="panel panel-primary">
		<div class="panel-heading haslink">
			<h4 class="panel-title">
				<a  data-parent="#accordion"> Parameters </a>
			</h4>
		</div>
		<div id="html" class="panel-collapse collapse in">
			<div class="panel-body">
				
					
		  		<div class="form-group hidden">
				  	<label class="col-sm-4">Align</label>
			  		<div class="col-sm-8">
			  			<select class="form-control" name="header_align" ng-model="item.params.header_align">
		    				<option value="1">Left</option>
							<option value="">Right</option>
						</select>
			  		</div>
				</div>
				
				
				<div class="form-group">
				  	<label class="col-sm-3 control-label"> Brand </label>
			  		<div class="col-sm-9">
			  			<select class="form-control" name="brand_type" ng-model="item.params.brand_type">
		    				<option value="">None</option>
							<option value="title">Title</option>
						</select>
			  		</div>
				</div>
				
				<div class="form-group">
				  	<label class="col-sm-3 control-label"> Show Social Link </label>
			  		<div class="col-sm-9">
			  			<select class="form-control" name="show_social" ng-model="item.params.show_social">
		    				<option value="1">Yes</option>
							<option value="">No</option>
						</select>
			  		</div>
				</div>
				
				<div class="form-group hidden">
					<label class="col-sm-3"> Content </label>
					<div class="col-md-9">
						<div class="summernote" name="content" ><?php echo $this->item->content;?></div>
					</div>
				</div>
				
				
				
				<hr/>
				
				<div class="form-group">
					<label class="col-sm-12">Show In Navigation</label>
					<div class="col-md-12">
						<div class="row">
						<?php foreach($rows as $row):
						if(in_array($row->type, array('nav','sidebar'))) continue;
						?>
						<div class="col-md-4">
						<label >
						<input type="checkbox" name="row_show[]" value="<?php echo $row->id;?>" <?php echo (in_array($row->id,$checked))?'checked':'';?>/> <?php echo $row->title;?>
						</label>
						</div>
						<?php endforeach;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
</div>
<input type="hidden" name="option" value="com_onepage">
<input type="hidden" name="task" value="prototype.saveItem">
<input type="hidden" name="id" value="<?php echo $this->item->id;?>">


<script>
function elitem($scope) {
	$scope.previewimglink = 'index.php?option=com_onepage&task=prototype.getImg&id='+getId();
	$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
	
	$scope.submit = function(v) {
		var sHTML = $('.summernote').code();
		$('#elform').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'v':v,'content':sHTML},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
						$scope.$parent.list = res.list;
					});
				}
				$('#elform .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
}
</script>