<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
//list($tItem,$params,$id,$children) = $this->fetchVars();
//$tItem->set('children',$children);
//$params = $this->item->params;
//$rows = jsGetValueNo0($params,'row_show',array());



?>

<nav class="navbar navbar-default op-nav [nav_cls]" role="navigation" id="head-nav">
	<div>[content]</div>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#[wrap_id]">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">[brand]</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse menu-wrap" id="[wrap_id]">
      <ul class="nav navbar-nav [nav_align]">
      	<!-- navbar-menu -->
		<li><a href="#[li_id]" class="smoothScroll">[li_title]</a></li>
		<!-- navbar-menu -->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- show -->
<div class="fixed-top-height [showfixed]"></div>
<!-- show -->