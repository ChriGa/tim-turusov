<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeNav extends jsOpelPlugin
{
	protected $name = 'nav';
	protected $isLevel1 = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	
	protected function _parseTpl($item)
	{
		$params =  $this->_parseCommonParams($item);
		$cls = jsGetValueNo0($params,'header_fixed',false)?'navbar-fixed-top':'';
		$align = jsGetValueNo0($params,'header_align',false)?'':'navbar-right';
	
		$this->addVariable('content', $item->content);
		
		$imgPath = OnepageHelperFront::getImgPath().jsGetValueNo0($params,'brandicon','no.png');
		$hasImage = JFile::exists($imgPath);
		$link = '';
		$brandType = jsGetValueNo0($params,'brand_type');
		$brand = '';
		
		if($brandType == 'image')
		{
			if($hasImage)
			{
				$link = OnepageHelperFront::getImgPath(true).jsGetValueNo0($params,'brandicon','no.png');
				$brand = "<img src='{$link}' alt='brand' title='Brand' class=\"img-responsive-height\">";
			}
		}
		elseif($brandType == 'title')
		{
			$brand = $item->title;
		}
		
		$this->addVariable('brand', $brand);
		
		$header_fixed = jsGetValueNo0($params,'header_fixed',false);
		// $this->removeShowTag( !$header_fixed ); use in future
		
		if($header_fixed)
		{
			$this->addVariable('showfixed', '');
		}
		else
		{
			$this->addVariable('showfixed', 'hidden');
		}
		
		$this->addVariable('nav_cls', $cls);
		$this->addVariable('nav_align', $align);
		
		// Menu
		$rows = jsGetValueNo0($params,'row_show',array());
		preg_match("/<!-- navbar-menu -->(\s|.)*?<!-- navbar-menu -->/i", $this->html,$matches);
		$li = $matches[0];
		if(count($rows) > 0)
		{ 
			$db = jsDB::instance();
			$query = $db->getQuery(true);
			$query->select('`id`,`title`,`type`,`params`');
			$query->from('#__onepage_page');
			$query->where("`id` IN ('".implode("','", $rows)."')");
			$query->where("`published` = 1");
			$nodes = jsDB::loadObjs($query,'obj');
			foreach($nodes as $item)
			{
				if(!in_array($item->id,$rows) ) {
					continue;
				}
				
				$a = $li;
				$a = str_replace('[li_id]',OnepageHelperEl::id($item),$a);
				$a = str_replace('[li_title]',$item->title,$a);
				$lis[] = $a;
			}
			
			$lis = implode("\r\n",$lis);
				
			$this->html = str_replace($li, $lis, $this->html);
		}
		else
		{
			$this->html = str_replace($li, '', $this->html);
		}
		
		
		return $this->html;
	}
	
	public function saveEl(&$node,$post)
	{
		////////////////////////////////////////////////////////
		$nav_show = JRequest::getVar('row_show',array());
		JArrayHelper::toInteger($nav_show);
		$nav_type = JRequest::getCmd('nav_type');
		$header_fixed = JRequest::getCmd('header_fixed');
		$brand_type = JRequest::getCmd('brand_type');
		
		$params = $node->params;
		$params['row_show'] = $nav_show;
		$params['nav_type'] = $nav_type;
		$params['header_fixed'] = $header_fixed;
		$params['brand_type'] = $brand_type;
		$params['header_align'] = JRequest::getCmd('header_align');;
		
	
		// Image
		$image = JRequest::getVar('brand', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "brand_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
	
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			$params['brandicon'] = $imageFile;
		}
		$node->params = $params;
	
		$content = JRequest::getString('content','','post',JREQUEST_ALLOWHTML);
	
		if($content == '<p><br /></p>')
		{
			$content = '';
		}
		$node->set('content',$content);
		
		// Here we have to update the showtitle
		$tEl = JTable::getInstance('Page','OnepageTable');
		$tEl->load($node->parent_id);
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->update('#__onepage_page');
		$query->set("`showtitle`=0");
		$query->where("`lft` > {$tEl->lft} AND `rgt` < {$tEl->rgt}");
		jsDB::query($query);
		
		if(!empty($nav_show))
		{
			$query = $db->getQuery(true);
			$query->update('#__onepage_page');
			$query->set("`showtitle`=1");
			$rids = implode(',',$nav_show);
			$query->where("`id` IN ({$rids})");
			jsDB::query($query);
		}
		
	
		return true;
	}
	
	protected function _export($item,$path)
	{
		$params = $item->params;
		// section background
		$bgImage = jsGetValueNo0($params,'brandicon','none.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
		$dst = $path.'images/';
		$dst .= $bgImage;
	
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}
	}
	
	protected function _import($old,$new,$path)
	{
		$params = jsGetValueNo0($old,'params',array());
	
		$imgPath = $path."/images/";
		$dst = OnepageHelper::getImgPath();
		$bgImage = jsGetValueNo0($params,'brandicon','none.png');
	
		$newFile = "brand_{$new->id}.".JFile::getExt($bgImage);
		if( JFile::exists($imgPath.$bgImage) )
		{
			JFile::copy( $imgPath.$bgImage, $dst.$newFile );
	
			$params = jsSetValue($params, 'brandicon', $newFile);
			//$new->set('params',$params);
			//$new->store();
		}
		
		$db = jsDB::instance();
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from('#__onepage_page');
		$query->where("`parent_id` = '{$new->parent_id}'");
		$query->where("`showtitle` = '1'");
		$objs = jsDB::loadObjs($query,'obj');
		$rids = array();
		
		foreach($objs as $obj)
		{
			$rids[] = $obj->id;
		}
		$params = $new->params;
		$params = jsSetValue($params, 'row_show', $rids);
		$new->set('params',$params);
		$new->store();
	}
	
	protected function _getJS($code,$item)
	{
		$params = $item->params;
		//$allow_popup = jsGetValueNo0($params,'allow_popup',0);
		//$id = OnepageHelperEl::id($item);
		//$code['nav'] = "$('body').scrollspy({ target: '#{$id}' });";
		$code['scrollspy'] = "$('body').scrollspy({ target: '.menu-wrap' });";
	
		return $code;
	}
}
