<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
$params = $this->item->params;

$uri = JFactory::getURI();
JHtml::script("media/jsbt3/js/form.js");
JHtml::script("media/jsbt3/js/blockui.js");
JHtml::stylesheet("media/jsbt3/css/blockui.css");
?>
<div class="col-md-[md] [wrap_cls]">

<form class="form-horizontal  opsubscriptionform " id="[wrap_id]"  action="index.php?option=com_onepage" method="POST"  data-parsley-validate>

    <div class="row">
    	<div class="col-md-12">
    		<div id="subscriptionform-[id]-error" class="subscriptionform-error text-danger"></div>
    	</div>
    	<div class="col-xs-12">
		    <input type="email" placeholder="ENTER E-MAIL ADDRESS" name="email"  class="form-control" data-parsley-required="true" data-title="Subject" data-parsley-errors-container="#subscriptionform-[id]-error">
		</div>
		
		<div class="col-xs-12">
		    <button class="btn btn-success btn-block" type="submit">SAVE YOUR PLACE</button>
		</div>
		
		
	</div>
	
	<input type="hidden" name="eid" value="<?php echo $this->item->id;?>"> 
	<input type="hidden" name="task" value="page.formSubmit"> 
	<input type="hidden" name="returnUrl" value="<?php echo base64_encode($uri);?>">
	<?php echo JHtml::_( 'form.token' );?>
	
</form>

</div>