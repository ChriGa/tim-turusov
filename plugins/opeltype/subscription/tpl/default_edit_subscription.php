<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$tConfig = jsAppConfig::getInstance('onepage');
$list = array();
if($tConfig->search('mc_apikey',false))
{
	$MailChimp = new jsMailchimp($tConfig->get('mc_apikey'));
	$list = $MailChimp->call('lists/list');
}

$acy = onepageHelper::getAcyMailingList();
?>

<div class="panel-group" id="accordion">

	<div class="panel panel-primary">
		<div class="panel-heading haslink">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#html"> Parameters </a>
			</h4>
		</div>
		
			<div class="panel-body">
				<div class="form-group">
					<label  for="subscr_id" class="col-sm-3 control-label"><?php echo JText::_('JS_MAILCHIMP'); ?></label>
					<div class="col-sm-9">
						<select class="form-control"  name="mclist_id" ng-model="item.params.mclist_id">
							<option value=""><?php echo JText::_('JS_NONE'); ?></option>
			
							<?php if(!empty($list)): ?>
							<?php foreach($list['data'] as $item): ?>
							<option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
							<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="subscr_id" class="col-sm-3 control-label"><?php echo JText::_('JS_ACYMAILING'); ?></label>
					<div class="col-sm-9">
						<select class="form-control" name="acylist_id" ng-model="item.params.acylist_id">
							<option value=""><?php echo JText::_('JS_NONE'); ?></option>
							<?php if(!empty($acy)): ?>
							<?php foreach($acy as $item): ?>
							<option value="<?php echo $item['listid']; ?>"><?php echo $item['name']; ?></option>
							<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
				</div>
				
			</div>
	</div>
	
	
</div>
<input type="hidden" name="option" value="com_onepage">
<input type="hidden" name="task" value="prototype.saveItem">
<input type="hidden" name="id" value="<?php echo $this->item->id;?>">


<script>
function elitem($scope) {
	
	$scope.submit = function(v) {
		$('#elform').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'v':v},
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
						$scope.$parent.list = res.list;
					});
				}
				NProgress.done();
			}
		});
	}
}
</script>