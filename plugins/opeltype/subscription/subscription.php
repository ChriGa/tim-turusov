<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeSubscription extends jsOpelPlugin
{
	protected $name = 'subscription';
	protected $isGroup = false;
	protected $isItem = true;
	protected $isEditable = false;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function quickSaveEl(&$node,$post)
	{
	
	
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
			
		$params['mclist_id'] = JRequest::getCmd('mclist_id');
		$params['acylist_id'] = JRequest::getInt('acylist_id');
		$node->set('params',$params);
		
		return true;
	}
	
	protected function _parseTpl($item)
	{
		$params = $this->_parseCommonParams( $item );
		
		$uri = JFactory::getURI();
		$returnUrl = base64_encode($uri);
		
		$this->addVariable('formtoken', JHtml::_( 'form.token' ));
		$this->addVariable('returnUrl', $returnUrl);
		
		return $this->html;
	}
	
	protected function _getJS($code,$item)
	{
		
		$code['notify'] = "\$.noty.defaults.callback = { onShow: function() { \$('#noty_top_layout_container').css({width:'100%',left:0}) } } ;";
		$code['notify'].= "\$.noty.defaults.theme='form-notify text-center';";
		$code['notify'].= "\$.noty.defaults.animation = {open: {height: 'toggle'}, close: {height: 'toggle'}, speed: 200};";
		
		$code['notify_func'] = "";
		
		$wrap_id = OnepageHelperEl::id($item);
		
		$txt = '';
		$txt.= "$('#{$wrap_id}').ajaxForm({";
		//$txt.= "url: 'index.php',";
		$txt.= "dataType:  'json',";
		$txt.= "type: 'POST',";
		$txt.= "success: function(res)	{ if(res.success) {	noty({text: '<i class=\"fa fa-check-circle\"></i> '+res.content,type: 'success', timeout: 3000 }); }}";
		$txt.= "});";
		
		/*$txt.= "$('#op-subscriptionform-{$item->id}').parsley().subscribe('parsley:field:error', function (fieldInstance) {";
		$txt.= "var errors = window.ParsleyUI.getErrorsMessages(fieldInstance);";
		$txt.= "var eString = errors[0];";
		$txt.= "fieldInstance.\$element.closest('.form-group').removeClass('has-success').addClass('has-error').children('.form-control-feedback').html('*'+eString);";
		$txt.= "});";
		
		$txt.= "$('#op-subscriptionform-{$item->id}').parsley().subscribe('parsley:field:success', function (fieldInstance) {";
		$txt.= "fieldInstance.\$element.closest('.form-group').removeClass('has-error').addClass('has-success').children('.form-control-feedback').html('');";
		$txt.= "});";*/
		
		
		$code[] = $txt;
		return $code;
	}
	
	protected function _getCSS($code,$item)
	{
		return $code;
	}
	
	public function formaction(&$node)
	{
		$tConfig = jsAppConfig::getInstance();
		$app	= JFactory::getApplication();
		$contact = $tConfig->getNo0('contact_email',$app->getCfg('mailfrom'));
		
		// Get the data from POST
		
		$email = JRequest::getString('email');
		$params = $node->params;
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)
		{
			return false;
		}
		else
		{
			$this->_subAcy($params,$email);
			$this->_subMC($params,$email);
		}
		
		return true;
	}
	
	protected function _subAcy($params,$email)
	{
		$acylist_id = jsGetValueNo0($params,'acylist_id',false);
		if( !$acylist_id )
		{
			return true;
		}
		
		
			
		$file = JPATH_SITE.'/administrator/components/com_acymailing/helpers/helper.php';
		if(JFile::exists($file))
		{
			$var = array();
			$var['subscriber'] = array();
			$var['listsub'] = array();
			$var['listsub'][$acylist_id] = array();
				
			$var['subscriber']['html'] = 1;
			$var['subscriber']['email'] = $email;
			//$var['subscriber']['name'] = '';
			$var['listsub'][$acylist_id]['status'] = 1;
			JRequest::setVar('data',$var);
				
				
			require_once ( $file );
			acymailing_checkRobots();
			$app = JFactory::getApplication();
				
			$config = acymailing_config();
			$subscriberClass = acymailing_get('class.subscriber');
				
				
			$status = $subscriberClass->saveForm();
			$subscriberClass->sendNotification();
		}
	}
	
	protected function _subMC($params,$email)
	{
		$tConfig = jsAppConfig::getInstance('onepage');
		$key = $tConfig->search('mc_apikey','');
		$mclist_id = jsGetValueNo0($params,'mclist_id',false);
		
		if( empty($key) ) {
			return false;
		}
		
		if(empty($mclist_id))
		{
			return true;
		}
		
		$MailChimp = new jsMailchimp($key);
    	$result = $MailChimp->call('lists/subscribe', array(
    		    	                'id'                => "{$mclist_id}",
    		    	                'email'             => array('email'=>$email),
    		    	                'merge_vars'        => array(),
    		    	                'double_optin'      => false,
    		    	                'update_existing'   => true,
    		    	                'replace_interests' => false,
    		    	                'send_welcome'      => false,
    	));
    	
		return (isset($result['email']))?true:false;
	}
	
	protected function _preloadJsSrc($codes,$item = null)
	{
		$codes['form'] = JFile::read(JPATH_SITE."/media/jsbt3/js/form.js");
		$codes['block'] = JFile::read(JPATH_SITE."/media/jsbt3/js/blockui.js");
		//JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
		return $codes;
	}
	
	protected function _preloadCssSrc($codes,$item = null)
	{
		$codes['block'] = JFile::read(JPATH_SITE."/media/jsbt3/css/blockui.css");
		//JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
		return $codes;
	}
}
