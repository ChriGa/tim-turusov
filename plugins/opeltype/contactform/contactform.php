<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeContactform extends jsOpelPlugin
{
	protected $name = 'contactform';
	protected $isGroup = false;
	protected $isItem = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function quickSaveEl(&$node,$post)
	{
	
	
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
		$msg_ph = JRequest::getString('msg_placeholder');
		$params['msg_placeholder'] = $msg_ph;
		
		$node->params = $params;
		
		return true;
	}
	
	protected function _parseTpl($item)
	{
		
		$params = $this->_parseCommonParams($item);
		$msg_placeholder = jsGetValueNo0($params, 'msg_placeholder', JText::_('COM_ONEPAGE_LABEL_MESSAGE').':' );
		
		$this->addVariable('msg_placeholder', $msg_placeholder);
		
		$uri = JFactory::getURI();
		$returnUrl = base64_encode($uri);
		
		$this->addVariable('formtoken', JHtml::_( 'form.token' ));
		$this->addVariable('returnUrl', $returnUrl);
		
		return $this->html;
	}
	
	protected function _getJS($code,$item)
	{
		$code['notify'] = "\$.noty.defaults.callback = { onShow: function() { \$('#noty_top_layout_container').css({width:'100%',left:0}) } } ;";
		$code['notify'].= "\$.noty.defaults.theme='form-notify text-center';";
		$code['notify'].= "\$.noty.defaults.animation = {open: {height: 'toggle'}, close: {height: 'toggle'}, speed: 200};";
		
		$code['notify_func'] = "";
		
		$wrap_id = OnepageHelperEl::id($item);
		$mixpanel_api = $this->appconfig->getNo0('mixpanel_apikey',false);
		
		$txt =  JFile::read(JPATH_SITE."/media/plg_opeltype_{$this->name}/{$this->name}.js");
		$txt = str_replace('[wrap_id]', $wrap_id, $txt);
		
		preg_match("/\/\*-- Mixpanel --\*\/(\s|.)*?\/\*-- Mixpanel --\*\//i", $txt,$matches);
		$li = $matches[0];
		if(empty($mixpanel_api))
		{
			$txt = str_replace($li, '', $txt);
		}
		
		/*$txt.= "$('#{$wrap_id}').ajaxForm({";
		//$txt.= "url: 'index.php',";
		$txt.= "dataType:  'json',";
		$txt.= "type: 'POST',";
		$txt.="beforeSubmit: function()	{NProgress.start();";
		if(!empty($mixpanel_api))
		{
			$txt.= "mixpanel.track( \"ContactForm.Send.{$wrap_id}\");";
		}
		$txt.='},';
		$txt.= "success: function(res)	{ if(res.success) {	
			noty({text: '<i class=\"fa fa-check-circle\"></i> '+res.content,type: 'success', timeout: 3000 }); 
			}NProgress.done();
		}";
		$txt.= "});";
		
		$txt.= "$('#op-contactform-{$item->id}').parsley().subscribe('parsley:field:error', function (fieldInstance) {";
		$txt.= "var errors = window.ParsleyUI.getErrorsMessages(fieldInstance);";
		$txt.= "var eString = errors[0];";
		$txt.= "fieldInstance.\$element.closest('.form-group').removeClass('has-success').addClass('has-error').children('.form-control-feedback').html('*'+eString);";
		$txt.= "});";
		
		$txt.= "$('#op-contactform-{$item->id}').parsley().subscribe('parsley:field:success', function (fieldInstance) {";
		$txt.= "fieldInstance.\$element.closest('.form-group').removeClass('has-error').addClass('has-success').children('.form-control-feedback').html('');";
		$txt.= "});";*/
		
		
		$code[] = $txt;
		return $code;
	}
	
	public function formaction(&$node)
	{
		$tConfig = jsAppConfig::getInstance();
		$app	= JFactory::getApplication();
		$contact = $tConfig->getNo0('contact_email',$app->getCfg('mailfrom'));
		
		// Get the data from POST
		
		$email = JRequest::getString('email');
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)
		{
			$result = array();
			$result['success'] = false;
			$result['content'] = JText::_('Invalid Email');
			return $result;
		}
		else
		{
			$name = JRequest::getString('firstname').' '.JRequest::getString('lastname');
			$subject = JRequest::getString('subject');
			$msg = JRequest::getString('message');
			
			if(!empty($contact) && !empty($msg))
			{
				$data = array();
				$data['email'] = $email;
				$data['name'] = $name;
				$data['subject'] = $subject;
				$data['message'] = $msg;
				$this->_sendEmail($data,$contact,$node);
			}
		}
		
		
		
		return true;
	}
	
	protected function _sendEmail($data,$contact,$node)
	{
		/*$name = $data['name'];
		$emailAddress = $data['email'];
		$body = $data['message'];
		
		// Prepare email body
		$prefix = JText::sprintf('COM_ONEPAGET_TEXT_ENQUIRY', JURI::base());
		$body	= $prefix."\n".$name.' <'.$emailAddress.'>'."\r\n\r\n".stripslashes($body);
		*/
		
		$email = jsEmail::instance();
		$email->subject = $data['subject'];
		$email->body = JText::_('COM_ONEPAGET_CONTACTFORM_HTML');
		
		// Parse The Variable
		$uri = JFactory::getURI();
		$page = OnepageHelperEl::getParentNode($node, 2);
		$email->addVariable('site_name', JURI::base());
		$email->addVariable('page_title', $page->title);
		$email->addVariable('img_link', $uri->root().'media/com_onepage/img/contactform.png');
		$email->addVariable('name', $data['name']);
		$email->addVariable('email', $data['email']);
		$email->addVariable('subject', $data['subject']);
		$email->addVariable('msg', $data['message']);

		// Add Style
		$style = '<style>';
		$style.= JFile::read(JPATH_SITE.'/media/jsbt3/css/bootstrap.min.css');
		$style.= JFile::read(JPATH_SITE.'/media/plg_opeltype_contactform/email.css');
		$style.= '</style>';
		$email->body = $style.$email->body;
	
		return $email->sendEmail($contact);
	
	}
	
	protected function _preloadJsSrc($codes,$item = null)
	{
		$codes['form'] = JFile::read(JPATH_SITE."/media/jsbt3/js/form.js");
		$codes['block'] = JFile::read(JPATH_SITE."/media/jsbt3/js/blockui.js");
		//JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
		return $codes;
	}
	
	protected function _preloadCssSrc($codes,$item = null)
	{
		$codes['block'] = JFile::read(JPATH_SITE."/media/jsbt3/css/blockui.css");
		//JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
		return $codes;
	}
}
