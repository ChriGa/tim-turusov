<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
$params = $this->item->params;

$uri = JFactory::getURI();
JHtml::script("media/jsbt3/js/form.js");
JHtml::script("media/jsbt3/js/parsley.min.js");
?>
<div class="col-md-[md] [wrap_cls]">

<form class="form  opcontactform " id="op-contactform-[id]"  action="index.php?option=com_onepage" method="POST"  data-parsley-validate>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?></label>
				<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?>:" name="firstname"  class="form-control" data-parsley-required="true" data-title="Subject" data-parsley-errors-container="#contactform-error">
				<span class="form-control-feedback"></span>
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?></label>
				<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?>:" name="email"  class="form-control" data-parsley-required="true" data-title="Subject" data-parsley-errors-container="#contactform-error">
				<span class="form-control-feedback"></span>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?></label>
				<textarea    placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_MESSAGE');?>" name="message" rows="12" class="form-control" data-parsley-required="true" data-parsley-errors-container="#contactform-error"></textarea>
				<span class="form-control-feedback"></span>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('Subject');?></label>
				<input type="text" placeholder="<?php echo JText::_('Subject');?>" name="subject"  class="form-control" data-parsley-required="true" data-title="Subject" data-parsley-errors-container="#contactform-error">
				<span class="form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<label class="sr-only" ><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?></label>
				<textarea    placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_MESSAGE');?>" name="message" rows="12" class="form-control" data-parsley-required="true" data-parsley-errors-container="#contactform-error"></textarea>
				<span class="form-control-feedback"></span>
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="form-group has-feedback">
				<label class="sr-only" ><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_FIRST');?></label>
				<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_FIRST');?>" name="firstname"  class="form-control" data-parsley-required="true" data-parsley-errors-container="#contactform-error">
				<span class="form-control-feedback"></span>
			</div>
			
			<div class="form-group has-feedback">
			    <label class="sr-only" for="contactform-<?php echo $tItem->id; ?>-name"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_LAST');?></label>
				<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME_LAST');?>" name="lastname"  class="form-control" >
				<span class="form-control-feedback"></span>
			</div>
			
			<div class="form-group has-feedback">
				<label class="sr-only" for="contactform-<?php echo $tItem->id; ?>-name"><?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?></label>
				<input type="email" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?>" name="email"  class="form-control" data-parsley-required="true" data-parsley-errors-container="#contactform-error">
				<span class="form-control-feedback"></span>
			</div>
			
			<div class="form-group visible-xs">
				<button class="btn btn-primary btn-block btn-lg">Submit</button>
			</div>	
			
			<div class="form-group hidden-xs">
				<button class="btn btn-primary btn-block">Submit</button>
			</div>	
			
			<div id="contactform-error" class="hidden">
				
			</div>	
		</div>
	</div>

	<input type="hidden" name="task" value="contact.submit"> 
	<input type="hidden" name="returnUrl" value="<?php echo base64_encode($uri);?>">
	<?php echo JHtml::_( 'form.token' );?>
	
</form>

</div>