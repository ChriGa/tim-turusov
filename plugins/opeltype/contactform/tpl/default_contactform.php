<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
/*$params = $this->item->params;

$uri = JFactory::getURI();
JHtml::script("media/jsbt3/js/form.js");
JHtml::script("media/jsbt3/js/blockui.js");
JHtml::stylesheet("media/jsbt3/css/blockui.css");*/
//JHtml::stylesheet("media/jsbt3/css/pnotify.min.css");
?>
<div class="col-md-[md] [wrap_cls]" data-wow-delay="[delay]s" data-wow-offset="[wow_offset]">

<form class="form  opcontactform " id="[wrap_id]"  action="index.php?option=com_onepage" method="POST"  data-parsley-validate>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?></label>
				<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOURNAME');?>:" name="firstname"  class="form-control" data-parsley-required="true" data-title="Subject" data-parsley-errors-container="#contactform-[id]-error">
				<span class="form-control-feedback"></span>
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?></label>
				<input type="email" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_YOUREMAIL');?>:" name="email"  class="form-control" data-parsley-required="true" data-title="Subject" data-parsley-errors-container="#contactform-[id]-error">
				<span class="form-control-feedback"></span>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('COM_ONEPAGE_LABEL_SUBJECT');?></label>
				<input type="text" placeholder="<?php echo JText::_('COM_ONEPAGE_LABEL_SUBJECT');?>:" name="subject"  class="form-control" data-parsley-required="true" data-title="Subject" data-parsley-errors-container="#contactform-[id]-error">
				<span class="form-control-feedback"></span>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group has-feedback">
				<label class="sr-only"><?php echo JText::_('COM_ONEPAGE_LABEL_MESSAGE');?></label>
				<textarea    placeholder="[msg_placeholder]" name="message" rows="6" class="form-control" data-parsley-required="true" data-parsley-errors-container="#contactform-[id]-error"></textarea>
				<span class="form-control-feedback"></span>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<button class="btn btn-primary btn-block btn-lg"><i class="fa fa-envelope"></i> Send</button>
		</div>
	</div>
	
	
	<div id="contactform-[id]-error" class="hidden"></div>
	<input type="hidden" name="eid" value="[id]"> 
	<input type="hidden" name="task" value="page.formSubmit"> 
	<input type="hidden" name="returnUrl" value="[returnUrl]">
	<input type="hidden" name="returnType" value="msg">
	[formtoken]
	
</form>

</div>