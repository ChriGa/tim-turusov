<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
?>

<div class="panel-group" id="accordion">
	
	<div class="panel panel-primary">
		<div class="panel-heading " >
			<h4 class="panel-title">Parameters</h4>
		</div>
		
		<div class="panel-body">
		
			<div class="form-group">
				<label class="control-label col-md-3">Message Placeholder</label>
				<div class="col-md-9">
					<input type="text" name="msg_placeholder" ng-model="item.params.msg_placeholder" class="form-control"/>
					
				</div>
			</div>
		</div>
	</div>

	
	
</div>
<input type="hidden" name="option" value="com_onepage">
<input type="hidden" name="task" value="prototype.saveItem">
<input type="hidden" name="id" value="<?php echo $this->item->id;?>">



<script>
function elitem($scope) {
	
	$scope.submit = function(v) {
		$('#elform').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'v':v},
			type: 'POST',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					if(v == 2){ 
						window.location.href = $('#returnLink').attr('href');
					}

					$scope.$apply(function() {
						$scope.$parent.list = res.list;
					});
				}
				NProgress.done();
			}
		});
	}
}
</script>