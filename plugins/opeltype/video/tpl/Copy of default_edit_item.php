<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

?>
<style>
.switchery {width:40px;height:20px;}
.switchery > small {width:20px;height:20px;}
</style>


<form id="elform" class="form-horizontal" role="form" action="index.php">
<div class="panel-group" id="accordion">
	
	<div class="panel panel-primary">
		<div class="panel-heading haslink noselect" style="position:relative">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#html"> Html </a>
					<span style="position:absolute;right:0;top:8px;">
						<input type="checkbox" class="js-switch " name="enable_html" value="1" ui-switch ng-model="item.params.enable_html"/>
					</span>
			</h4>
		</div>
		<div id="html" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="form-group">
				<div class="col-md-12">
					<div class="input-group">
						<span class="input-group-addon">Enable?</span>
  						<select name="enable_html1" ng-model="item.params.enable_html1" class="form-control">
							<option value="0"><?php echo JText::_(JS_NO);?></option>
						<option value="1"><?php echo JText::_(JS_YES);?></option>
					</select>
					</div>
				</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
					<div class="summernote" name="content" ><?php echo $this->item->content;?></div>
				</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="panel panel-primary">
		<div class="panel-heading haslink noselect" style="position:relative;">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#image"> Image </a>
					
					<span style="position:absolute;right:0;top:8px;">
						<input type="checkbox" class="js-switch " name="enable_image" value="1" ui-switch  ng-model="item.params.enable_image"/>
					</span>
			</h4>
		</div>
		<div id="image" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="row">
				{{item.params.enable_image}}
				<input type="checkbox" class="js-switch " name="" value="1" ui-switch  ng-model="test"/>
					<div class="col-md-4">
						<img ng-src="{{previewimg}}" class="img-responsive"/>
					</div>
						
					<div class="col-md-4">
						<div class="form-group1">
							<label class="">Enable?</label>
							<select name="enable_image1" ng-model="item.params.enable_image1" class="form-control">
								<option value="0"><?php echo JText::_(JS_NO);?></option>
								<option value="1"><?php echo JText::_(JS_YES);?></option>
							</select>
						</div>
						
						
						
						<div class="form-group1">
							<label class="">Image Class</label>
							<select name="cls" ng-model="item.params.cls" class="form-control">
								<option value="">None</option>
								<option value="img-circle">Circle</option>
								<option value="img-rounded">Round Corner</option>
								<option value="img-thumbnail">Thumbnail</option>
							</select>
						</div>
						
						<div class="form-group1">
							<label>Allow Popup?</label>
	  						<select name="allow_popup" ng-model="item.params.allow_popup" class="form-control">
								<option value="0"><?php echo JText::_(JS_NO);?></option>
								<option value="1"><?php echo JText::_(JS_YES);?></option>
							</select>
						</div>
						
						<div class="form-group1">
							<label>Responsive</label>
	  						<select name="responsive" ng-model="item.params.responsive" class="form-control ">
								<option value="img-responsive">Max-Width</option>
								<option value="img-responsive-height">Max-Height</option>
							</select>
						</div>
						
						<div class="input-group hidden">
							<div class="input-group">
								<span class="input-group-addon">W</span>
	  							<input type="text" name="width" ng-model="item.params.width" class="form-control " placeholder="Fixed Width">
								<span class="input-group-addon">H</span>
								<input type="text" name="height" ng-model="item.params.height" class="form-control " placeholder="Fixed Height">
							</div>
						</div>
					</div>
			
					<div class="col-md-4">
						<div class="form-group1">
							<label class="">Image Source</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="input-group">
						    	<div class="form-control" data-trigger="fileinput">
									<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
								</div>
						   		<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
						    	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						  		</div>
							</div>
						</div>
						<div class="form-group1">
							<label>Caption</label>
							<textarea name="caption" rows="4" class="form-control" ng-model="item.params.caption"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading haslink">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#setting"> Unit Setting </a>
			</h4>
		</div>
		<div id="setting" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="form-group">
					<div class="col-md-3">Title</div>
					<div class="col-md-9">
						<input type="text" class="form-control" name="title" ng-model="item.title"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">Unit</div>
					<div class="col-md-9">
						<select class="form-control" name="md"  ng-model="item.md">
							<?php for($i=3;$i<=12;$i++):?>
							<option value="<?php echo $i;?>"><?php echo $i;?></option>
							<?php endfor;?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3">Class</div>
					<div class="col-md-9">
						<input type="text" class="form-control" name="wrap_cls" placeholder="Class in the wrap div" ng-model="item.params.wrap_cls"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<input type="hidden" name="option" value="com_onepage">
<input type="hidden" name="task" value="prototype.saveItem">
<input type="hidden" name="id" value="<?php echo $this->item->id;?>">
</form>

<script>
function elitem($scope) {
	$scope.previewimglink = 'index.php?option=com_onepage&task=prototype.getImg&id='+getId();
	$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
	
	$scope.submit = function(v) {
		var sHTML = $('.summernote').code();
		$('#elform').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'v':v,'content':sHTML},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
					});
					
					if(v == 1) {
						window.location.href= res.link;
					} else if(v == 2){ 
						window.location.href = $('#returnLink').attr('href');
					}
				}
				$('#elform .fileinput').fileinput('clear');
				NProgress.done();
			}
		});
	}
}
</script>