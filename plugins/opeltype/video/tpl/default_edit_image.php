<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

?>

	
	<div class="panel panel-primary">
		<div class="panel-heading haslink noselect" style="position:relative;">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#image"> Image </a>
					
					<span style="position:absolute;right:0;top:8px;">
						<input type="checkbox" class="js-switch " name="enable_image" value="1" ui-switch  ng-model="item.params.enable_image"/>
					</span>
			</h4>
		</div>
		<div id="image" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="row">
				
					<div class="col-md-4">
						<img ng-src="{{previewimg}}" class="img-responsive"/>
					</div>
					<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group1">
								<label class="">Image Source</label>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group">
							    	<div class="form-control" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
							   		<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
							    	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
							  		</div>
								</div>
							</div>
							
							<div class="form-group1 hidden">
								<label class="">Mobile Image( If you want to display different image in mobile )</label>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group">
							    	<div class="form-control" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
							   		<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="mobile_image"></span>
							    	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
							  		</div>
								</div>
							</div>
						</div>
					
						<div class="col-md-6">
							
							<div class="form-group1">
								<label class="">Image Class</label>
								<select name="cls" ng-model="item.params.cls" class="form-control">
									<option value="">None</option>
									<option value="img-circle">Circle</option>
									<option value="img-rounded">Round Corner</option>
									<option value="img-thumbnail">Thumbnail</option>
								</select>
							</div>
							
							<div class="form-group1">
								<label>Responsive</label>
		  						<select name="responsive" ng-model="item.params.responsive" class="form-control ">
									<option value="img-responsive">Max-Width</option>
									<option value="img-responsive-height">Max-Height</option>
								</select>
							</div>
							
							<div class="form-group1">
								<label>Alignment</label>
		  						<select name="ialign" ng-model="item.params.ialign" class="form-control ">
									<option value="">Left</option>
									<option value="center-block">Center</option>
									<option value="pull-right">Right</option>
								</select>
							</div>
							
							
							<div class="input-group hidden">
								<div class="input-group">
									<span class="input-group-addon">W</span>
		  							<input type="text" name="width" ng-model="item.params.width" class="form-control " placeholder="Fixed Width">
									<span class="input-group-addon">H</span>
									<input type="text" name="height" ng-model="item.params.height" class="form-control " placeholder="Fixed Height">
								</div>
							</div>
						</div>
				
						<div class="col-md-6">
							<div class="form-group1">
								<label>Click Event</label>
		  						<select name="allow_popup" ng-model="item.params.allow_popup" class="form-control">
									<option value=""><?php echo JText::_(JS_NONE);?></option>
									<option value="1"><?php echo JText::_('Popup');?></option>
								</select>
							</div>
							
							<div class="form-group1" ng-show="item.params.allow_popup">
								<label>Caption</label>
								<textarea name="caption" rows="4" class="form-control" ng-model="item.params.caption"></textarea>
							</div>
						</div>
					</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
