<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeVideo extends jsOpelPlugin
{
	protected $name = 'video';
	protected $isGroup = false;
	protected $isItem = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	
	public function saveEl(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');		
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			$params['image'] = $imageFile;
		}
		
		
		$params['vlink'] = JRequest::getString('vlink','','post',JREQUEST_ALLOWRAW);
		$params['height'] = JRequest::getInt('height');
		//$params['enable_image'] = $filter->clean($params['enable_image']);
		//$params['enable_html'] = jsGetValue($post,'enable_html',1);
		//$params['enable_html'] = $filter->clean($params['enable_html'],'int');
		
		
		$node->set('params',$params);
		
		//$content = JRequest::getString('content','','post',JREQUEST_ALLOWHTML);
		//$node->set('content',$content);
		
		return true;
	}
	
	protected function _parseTpl($item)
	{
		//list($tItem,$params,$id,$children) = onepageHelper::getItemTpl($item);
		$params = $this->_parseCommonParams($item);
		// Html
		
		$iframe = jsGetValueNo0($params,'vlink');
		$height = jsGetValueNo0($params,'height');
		preg_match('/height="(\w+)"/', $iframe,$match);
		$iframe = preg_replace('/width="(\w+)"/', 'width="100%"', $iframe);
		if(empty($height))
		{
			$iframe = preg_replace('/height="(\w+)"/', 'height="100%"', $iframe);
		}
		else
		{
			$iframe = preg_replace('/height="(\w+)"/', "height=\"{$height}\"", $iframe);
		}
		
		$this->addVariable('iframe', $iframe);
		
		return $this->html;
	}
	
	protected function _getJS($code,$item)
	{
		//$params = $item->params;
		
		return $code;
	}
	
	protected function _export($item,$path)
	{
		$params = $item->params;
		// section background
		$imgPath = OnepageHelper::getImgPath().jsGetValueNo0($params,'image','no.png');
		
		$dst = $path.'images/';
		$dst .= jsGetValueNo0($params,'image','no.png');
	
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}
	}
	
	protected function _import($old,$new,$path)
	{
		$params = jsGetValueNo0($old,'params',array());
	
		$imgPath = $path;//."/images/";
		$dst = OnepageHelper::getImgPath();
		$iImage = jsGetValueNo0($params,'image','none.png');
	
		$newFile = "image_{$new->id}.".JFile::getExt($iImage);
		if( JFile::exists($imgPath.$iImage) )
		{
			JFile::copy( $imgPath.$iImage, $dst.$newFile );
	
			$params = jsSetValue($params, 'image', $newFile);
			$new->set('params',$params);
			$new->store();
		}
	
	}
	
	protected function _removeEl( $node )
	{
		$params = $node->params;
		// section background
		$bgImage = jsGetValueNo0($params,'image','no.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
	
	
		if( JFile::exists($imgPath) )
		{
			JFile::delete( $imgPath );
		}
	}
}
