<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeHover extends jsOpelPlugin
{
	protected $name = 'hover';
	protected $isGroup = false;
	protected $isItem = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$node->set('params',$params);
		
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');		
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			$params['image'] = $imageFile;
		}
		
		$mImage = JRequest::getVar('mobile_image', null, 'files');
		$imgExt = JFile::getExt($mImage['name']);
		$imageFile = "mobile_image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		if(!empty($mImage))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			if ( !JFile::upload( $mImage['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			$params['mobile_image'] = $imageFile;
		}
		
		$filter = JFilterInput::getInstance();
		
		$params['clickevent'] = JRequest::getCmd('clickevent');
		$params['ce_link'] = JRequest::getString('ce_link');
		$node->set('params',$params);
		
		$content = JRequest::getString('content','','post',JREQUEST_ALLOWHTML);
		if($content == '<p><br /></p>')
		{
			$content = '';
		}
		$node->set('content',$content);
		
		return true;
	}
	
	protected function _parseTpl($item)
	{
		//list($tItem,$params,$id,$children) = onepageHelper::getItemTpl($item);
		$params = $this->_parseCommonParams($item);
		// Html
		$this->addVariable('content', $item->content);
		
		// Image
		$image = jsGetValueNo0($params, 'image');
		$hasImage = JFile::exists(OnepageHelperFront::getImgPath().$image);
		
		$cls = jsGetValueNo0($params, 'cls','');
		$cls.= ' '.jsGetValueNo0($params, 'responsive','');
		
		$ce = jsGetValueNo0($params,'clickevent',0);
		
		$filePath = ($hasImage)?OnepageHelperFront::getImgPath(true).$image:'#';
		
		
		$this->addVariable('popup_img_link', $pfilePath);
		$this->addVariable('cls', $cls);
		$this->addVariable('attr', $attr);
		$this->addVariable('img_link', $filePath);
		
		
		if( $ce == 'popup' )
		{
			$pfilePath = ($hasImage && ($ce == 'popup') )?OnepageHelperFront::getImgPath(true).$image:'javascript:void(0)';
			$this->addVariable('ce_link', $pfilePath);
			$this->addVariable('link_cls', 'mplightbox');
		}
		elseif( $ce == 'link' )
		{
			$ce_link = jsGetValueNo0($params, 'ce_link','');
			$this->addVariable('ce_link', $ce_link);
			$this->addVariable('link_cls', '');
		}
		else
		{
			$this->addVariable('ce_link', 'javascript:void(0);');
		}
		
		
		
		return $this->html;
	}
	

	protected function _getJS($code,$item)
	{
		$params = $item->params;
		$allow_popup = jsGetValueNo0($params,'allow_popup',0);
		$ce = jsGetValueNo0($params,'clickevent',0);
		
		$mixpanel_api = $this->appconfig->getNo0('mixpanel_apikey',false);
		$id = OnepageHelperEl::id($item);
		
		
	
		
		
		if($this->layout != 'desktop')
		{
			if($ce == 'popup')
			{
				$code['mplightbox'] = '$(".mplightbox").magnificPopup({type:"image"}); ';
					
				if(!empty($mixpanel_api))
				{
					$code["mp-hover-{$item->id}"] = "$('.mplightbox').on('click',function(){
								mixpanel.track( \"Hover.Click.{$id}\");
							});";
				}
			}
			
		}
		else
		{
			if($ce == 'popup')
			{
				$code['mplightbox'] = '$(".mplightbox").magnificPopup({type:"image"}); ';
					
				if(!empty($mixpanel_api))
				{
					$code["mp-hover-{$item->id}"] = "$('.mplightbox').on('click',function(){
								mixpanel.track( \"Hover.Click.{$id}\");
							});";
				}
			}
			
			$code['hover'] = "$('.op-hover-block').hover(
						function() {
							$(this).find('.op-hover-overlay').removeClass('hidden').addClass(' animated fadeIn');
						}, function() {
							$(this).find('.op-hover-overlay').removeClass('animated fadeIn').addClass('hidden');
						}
					);";
		}
		
		//$code['mplightbox'] = '$(".mplightbox").magnificPopup({type:"image"}); ';
		return $code;
	}
	
	protected function _export($item,$path)
	{
		$params = $item->params;
		// section background
		$imgPath = OnepageHelper::getImgPath().jsGetValueNo0($params,'image','no.png');
		
		$dst = $path.'images/';
		$dst .= jsGetValueNo0($params,'image','no.png');
	
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}
	}
	
	protected function _import($old,$new,$path)
	{
		$params = jsGetValueNo0($old,'params',array());
	
		$imgPath = $path;//."/images/";
		$dst = OnepageHelper::getImgPath();
		$iImage = jsGetValueNo0($params,'image','none.png');
	
		$newFile = "image_{$new->id}.".JFile::getExt($iImage);
		if( JFile::exists($imgPath.$iImage) )
		{
			JFile::copy( $imgPath.$iImage, $dst.$newFile );
	
			$params = jsSetValue($params, 'image', $newFile);
			$new->set('params',$params);
			$new->store();
		}
	
	}
	
	protected function _removeEl( $node )
	{
		$params = $node->params;
		// section background
		$bgImage = jsGetValueNo0($params,'image','no.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
	
	
		if( JFile::exists($imgPath) )
		{
			JFile::delete( $imgPath );
		}
	}
	
	protected function _preloadJsSrc($codes,$item = null)
	{
		$codes['magnific'] = JFile::read(JPATH_SITE."/media/jsbt3/js/jquery.magnific-popup.min.js");
		//JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
		$codes['hover'] = JFile::read(JPATH_SITE."/media/jsbt3/js/jquery.hcaptions.js");
		return $codes;
	}
	
	protected function _preloadCssSrc($codes,$item = null)
	{
		$codes['magnific'] = JFile::read(JPATH_SITE."/media/jsbt3/css/magnific-popup.css");
		//JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
		return $codes;
	}
}
