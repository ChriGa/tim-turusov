<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

?>

	
  	
  	
<script type="text/ng-template" id="item.html">
	<div class="form-group">
		<label class="col-sm-4">Div Class</label>
	  	<div class="col-sm-8">
	  		<input type="text" class="form-control" name="wrap_cls" ng-model="rtItem.params.wrap_cls" placeholder=""/>
	  	</div>
	</div>
</script>	