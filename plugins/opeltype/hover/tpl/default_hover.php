<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
/*$params = $this->item->params;
$ce = jsGetValueNo0($params,'clickevent',0);

if($ce == 'popup')
{
	JHtml::stylesheet('media/jsbt3/css/magnific-popup.css');
	JHtml::script('media/jsbt3/js/jquery.magnific-popup.min.js');
}
JHtml::script('media/jsbt3/js/jquery.hcaptions.js');
//JHtml::stylesheet('media/jsbt3/css/animations.css');
$attr = '';*/
?>
<div id="[wrap_id]" class="op-hover col-md-[md] [wrap_cls]" data-wow-delay="[delay]s" data-wow-offset="[wow_offset]">

	<a href="[ce_link]" class="op-hover-image-a [link_cls]">
		<div class="op-hover-block">
			<img class="[cls] " src="[img_link]" <?php echo $attr;?> alt="[title]"/>
			<div class="op-hover-overlay hidden">
				<div class="op-hover-table">
					<div class="op-hover-cell">[content]</div>
				</div>
			</div>
		</div>
	</a>
</div>