<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

?>

	
	<div class="panel panel-primary">
		<div class="panel-heading haslink noselect" style="position:relative;">
			<h4 class="panel-title">
				<a  data-parent="false" > Image </a>
					
			</h4>
		</div>
		<div id="image" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="row">
				
					<div class="col-md-4">
						<img ng-src="{{previewimg}}" class="img-responsive"/>
					</div>
					<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group1">
								<label class="">Image Source</label>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group">
							    	<div class="form-control" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
							   		<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
							    	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
							  		</div>
								</div>
							</div>
							
							<div class="form-group1 hidden">
								<label class="">Mobile Image( If you want to display different image in mobile )</label>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group">
							    	<div class="form-control" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
									</div>
							   		<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="mobile_image"></span>
							    	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
							  		</div>
								</div>
							</div>
						</div>
					
				
						<div class="col-md-12">
							<hr/>
							<div class="form-group">
								<label class="col-md-4 control-label">Click Event</label>
								<div class="col-md-8">
			  						<select name="clickevent" ng-model="item.params.clickevent" class="form-control">
										<option value=""><?php echo JText::_(JS_NONE);?></option>
										<option value="popup">Popup</option>
										<option value="link">Link</option>
									</select>
								</div>
							</div>
							
							<div class="form-group " ng-show="item.params.clickevent=='link'">
								<label class="col-md-4 control-label">Link</label>
								<div class="col-md-8">
									<textarea name="ce_link" rows="2" class="form-control" ng-model="item.params.ce_link"></textarea>
								</div>
							</div>
						</div>
					</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
