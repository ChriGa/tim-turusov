<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeRow extends jsOpelPlugin
{
	protected $name = 'row';
	protected $isLevel1 = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
		$params['has_wrap'] = JRequest::getInt('has_wrap');
		$params['height'] = JRequest::getInt('height');
		$params['bg_color'] = JRequest::getCmd('bg_color');
		$params['hide_bg_image'] = JRequest::getInt('hide_bg_image');
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$params['wrap_id'] = JRequest::getString('wrap_id');
		$node->showtitle = JRequest::getInt('showtitle');
	
		// Image
		$image = JRequest::getVar('bg_image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$filePath = OnepageHelper::getImgPath()."bg_image_{$node->id}.{$imgExt}";
	
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
	
			$params['bg_image'] = "bg_image_{$node->id}.{$imgExt}";
		}
		$node->params = $params;
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
		$params['has_wrap'] = JRequest::getInt('has_wrap');
		$params['height'] = JRequest::getInt('height');
		$params['bg_color'] = JRequest::getCmd('bg_color');
		$params['font_color'] = JRequest::getCmd('font_color');
		$params['hide_bg_image'] = JRequest::getInt('hide_bg_image');
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$params['wrap_id'] = JRequest::getString('wrap_id');
		$node->showtitle = JRequest::getInt('showtitle');
		
		// Image
		$image = JRequest::getVar('bg_image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$filePath = OnepageHelper::getImgPath()."bg_image_{$node->id}.{$imgExt}";
		
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			$params['bg_image'] = "bg_image_{$node->id}.{$imgExt}";
		}
		$node->params = $params;
		
		$content = JRequest::getString('content','','post',JREQUEST_ALLOWHTML);
		
		if($content == '<p><br /></p>')
		{
			$content = '';
		}
		$node->set('content',$content);
		
		return true;
	}
	
	protected function _parseGroupTpl($item,$children)
	{
		$params = $this->_parseCommonParams($item);
		$bgImage = jsGetValueNo0($params,'bg_image',false);
		$hideImage = jsGetValueNo0($params,'hide_bg_image',false)?true:false;
		$addImageCls = (!$hideImage && JFile::exists(OnepageHelperFront::getImgPath().$bgImage ) );
		
		$hasBGImage = jsGetValueNo0($params,'bg_image',false)?'has-bg-image':'';
		
		//$this->addVariable('id', $item->id);
		//$wrap_cls = jsGetValueNo0($params,'wrap_cls','');
		//$wrap_cls.= jsGetValueNo0($params,'has_wrap',false)?'container-100':'';
		//$this->addVariable('wrap_cls',  $wrap_cls );
		
		//$this->addVariable('title', $item->title);
		//$wrap_id = OnepageHelperEl::id($item);
		//$this->addVariable('wrap_id',  $wrap_id );
		
		
		
		$bgClass = ($addImageCls)?'bg-image':'';
		$bgClassOverlay = ($addImageCls)?'bg-image-overlay':'';
		$hasWrap = jsGetValueNo0($params,'has_wrap',false)?'container-fluid':'container';
		
		$cHtml = implode("\r\n",$children);
		$this->addVariable('children',  $cHtml );
		
		$this->addVariable('bg-image',  $bgClass );
		$this->addVariable('bg-image-overlay',  $bgClassOverlay );
		$this->addVariable('container',  $hasWrap );
		
		$this->addVariable('has-bg-image',  $hasBGImage );
		$this->addVariable('header', $item->content);
		return $this->html;
	}
	
	public function saveChild(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			$params['image'] = $imageFile;
		}
	
		$filter = JFilterInput::getInstance();
		$params['cls'] = $filter->clean($post['cls'],'string');
		$caption = JRequest::getString('caption','','post',JREQUEST_ALLOWHTML);
		$params['caption'] = $caption;
	
		$node->set('params',$params);
		return true;
	}
	
	protected function _getCSS($code,$item)
	{
		$uri = JFactory::getURI();
		$params = $item->params; // obj
		//if(!jsGetValueNo0($params,'has_wrap',false)) {continue;}
		// section style
		$height = jsGetValueNo0($params,'height',false);
		$bg_color = jsGetValueNo0($params,'bg_color',false);
		$font_color = jsGetValueNo0($params,'font_color',false);
		
		$section_id = OnepageHelperEl::id($item);
		
		$txt = '';
		$txt .= "#{$section_id} {";
		if($height)
		{
			$txt .= "height:{$height}px;";
		}
		
		if($bg_color)
		{
			$txt .= "background-color:#{$bg_color};";
		}
		if($font_color)
		{
			$txt .= "color:#{$font_color};";
		}
		$txt .= "}";
		
		// section background
		$bgImage = jsGetValueNo0($params,'bg_image','none.png');
		$hideImage = jsGetValueNo0($params,'hide_bg_image',false)?true:false;
		$addImageCls = (!$hideImage && JFile::exists(OnepageHelperFront::getImgPath().$bgImage ) );
		if($addImageCls)
		{
			$txt .= "#bg-image-{$item->id} {";
			//$txt = "left:0;right:0;top:0;bottom:0;";
			$txt .= "background-image: url(\"{$uri->root()}images/onepage/{$bgImage}\");";
			$txt .= "}";
		}
		$code[] = $txt;
		return $code;
	}
	
	protected function _export($item,$path)
	{
		$params = $item->params;
		// section background
		$bgImage = jsGetValueNo0($params,'bg_image','none.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
		$dst = $path.'images/';
		$dst .= $bgImage;
		
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}
	}
	
	protected function _import($old,$new,$path)
	{
		$params = jsGetValueNo0($old,'params',array());
	
		$imgPath = $path;//."/images/"; // source
		$dst = OnepageHelper::getImgPath();
		$bgImage = jsGetValueNo0($params,'bg_image','none.png');
	
		$newFile = "bg_image_{$new->id}.".JFile::getExt($bgImage);
		if( JFile::exists($imgPath.$bgImage) )
		{
			JFile::copy( $imgPath.$bgImage, $dst.$newFile );
	
			$params = jsSetValue($params, 'bg_image', $newFile);
			$new->set('params',$params);
			$new->store();
		}
	}
	
	protected function _removeEl( $node )
	{
		$params = $node->params;
		// section background
		$bgImage = jsGetValueNo0($params,'bg_image','none.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
		
		
		if( JFile::exists($imgPath) )
		{
			JFile::delete( $imgPath );
		}
	}
}
