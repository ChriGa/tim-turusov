<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

?>

	
  	
  	
<script type="text/ng-template" id="row.html">
	<div class="form-group">
	  	<label class="col-sm-4">Full Width?</label>
  		<div class="col-sm-8">
  			<select class="form-control" name="has_wrap" ng-model="rtItem.params.has_wrap">
		  		<option value="1"><?php echo JText::_('JS_YES');?></option>
	  			<option value="0"><?php echo JText::_('JS_NO');?></option>
		  	</select>
  		</div>
	</div>
  	
  		<div class="form-group">
		  	<label class="col-sm-4">Show Title?</label>
	  		<div class="col-sm-8">
	  			<select class="form-control" name="showtitle" ng-model="rtItem.showtitle">
		  		<option value="1"><?php echo JText::_('JS_YES');?></option>
	  		<option value="0"><?php echo JText::_('JS_NO');?></option>
		  	</select>
	  		</div>
		</div>
		
  		<div class="form-group hidden">
		  	<label class="col-sm-4">Minimum Height</label>
	  		<div class="col-sm-8">
	  			<input type="text" class="form-control"  placeholder="Leave it empty if you don't need" name="height" ng-model="rtItem.params.height">
	  		</div>
		</div>
		
		<div class="form-group">
		  	<label class="col-sm-4">Section ID</label>
	  		<div class="col-sm-8">
	  			<input type="text" class="form-control" name="wrap_id" ng-model="rtItem.params.wrap_id" placeholder="Leave it empty if you don't need"/>
	  		</div>
		</div>
		
		<div class="form-group">
		  	<label class="col-sm-4">Section Class</label>
	  		<div class="col-sm-8">
	  			<textarea rows="2" class="form-control" name="wrap_cls" ng-model="rtItem.params.wrap_cls"></textarea>
	  		</div>
		</div>
		<hr/>
		
		<div class="form-group">
		  	<label class="col-sm-4">Background Color</label>
	  		<div class="col-sm-8">
	  			<input type="text" class="form-control" placeholder="#fff" name="bg_color" ng-model="rtItem.params.bg_color">
	  		</div>
		</div>
		
		<div class="form-group">
		  	<label class="col-sm-4">Background Image</label>
	  		<div class="col-sm-8">
	  			<div class="fileinput fileinput-new" data-provides="fileinput">
				  <div class="input-group">
				    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
				    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="bg_image"></span>
				    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
				  </div>
				</div>
	  		</div>
		</div>
		
		<div class="form-group">
	  		<div class="col-sm-8 col-md-offset-4">
	  			<label><input type="checkbox" value=1 name="hide_bg_image">Hide The Background Image</label>
	  		</div>
		</div>
</script>	