<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
//list($tItem,$params,$id,$children) = $this->fetchVars();
//$tItem->set('children',$children);
/*$params = $this->item->params;
$bgImage = jsGetValueNo0($params,'bg_image',false);
$hideImage = jsGetValueNo0($params,'hide_bg_image',false)?true:false;
$addImageCls = (!$hideImage && JFile::exists(OnepageHelperFront::getImgPath().$bgImage ) );
*/
?>

<section class="wrap-section [wrap_cls]" id="[wrap_id]" data-wow-delay="[delay]s" data-wow-offset="[wow_offset]">
	<div class="[bg-image]" id="bg-image-[id]"></div>
	<div class="[bg-image-overlay]" id="bg-image-overlay-[id]"></div>
	<div class="[container] [has-bg-image] " >
		<div>[header]</div>
		<div class="row">[children]</div>
	</div>
</section>
