<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeOverlay extends jsOpelPlugin
{
	protected $name = 'overlay';
	protected $isGroup = true;
	protected $isItem = false;
	protected $isEditable = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$node->set('params',$params);
		
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
		$params['height'] = JRequest::getInt('height');
		$params['bg_color'] = JRequest::getCmd('bg_color');
		$params['hide_bg_image'] = JRequest::getInt('hide_bg_image');
		$params['wrap_cls'] = JRequest::getString('wrap_cls');
		$params['wrap_id'] = JRequest::getString('wrap_id');
		
		$params['vertical'] = JRequest::getCmd('vertical');
		$params['horizontal'] = JRequest::getCmd('horizontal');
		
		// Image
		$image = JRequest::getVar('bg_image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$filePath = OnepageHelper::getImgPath()."bg_image_{$node->id}.{$imgExt}";
		
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
				
			$params['bg_image'] = "bg_image_{$node->id}.{$imgExt}";
		}
		$node->params = $params;
		
		return true;
	}
	
	protected function _parseGroupTpl($item,$children)
	{
		$params = $this->_parseCommonParams($item);
		$bgImage = jsGetValueNo0($params,'bg_image',false);
		$hideImage = jsGetValueNo0($params,'hide_bg_image',false)?true:false;
		$addImageCls = (!$hideImage && JFile::exists(OnepageHelperFront::getImgPath().$bgImage ) );
		$wrap_cls = jsGetValueNo0($params,'wrap_cls','');
		$hasBGImage = jsGetValueNo0($params,'bg_image',false)?'has-bg-image':'';
		$bgClass = ($addImageCls)?'bg-image':'';
		
		$cHtml = implode("\r\n",$children);
		$this->addVariable('children',  $cHtml );
		
		$this->addVariable('id', $item->id);
		$this->addVariable('bg-image',  $bgClass );
		$this->addVariable('wrap_cls',  $wrap_cls );
		$this->addVariable('has-bg-image',  $hasBGImage );
		$this->addVariable('title', $item->title);
		
		
		$wrap_id = OnepageHelperEl::id($item);
		$this->addVariable('wrap_id',  $wrap_id );
		$this->addVariable('header', $item->content);
		return $this->html;
	}
	
	protected function _getCSS( $code,$item )
	{
		$css = JFile::read(JPATH_SITE.'/media/plg_opeltype_overlay/style.css');
		$css = $this->_getItemCSS($css, $item);
		
		$code[] = $css;
		return $code;
	}
	
	/**
	 * @params $code -> css code
	 * @params $item -> node || tpl
	 * @return $code;
	 */
	protected function _getItemCSS( $code,$item )
	{
		$uri = JFactory::getURI();
		$params = $item->params;
		$height = jsGetValueNo0($params,'height','auto');
		$height.= ($height =='auto')?'':'px';
		$bg_color = jsGetValueNo0($params,'bg_color','transparent');
		$bg_color = ($bg_color =='transparent')?'':'#'.$bg_color;
		$vertical = jsGetValueNo0($params,'vertical','top');
	
		$wrap_id = OnepageHelperEl::id($item);
	
		$css = $code;
		if(jsGetValueNo0($item,'level'))
		{
			$bgImage = jsGetValueNo0($params,'bg_image','none.png');
			$hideImage = jsGetValueNo0($params,'hide_bg_image',false)?true:false;
			$addImageCls = (!$hideImage && JFile::exists(OnepageHelperFront::getImgPath().$bgImage ) );
			$imgLink = ($addImageCls)?'url("'.$uri->root()."images/onepage/{$bgImage}".'")':'none';
				
			$css = str_replace('[wrap_id]', $wrap_id, $css);
			$css = str_replace('[height]', $height, $css);
			$css = str_replace('[bg_color]', $bg_color, $css);
			$css = str_replace('[id]', $item->id, $css);
			$css = str_replace('[vertical_align]', $vertical, $css);
			$css = str_replace('[img_link]', $imgLink, $css);
		}
		$code = $css;
		return $code;
	}
	
	
	protected function _export($item,$path)
	{
		$params = $item->params;
		// section background
		$bgImage = jsGetValueNo0($params,'bg_image','none.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
		$dst = $path.'images/';
		$dst .= $bgImage;
		
		if( !empty($imgPath) && JFile::exists($imgPath) )
		{
			JFile::copy( $imgPath, $dst );
		}
	}
	
	protected function _import($old,$new,$path)
	{
		$params = jsGetValueNo0($old,'params',array());
		
		$imgPath = $path;//."/images/";
		$dst = OnepageHelper::getImgPath();
		$bgImage = jsGetValueNo0($params,'bg_image','none.png');
		
		$newFile = "bg_image_{$new->id}.".JFile::getExt($bgImage);
		if( JFile::exists($imgPath.$bgImage) )
		{
			JFile::copy( $imgPath.$bgImage, $dst.$newFile );
				
			$params = jsSetValue($params, 'bg_image', $newFile);
			$new->set('params',$params);
			$new->store();
		}
	}
	
	protected function _removeEl( $node )
	{
		$params = $node->params;
		// section background
		$bgImage = jsGetValueNo0($params,'bg_image','none.png');
		$imgPath = OnepageHelper::getImgPath().$bgImage;
	
	
		if( JFile::exists($imgPath) )
		{
			JFile::delete( $imgPath );
		}
	}
}
