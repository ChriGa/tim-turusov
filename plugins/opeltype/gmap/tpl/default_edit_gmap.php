<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
?>

<div class="panel-group" id="accordion">

	<div class="panel panel-primary">
		<div class="panel-heading haslink">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#html"> Markers </a>
			</h4>
		</div>
		<div id="html" class="panel-collapse collapse in">
			<div class="panel-body" style="padding:15px 20px">
				<div class="form-group">
					<div class="col-md-12 text-right">
						<button class="btn btn-xs btn-success" type="button" ng-click="addMarker()"><i class="fa fa-plus"></i> Add </button>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4"  ng-repeat="marker in markers" style="margin-top:10px;">
						<div style="border:1px solid #ccc;padding:10px 10px 0;">
						<div class="form-group">
							<label class="control-label col-md-4">#{{$index+1}}</label>
							<div class="col-md-8 text-right">
								<button class="btn btn-xs btn-danger" type="button" ng-click="deleteMarker($index)"><i class="fa fa-times"></i>  </button>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Latitude</label>
							<div class="col-md-8">
								<input class="form-control" type="text" name="lat[]" ng-model="marker.lat"/>
								
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-4">Longitude</label>
							<div class="col-md-8">
								<input class="form-control" type="text" name="lng[]" ng-model="marker.lng"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-4">Title</label>
							<div class="col-md-8">
								<input class="form-control" type="text" name="mtitle[]" ng-model="marker.title"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-4">Description</label>
							<div class="col-md-8">
								<textarea class="form-control" rows="2" name="mtext[]" ng-model="marker.infotext"/></textarea>
							</div>
						</div>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading haslink">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="#setting"> GMap Setting </a>
			</h4>
		</div>
		<div id="setting" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="form-group">
					<div class="col-md-3">Height</div>
					<div class="col-md-9">
						<input type="text" class="form-control" name="height" placeholder="Default 300" ng-model="item.params.height"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<input type="hidden" name="option" value="com_onepage">
<input type="hidden" name="task" value="prototype.saveItem">
<input type="hidden" name="id" value="<?php echo $this->item->id;?>">

<script>
function elitem($scope) {
	$scope.markers = $scope.item.params.markers;

	$scope.addMarker = function() {
		$scope.markers.push({});
	}

	$scope.deleteMarker = function(idx) {
		$scope.markers.splice(idx,1);
	}

	$scope.submit = function(v) {
		//var sHTML = $('.summernote').code();
		$('#elform').ajaxSubmit({
			url: 'index.php',
			dataType:  'json',
			data:{'v':v},
			type: 'POST',
			url: 'index.php',
			beforeSubmit: function()	{
				//$('#changeformModal').block({ message: "Processing..." });
				//$.blockUI({ message: "Processing..." });
				NProgress.start();
			},
			success: function(res)	{
				if(res.success) {
					$scope.$apply(function() {
						$scope.previewimg = $scope.previewimglink+'&f='+new Date().getTime();
						$scope.$parent.list = res.list;
					});
				}
				NProgress.done();
			}
		});
	}
}
</script>