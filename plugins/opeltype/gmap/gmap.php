<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeGmap extends jsOpelPlugin
{
	protected $name = 'gmap';
	protected $isGroup = false;
	protected $isItem = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
		
		$params['height'] = JRequest::getInt('height',300);
		
		$lat = JRequest::getVar('lat',array());
		$lng = JRequest::getVar('lng',array());
		$mtitle = JRequest::getVar('mtitle',array());
		$mtext = JRequest::getVar('mtext',array());
		
		$markers = array();
		foreach($lat as $k => $v)
		{
			$m = array();
			$m['lat'] = $lat[$k];
			$m['lng'] = $lng[$k];
			$m['title'] = $mtitle[$k];
			$m['infotext'] = $mtext[$k];
			
			$markers[] = $m;
		}
		$params['markers'] = $markers;
		
		$node->set('params',$params);
		
		//$node->set('content',$content);
		
		return true;
	}
	
	protected function _parseTpl($item)
	{
		
		$this->params = $this->_parseCommonParams($item);
		
		return $this->html;
	}
	
	protected function _getJS($code,$item)
	{
		$db = jsDB::instance();
		$params = $item->params;
		
		$markers = jsGetValue($params,'markers',array());
		$first = array();
		
		
		$eid = OnepageHelperEl::id($item);
		
		$txt = '';
		/*$txt.="var \$gmag_{$id} = $('#{$id}').parent();";
		$tConfig = jsAppConfig::getInstance();
		$txt.="if( \$gmag_{$id}.hasClass('opautoheight')) {";
			//$txt.=" \$gmag_{$id}.height( \$gmag_{$id}.height() - \$gmag_{$id}.outerHeight(true) + \$gmag_{$id}.parents('.opheight').height());";
			$txt.="}";*/
		
		
		$device = onepageHelperFront::checkDevice();
		
		if($device != 'mobile')
		{
			if(count($markers) > 0)
			{
				$txt.="var gmap{$item->id} = new GMaps({";
				$txt.="div: '#{$eid}',";
				$txt.="width: '100%',";
				$txt.="height: '".jsGetValueNo0($params, 'height',300)."px',";
				
				
				
					$first = $markers[0];
					$txt.="lat: {$first['lat']},";
					$txt.="lng: {$first['lng']}";
				
				
				$txt.="});";
				$txt.="\r\n";
				foreach($markers as $marker)
				{
					if( empty($marker['lat']) || empty($marker['lng']) )
					{
						continue;
					}
				
					$t = explode("\n", $marker['infotext'] );
					$t = '<p>'.implode('</p><p>',$t).'</p>';
					$t = $db->quote($t);
					$txt.="gmap{$item->id}.addMarker({
							    		lat: {$marker['lat']},
							    		lng: {$marker['lng']},
							    		title: '{$marker['title']}',
							    		infoWindow: {
								          content: {$t}
								        }
							    	});";
					$txt.="\r\n";
				}
			
			}
		}
		else
		{
			if(count($markers) > 0)
			{
				$txt.="url = GMaps.staticMapURL({";
			
				$first = $markers[0];
				$txt.="lat: {$first['lat']},";
				$txt.="lng: {$first['lng']},";
			
				$ms = array();
				foreach($markers as $marker)
				{
					$m = array('lat'=>$marker['lat'],'lng'=>$marker['lng']);
					$ms[] = $m;
				}
				$txt.="markers:".jsJSON::encode($ms);
				$txt.="});";
			
				$txt.= "$('<img/>').attr('src', url).attr('class','img-responsive').appendTo('#{$eid}');";
			}
		}
		$code[] = $txt;
		return $code;
	}
	
}
