<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeVertical extends jsOpelPlugin
{
	protected $name = 'vertical';
	protected $isGroup = true;
	protected $isEditable = false;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
			
		$params['indicators'] = JRequest::getInt('indicators');
		$params['navcontrol'] = JRequest::getInt('navcontrol');
		$node->set('params',$params);
	
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
			
		$params['height'] = JRequest::getInt('height');
		$node->set('params',$params);
		
		return true;
	}
	
	public function saveChild(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');		
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			$params['image'] = $imageFile;
		}
		
		$filter = JFilterInput::getInstance();
		$params['cls'] = $filter->clean($post['cls'],'string');
		$caption = JRequest::getString('caption','','post',JREQUEST_ALLOWHTML);
		$params['caption'] = $caption;
		
		$node->set('params',$params);
		return true;
	}
	
	protected function _parseGroupTpl($item,$children)
	{
		$params = $this->_parseCommonParams($item);
		
		$nodes = $item->get('children');
		$newChildren = array();
		foreach ($nodes as $k => $node)
		{
			$active = ($k == 0)?'active':'';
			$newChildren[] = $children[$k];
			
		}
		
		$cHtml = implode("\r\n",$newChildren);
		$this->addVariable('children',  $cHtml );
		
		return $this->html;
	}
	
	protected function _getCSS( $code,$item )
	{
		$uri = JFactory::getURI();
		$params = $item->params; // obj
		$height = jsGetValueNo0($params, 'height',0);
		$section_id = OnepageHelperEl::id($item);
	
		$txt = '';
		$txt .= "#{$section_id} {";
		if($height)
		{
			$txt .= "min-height:{$height}px;";
		}
		$txt .= "}";
	
		
		$code[] = $txt;
		return $code;
	}
}
