<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

$params = $this->item->params;

JHtml::script('media/jsbt3/js/owl.carousel.min.js');
JHtml::stylesheet('media/jsbt3/css/owl.carousel.css');
JHtml::stylesheet('media/jsbt3/css/owl.theme.css');
?>


<div class="col-md-[md] [wrap_cls]" >

	<div class="opcarousel " id="[wrap_id]">
		
		<div class="owlcarousel owl-carousel" > 
			<!-- Carousel items -->
				[children]
			<!-- Carousel items -->
		</div>
	</div>
</div>	