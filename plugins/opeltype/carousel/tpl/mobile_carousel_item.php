<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on May-2013
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013- J-SOHO
 */

// no direct access
defined('_JEXEC') or die;
$params = $this->item->params;
$allow_popup = jsGetValueNo0($params,'allow_popup',0);

?>
<div class="item  opheight [wrap_cls]">
	
	<?php if(jsGetValueNo0($params, 'enable_html',false) && (jsGetValueNo0($params, 'position','')=='up') ):?>
	<div class="">[content]</div>
	<?php endif;?>
		
		<?php if(jsGetValueNo0($params, 'enable_image',false)):?>
		<img class="[cls] "  src="[img_link]" <?php echo $attr;?> alt="[title]"/>
		<div class="carousel-caption">[caption]</div>
		<?php endif;?>
		
	<?php if(jsGetValueNo0($params, 'enable_html',false) && (!jsGetValueNo0($params, 'position',false)) ):?>
	<div class="">[content]</div>
	<?php endif;?>
	
</div>