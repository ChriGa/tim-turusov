<?php
/**
 * @version     1.0 +
 * @plugin     J-SOHO - jsmoney for dlocker
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

class plgOpeltypeCarousel extends jsOpelPlugin
{
	protected $name = 'carousel';
	protected $isGroup = true;
	protected static $items = array();
	
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	
	public function quickSaveEl(&$node,$post)
	{
		$params = $node->params;
			
		$params['indicators'] = JRequest::getInt('indicators');
		$params['navcontrol'] = JRequest::getInt('navcontrol');
		$node->set('params',$params);
	
		return true;
	}
	
	public function saveEl(&$node,$post)
	{
		$params = $node->params;
			
		$params['indicators'] = JRequest::getInt('indicators');
		$params['navcontrol'] = JRequest::getInt('navcontrol');
		$node->set('params',$params);
		
		return true;
	}
	
	public function saveChild(&$node,$post)
	{
		$image = JRequest::getVar('image', null, 'files');
		$imgExt = JFile::getExt($image['name']);
		$imageFile = "image_{$node->id}.{$imgExt}";
		$filePath = OnepageHelper::getImgPath().$imageFile;
		$params = $node->params;
		if(!empty($image))
		{
			if( !in_array( strtolower($imgExt ), jsImage::getSupportedImageTypes()))
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('UNSUPPORTED_IMAGETYPE');
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			if ( !JFile::upload( $image['tmp_name'], $filePath ) )
			{
				// Error in upload
				$result['success']= false;
				$result['title']= JText::_('ERROR');
				$result['content']= JText::_('ONEPAGE_ERROR_UNABLE_TO_UPLOAD_FILE');		
				$result = jsJson::encode($result);
				jsExit($result);
			}
			
			$params['image'] = $imageFile;
		}
		
		$filter = JFilterInput::getInstance();
		$params['cls'] = $filter->clean($post['cls'],'string');
		$caption = JRequest::getString('caption','','post',JREQUEST_ALLOWHTML);
		$params['caption'] = $caption;
		
		$node->set('params',$params);
		return true;
	}
	
	protected function _parseGroupTpl($item,$children)
	{
		$params = $this->_parseCommonParams($item);
		
		$indicator = jsGetValueNo0($params,'indicators',0);
		$navcontrol = jsGetValueNo0($params,'navcontrol',0);
		$interval = jsGetValueNo0($params,'interval',0);
		$interval = ($interval == 0)?'false':$interval;
		
		$nodes = $item->get('children');
		$newChildren = array();
		foreach ($nodes as $k => $node)
		{
			$active = ($k == 0)?'active':'';
			$newChildren[] = "<div class=\"item {$active} op-carousel-item  opheight\">";
			$newChildren[] = $children[$k];
			$newChildren[] = "</div>";
		}
		
		// Indicators
		preg_match("/<!-- Carousel Indicator -->(\s|.)*?<!-- Carousel Indicator -->/i", $this->html,$matches);
		$li = $matches[0];
		if( $indicator && count($nodes) > 1 )
		{
			$lis = array();
			foreach ($nodes as $k => $node)
			{
				$a = $li;
				$icls = ($k==0)?'active':'';
				$a = str_replace('[index]',$k,$a);
				$a = str_replace('[indicator_cls]',$icls,$a);
				$lis[] = $a;
			}
			$lis = implode("\r\n",$lis);
			
			$this->html = str_replace($li, $lis, $this->html);
		}
		else
		{
			$this->html = str_replace($li, '', $this->html);
		}
		
		// Arrows
		if( !$navcontrol || count($nodes)<2 )
		{
			$this->html = preg_replace("/<!-- Carousel nav -->(\s|.)*?<!-- Carousel nav -->/i",'', $this->html);
		}
		
		
		$cHtml = implode("\r\n",$newChildren);
		$this->addVariable('children',  $cHtml );
		
		return $this->html;
	}
	
	protected function _getJS($code,$item)
	{
		$params = $item->params;
		$allow_popup = jsGetValueNo0($params,'allow_popup',0);
		$ce = jsGetValueNo0($params,'clickevent',0);
	
		$mixpanel_api = $this->appconfig->getNo0('mixpanel_apikey',false);
		$id = OnepageHelperEl::id($item);
	
		if($this->layout != 'desktop')
		{
			/*$code['op-carousel'] = "$('.opcarousel').each(function(idx,el) {
				$(el).find('.owlcarousel').owlCarousel({singleItem:true,transitionStyle : 'fade'});
			})";*/
			//$code[] = "$('#{$id}').find('.owlcarousel').owlCarousel({singleItem:true,transitionStyle : 'fade'});";
			//$code['test'] = "alert('{$this->layout}');" ;
			$code['owlcarousel'] = "$('.owl-carousel').owlCarousel({items:1,dots: true});";
		}
		else
		{
			
		}
	
		//$code['mplightbox'] = '$(".mplightbox").magnificPopup({type:"image"}); ';
		return $code;
	}
	
	protected function _preloadJsSrc($codes,$item = null)
	{
		if($this->layout != 'desktop')
		{
			$codes['owlcarousel'] = JFile::read(JPATH_SITE."/media/jsbt3/js/owl.carousel2.min.js");
		}
		
		return $codes;
	}
	
	protected function _preloadCssSrc($codes,$item = null)
	{
		if($this->layout != 'desktop')
		{
			$codes['owlcarousel'] = JFile::read(JPATH_SITE."/media/jsbt3/css/owl.carousel2.css");
			$codes['owlcarousel'].= JFile::read(JPATH_SITE."/media/jsbt3/css/owl.theme.default.css");
		}
		
		return $codes;
	}
}
