<?php
/**
 * @version     1.0 +
 * @package     J-SOHO - com_onepage
 * @author      J-SOHO {@link  http://www.j-soho.com}
 * @author      Created on Jan-2014
 * @license GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @Copyright Copyright (C) 2013-2014 J-SOHO
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.folder');
/**
 * Joomla! SEF Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	System.sef
 */
class plgSystemOnepageCSS extends JPlugin
{
	public $jsExtensions = array();
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->jsExtensions =  array('com_onepage');
		//$this->jsExtensions[] = 'com_jsmoney';
		$this->jsExtensions[] = 'com_jslang';
		$this->jsExtensions[] = 'com_jsbt3';
	}
	
	/**
	 * Converting the site URL to fit to the HTTP request
	 */
	
	public function onAfterInitialise()
	{
		// custom
		if(JFolder::exists(JPATH_LIBRARIES . '/jskit'))
		{
			JLoader::discover('js', JPATH_LIBRARIES . '/jskit');
			JLoader::registerPrefix('js', JPATH_LIBRARIES . '/jskit');
			jsLoader::setup();
		}
	}
	
	function onBeforeRender()
	{
		$app = JFactory::getApplication();

		$id = JRequest::getInt('id');
		$option = JRequest::getCmd('option');
		$view = JRequest::getCmd('view');
		$page = JRequest::getCmd('page');
		$layout = JRequest::getCmd('layout','default');
		
		if( ( !in_array($option,$this->jsExtensions)) || ($layout == 'modal')  )
		{
			return true;
		}
		
		$db = JFactory::getDbo();
		$doc = JFactory::getDocument();
		$doc->_script = array();
		
		
		if (!$app->isAdmin()) {
			$base = '/'.basename(JPATH_ROOT);
			
			$css = array();
			
			if(JVERSION >= 3)
			{
				$css[] = 'template.css';
				$css[] = 'position.css';
				$css[] = 'layout.css';
				$css[] = 'print.css';
				$css[] = 'general.css';
				$css[] = 'personal.css';
				$css[] = 'system.css';
				$css[] = 'nature.css';
			}
			else
			{
				$css[] = 'template.css';
				$css[] = 'position.css';
				$css[] = 'layout.css';
				$css[] = 'print.css';
				$css[] = 'general.css';
				$css[] = 'personal.css';
				$css[] = 'system.css';
			}
			
			foreach($doc->_styleSheets as $k => $v)
			{
				$basename = basename($k);
				if(in_array($basename,$css))
				{
					unset($doc->_styleSheets[$k]);
				}
			}
			
			$js = array();
			//$css[] = 'system.css';
			if(JVERSION >= 3)
			{
				$js[] = 'core.js';
				$js[] = 'jui/js/jquery';
				$js[] = 'jui/js/bootstrap';
				$js[] = 'caption.js';
			}
			else
			{
				$js[] = 'core.js';
				$js[] = 'caption.js';
			}
			
			foreach($doc->_scripts as $k => $v)
			{
				foreach($js as $jk => $jv)
				{
					if(strpos($k, $jv) !== false)
					{
						unset($doc->_scripts[$k]);
						break;
					}
				}
			}
			
			
		}
		else
		{
			$css = array();
			//$css[] = 'system.css';
			
			$css[] = 'template.css';
			$css[] = 'modal.css';
			$css[] = 'system.css';
			if(JVERSION >= 3)
			{
				
			}
			else
			{
				//$css[] = 'system.css';
			}
			
			if($view != 'menu')
			{
				//return true;
			}
			foreach($doc->_styleSheets as $k => $v)
			{
				$basename = basename($k);
				if(in_array($basename,$css))
				{
					unset($doc->_styleSheets[$k]);
				}
			}
			
			$js = array();
			if(JVERSION >= 3)
			{
				$js[] = 'multiselect.js';
				$js[] = 'mootools';
				$js[] = 'jui/js/jquery';
				$js[] = 'jui/js/bootstrap';
				$js[] = 'template.js';
				$js[] = 'modal.js';
				$js[] = 'validate.js';
				//$js[] = 'caption.js';
			}
			else
			{
				$js[] = 'multiselect.js';
				$js[] = 'mootools';
				$js[] = 'validate.js';
				$js[] = 'modal.js';
			}
			
			//$css[] = 'system.css';
			
			foreach($doc->_scripts as $k => $v)
			{
				foreach($js as $jk => $jv)
				{
					if(strpos($k, $jv) !== false)
					{
						unset($doc->_scripts[$k]);
						break;
					}
				}
			}
		}
		
		//print_r($doc->_script);exit;
	}
	
	public function onAfterRender()
	{
		$app = JFactory::getApplication();
		
		$id = JRequest::getInt('id');
		$option = JRequest::getCmd('option');
		$view = JRequest::getCmd('view');
		$page = JRequest::getCmd('page');
		$layout = JRequest::getCmd('layout','default');
		
		if( ( !in_array($option,$this->jsExtensions)) || ($layout == 'modal')  )
		{
			return true;
		}
		
		if ($app->isAdmin()) {
			$buffer = JResponse::getBody();
			if(JVERSION < 3)
			{
				echo '<!DOCTYPE html>';
				echo $buffer;exit;
			}
		}
		else
		{
			if(JVERSION < 3)
			{
				$buffer = JResponse::getBody();
				$buffer = '<!DOCTYPE html>'.$buffer;
				JResponse::setBody($buffer);
			}
			
		}
		
	}
	
	public function onExtensionAfterInstall($installer,$eid)
	{
		$db = JFactory::getDbo();
		$query = " UPDATE `#__extensions`"
				." SET `enabled` = 1"
				." WHERE `folder` = 'opeltype' AND `type`='plugin'"
				;
		$db->setQuery($query);
		$db->query();
	
	}
}
